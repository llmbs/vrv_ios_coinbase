

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface AboutUsView : UIViewController <MFMailComposeViewControllerDelegate>
{
    __weak IBOutlet UIView *MainView;
    __weak IBOutlet UIButton *faq;
    __weak IBOutlet UIButton *about;
    
    
    /*/Storyboard */
    
    __weak IBOutlet UILabel *about_us;
    __weak IBOutlet UILabel *about_us2;
    
    __weak IBOutlet UILabel *information_txt;
    
    __weak IBOutlet UILabel *about_us_screen_title;
    
    
    __weak IBOutlet UILabel *email_txt;
    
    
    __weak IBOutlet UILabel *faq_txt;
    
    __weak IBOutlet UILabel *eula_txt;
    
    __weak IBOutlet UILabel *privacy_txt;
    
    __weak IBOutlet UILabel *cookie_txt;
    
    __weak IBOutlet UILabel *scroll_txt;
    
    __weak IBOutlet UILabel *brief_txt;
    
    __weak IBOutlet UILabel *business_txt;
    
    __weak IBOutlet UILabel *vrvtutorial;
    
    
    
    
    
    
    
    
    
    
}
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *ButtonCollectons;

- (IBAction)Menubuttonclick:(id)sender;
- (IBAction)EmailUsButtonclick:(id)sender;
- (IBAction)FAQButtonClick:(id)sender;
- (IBAction)TremsofServiceButtonClick:(id)sender;
- (IBAction)PrivcyButtonClick:(id)sender;
- (IBAction)CookieButtonClick:(id)sender;
- (IBAction)SweeptakesRuleButtonClick:(id)sender;
- (IBAction)YourBusinessButtonClick:(id)sender;
- (IBAction)WatchTutorialButtonClick:(id)sender;
- (IBAction)BusinessSummary:(id)sender;
- (IBAction)Aboutbuttonclick:(id)sender;




@end
