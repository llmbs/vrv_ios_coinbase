
#import "Reachability.h"
#import "WinnersInView.h"
#import "AppDelegate.h"
#import "LeftRightViewController.h"
#import "VRV-Swift.h"

@interface WinnersInView ()
{
    NSTimer *OneSecondtimer;
    TimerData *data;
    
    NSDate *endTime;
    NSDate *currentTime;
}

@end

@implementation WinnersInView


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UISwipeGestureRecognizer *gestureRecognizerRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandlerRight:)];
    [gestureRecognizerRight setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [self.view addGestureRecognizer:gestureRecognizerRight];

    
    
    LearnMoreButton.layer.cornerRadius = 4;
    LearnMoreButton.clipsToBounds = YES;
    
    [self getTimerData];
    [[UIButton appearance] setExclusiveTouch:YES];
    
    
    
    winners_in.text = NSLocalizedString(@"winners_in", "");
    
    hrs.text = NSLocalizedString(@"hrs", "");
    
    minutes.text = NSLocalizedString(@"minutes", "");
    
    secs.text = NSLocalizedString(@"secs", "");
    
    winners.text = NSLocalizedString(@"winners", "");
    
    
    
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark VOID MEthod.


-(void)enableUserinteraction:(id)sender {
    
    [self.view setUserInteractionEnabled:YES];
    
}

- (void)showAlert:(NSString *)message {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"dialog_ok", "")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               
                               {
                                  
                                   
                                   
                               }];
    [alert addAction:okButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}


-(void)swipeHandlerRight:(id)sender
{
    
    LeftRightViewController *LeftrightviewScreenObject = [self.storyboard instantiateViewControllerWithIdentifier:@"leftright"];
    UIViewController *newVC = LeftrightviewScreenObject;
    NSMutableArray *vcs =  [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
    [vcs insertObject:newVC atIndex:vcs.count-1];
    [self.navigationController setViewControllers:vcs animated:YES];
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(void)onesecondtimermethod{
    self->currentTime = [NSDate dateWithTimeInterval:1.0 sinceDate:self->currentTime];

    NSTimeInterval ti = [self->endTime timeIntervalSinceDate:self->currentTime];
    
    NSUInteger h, m, s;
    h = (ti / 3600);
    m = ((NSUInteger)(ti / 60)) % 60;
    s = ((NSUInteger) ti) % 60;
    
    HourLabel.text = [NSString stringWithFormat:@"%ld", (long)h];
    MinuteLabel.text = [NSString stringWithFormat:@"%ld", (long)m];
    SecondLabel.text = [NSString stringWithFormat:@"%ld", (long)s];
}


#pragma mark IBACTION METHOD.

- (IBAction)leftMenuButtonClick:(id)sender {
 [OneSecondtimer invalidate];
 
    LeftRightViewController *LeftrightviewScreenObject = [self.storyboard instantiateViewControllerWithIdentifier:@"leftright"];
    UIViewController *newVC = LeftrightviewScreenObject;
    NSMutableArray *vcs =  [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
    [vcs insertObject:newVC atIndex:vcs.count-1];
    [self.navigationController setViewControllers:vcs animated:YES];
    [self.navigationController popViewControllerAnimated:YES];
    
}


- (IBAction)LearnMoreButtonClick:(id)sender {
    
    NSURL *quantis = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/quantis"];
    
    [[UIApplication sharedApplication] openURL: quantis options:@{} completionHandler:^(BOOL success) {
        if (success)
            NSLog(@"Opened quantis");
        
    }];
    
}

- (void)getTimerData {
    [self.view setUserInteractionEnabled: NO];
    [self showProgressView];
    [self performSelector:@selector(enableUserinteraction:) withObject:self afterDelay:0.2 ];
    
    [TimerService getTimerWithSuccess:^(TimerData * _Nonnull time) {
        [self hideProgressView];
        [self configure:time];
     
    } failure:^(NSError * _Nullable error) {
        [self handleError:error];
    
    }];
}

- (void)configure:(TimerData *)data {
    self->data = data;
    
    NSString *currentTime = [NSString stringWithFormat:@"%@ %@", [data.date_now componentsSeparatedByString:@" "][0], data.time_now];
    NSString *endTime = [NSString stringWithFormat:@"%@ %@", [data.date componentsSeparatedByString:@" "][0], data.time];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    self->endTime = [formatter dateFromString:endTime];
    self->currentTime = [formatter dateFromString:currentTime];
    
    [self onesecondtimermethod];
    
    OneSecondtimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                      target:self
                                                    selector:@selector(onesecondtimermethod)
                                                    userInfo:nil
                                                     repeats:YES];
}

@end
