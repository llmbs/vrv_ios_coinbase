

#import "UserRoleInfoView.h"
#import "SignUpView.h"
#import "CommanFunction.h"
#import "LoginView.h"




@interface UserRoleInfoView ()
{
    NSTextContainer *textContainer;
    NSLayoutManager *layoutManager;
    
    UITextView *infotextview;
}
@end

@implementation UserRoleInfoView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
   
    Eighteen.layer.cornerRadius = 4;
    terms.layer.cornerRadius = 4;
    privacy.layer.cornerRadius = 4;
    cookie.layer.cornerRadius = 4;
    
    [BackButton setBackgroundImage:[CommanFunction imageWithColor:[UIColor grayColor]] forState:UIControlStateHighlighted];


 
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark IBaction Method.

- (IBAction)NextButtonClick:(id)sender {
    //signupid
    SignUpView *SignUpScreenObj = [self.storyboard instantiateViewControllerWithIdentifier:@"signupid"];
    [self.navigationController pushViewController:SignUpScreenObj animated:YES];
    
}


- (IBAction)BackButtonClick:(id)sender {
     // [self.navigationController popViewControllerAnimated:YES];
    LoginView *LoginScreenObj = [self.storyboard instantiateViewControllerWithIdentifier:@"loginid"];
    [self.navigationController pushViewController:LoginScreenObj animated:YES];
    
}

- (IBAction)TermsClick:(id)sender {
    
    NSURL *terms = [NSURL URLWithString:@"http://voluntaryrefundvalue.com/admin/Vrv_pdfs/Terms_of_service.pdf"];
    
    [[UIApplication sharedApplication] openURL: terms options:@{} completionHandler:^(BOOL success) {
        if (success)
            NSLog(@"Opened terms");
        
    }];
    
}

- (IBAction)PrivacyClick:(id)sender {
    
    NSURL *privacy = [NSURL URLWithString:@"http://voluntaryrefundvalue.com/admin/Vrv_pdfs/Privacy_policy.pdf"];
    
    [[UIApplication sharedApplication] openURL: privacy options:@{} completionHandler:^(BOOL success) {
        if (success)
            NSLog(@"Opened privacy");
        
    }];
    
}

- (IBAction)CookieClick:(id)sender {
    
    NSURL *cookie = [NSURL URLWithString:@"http://voluntaryrefundvalue.com/admin/Vrv_pdfs/Cookie_use.pdf"];
    
    [[UIApplication sharedApplication] openURL: cookie options:@{} completionHandler:^(BOOL success) {
        if (success)
            NSLog(@"Opened cookie");
        
    }];
    
}


@end
