

#import <UIKit/UIKit.h>

@class SPGooglePlacesAutocompleteQuery;
@interface SignUpView : UIViewController<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource> {
    __weak IBOutlet UIScrollView *MainScrollview;
    
    __weak IBOutlet UIView *MainView;
    
    
    __weak IBOutlet UITextField *FirstNameTextField;
    __weak IBOutlet UITextField *LastNameTextField;
    __weak IBOutlet UITextField *GmailTextField;
    __weak IBOutlet UITextField *MobileNumberTextField;
    __weak IBOutlet UITextField *BirthdayTextField;
 
    __weak IBOutlet UITextField *PasswordTextField;
    __weak IBOutlet UITextField *CurrentPasswordTextField;
  
    
    __weak IBOutlet UIImageView *MaleImageview;
    __weak IBOutlet UIImageView *FemaleImageview;
    __weak IBOutlet UIImageView *OptionalImageview;
    
    
    __weak IBOutlet UILabel *sign_up;
    
    __weak IBOutlet UITextField *gender;
    
    __weak IBOutlet UITextField *male;
    
    __weak IBOutlet UITextField *female;
    
    __weak IBOutlet UITextField *optional;
    
//    __weak IBOutlet UITextField *birthday;
//
//    __weak IBOutlet UITextField *first_name;
//
//    __weak IBOutlet UITextField *last_name;
//
//    __weak IBOutlet UITextField *email;
//
//    __weak IBOutlet UITextField *mobile;
//
//    __weak IBOutlet UITextField *password;
//
//    __weak IBOutlet UITextField *confirm_password;
    
//__weak IBOutlet UIButton *create_account;
    
    __weak IBOutlet UIButton *already_a_member_login;
    
    
    
   
    
    
    __weak IBOutlet UITableView *AddressListTableview;

    SPGooglePlacesAutocompleteQuery *searchQuery;
    
    NSArray *searchResultPlaces;
    UITextField *currenttextField;
    
    
    __weak IBOutlet UIButton *BackButton;
    __weak IBOutlet UIButton *CreateBtn;
    __weak IBOutlet UIButton *Help;
    
    
    
    __weak IBOutlet UIActivityIndicatorView *LoadingIndicator;
    
}
- (IBAction)BackButtonClick:(id)sender;
- (IBAction)SignUpButtonClick:(id)sender;

- (IBAction)MaleButtonClick:(id)sender;
- (IBAction)FemaleButtonClick:(id)sender;
- (IBAction)MemberButtonClick:(id)sender;
- (IBAction)hiddenBtn:(id)sender;
- (IBAction)OptionalBtnClick:(id)sender;

@end
