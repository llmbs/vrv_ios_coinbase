
#import "SignUpView.h"
#import "AFNetworking.h"
#import "Reachability.h"
#import "Defaults.h"
#import "CommanFunction.h"
#import "LoginView.h"
#import <SafariServices/SafariServices.h>
#import "LeftRightViewController.h"
#import "AddressCell.h"
#import "SPGooglePlacesAutocompleteQuery.h"
#import "SPGooglePlacesAutocompletePlace.h"
#import "OTPSendView.h"
#import "VRV-Swift.h"

@interface SignUpView ()
{
    NSUserDefaults *dlf;
    NSString *GenderIdentifier;
    UIDatePicker *datePicker;
}

@end

@implementation SignUpView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // Use this for testing signup screen
    
//       FirstNameTextField.text = @"Elisha";
//       LastNameTextField.text = @"Dumas";
//        GmailTextField.text = @"yellowf40@gmail.com";
//        MobileNumberTextField.text = @"408-219-1318";
//        BirthdayTextField.text = @"05/15/2002";
//        PasswordTextField.text = @"Ca801099!";
//        CurrentPasswordTextField.text = @"Ca801099!";
//    GenderIdentifier = @"2";
    
    
    
    

    
    
    dlf = [NSUserDefaults standardUserDefaults];
    CreateBtn.layer.cornerRadius = 4;
    
  
    AddressListTableview.hidden = YES;
    AddressListTableview.layer.cornerRadius =8;
    AddressListTableview.layer.masksToBounds = YES;
    GenderIdentifier = @"";
   Help.layer.cornerRadius = 4;
    
    
    [MainScrollview setScrollEnabled:YES];
    MainScrollview.contentSize = CGSizeMake(self.view.frame.size.width, MainView.frame.size.height);
    
   
    
    FemaleImageview.image = [UIImage imageNamed:@"uncheck"];
    
    [BackButton setBackgroundImage:[CommanFunction imageWithColor:[UIColor grayColor]] forState:UIControlStateHighlighted];
  
  
    
    // [self addtoolbar];
    [self addbirthdaypicker];
    [[UIButton appearance] setExclusiveTouch:YES];
    
    /*/LOCALIZATION STORYBOARD*/
   
    sign_up.text = NSLocalizedString(@"sign_up", "");
    
    gender.text = NSLocalizedString(@"gender", "");
    
    male.text = NSLocalizedString(@"male", "");
    
    female.text = NSLocalizedString(@"female", "");
    
    optional.text = NSLocalizedString(@"optional", "");
    
    BirthdayTextField.placeholder = NSLocalizedString(@"birthday", "");
    
    FirstNameTextField.placeholder = NSLocalizedString(@"first_name", "");
    
    LastNameTextField.placeholder = NSLocalizedString(@"last_name", "");
    
    GmailTextField.placeholder = NSLocalizedString(@"email", "");
    
    MobileNumberTextField.placeholder = NSLocalizedString(@"mobile", "");
    
    PasswordTextField.placeholder = NSLocalizedString(@"password", "");
    
    CurrentPasswordTextField.placeholder = NSLocalizedString(@"confirm_password", "");
    
    [CreateBtn setTitle:NSLocalizedString(@"create_account", "") forState:UIControlStateNormal];
    
    [already_a_member_login setTitle:NSLocalizedString(@"already_a_member_login", "") forState:UIControlStateNormal];
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark IBACTION METHOD.

- (IBAction)hiddenBtn:(id)sender {
    
    NSURL *password = [NSURL URLWithString:@"http://voluntaryrefundvalue.com/admin/Vrv_pdfs/Password_help.pdf"];
    
    [[UIApplication sharedApplication] openURL: password options:@{} completionHandler:^(BOOL success) {
        if (success)
            NSLog(@"Opened passwordhelp");
        
    }];

    
}

- (IBAction)BackButtonClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    
}
- (IBAction)MemberButtonClick:(id)sender {
    
    
    LoginView *LoginScreenObj = [self.storyboard instantiateViewControllerWithIdentifier:@"loginid"];
    [self.navigationController pushViewController:LoginScreenObj animated:YES];

}
- (IBAction)SignUpButtonClick:(id)sender {
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    if (remoteHostStatus == NotReachable) {
        
        [self showAlert:NSLocalizedString(@"internetActivity_off", "")];
        
    } else
    
    [self CheckSignUpValidation];
}

- (IBAction)MaleButtonClick:(id)sender {
    GenderIdentifier = NSLocalizedString(@"male", "");
    MaleImageview.image = [UIImage imageNamed:@"check"];
    FemaleImageview.image = [UIImage imageNamed:@"uncheck"];
    OptionalImageview.image = [UIImage imageNamed:@"uncheck"];
}

- (IBAction)FemaleButtonClick:(id)sender {
    GenderIdentifier = NSLocalizedString(@"female", "");
    MaleImageview.image = [UIImage imageNamed:@"uncheck"];
    OptionalImageview.image = [UIImage imageNamed:@"uncheck"];
    FemaleImageview.image = [UIImage imageNamed:@"check"];
}

- (IBAction)OptionalBtnClick:(id)sender {
    
    GenderIdentifier = NSLocalizedString(@"optional", "");
    MaleImageview.image = [UIImage imageNamed:@"uncheck"];
    FemaleImageview.image = [UIImage imageNamed:@"uncheck"];
    OptionalImageview.image = [UIImage imageNamed:@"check"];
    
}

#pragma mark GooglePlace Delegate Method.

- (SPGooglePlacesAutocompletePlace *)placeAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%@",searchResultPlaces[indexPath.row]);
    return searchResultPlaces[indexPath.row];
}


#pragma mark UITableviee DataSourece And Delegate Method.

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return searchResultPlaces.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"adreshcel";
    
    AddressCell *Cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (Cell == nil) {
        Cell = [[AddressCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    Cell.addressnamelabel.text = [self placeAtIndexPath:indexPath].name;
    return Cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    AddressListTableview.hidden = YES;
    [currenttextField resignFirstResponder];
    currenttextField.text = [self placeAtIndexPath:indexPath].name;
}


#pragma mark VOID METHOD.

- (void)logout {
    
    [ConfigurationManager shared].currentUser = nil;
    
    [self showAlertLoginFailed:NSLocalizedString(@"error_login_failed", "")];
    
    
    
}

- (void)ForceUserLogout {
    
    [ConfigurationManager shared].currentUser = nil;
    
    [AuthenticationService logoutWith:[ConfigurationManager shared].currentUser.getUserId success:^(BOOL success) {
        [self logout];
    } failure:^(NSError * _Nullable error) {
        [self logout];
    }];
}


- (void)focusPasswordTextField {

    [PasswordTextField becomeFirstResponder];
}

- (void)delayCurrentPasswordTextField {
    
    [CurrentPasswordTextField becomeFirstResponder];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {

    if (textField == MobileNumberTextField && MobileNumberTextField.text.length <= 25) {
        
      //  [MobileNumberTextField resignFirstResponder];

        [self performSelector:@selector(focusPasswordTextField) withObject:self afterDelay:0.01 ];


    }


     if (textField == PasswordTextField) {
        [PasswordTextField resignFirstResponder];

    }

}

- (void)showAlert:(NSString *)message {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"dialog_ok", "")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   
                               }];
    [alert addAction:okButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)showAlertLoginFailed:(NSString *)message {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:NSLocalizedString(@"error_login_failed", "")
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"dialog_ok", "")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
        
        [ConfigurationManager shared].currentUser = nil;
        
        LoginView *LoginScreenObj = [self.storyboard instantiateViewControllerWithIdentifier:@"loginid"];
        [self.navigationController pushViewController:LoginScreenObj animated:YES];
                                   
                               }];
    
    
    [alert addAction:okButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)CheckSignUpValidation{
    if ([GenderIdentifier isEqualToString:@""]) {
        [self showAlert:NSLocalizedString(@"error_empty_gender", "")];
        [MainScrollview setContentOffset:CGPointZero animated:YES];


    } else if (BirthdayTextField.text.length == 0) {
        [BirthdayTextField becomeFirstResponder];
        [self showAlert:NSLocalizedString(@"error_empty_dob", "")];

    } else if (FirstNameTextField.text.length < 3 ) {
        [FirstNameTextField becomeFirstResponder];
        [self showAlert:NSLocalizedString(@"error_empty_firstname", "")];


    } else if (LastNameTextField.text.length < 3) {
        [LastNameTextField becomeFirstResponder];
        [self showAlert:NSLocalizedString(@"error_empty_lastname", "")];


   }
    
      else if ([self IsValidEmail:GmailTextField.text] != true) {
        [GmailTextField becomeFirstResponder];
        [self showAlert:NSLocalizedString(@"error_enter_valid_email", "")];
        
        
    } else if (MobileNumberTextField.text.length < 1 || MobileNumberTextField.text.length > 25) {
        [MobileNumberTextField becomeFirstResponder];
        
        MobileNumberTextField.text = @"";
        
        [self showAlert:NSLocalizedString(@"error_enter_valid_phone", "")];
        
        
    } else if (PasswordTextField.text.length < 8) {
        [PasswordTextField becomeFirstResponder];
        [self showAlert:NSLocalizedString(@"error_password_length", "")];
        
    } else if (CurrentPasswordTextField.text.length == 0) {
        [CurrentPasswordTextField becomeFirstResponder];
        [self showAlert:NSLocalizedString(@"error_password_confirmation", "")];
        
        
    } else if (![PasswordTextField.text isEqualToString:CurrentPasswordTextField.text]) {
        [CurrentPasswordTextField becomeFirstResponder];
        [self showAlert:NSLocalizedString(@"error_password_confirmation", "")];
        
       
    } else {
       
        
        [self showProgressView];
        
    
        
        //laravel register api  /api/v1/register
        [AuthenticationService registerWith:GmailTextField.text password:PasswordTextField.text first_name:FirstNameTextField.text last_name:LastNameTextField.text mobile:MobileNumberTextField.text gender:GenderIdentifier birth_day:BirthdayTextField.text success:^(UserDetails * _Nonnull userInfo) {
            
            [self showTermsScreen];
           // [self makeAutomaticLogin];
    
            
        } failure:^(NSError * _Nullable error) {
            
            [self hideProgressView];
            [self->GmailTextField becomeFirstResponder];
            [self showAlert:NSLocalizedString(@"signup_fail", "")];
            
           // [self showAlert:error.localizedDescription];
           
        }];
    }
}

- (void)showTermsScreen {
    
    [self hideProgressView];
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:NSLocalizedString(@"signup_success_title", "")
                                  
                                  message:NSLocalizedString(@"reg_success_message", "")
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* LoginButton = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"login_now", "")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   
                                    [self makeAutomaticLogin];
                                   
                               }];
    
    UIAlertAction* LoginScreenButton = [UIAlertAction
                                  actionWithTitle:NSLocalizedString(@"try_login_screen", "")
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action)
                                  {
                                      
                                        [self AreYouSure];
                                       
                                      
                                  }];
    
    [alert addAction:LoginButton];
    [alert addAction:LoginScreenButton];
    
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)AreYouSure {
    
    [self hideProgressView];
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:NSLocalizedString(@"reg_success_message", "")
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* YesButton = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"green_yes", "")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   
                                    [self MemberButtonClick:self];
                                   
                               }];
    
    UIAlertAction* NoButton = [UIAlertAction
                                  actionWithTitle:NSLocalizedString(@"green_no", "")
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action)
                                  {
                                      
                                  [self showTermsScreen];
                                       
                                      
                                  }];
    
    [alert addAction:YesButton];
    [alert addAction:NoButton];
    
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)makeAutomaticLogin {
    
    [self showProgressView];
    
    [AuthenticationService loginWith:self->GmailTextField.text password:self->PasswordTextField.text success:^(UserDetails * _Nonnull user) {
        
        //TODO : Handle Login
        [self hideProgressView];
        
        LeftRightViewController *LeftrightviewScreenObject = [self.storyboard instantiateViewControllerWithIdentifier:@"leftright"];
        UIViewController *newVC = LeftrightviewScreenObject;
        NSMutableArray *vcs =  [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
        [vcs insertObject:newVC atIndex:vcs.count-1];
        [self.navigationController setViewControllers:vcs animated:YES];
        [self.navigationController popViewControllerAnimated:YES];
        
        
    } failure:^(NSError * _Nullable error) {
        [self hideProgressView];
        
        [self showTermsScreen];
       // [self ForceUserLogout];
        
        
    }];
    
    
}


-(void)textFieldDidBeginEditing:(UITextField *)textField{
    AddressListTableview.hidden = YES;
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
    }];
    return [super canPerformAction:action withSender:sender];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    currenttextField = textField;
    
    //Disables whitespace
    
//    if ([string isEqualToString:@" "] && textField.text.length == 0)
//    {
//        return NO;
//
//    }
//
//    if (textField ==  MobileNumberTextField) {
//
//        int length = (int)[self getLength:textField.text];
//
//
//        if(length == 10)
//        {
//            if(range.length == 0)
//                return NO;
//        }
//
//        if(length == 3)
//        {
//            NSString *num = [self formatNumber:textField.text];
//            textField.text = [NSString stringWithFormat:@"%@-",num];
//
//            if(range.length > 0)
//                textField.text = [NSString stringWithFormat:@"%@",[num substringToIndex:3]];
//        }
//        else if(length == 6)
//        {
//            NSString *num = [self formatNumber:textField.text];
//
//            textField.text = [NSString stringWithFormat:@"%@-%@-",[num  substringToIndex:3],[num substringFromIndex:3]];
//
//            if(range.length > 0)
//                textField.text = [NSString stringWithFormat:@"%@-%@",[num substringToIndex:3],[num substringFromIndex:3]];
//        }
        
//    }
    
    
    
    return YES;
    
    
    
}





-(void)addbirthdaypicker{
    datePicker = [[UIDatePicker alloc]init];
    datePicker.datePickerMode=UIDatePickerModeDate;
    datePicker.maximumDate = [NSDate date];
    BirthdayTextField.inputView = datePicker;
    UIToolbar *toolBar =[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    toolBar.tintColor = [UIColor grayColor];
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"signup_done", "") style:UIBarButtonItemStylePlain target:self action:@selector(ShowSelectedDate)];
    UIBarButtonItem *space = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    toolBar.items = @[space,doneBtn];
    BirthdayTextField.inputAccessoryView = toolBar;
}

-(int)BirthDateValidation{
    
    NSString *birthDate = BirthdayTextField.text;
    NSDate *todayDate = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"MM/dd/yyyy";
    int time = [todayDate timeIntervalSinceDate:[dateFormatter dateFromString:birthDate]];
    
    int allDays = (((time/60)/60)/24);
    int days = allDays%365;
    int years = (allDays-days)/365;
    
    NSLog(@"You live since %i years and %i days",years,days);
    return years;
}

-(void)ShowSelectedDate
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    formatter.dateFormat = @"MM/dd/yyyy";
    BirthdayTextField.text =[NSString stringWithFormat:@"%@",[formatter stringFromDate:datePicker.date]];
    [BirthdayTextField resignFirstResponder];
}

-(void) DisplayAlertMessage:(NSString *)Message{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"VRVC" message:Message preferredStyle:UIAlertControllerStyleAlert];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [alertController dismissViewControllerAnimated:YES completion:^{
            
        }];
        
    });
    
    // return;
}

- (void) runSpinAnimationOnView:(UIView*)view duration:(CGFloat)duration rotations:(CGFloat)rotations repeat:(float)repeat
{
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 /* full rotation*/ * rotations * duration];
    rotationAnimation.duration = duration;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = repeat;
    
    
    [view.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == FirstNameTextField) {
        [textField resignFirstResponder];
        [LastNameTextField becomeFirstResponder];
    }
    else if (textField == LastNameTextField) {
        [textField resignFirstResponder];
        [GmailTextField becomeFirstResponder];
    }
    else if (textField == GmailTextField) {
        [textField resignFirstResponder];
        [MobileNumberTextField becomeFirstResponder];
    }
    
    else if (textField == MobileNumberTextField) {
        [textField resignFirstResponder];
        [PasswordTextField becomeFirstResponder];
    }
    
    else if (textField == PasswordTextField) {
        [textField resignFirstResponder];
        
        [self performSelector:@selector(delayCurrentPasswordTextField) withObject:self afterDelay:0.01 ];
        
    }
    
    else if (textField == CurrentPasswordTextField) {
        [textField resignFirstResponder];
    
    }
    
    return NO;
}
    

-(BOOL)IsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

-(BOOL)myMobileNumberValidate:(NSString*)number
{
    NSString *numberRegEx = @"[0-9]{10}";
    NSPredicate *numberTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", numberRegEx];
    if ([numberTest evaluateWithObject:number] == YES)
        return TRUE;
    else
        return FALSE;
}



#pragma mark UITextField Delegate Method.

// we currently arent using a filter    else if (![self isValidPassword:PasswordTextField.text])

-(BOOL)isValidPassword:(NSString *)checkString{
    NSString *stricterFilterString = @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$#!%*?&])[A-Za-z\\d$@$#!%*?&]{8,}";
    NSPredicate *passwordTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", stricterFilterString];
    return [passwordTest evaluateWithObject:checkString];
   
   


}


-(BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    if (textField == BirthdayTextField) {
        [dlf setObject:BirthdayTextField.text forKey:@"PreviousBdate"];
        [dlf synchronize];
        
        int personyears =  [self BirthDateValidation];
        if (personyears < 18) {
           
            [BirthdayTextField becomeFirstResponder];
            [self showAlert:NSLocalizedString(@"age_signup", "")];
            
        }
        
    }
    
    return YES;
}

//- (NSString *)formatNumber:(NSString *)mobileNumber
//{
//    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
//    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
//    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
//    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
//    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
//
//    NSLog(@"%@", mobileNumber);
//
//    int length = (int)mobileNumber.length;
//    if(length > 10)
//    {
//        mobileNumber = [mobileNumber substringFromIndex: length-10];
//        NSLog(@"%@", mobileNumber);
//
//    }
//
//
//    return mobileNumber;
//}
//
//- (int)getLength:(NSString *)mobileNumber
//{
//    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
//    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
//    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
//    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
//    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
//
//    int length = (int)mobileNumber.length;
//
//    return length;
//}
@end
