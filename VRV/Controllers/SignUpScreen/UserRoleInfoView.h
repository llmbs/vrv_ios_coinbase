
#import <UIKit/UIKit.h>

@interface UserRoleInfoView : UIViewController<UITextFieldDelegate>
{
   
    __weak IBOutlet UIView *MainView;
    __weak IBOutlet UIButton *BackButton;
    __weak IBOutlet UIButton *Eighteen;
    
    __weak IBOutlet UIButton *terms;
    __weak IBOutlet UIButton *privacy;
    __weak IBOutlet UIButton *cookie;
    
    
}

- (IBAction)NextButtonClick:(id)sender;
- (IBAction)BackButtonClick:(id)sender;

- (IBAction)TermsClick:(id)sender;
- (IBAction)PrivacyClick:(id)sender;
- (IBAction)CookieClick:(id)sender;




@end
