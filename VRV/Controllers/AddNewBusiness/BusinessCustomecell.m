

#import "BusinessCustomecell.h"
#import "CommanFunction.h"

#import "VRV-Swift.h"

@implementation BusinessCustomecell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code.corner
    
    self.Removebutton.layer.cornerRadius = 4;
    self.EditButton.layer.cornerRadius = 4;
    self.NotAcceptingRadius.layer.cornerRadius = 4;
    self.googlemapwhite.layer.cornerRadius = 4;
   
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

// removed stringWithFormat:@"%@ %@ %@ %@",  data.phone,

- (void)configureWith:(BusinessData *)data {
    _NameAdressLabel.text = [NSString stringWithFormat:@"%@ %@ %@", data.company_name, data.address1, data.address2];
    _NameFirstCharaterlabel.text = [[data.company_name substringToIndex:1] uppercaseString];
}

@end
