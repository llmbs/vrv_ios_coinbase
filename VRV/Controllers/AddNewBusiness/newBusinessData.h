

#import <Foundation/Foundation.h>

@interface newBusinessData : NSObject

@property(strong,nonatomic) NSString *CompanyName;
@property(strong,nonatomic) NSString *address1;
@property(strong,nonatomic) NSString *address2;
@property(strong,nonatomic) NSString *city;
@property(strong,nonatomic) NSString *state;
@property(strong,nonatomic) NSString *zipcode;

@property(strong,nonatomic) NSString *business_id;
@property(strong,nonatomic) NSString *cigarette_buds;
@property(strong,nonatomic) NSString *cmp_ein;
@property(strong,nonatomic) NSString *country;
@property(strong,nonatomic) NSString *created_date;
@property(strong,nonatomic) NSString *drv_licence_no;
@property(strong,nonatomic) NSString *first_name;
@property(strong,nonatomic) NSString *job_title;
@property(strong,nonatomic) NSString *last_attempt;
@property(strong,nonatomic) NSString *last_name;
@property(strong,nonatomic) NSString *natural_wine_corks;
@property(strong,nonatomic) NSString *phone;


@end
