

#import "newBusinessView.h"
#import "BusinessCustomecell.h"
#import "BusinessSignUpView.h"
#import "Reachability.h"
#import "AFNetworking.h"
#import "CommanFunction.h"
#import "Defaults.h"
#import "newBusinessData.h"
#import "BusinessRoleinfoview.h"
#import "AboutUsView.h"
#import "LeftRightViewController.h"
#import "VRV-Swift.h"


@interface newBusinessView ()
{
    NSUserDefaults *dlf;
  
    NSMutableArray *BusinessListArray;
    NSMutableArray *TempArray,*STempArray;
    
    
    
    NSString *BusinessID;
    NSString *CompanyName;
    
    newBusinessData *BusinessdataClass;
    
    

}
@end

@implementation newBusinessView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    dlf = [NSUserDefaults standardUserDefaults];
    
   // yesbuttoncount = 0;
   // yesbuttoncountOne = 0;
    //SearchBtnTag = 0;
    MainmenuRad.layer.cornerRadius = 4;
    FinishedRad.layer.cornerRadius = 4;
    ReviewBusinessView.layer.cornerRadius = 4;
    AddBusinessView.layer.cornerRadius = 4;
    DeleteAllImageView.layer.cornerRadius = 4;
    DeleteButton.layer.cornerRadius = 4;
    
    
    add.layer.cornerRadius = 4;
   review.layer.cornerRadius = 4;

 
   
    STempArray = [[NSMutableArray alloc]init];
    

    TempArray=[[NSMutableArray alloc]init];
    BusinessListArray=[[NSMutableArray alloc]init];
    

//    for (UIButton *btn in _AllButtonCollections) {
  //      [btn setBackgroundImage:[CommanFunction imageWithColor:[UIColor grayColor]] forState:UIControlStateHighlighted];
        
   // }
    
    [[UIButton appearance] setExclusiveTouch:YES];
}

-(void)viewDidAppear:(BOOL)animated{
 
     [self RetrieveUsersBusinesses];
}

#pragma Mark VOID METHODS.

-(void)RoundCornerForUIViewAndShadowEffect:(UIView *)MyView{
    (MyView.layer).cornerRadius = 10.0f;
    
    (MyView.layer).shadowColor = [UIColor blackColor].CGColor;
    (MyView.layer).shadowOpacity = 0.8;
    (MyView.layer).shadowRadius = 3.0;
    (MyView.layer).shadowOffset = CGSizeMake(2.0, 2.0);
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UITableView Delegate Method.

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return BusinessListArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *simpleTableIdentifier = @"BusinessCustomecell";
    
    BusinessCustomecell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil)
    {

        cell = [[BusinessCustomecell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    BusinessData *data = self->BusinessListArray[indexPath.row];
    
 
     cell.userFirstchahcterImageview.layer.cornerRadius = 4;
     cell.userFirstchahcterImageview.layer.masksToBounds = YES;
    
    
    //removed NSString stringWithFormat:@"%@ - %@ - %@ - %@."data.phone
    
     cell.NameAdressLabel.text = [NSString stringWithFormat:@"%@ - %@ - %@",data.company_name,
                                  data.address1,data.address2];
   
    NSString *firstLetter = [data.company_name substringToIndex:1];
    firstLetter = firstLetter.uppercaseString;
    cell.NameFirstCharaterlabel.text = firstLetter;
    
    
    cell.EditButton.tag = indexPath.row;
    cell.Removebutton.tag = indexPath.row;
    
    [cell.EditButton addTarget:self action:@selector(EditButtonClickOnCell:) forControlEvents:UIControlEventTouchUpInside];
    [cell.Removebutton addTarget:self action:@selector(RemoveButtonClickOnCell:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

- (void)showAlert:(NSString *)message {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Business Registration"
                                  message:@"If you want to partner with us and collect recyclable materials, touch next and fill out the required information.\n\nOnce you finish, your business will populate in the recycling locations map and users can recycle at your facility."
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* NextButton = [UIAlertAction
                               actionWithTitle:@"Next"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   
                                   BusinessSignUpView *BusinessSignUpObj = [self.storyboard instantiateViewControllerWithIdentifier:@"Bsignupid"];
                                   [self.navigationController pushViewController:BusinessSignUpObj animated:YES];
                                   
                                 
                    
                                  
                                   
                               }];
    
    UIAlertAction* CancelButton = [UIAlertAction
                                 actionWithTitle:@"Cancel"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     
                                 }];
    [alert addAction:CancelButton];
    [alert addAction:NextButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}


#pragma mark IBACTION Method.


- (IBAction)MenuButtonclick:(id)sender {
    
    LeftRightViewController *LeftrightviewScreenObject = [self.storyboard instantiateViewControllerWithIdentifier:@"leftright"];
    UIViewController *newVC = LeftrightviewScreenObject;
    NSMutableArray *vcs =  [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
    [vcs insertObject:newVC atIndex:vcs.count-1];
    [self.navigationController setViewControllers:vcs animated:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)EditButtonClickOnCell:(UIView *)sender{
    BusinessData *data = self->BusinessListArray[sender.tag];
    
    BusinessSignUpView *BusinessSignUpObj = [self.storyboard instantiateViewControllerWithIdentifier:@"Bsignupid"];
    BusinessSignUpObj.selectedBusiness = data;
    [self.navigationController pushViewController:BusinessSignUpObj animated:YES];
    
    BusinessSignUpObj.selectedEdit=YES;
    
}

-(IBAction)RemoveButtonClickOnCell:(UIButton *)sender{
    
    
    if(BusinessListArray.count > 0) {
        
        BusinessData *data = self->BusinessListArray[sender.tag];
        
        NSString *message = [NSString stringWithFormat:@"Are you sure you want to remove %@?", data.company_name];
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Delete business?"
                                      message:message
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"Yes"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        
                                        BusinessData *data = self->BusinessListArray[sender.tag];
                                        [self showProgressView];
                                        [BusinessService deleteSingleBusinessWithBusiness_id:data.id success:^(BOOL success) {
                                            [self hideProgressView];
                                            [self showUniversalAlert:@"Your business has been deleted."];
                                            [self->BusinessListArray removeObject:data];
                                            [self->BusinesslistTabelView reloadData];
                                            
                                            if (self->BusinessListArray.count == 0) {
                                                self->_BusinessCount.text = [NSString stringWithFormat:@"You have %lu businesses.",(unsigned long)self->BusinessListArray.count];
                                            } else if (self->BusinessListArray.count == 1) {
                                                self->_BusinessCount.text = [NSString stringWithFormat:@"You have %lu business.",(unsigned long)self->BusinessListArray.count];
                                                
                                            }else if (self->BusinessListArray.count >=2 ) {
                                                
                                                self->_BusinessCount.text = [NSString stringWithFormat:@"You have %lu businesses.",(unsigned long)self->BusinessListArray.count];
                                            }
                                            
                                        } failure:^(NSError * _Nullable error) {
                                            [self handleError:error];
                                            [self hideProgressView];
                                        }];
                                    }];
        
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"No"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       
                                       
                                   }];
        
        
        [alert addAction:yesButton];
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
        
        
    }
    
  // api/v1/business/delete_one
    
}
            

-(UIImage*) drawText:(NSString*) text
             inImage:(UIImage*)  image
             atPoint:(CGPoint)   point
{
    
    UIFont *font = [UIFont boldSystemFontOfSize:30];
    UIGraphicsBeginImageContext(image.size);
    [image drawInRect:CGRectMake(0,0,image.size.width,image.size.height)];
  
   
     CGRect rect = CGRectMake(20,22 , image.size.width, image.size.height);
    
    [[UIColor whiteColor] set];
    [text drawInRect:CGRectIntegral(rect) withFont:font];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}
- (IBAction)FinishedButtonClick:(id)sender {
    
    AboutUsView *AboutUsScreenObj = [self.storyboard instantiateViewControllerWithIdentifier:@"AboutUsViewId"];
    [self.navigationController pushViewController:AboutUsScreenObj animated:YES];
}

- (IBAction)ReviewYourBusinessButonClick:(id)sender {


    [self RetrieveUsersBusinesses];
}


- (IBAction)AddBusinessButtonClick:(id)sender {
   
    [self showAlert:@""];
    
    
    
}

- (IBAction)DeleticonButtonClick:(id)sender {
    if(BusinessListArray.count > 0){
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Delete all businesses?"
                                      message:@"Are you sure you want to remove every business?"
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                   actionWithTitle:@"Yes"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       [self showProgressView];
                                       [BusinessService deleteAllBusinessWithSuccess:^(BOOL success) {
                                           [self hideProgressView];
                                           [self->BusinessListArray removeAllObjects];
                                          // int c = (int)[self->BusinessListArray count]-1;
                                           //for (int l = 0; l <= c; l++) {[self->BusinessListArray removeObjectAtIndex:0];}
                                           
                                           [self->BusinesslistTabelView reloadData];
                                           
                                           if (self->BusinessListArray.count == 0) {
                                               self->_BusinessCount.text = [NSString stringWithFormat:@"You have %lu businesses.",(unsigned long)self->BusinessListArray.count];
                                           } else if (self->BusinessListArray.count == 1) {
                                               self->_BusinessCount.text = [NSString stringWithFormat:@"You have %lu business.",(unsigned long)self->BusinessListArray.count];
                                               
                                           }else if (self->BusinessListArray.count >=2 ) {
                                               
                                               self->_BusinessCount.text = [NSString stringWithFormat:@"You have %lu businesses.",(unsigned long)self->BusinessListArray.count];
                                           }
                                       } failure:^(NSError * _Nullable error) {
                                           [self handleError:error];
                                       }];
                                 }];
        
        UIAlertAction* noButton = [UIAlertAction
                                    actionWithTitle:@"No"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                   {
                                       
                                       
                                   }];
        
        
        [alert addAction:yesButton];
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
         
        
}



}


#pragma mark API CALLIG VOID METHOD.
                                    
 - (void)showAlertInternet:(NSString *)message {
    UIAlertController * alert=   [UIAlertController
    alertControllerWithTitle:@""
    message:message
    preferredStyle:UIAlertControllerStyleAlert];
                                        
    UIAlertAction* okButton = [UIAlertAction
    actionWithTitle:@"OK"
    style:UIAlertActionStyleDefault
    handler:^(UIAlertAction * action)
    {
        
        
    }];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)showUniversalAlert:(NSString *)message {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   
                                   
                               }];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}

                                    
                                    

-(void)RetrieveUsersBusinesses {
   [self showProgressView];

    
    [BusinessService getAllBusinessWithSuccess:^(NSArray<BusinessData *> * _Nonnull data) {
        self->BusinesslistTabelView.hidden = NO;
        [self hideProgressView];
        self->BusinessListArray = [NSMutableArray arrayWithArray:data];
        [self->BusinesslistTabelView reloadData];
        
        
        if (self->BusinessListArray.count == 0) {
            self->_BusinessCount.text = [NSString stringWithFormat:@"You have %lu businesses.",(unsigned long)self->BusinessListArray.count];
        } else if (self->BusinessListArray.count == 1) {
             self->_BusinessCount.text = [NSString stringWithFormat:@"You have %lu business.",(unsigned long)self->BusinessListArray.count];
            
        }else if (self->BusinessListArray.count >=2 ) {
            
            self->_BusinessCount.text = [NSString stringWithFormat:@"You have %lu businesses.",(unsigned long)self->BusinessListArray.count];
        }
        
        
        
    } failure:^(NSError * _Nullable error) {
        [self handleError:error];
        [self hideProgressView];
        self->_BusinessCount.text = [NSString stringWithFormat:@"You have 0 businesses."];
        self->BusinesslistTabelView.hidden = YES;
        
    }];
}

@end
