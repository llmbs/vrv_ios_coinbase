

#import <UIKit/UIKit.h>

@class BusinessData;

@interface BusinessCustomecell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *userFirstchahcterImageview;
@property (weak, nonatomic) IBOutlet UILabel *NameAdressLabel;
@property (weak, nonatomic) IBOutlet UIButton *EditButton;
@property (weak, nonatomic) IBOutlet UIButton *Removebutton;
@property (weak, nonatomic) IBOutlet UILabel *NameFirstCharaterlabel;
@property (weak, nonatomic) IBOutlet UIButton *googlemapwhite;

@property (weak, nonatomic) IBOutlet UIButton *NotAcceptingRadius;

- (void)configureWith:(BusinessData *)data;

@end
