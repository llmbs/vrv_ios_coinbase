


#import <UIKit/UIKit.h>

@interface newBusinessView : UIViewController<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,UISearchResultsUpdating,UITextFieldDelegate,UITextFieldDelegate>
{
    __weak IBOutlet UITableView *BusinesslistTabelView;
    
    __weak IBOutlet UIView *ReviewBusinessView;
    __weak IBOutlet UIView *AddBusinessView;
    
    
    
    //IBOutlet UILabel *BusinessCountLabel;

 
    
    __weak IBOutlet UIButton *add;
    __weak IBOutlet UIButton *review;
    
    __weak IBOutlet UIButton *DeleteButton;
    __weak IBOutlet UIImageView *DeleteAllImageView;
    __weak IBOutlet UIButton *FinishedRad;
    __weak IBOutlet UIButton *MainmenuRad;
    
   
    
    
    
    
}
@property (strong, nonatomic) UISearchController *searchController;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *AllButtonCollections;
@property (nonatomic, weak) IBOutlet UITextField *BusinessCount;

- (IBAction)FinishedButtonClick:(id)sender;
- (IBAction)ReviewYourBusinessButonClick:(id)sender;
- (IBAction)AddBusinessButtonClick:(id)sender;
- (IBAction)DeleticonButtonClick:(id)sender;
- (IBAction)MenuButtonclick:(id)sender;



@end
