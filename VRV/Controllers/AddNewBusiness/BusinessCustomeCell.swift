//
//  BusinessCustomeCell.swift
//  VRV
//
//  Created by baps on 26/09/18.
//  Copyright © 2018 Abhishek Sheth. All rights reserved.
//

import UIKit

@objc protocol BusinessCustomeCellDelegate {
    
    
    func didTapNotAccepting(from cell : BusinessCustomeCell)
    func didTapOpenInMap(from cell : BusinessCustomeCell)
    func didTapImprove(from cell : BusinessCustomeCell)
   
}

class BusinessCustomeCell: UITableViewCell {
    
    @IBOutlet var userFirstchahcterImageview : UIImageView!
    @IBOutlet var NameAdressLabel : UILabel!
    @IBOutlet var EditButton : UIButton!
    @IBOutlet var Removebutton : UIButton!
    @IBOutlet var NameFirstCharaterlabel : UILabel!
    @IBOutlet var googlemapwhite : UIButton!
    @IBOutlet weak var imgGoogleMapWhite: UIImageView!
    @IBOutlet var NotAcceptingRadius : UIButton!
    @IBOutlet var AccuracyRadius : UIButton!
    @IBOutlet weak var imgAccuracyRadius: UIImageView!
    @IBOutlet weak var imgRecycle: UIImageView!
    @IBOutlet var hourGlassWhite : UIButton!
    @IBOutlet weak var lctDescriptionBottom: NSLayoutConstraint!
    
    @objc var delegate : BusinessCustomeCellDelegate? = nil

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //self.Removebutton.layer.cornerRadius = 4
        //self.EditButton.layer.cornerRadius = 4
        self.selectionStyle = .none
        self.NotAcceptingRadius.layer.cornerRadius = 4
        self.googlemapwhite.layer.cornerRadius = 4
        self.userFirstchahcterImageview.layer.cornerRadius = 4
        self.AccuracyRadius.layer.cornerRadius = 4
        UIButton.appearance().isExclusiveTouch = true
    }
    
    // removed "%@- %@- %@, %@"   data.phone
    
    @objc func configureWith(data : BusinessData) {
        NameAdressLabel.text = String(format: "%@ - %@, %@- %.1f miles away", data.company_name ?? "", data.address1 ?? "", data.address2 ?? "", data.distance?.floatValue ?? 0)
        NameFirstCharaterlabel.text = String(data.company_name!.prefix(1))
    }
    
    @objc func setupConstraints() {
        self.lctDescriptionBottom.constant = 20.0
        self.imgRecycle.isHidden = true
        self.imgAccuracyRadius.isHidden = true
        self.imgGoogleMapWhite.isHidden = true
        self.googlemapwhite.isHidden = true
        self.AccuracyRadius.isHidden = true
        self.NotAcceptingRadius.isHidden = true
    }
    
    @objc func configureForScoreAndRank(data: ScoreRankData) {
        let customer_id: Int = Int(data.customer_id)
        let score: Int = Int(data.score)
        let rank: Int = Int(data.rank)
    
        NameAdressLabel.text = "Id: \(customer_id) | \(data.first_name) \(data.last_name) has a score of \(score) and is ranked \(rank)."
        NameFirstCharaterlabel.text = "\(rank)"
    }
    
    @IBAction func actionNotAccepting() {
        self.delegate?.didTapNotAccepting(from: self)
    }
    
    @IBAction func openGoogleMaps() {
        self.delegate?.didTapOpenInMap(from: self)
    }
    
    @IBAction func actionImprove() {
        self.delegate?.didTapImprove(from: self)
    }
   
}
