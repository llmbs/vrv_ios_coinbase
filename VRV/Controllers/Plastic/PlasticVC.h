//
//  PlasticVC.h
//  VRV
//
//  Created by My Mac on 01/09/21.
//  Copyright © 2021 Abhishek Sheth. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

NS_ASSUME_NONNULL_BEGIN

@interface PlasticVC : UIViewController<UIScrollViewDelegate,UITextFieldDelegate,UICollectionViewDelegate,MFMailComposeViewControllerDelegate>
{
    __weak IBOutlet UIScrollView *MainScrollview;
    __weak IBOutlet UIView *materialView;
    __weak IBOutlet UIView *tabHeader;
    __weak IBOutlet UIView *wholeview;
    
    
    __weak IBOutlet UITextField *MaterialQuantityTxt;
    __weak IBOutlet UITextField *MaterialDepositTextField;
    
    
    
    __weak IBOutlet UIButton *gamer;
    

    
    
    __weak IBOutlet UIView *GameView;
    __weak IBOutlet UIImageView *BNB;
    __weak IBOutlet UIImageView *USDC;
    
    __weak IBOutlet UILabel *percent;
    
    
    
    /*/ Localization section*/
    
    __weak IBOutlet UIView *whiteArea;
    
    __weak IBOutlet UILabel *ready_to_recycle;
    
    __weak IBOutlet UIButton *rtr_help;
    
    
    __weak IBOutlet UILabel *turn_in;
    
    __weak IBOutlet UILabel *rtr_car_keys;
    
    __weak IBOutlet UILabel *Game_on;
    
    
    
    __weak IBOutlet UILabel *plasticq;
    
    __weak IBOutlet UITextField *input_01;
    
    
    
    __weak IBOutlet UILabel *vrv_coin;
    
    
    __weak IBOutlet UIButton *business_access_submit;
    
    __weak IBOutlet UIButton *retrieve_address;
    
    __weak IBOutlet UILabel *BelensLabel;
    
    
    
    
}

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *AllButtonCollections;

@property (strong, nonatomic) NSString *token;
@property (nonatomic) NSInteger level;

- (IBAction)MenuButtonClick:(id)sender;
- (IBAction)SubmitMaterial:(id)sender;
- (IBAction)HowtoWinClick:(id)sender;
- (IBAction)GameButtonClick:(id)sender;
- (IBAction)PaymentAddress:(id)sender;
- (IBAction)HourGlassButtonClick:(id)sender;
-(IBAction)RefreshClick:(id)sender;


@end

NS_ASSUME_NONNULL_END
