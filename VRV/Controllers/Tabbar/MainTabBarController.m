//
//  MainTabBarController.m
//  VRV
//
//  Created by My Mac on 01/09/21.
//  Copyright © 2021 Abhishek Sheth. All rights reserved.
//

#import "MainTabBarController.h"
#import "CorkVC.h"
#import "ClothesVC.h"
#import "PlasticVC.h"
#import "Defaults.h"

@interface MainTabBarController ()

@end

@implementation MainTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
}

-(void)setupTabBar {
    
    UIStoryboard *main = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    CorkVC *corkObj = [main instantiateViewControllerWithIdentifier:@"CorkVC"];

    ClothesVC *clothesObj = [main instantiateViewControllerWithIdentifier:@"ClothesVC"];

    PlasticVC *plasticObj = [main instantiateViewControllerWithIdentifier:@"PlasticVC"];

    
    
    UITabBarItem *item3 = [[UITabBarItem alloc]init];
    item3.title = NSLocalizedString(@"p_schedule", "");
    
    UITabBarItem *item = [[UITabBarItem alloc]init];
    item.title = NSLocalizedString(@"c_turn", "");

    UITabBarItem *item2 = [[UITabBarItem alloc]init];
    item2.title = NSLocalizedString(@"cl_game", "");
    

    corkObj.tabBarItem = item;
    clothesObj.tabBarItem = item2;
    plasticObj.tabBarItem = item3;
    
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                       [UIColor blackColor], NSForegroundColorAttributeName,
                                                       [UIFont fontWithName:@"SinkinSans-600SemiBold" size:15],
                                                       NSFontAttributeName,
                                                       nil] forState:UIControlStateNormal];
    
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;

    if (screenHeight <= 736) {
        // Adjust title positions for smaller screens
        [item setTitlePositionAdjustment:UIOffsetMake(0, -15)];
        [item2 setTitlePositionAdjustment:UIOffsetMake(0, -15)];
        [item3 setTitlePositionAdjustment:UIOffsetMake(0, -15)];
    }

    
    self.viewControllers = @[plasticObj,corkObj,clothesObj];
    
}

@end
