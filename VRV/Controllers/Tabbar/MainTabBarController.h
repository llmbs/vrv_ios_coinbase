//
//  MainTabBarController.h
//  VRV
//
//  Created by My Mac on 01/09/21.
//  Copyright © 2021 Abhishek Sheth. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MainTabBarController : UITabBarController

-(void)setupTabBar;

@end

NS_ASSUME_NONNULL_END
