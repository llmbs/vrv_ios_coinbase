

#import <UIKit/UIKit.h>

@interface OTPSendView : UIViewController<UITextFieldDelegate,UIGestureRecognizerDelegate>
{
    __weak IBOutlet UIView *SecurityCodeBackView;
    
    __weak IBOutlet UITextField *SecurityCodeTextField;
    
    __weak IBOutlet UIButton *CancleButton;
    __weak IBOutlet UIButton *SubmitButton;
    __weak IBOutlet UILabel *lbltopMessage;
    __weak IBOutlet UILabel *DeleteMessage;
    __weak IBOutlet UILabel *EmailMessage;
    __weak IBOutlet UILabel *MobileMessage;
    
    
    __weak IBOutlet UITextField *EmailtextField;
    __weak IBOutlet UITextField *mobiletextField;
    
    
    
    UITextField *currenttextField;
    
    __weak IBOutlet UIImageView *PadLock;
    __weak IBOutlet UIImageView *VrvIcon;
    
    __weak IBOutlet UILabel *title;
    
    
 
    
  
    
}
- (IBAction)CancleButtonClick:(id)sender;
- (IBAction)SubmitButtonClick:(id)sender;
-(IBAction)MenuButtonclick:(id)sender;
  @property(assign)BOOL isDeleteuser;
  @property(assign)BOOL isEmail;
  @property(assign)BOOL isMobile;
  @property(assign)BOOL isAccessBusinesses;

  


@end
