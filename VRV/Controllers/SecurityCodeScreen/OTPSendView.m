#import "OTPSendView.h"
#import "newBusinessView.h"
#import "QuartzCore/QuartzCore.h"
#import "IQToolbar.h"
#import "IQKeyboardManager.h"
#import "CommanFunction.h"
#import "LoginView.h"
#import "Reachability.h"
#import "AFNetworking.h"
#import "Defaults.h"
#import "CommanFunction.h"
#import "Reachability.h"
#import "SettingViewController.h"
#import "LeftRightViewController.h"
#import <UIKit/UIKit.h>
#import "AboutUsView.h"
#import "VRV-Swift.h"


@interface OTPSendView ()
{
    NSUserDefaults *dlf;
}
@end

@implementation OTPSendView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    title.text = NSLocalizedString(@"app_title", "");
    
    
    lbltopMessage.text = NSLocalizedString(@"business_access_title", "");
    
    DeleteMessage.text = NSLocalizedString(@"delete_account", "");
    
    EmailMessage.text = NSLocalizedString(@"change_email_settings", "");
    
    MobileMessage.text = NSLocalizedString(@"change_mobile_settings", "");
    
    
    
    
    SecurityCodeTextField.placeholder = NSLocalizedString(@"business_access_password", "");
    
    [CancleButton setTitle:NSLocalizedString(@"txt_cancel", "") forState:UIControlStateNormal];
    
    [SubmitButton setTitle:NSLocalizedString(@"business_access_submit", "") forState:UIControlStateNormal];
    
    
    [[UIButton appearance] setExclusiveTouch:YES];
    
//    UISwipeGestureRecognizer *gestureRecognizerRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandlerRight:)];
//    [gestureRecognizerRight setDirection:(UISwipeGestureRecognizerDirectionRight)];
//    [self.view addGestureRecognizer:gestureRecognizerRight];
//

   

   
    dlf = [NSUserDefaults standardUserDefaults];
    
    PadLock.hidden = YES;
    CancleButton.layer.cornerRadius = 13.0f;
    CancleButton.clipsToBounds = YES;
    
    SubmitButton.layer.cornerRadius = 13.0f;
    SubmitButton.clipsToBounds = YES;
    
    SecurityCodeBackView.layer.cornerRadius = 13.0f;
    
//    SecurityCodeBackView.layer.borderWidth = 3.0;
//    SecurityCodeBackView.layer.borderColor = [UIColor colorWithRed:170.0/255.0 green:126.0/255.0 blue:76.0/255.0 alpha:1.0].CGColor;

//
//    CancleButton.layer.borderWidth = 3.0;
//    CancleButton.layer.borderColor = [UIColor colorWithRed:170.0/255.0 green:126.0/255.0 blue:76.0/255.0 alpha:1.0].CGColor;
//
//
//    SubmitButton.layer.borderWidth = 3.0;
//    SubmitButton.layer.borderColor = [UIColor colorWithRed:170.0/255.0 green:126.0/255.0 blue:76.0/255.0 alpha:1.0].CGColor;
    
    DeleteMessage.hidden = YES;
    EmailMessage.hidden = YES;
    MobileMessage.hidden = YES;
    
    
    
    if (_isDeleteuser) {
        
        
        
        DeleteMessage.hidden = NO;
        lbltopMessage.hidden = YES;
        EmailMessage.hidden = YES;
        MobileMessage.hidden = YES;
        
        
        
    }
    
    
    else if ( _isEmail)  {
        
        PadLock.hidden = NO;
        VrvIcon.hidden = YES;
        EmailMessage.hidden = NO;
        lbltopMessage.hidden = YES;
        DeleteMessage.hidden = YES;
        MobileMessage.hidden = YES;
        
      
    }
  
    else if (_isMobile)  {
        
        PadLock.hidden = NO;
        VrvIcon.hidden = YES;
        
        MobileMessage.hidden = NO;
        lbltopMessage.hidden = YES;
        DeleteMessage.hidden = YES;
        EmailMessage.hidden = YES;
    
     
    }
    
    
    SecurityCodeTextField.delegate = self;
    SecurityCodeTextField.returnKeyType = UIReturnKeyDone;
    
    [SubmitButton setBackgroundImage:[CommanFunction imageWithColor:[UIColor grayColor]] forState:UIControlStateHighlighted];
    [CancleButton setBackgroundImage:[CommanFunction imageWithColor:[UIColor grayColor]] forState:UIControlStateHighlighted];
    
    
    [SecurityCodeTextField becomeFirstResponder];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark VOID METHOD.


//-(void)swipeHandlerRight:(id)sender
//{
//
//    if (_isDeleteuser || _isEmail || _isMobile) {
//
//        SettingViewController *SettingViewScreenobj = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingViewController"];
//        UIViewController *newVC = SettingViewScreenobj;
//        NSMutableArray *vcs =  [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
//        [vcs insertObject:newVC atIndex:vcs.count-1];
//        [self.navigationController setViewControllers:vcs animated:YES];
//        [self.navigationController popViewControllerAnimated:YES];
//
//    }else{
//
//        AboutUsView *AboutUsScreenObj = [self.storyboard instantiateViewControllerWithIdentifier:@"AboutUsViewId"];
//        UIViewController *newVC = AboutUsScreenObj;
//        NSMutableArray *vcs =  [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
//        [vcs insertObject:newVC atIndex:vcs.count-1];
//        [self.navigationController setViewControllers:vcs animated:YES];
//        [self.navigationController popViewControllerAnimated:YES];
//
//
//
//
//    }
//
//
//}

- (void)showAlertApiSuccess:(NSString *)message {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"dialog_ok", "")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {

                                   SettingViewController *SettingScreenObj = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingViewController"];
                                   UIViewController *newVC = SettingScreenObj;
                                   NSMutableArray *vcs =  [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
                                   [vcs insertObject:newVC atIndex:vcs.count-1];
                                   [self.navigationController setViewControllers:vcs animated:YES];
                                   [self.navigationController popViewControllerAnimated:YES];
                                   
                                   
                               }];
    [alert addAction:okButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}


- (void)showAlert:(NSString *)message {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"dialog_ok", "")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   [self->SecurityCodeTextField becomeFirstResponder];
                                   
                               }];
    [alert addAction:okButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)showAlertDelete:(NSString *)message {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"dialog_ok", "")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                  
                                   LoginView *LoginScreenObj = [self.storyboard instantiateViewControllerWithIdentifier:@"loginid"];
                                   [self.navigationController pushViewController:LoginScreenObj animated:YES];
                                   
                               }];
    [alert addAction:okButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}


-(void)RoundCornerForUIViewAndShadowEffect:(UIView *)MyView{
    
    (MyView.layer).cornerRadius = 10.0f;
    (MyView.layer).shadowColor = [UIColor blackColor].CGColor;
    (MyView.layer).shadowOpacity = 0.8;
    (MyView.layer).shadowRadius = 3.0;
    (MyView.layer).shadowOffset = CGSizeMake(2.0, 2.0);
}

- (IBAction)MenuButtonclick:(id)sender {
    
    LeftRightViewController *LeftrightviewScreenObject = [self.storyboard instantiateViewControllerWithIdentifier:@"leftright"];
    UIViewController *newVC = LeftrightviewScreenObject;
    NSMutableArray *vcs =  [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
    [vcs insertObject:newVC atIndex:vcs.count-1];
    [self.navigationController setViewControllers:vcs animated:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)CancleButtonClick:(id)sender {
    
    
    if (_isDeleteuser || _isEmail || _isMobile) {
        
       
        SettingViewController *SettingViewScreenobj = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingViewController"];
        UIViewController *newVC = SettingViewScreenobj;
        NSMutableArray *vcs =  [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
        [vcs insertObject:newVC atIndex:vcs.count-1];
        [self.navigationController setViewControllers:vcs animated:YES];
        [self.navigationController popViewControllerAnimated:YES];
        
        
    }else{
        
        AboutUsView *AboutUsScreenObj = [self.storyboard instantiateViewControllerWithIdentifier:@"AboutUsViewId"];
        UIViewController *newVC = AboutUsScreenObj;
        NSMutableArray *vcs =  [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
        [vcs insertObject:newVC atIndex:vcs.count-1];
        [self.navigationController setViewControllers:vcs animated:YES];
        [self.navigationController popViewControllerAnimated:YES];
        
        
        
    }
    }

- (IBAction)SubmitButtonClick:(id)sender {
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus status =[reachability currentReachabilityStatus];
    if (status == NotReachable) {
        
        [self showAlert:NSLocalizedString(@"internetActivity_off", "")];
        
        
    }
    
    
   else if (SecurityCodeTextField.text.length == 0) {
        
        [self showAlert:NSLocalizedString(@"error_password_incorrect", "")];
        
    }

    
     else if (_isMobile)  {
        
        
        
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:NSLocalizedString(@"new_mobile", "")
                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* submit = [UIAlertAction actionWithTitle:NSLocalizedString(@"business_access_submit", "") style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action) {
                                                           
                                                           
                                                         
                                                           
                                                           if (self->mobiletextField.text.length < 1 || self->mobiletextField.text.length > 25 ) {
                                                               
                                                               self->mobiletextField.text = @"";
                                                               
                                                               UIAlertController * alert=   [UIAlertController
                                                                                             alertControllerWithTitle:@""
                                                                                             message:NSLocalizedString(@"error_enter_valid_phone", "")
                                                                                             preferredStyle:UIAlertControllerStyleAlert];
                                                               
                                                               UIAlertAction* okButton = [UIAlertAction
                                                                                          actionWithTitle:NSLocalizedString(@"dialog_ok", "")
                                                                                          style:UIAlertActionStyleDefault
                                                                                          handler:^(UIAlertAction * action)
                                                                                          {
                                                                                              
                                                                                              self->SecurityCodeTextField.text = @"";
                                                                                            //  [self->SecurityCodeTextField becomeFirstResponder];
                                                                                              
                                                                                              
                                                                                            
                                                                                          }];
                                                               [alert addAction:okButton];
                                                               [self presentViewController:alert animated:YES completion:nil];
                                                               
                                                           } else {
                                                               
                                                               
                                                               [self updateUserMobile:self->mobiletextField.text passwod:self->SecurityCodeTextField.text];
                                                           }
                                                           
                                                           
                                                       }];
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"txt_cancel", "") style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action) {
                                                           [alert dismissViewControllerAnimated:YES completion:nil];
                                                       }];
        
        [alert addAction:submit];
        [alert addAction:cancel];
        
        [alert addTextFieldWithConfigurationHandler:^(UITextField *textFieldM) {
            
            self->mobiletextField = textFieldM;
            self->mobiletextField.delegate = self;
            textFieldM.placeholder = NSLocalizedString(@"new_mobile", "");
            textFieldM.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
            textFieldM.textContentType = UITextContentTypeTelephoneNumber;
            
        }];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        
        
    }
    
   
    else if (_isAccessBusinesses){
        
        [self showProgressView];
        [self.view setUserInteractionEnabled: NO];
         [self performSelector:@selector(enableUserinteraction:) withObject:self afterDelay:0.2 ];
        [self->SecurityCodeTextField resignFirstResponder];

        
        [AuthenticationService loginWith:ConfigurationManager.shared.currentUser.email password:SecurityCodeTextField.text success:^(UserDetails * _Nonnull user) {
            [self hideProgressView];
           
            
            newBusinessView *NewBusViewScreenobj = [self.storyboard instantiateViewControllerWithIdentifier:@"newbusinessID"];
            [self.navigationController pushViewController:NewBusViewScreenobj animated:YES];
        } failure:^(NSError * _Nullable error) {
            
           // [self handleError:error];
            [self showAlert:error.localizedDescription];
            [self hideProgressView];
            
        }];
        
    }
    

    
   else if (_isDeleteuser) {
       
       [self showProgressView];
       [self.view setUserInteractionEnabled: NO];
       [self performSelector:@selector(enableUserinteraction:) withObject:self afterDelay:0.2 ];
       
        [self->SecurityCodeTextField resignFirstResponder];
       
       [UserService deleteUserWithEmail:ConfigurationManager.shared.currentUser.email password:SecurityCodeTextField.text success:^(BOOL success) {
           [self hideProgressView];
           
           [ConfigurationManager shared].currentUser = nil;
           
           [self showAlertDelete:NSLocalizedString(@"account_delete", "")];
           
           
           
       } failure:^(NSError * _Nullable error) {
           
          // [self handleError:error];
           
          
           
           [self showAlert:NSLocalizedString(@"error_password_incorrect", "") ];
           [self hideProgressView];
        
       }];
    }
    
    
    
   else if  (_isEmail) {
        
        

        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:NSLocalizedString(@"new_email", "")
                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* submit = [UIAlertAction actionWithTitle:NSLocalizedString(@"business_access_submit", "") style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action) {
                                                           
                                                          //need email validation format
                                                           
                                                           if ([self IsValidEmail:self->EmailtextField.text] == FALSE) {
                                                               
                                                               UIAlertController * alert=   [UIAlertController
                                                                                             alertControllerWithTitle:@""
                                                                                             message:NSLocalizedString(@"error_enter_valid_email", "")
                                                                                             preferredStyle:UIAlertControllerStyleAlert];
                                                               
                                                               UIAlertAction* okButton = [UIAlertAction
                                                                                          actionWithTitle:NSLocalizedString(@"dialog_ok", "")
                                                                                          style:UIAlertActionStyleDefault
                                                                                          handler:^(UIAlertAction * action)
                                                                                          
                                                                                          {
                                                                                              self->SecurityCodeTextField.text = @"";
                                                                                            //  [self->SecurityCodeTextField becomeFirstResponder];
                                                                                              
                                                                                              
                                                                                          }];
                                                               [alert addAction:okButton];
                                                               
                                                               [self presentViewController:alert animated:YES completion:nil];
                                                               
                                                           } else {
                                                               [self updateUserEmail:self->EmailtextField.text passwod:self->SecurityCodeTextField.text];
                                                           }
                                                           
                                                           
                                                       }];
                                                            UIAlertAction* cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"txt_cancel", "") style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction * action) {
                                                           [alert dismissViewControllerAnimated:YES completion:nil];
                                                       }];
        
        [alert addAction:submit];
        [alert addAction:cancel];
        
        [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            
            self->EmailtextField = textField;
            textField.placeholder = NSLocalizedString(@"new_email", "");
            self->EmailtextField.textContentType = UITextContentTypeEmailAddress;
            
        }];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        
        
    }
    
    

}

-(void)enableUserinteraction:(id)sender {
    
    [self.view setUserInteractionEnabled:YES];
    
}

- (void)updateUserMobile:(NSString *)mobile passwod:(NSString *)password {
    [self->SecurityCodeTextField resignFirstResponder];
    [self showProgressView];
     [self.view setUserInteractionEnabled: NO];
    [self performSelector:@selector(enableUserinteraction:) withObject:self afterDelay:0.2 ];
    
    
    [UserService updateUserMobileWithMobile:mobile password:password success:^(NSInteger StatusCode) {
       
        
        if (StatusCode == 200) {
            
            [self hideProgressView];
            [self showAlertApiSuccess:NSLocalizedString(@"updateEmail_update", "")];
            
        } else if (StatusCode == 400) {
            
            
            [self hideProgressView];
            [self showAlertApiSuccess:NSLocalizedString(@"updateEmail_fail", "")];
        
        } else  {
            
            [self hideProgressView];
            [self showAlertApiSuccess:NSLocalizedString(@"updateEmail_fail", "")];
        }
        
        
        
    } failure:^(NSError * _Nullable error) {
        
        
        [self hideProgressView];
        [self showAlertApiSuccess:NSLocalizedString(@"updateEmail_fail", "")];
        self->SecurityCodeTextField.text = @"";

    }];
}

- (void)updateUserEmail:(NSString *)email passwod:(NSString *)password {
    [self->SecurityCodeTextField resignFirstResponder];
    [self showProgressView];
     [self.view setUserInteractionEnabled: NO];
   [self performSelector:@selector(enableUserinteraction:) withObject:self afterDelay:0.2 ];
    
    [UserService updateUserEmailWithEmail:email password:password success:^(NSInteger StatusCode) {
       
        if (StatusCode == 200) {
            
            [self hideProgressView];
            [self showAlertApiSuccess:NSLocalizedString(@"updateEmail_update", "")];
            
        } else if (StatusCode == 400) {
            
            
            [self hideProgressView];
            [self showAlertApiSuccess:NSLocalizedString(@"updateEmail_fail", "")];
        
        } else  {
            
            [self hideProgressView];
            [self showAlertApiSuccess:NSLocalizedString(@"updateEmail_fail", "")];
        }
        
        
        
    } failure:^(NSError * _Nullable error) {
        
        [self hideProgressView];
        [self showAlertApiSuccess:NSLocalizedString(@"updateEmail_fail", "")];
        self->SecurityCodeTextField.text = @"";
      
       
    }];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if ([string isEqualToString:@" "] && textField.text.length == 0) {
        return NO;
    }
    
//    if (textField ==  mobiletextField) {
//        int length = (int)[self getLength:textField.text];
//
//        if (([string rangeOfCharacterFromSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]].location != NSNotFound) && !(range.length==1 && string.length==0)) {
//            return NO;
//        }
//
//        if ([string isEqualToString:[UIPasteboard generalPasteboard].string]) {
//
//            return NO;
//
//        }
//
//        if(length == 10)
//        {
//            if(range.length == 0)
//            return NO;
//        }
//
//        if(length == 3)
//        {
//            NSString *num = [self formatNumber:textField.text];
//            textField.text = [NSString stringWithFormat:@"%@-",num];
//
//            if(range.length > 0)
//            textField.text = [NSString stringWithFormat:@"%@",[num substringToIndex:3]];
//        }
//        else if(length == 6)
//        {
//            NSString *num = [self formatNumber:textField.text];
//
//            textField.text = [NSString stringWithFormat:@"%@-%@-",[num  substringToIndex:3],[num substringFromIndex:3]];
//
//            if(range.length > 0)
//            textField.text = [NSString stringWithFormat:@"%@-%@",[num substringToIndex:3],[num substringFromIndex:3]];
//        }
//
//    }
    
    return YES;
}

//- (NSString *)formatNumber:(NSString *)mobileNumber
//{
//    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
//    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
//    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
//    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
//    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
//
//    NSLog(@"%@", mobileNumber);
//
//    int length = (int)mobileNumber.length;
//    if(length > 10)
//    {
//        mobileNumber = [mobileNumber substringFromIndex: length-10];
//        NSLog(@"%@", mobileNumber);
//
//    }
//
//
//    return mobileNumber;
//}
//
//- (int)getLength:(NSString *)mobileNumber {
//    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
//    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
//    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
//    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
//    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
//
//    int length = (int)mobileNumber.length;
//
//    return length;
//}

-(BOOL)IsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];

}


    @end

