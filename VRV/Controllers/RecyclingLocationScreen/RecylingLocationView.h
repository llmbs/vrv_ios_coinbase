

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import <CoreLocation/CoreLocation.h>
#import "BusinessCustomecell.h"




@interface RecylingLocationView : UIViewController<GMSMapViewDelegate,CAAnimationDelegate,UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate>
{
   
    __weak IBOutlet UIButton *WineButton;
    __weak IBOutlet UIButton *ClothesButton;
    __weak IBOutlet UIButton *MenuCircle;
    __weak IBOutlet UIButton *PlasticButton;
    
    __weak IBOutlet UILabel *recycling_locations;
    
    CLPlacemark *placemark;
    
    __weak IBOutlet UIButton *greybtnPlastic;
    __weak IBOutlet UIButton *greybtnInfo;
    __weak IBOutlet UIButton *greybtnWine;
    __weak IBOutlet UIButton *greybtnClothes;
    __weak IBOutlet UITableView *tableView;
    __weak IBOutlet UILabel *countdownLabel;
    NSTimer *countdownTimer;
    int secondsCount;
    
}

- (IBAction)MenuButtonClick:(id)sender;
- (IBAction)SearchButtonClick:(id)sender;
- (IBAction)WineButtonClick:(id)sender;
- (IBAction)ClothesButtonClick:(id)sender;
- (IBAction)NotAcceptingButtonClick:(id)sender;
- (IBAction)GoogleButtonClick:(id)sender;
- (IBAction)hourGlassButtonClick:(id)sender;
- (IBAction)PlasticButtonClick:(id)sender;

@end
