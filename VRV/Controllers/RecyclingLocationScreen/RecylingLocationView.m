

#import "RecylingLocationView.h"
#import <GoogleMaps/GoogleMaps.h>
#import "Reachability.h"
#import "AFNetworking.h"
#import "Defaults.h"
#import "LeftRightViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "VRV-Swift.h"
#import "ReadytoRecycleView.h"
#import "INTULocationManager.h"
#import "MainTabBarController.h"

@interface RecylingLocationView () <BusinessCustomeCellDelegate> {
    NSMutableArray<BusinessData *> *data;
    NSString *userCurrentCity;
    NSString *selectedType;
    CLLocation *lastLocation;
    NSInteger locationRequestId;
}

@end

@implementation RecylingLocationView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    data = [NSMutableArray new];
    
    UISwipeGestureRecognizer *gestureRecognizerRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandlerRight:)];
    [gestureRecognizerRight setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [self.view addGestureRecognizer:gestureRecognizerRight];
    
    
    greybtnPlastic.layer.cornerRadius=10;
    greybtnInfo.layer.cornerRadius=10;
    greybtnWine.layer.cornerRadius=10;
    greybtnClothes.layer.cornerRadius=10;
    
    
    
    
    
    /*/Storyboard localization*/
    
    recycling_locations.text = NSLocalizedString(@"recycling_locations", "");
    
    
    [greybtnPlastic setTitle:NSLocalizedString(@"plastic_tab", "") forState:(UIControlStateNormal)];
    
    [greybtnWine setTitle:NSLocalizedString(@"txtCorks", "") forState:(UIControlStateNormal)];
    
    [greybtnClothes setTitle:NSLocalizedString(@"txtClothes", "") forState:(UIControlStateNormal)];
    
    
    
    
    [self->tableView registerNib:[BusinessCustomeCell Nib] forCellReuseIdentifier:@"BusinessCustomeCell"];
    
    //abhi & sha default location manager
    
//    [self showProgressView];
//    [self.view setUserInteractionEnabled: NO];
//
//    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
//    self->locationRequestId = [locMgr requestLocationWithDesiredAccuracy:INTULocationAccuracyBlock
//                                       timeout:5.0
//                          delayUntilAuthorized:YES    // This parameter is optional, defaults to NO if omitted
//                                         block:^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
//                                             if (status == INTULocationStatusSuccess) {
//                                                 // Request succeeded, meaning achievedAccuracy is at least the requested accuracy, and
//                                                 // currentLocation contains the device's current location.
//                                                 [self hideProgressView];
//                                                 [self showAlertGpsUpdated:@"Location successfully updated."];
//                                                 self->lastLocation = currentLocation;
//
//
//                                             }
//                                             else if (status == INTULocationStatusTimedOut) {
//                                                 // Wasn't able to locate the user with the requested accuracy within the timeout interval.
//                                                 // However, currentLocation contains the best location available (if any) as of right now,
//                                                 // and achievedAccuracy has info on the accuracy/recency of the location in currentLocation.
//                                                 [self hideProgressView];
//                                                 self->lastLocation = currentLocation;
//                                                 [self enableUserinteraction:self];
//
//                                             }
//                                             else {
//                                                 // An error occurred, more info is available by looking at the specific status returned.
//                                                 [self hideProgressView];
//                                                 [self showAlertTutorial:@""];
//                                                 [self enableUserinteraction:self];
//                                             }
//                                         }];
    
    [[UIButton appearance] setExclusiveTouch:YES];
    countdownLabel.hidden = YES;
    
    
}
    
-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    [locMgr cancelLocationRequest:self->locationRequestId];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark VOID METHOD.

-(void)timerRun{
    countdownLabel.hidden = NO;
    
    secondsCount = secondsCount - 1;
    int minuts = secondsCount / 60;
    int seconds = secondsCount - (minuts * 60);
    
    NSString *timerOutput = [NSString stringWithFormat:@"%2d", seconds];
    countdownLabel.text = timerOutput;
    
    if (seconds == 0) {
    
        [countdownTimer invalidate];
        countdownTimer = nil;
        countdownLabel.hidden = YES;
    }
}

-(void)setTimer {
    
    secondsCount = 5;
    countdownTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerRun) userInfo:nil repeats:YES];
    
}

-(void)getGpsCoordinates {
    
    [self showProgressView];
    [self setTimer];
    
    
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    self->locationRequestId = [locMgr requestLocationWithDesiredAccuracy:INTULocationAccuracyBlock
                                                                 timeout:5.0
                                                    delayUntilAuthorized:YES    // This parameter is optional, defaults to NO if omitted
                                                                   block:^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
                                                                       if (status == INTULocationStatusSuccess) {
                                                                           // Request succeeded, meaning achievedAccuracy is at least the requested accuracy, and
                                                                           // currentLocation contains the device's current location.
                                                                           [self hideProgressView];
                                                                           self->lastLocation = currentLocation;
                                                                           [self getData:self->selectedType];
                                                                           [self->countdownTimer invalidate];
                                                                           self->countdownTimer = nil;
                                                                           self->countdownLabel.hidden = YES;
                                                                           
                                                                           
                                                                       }
                                                                       else if (status == INTULocationStatusTimedOut) {
                                                                           // Wasn't able to locate the user with the requested accuracy within the timeout interval.
                                                                           // However, currentLocation contains the best location available (if any) as of right now,
                                                                           // and achievedAccuracy has info on the accuracy/recency of the location in currentLocation.
                                                                           [self hideProgressView];
                                                                           self->lastLocation = currentLocation;
                                                                           [self getData:self->selectedType];
                                                                           [self->countdownTimer invalidate];
                                                                           self->countdownTimer = nil;
                                                                           self->countdownLabel.hidden = YES;
                                                                          
                                                                           
                                                                       }
                                                                       else {
                                                                           // An error occurred, more info is available by looking at the specific status returned.
                                                                           [self hideProgressView];
                                                                           [self showAlertTutorial:@""];
                                                                           [self->countdownTimer invalidate];
                                                                           self->countdownTimer = nil;
                                                                           self->countdownLabel.hidden = YES;
                                                                           
                                                                       }
                                                                   }];
}


- (void)showAlertGpsUpdated:(NSString *)message {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"dialog_ok", "")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   [self enableUserinteraction:self];
                                   
                               }];
    [alert addAction:okButton];
    
    
    
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

-(void)enableUserinteraction:(id)sender {
    
    [self.view setUserInteractionEnabled:YES];
    
}


-(void)swipeHandlerRight:(id)sender {
    
    LeftRightViewController *LeftrightviewScreenObject = [self.storyboard instantiateViewControllerWithIdentifier:@"leftright"];
    UIViewController *newVC = LeftrightviewScreenObject;
    NSMutableArray *vcs =  [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
    [vcs insertObject:newVC atIndex:vcs.count-1];
    [self.navigationController setViewControllers:vcs animated:YES];
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)showAlertTutorial:(NSString *)message {
    
UIAlertController * alert=   [UIAlertController
                              alertControllerWithTitle:NSLocalizedString(@"location_access_title", "")
                              
                              message:NSLocalizedString(@"location_access_para", "")
                              
                              
                              preferredStyle:UIAlertControllerStyleAlert];

UIAlertAction* tutorialButton = [UIAlertAction
                                 actionWithTitle:NSLocalizedString(@"recycling_location_tutorial", "")
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     
                                     [self MenuButtonClick:self];
                                     
                                     NSURL *recloc = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/apple_location_access"];
                                     
                                     [[UIApplication sharedApplication] openURL: recloc options:@{} completionHandler:^(BOOL success) {
                                         if (success)
                                             NSLog(@"Opened tutorial");
                                         
                                     }];
                                     
                                     
                                 }];


UIAlertAction* okButton = [UIAlertAction
                           actionWithTitle:NSLocalizedString(@"recycling_location_settings", "")
                           style:UIAlertActionStyleDefault
                           handler:^(UIAlertAction * action)
                           {
                               [self MenuButtonClick:self];
                               
                               NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                               if (url != nil) {
                                   [[UIApplication sharedApplication] openURL:url options:[NSDictionary new] completionHandler:nil];
                               }
                               
                               
                           }];
    
    
    UIAlertAction* resetButton = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"reset_network", "")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   [self MenuButtonClick:self];
                                   
                                   NSURL *resetTutorial = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/reset_network"];
                                   
                                   [[UIApplication sharedApplication] openURL: resetTutorial options:@{} completionHandler:^(BOOL success) {
                                       if (success)
                                           NSLog(@"Opened reset tutorial");
                                       
                                   }];
                                   
                                   
                               }];


UIAlertAction*  cancelButton = [UIAlertAction
                                actionWithTitle:NSLocalizedString(@"dialog_ok", "")
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                    [self MenuButtonClick:self];
                                    
                                }];

[alert addAction:okButton];
[alert addAction:tutorialButton];
[alert addAction:resetButton];
[alert addAction:cancelButton];
[self presentViewController:alert animated:YES completion:nil];
  
}


- (void)showAlert:(NSString *)message {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"dialog_ok", "")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   
                               }];
    [alert addAction:okButton];
    
    
    
    
    [self presentViewController:alert animated:YES completion:nil];
    
}
- (void)showAlertInfo:(NSString *)message {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:NSLocalizedString(@"recycling_locations", "")
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction* recycleButton = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"ready_to_recycle", "")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
        
        [self hourGlassButtonClick:self];
                                       
                                   }];
    
    
    UIAlertAction* gpsButton = [UIAlertAction
                                actionWithTitle:NSLocalizedString(@"gps_issue", "")
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                    
                                    NSURL *instantad = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/country_page"];
                                    
                                    [[UIApplication sharedApplication] openURL: instantad options:@{} completionHandler:^(BOOL success) {
                                        if (success)
                                            NSLog(@"Opened gps_issues.pdf");
                                        
                                    }];
                                }];
                                
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"dialog_ok", "")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   
                               }];
    
    UIAlertAction* safetyButton = [UIAlertAction
                              actionWithTitle:NSLocalizedString(@"recycling_location_safety", "")
                              style:UIAlertActionStyleDefault
                              handler:^(UIAlertAction * action)
                              {
                                  
                                  NSURL *instantad = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/country_page"];
                                  
                                  [[UIApplication sharedApplication] openURL: instantad options:@{} completionHandler:^(BOOL success) {
                                      if (success)
                                          NSLog(@"Opened safety.pdf");
                                      
                                  }];
                                  
                                  
                                  
                              }];
    
    [alert addAction:recycleButton];
    [alert addAction:safetyButton];
    [alert addAction:gpsButton];
    [alert addAction:okButton];
    
    
    
    
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark IBAction Methods.

- (IBAction)MenuButtonClick:(id)sender {
    
    
    
    LeftRightViewController *LeftrightviewScreenObject = [self.storyboard instantiateViewControllerWithIdentifier:@"leftright"];
    UIViewController *newVC = LeftrightviewScreenObject;
    NSMutableArray *vcs =  [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
    [vcs insertObject:newVC atIndex:vcs.count-1];
    [self.navigationController setViewControllers:vcs animated:YES];
    [self.navigationController popViewControllerAnimated:YES];
    
    
}

- (IBAction)hourGlassButtonClick:(id)sender {
    
//    ReadytoRecycleView *ReadytoRecycleScreenObj = [self.storyboard instantiateViewControllerWithIdentifier:@"ReadytorecycleID"];
//    [self.navigationController pushViewController:ReadytoRecycleScreenObj animated:YES];
    
    MainTabBarController *tabController = [[MainTabBarController alloc]init];
    [tabController setupTabBar];
    [self.navigationController pushViewController:tabController animated:YES];
    
    
}


- (IBAction)SearchButtonClick:(id)sender {
    
    // pdf url
    
//    NSURL *recloc = [NSURL URLWithString:@"https://voluntaryrefundvalue.com/admin/Vrv_pdfs/Recycling_locations.pdf"];
//
//    [[UIApplication sharedApplication] openURL: recloc options:@{} completionHandler:^(BOOL success) {
//        if (success)
//            NSLog(@"Opened recloc");
//
//    }];
    
    
    // Uialert
    
    [self showAlertInfo:NSLocalizedString(@"recycling_locations_info", "")];
    
}

- (IBAction)PlasticButtonClick:(id)sender {
    selectedType = @"plastic";
    [self.view setUserInteractionEnabled: NO];
    [self performSelector:@selector(enableUserinteraction:) withObject:self afterDelay:0.2 ];
    
    [WineButton setImage:[UIImage imageNamed:@"wine"] forState:UIControlStateNormal];
    [ClothesButton setImage:[UIImage imageNamed:@"clothes"] forState:UIControlStateNormal];
    [PlasticButton setImage:[UIImage imageNamed:@"plastic_active"]   forState:UIControlStateNormal];
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    if (remoteHostStatus == NotReachable)
    {
        [self showAlert:NSLocalizedString(@"internetActivity_off", "")];
        
    }
    
    else  {
        
        [self getGpsCoordinates];
    }
        

}



- (IBAction)WineButtonClick:(id)sender {
    selectedType = @"cork";
    [self.view setUserInteractionEnabled: NO];
    [self performSelector:@selector(enableUserinteraction:) withObject:self afterDelay:0.2 ];
    
    [WineButton setImage:[UIImage imageNamed:@"wine_active"] forState:UIControlStateNormal];
    [ClothesButton setImage:[UIImage imageNamed:@"clothes"] forState:UIControlStateNormal];
    [PlasticButton setImage:[UIImage imageNamed:@"plastic_inactive"] forState:UIControlStateNormal];
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    if (remoteHostStatus == NotReachable)
    {
        [self showAlert:NSLocalizedString(@"internetActivity_off", "")];
        
    }
    
    else  {
        
        [self getGpsCoordinates];
    }
        

}


- (IBAction)ClothesButtonClick:(id)sender {
    selectedType = @"cloth";
    [self.view setUserInteractionEnabled: NO];
    [self performSelector:@selector(enableUserinteraction:) withObject:self afterDelay:0.2 ];
    
    [ClothesButton setImage:[UIImage imageNamed:@"clothes-active"] forState:UIControlStateNormal];
    [WineButton setImage:[UIImage imageNamed:@"wine"] forState:UIControlStateNormal];
    [PlasticButton setImage:[UIImage imageNamed:@"plastic_inactive"] forState:UIControlStateNormal];
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    if (remoteHostStatus == NotReachable)
    {
        [self showAlert:NSLocalizedString(@"internetActivity_off", "")];
        
    }
    
//    else if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied) {
//
//         [self showAlertTutorial:@""];
//
//    }
    
    else  {
        
        [self getGpsCoordinates];
    }
    
    
}
- (void)getData:(NSString *)key {
   
   
    [BusinessService getBusiness:key lat:lastLocation.coordinate.latitude lng:lastLocation.coordinate.longitude success:^(NSArray<BusinessData *> *_Nonnull data) {
        
        
        [self hideProgressView];
        self->data = [NSMutableArray arrayWithArray:data];
        [self->tableView reloadData];
        
        
        
        
        
    } failure:^(NSError * _Nullable error) {
        [self handleError:error];
        
       // [self showAlert:error.localizedDescription];
        [self hideProgressView];
       
        
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return data.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    BusinessData *business = data[indexPath.row];
    BusinessCustomeCell *cell = (BusinessCustomeCell *)[tableView dequeueReusableCellWithIdentifier:@"BusinessCustomeCell" forIndexPath:indexPath];
    cell.delegate = self;
    cell.tag = indexPath.row;
    [cell configureWithData:business];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    BusinessData *business = data[indexPath.row];
}

- (void)didTapOpenInMapFrom:(BusinessCustomeCell *)cell {
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    if (remoteHostStatus == NotReachable)
    {
        [self showAlert:NSLocalizedString(@"internetActivity_off", "")];
        
    } else {
    
    BusinessData *business = data[cell.tag];
    [self showProgressView];
    NSString *address = [NSString stringWithFormat:@"%@", business.address1];
    
    
    [LocationManager reverseGeoCodeWithAddress:address success:^(NSString * _Nonnull lat, NSString * _Nonnull longitude) {
        [self hideProgressView];
        double lt = [lat doubleValue];
        double lng = [longitude doubleValue];
        
       
        
        CLLocationCoordinate2D location = CLLocationCoordinate2DMake(lt, lng);
        [LocationManager openInGoogleMaps:location address:business.address1];
    } failure:^(NSError * _Nullable error) {
       // [self handleError:error];
        [self showAlert:error.localizedDescription];
    }];
}
}

- (void)didTapNotAcceptingFrom:(BusinessCustomeCell *)cell {
    BusinessData *business = data[cell.tag];
    NSString *message = [NSString stringWithFormat:NSLocalizedString(@"green_not_accepting_para2", ""), business.company_name];
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:NSLocalizedString(@"green_not_accepting", "")
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:NSLocalizedString(@"green_yes", "")
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                    [self showProgressView];
                                    [self.view setUserInteractionEnabled: NO];
                                    [self performSelector:@selector(enableUserinteraction:) withObject:self afterDelay:0.2 ];
                                    
                                    
                                    [BusinessService reportBusiness:business.id type:self->selectedType success:^(BOOL success) {
                                        
                                        [self hideProgressView];
                                        
                                        [self showAlert:NSLocalizedString(@"not_accepting_api", "")];
                                        
                                       
                                    
                                    } failure:^(NSError * _Nullable error) {
                                        
                                        [self hideProgressView];
                                        [self showAlert:error.localizedDescription];
                                  
                                    }];
                                }];
    
    UIAlertAction* ignoreButton = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"green_ignore", "")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       
                                   }];
    
    
    [alert addAction:yesButton];
    [alert addAction:ignoreButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)didTapImproveFrom:(BusinessCustomeCell *)cell {
    
    BusinessData *business = data[cell.tag];
    NSString *message = [NSString stringWithFormat:NSLocalizedString(@"tid_accuracy_para3", ""), business.company_name];
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:NSLocalizedString(@"tid_accuracy2", "")
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ImproveButton = [UIAlertAction
                                actionWithTitle:NSLocalizedString(@"tid_improve", "")
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                    [self showProgressView];
                                    [self.view setUserInteractionEnabled: NO];
                                    [self performSelector:@selector(enableUserinteraction:) withObject:self afterDelay:0.2 ];
                                    
                                    NSString *lat = [NSString stringWithFormat:@"%f", self->lastLocation.coordinate.latitude];
                                    NSString *lng = [NSString stringWithFormat:@"%f", self->lastLocation.coordinate.longitude];
                                    
                                    [BusinessService improveCoordinatesForBusiness:business.id lat:lat lng:lng success:^(BOOL success) {
                                        
                                        
                                        
                                        [self hideProgressView];
                                        
                                        [self showAlert:NSLocalizedString(@"coordinate_api", "")];
                                        
                                        

                                        
                                        
                                    } failure:^(NSError * _Nullable error) {
                                        
                                        [self hideProgressView];
                                        [self showAlert:error.localizedDescription];
                                     
                                        
                                    }];
                                    
                                    
                                    
                                   
                                }];
    
    UIAlertAction* dismissButton = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"txt_dismiss", "")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       
                                   }];
    
    [alert addAction:dismissButton];
    [alert addAction:ImproveButton];
    
    
    [self presentViewController:alert animated:YES completion:nil];
    
    
}

@end
