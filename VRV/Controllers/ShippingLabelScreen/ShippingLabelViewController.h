

#import <UIKit/UIKit.h>

@interface ShippingLabelViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>



{
    
    __weak IBOutlet UIScrollView *MainScrollview;
  
    __weak IBOutlet UIView *MainView;
    __weak IBOutlet UIView *BrownFooter;
    
    __weak IBOutlet UITextField *SelectyourBusinessTextField;
    __weak IBOutlet UITextField *Address1TextField;
    __weak IBOutlet UITextField *Address2TextField;
    __weak IBOutlet UITextField *PhoneTextField;
    
    __weak IBOutlet UILabel *SelectBusinessHeader;
    
   // IBOutlet UITextField *CityTextField;
   // IBOutlet UITextField *StateTextField;
//IBOutlet UITextField *ZipcodeTextField;
   // IBOutlet UITextField *RecyclerTextField;
   // IBOutlet UITextField *CorkTypeTextField;
    __weak IBOutlet UITextField *weTextField;
    __weak IBOutlet UITextField *leTextField;
    __weak IBOutlet UITextField *wiTextField;
    __weak IBOutlet UITextField *heTextField;
    __weak IBOutlet UIButton *selectyourBusinessradius;
    __weak IBOutlet UIButton *submitradius;
    
   

    
    __weak IBOutlet UIView *BusinessListPopup;
    

}
@property (weak, nonatomic) IBOutlet UISegmentedControl *segout;
@property(strong,nonatomic) NSString *CorkTypeString;
@property(strong,nonatomic) NSString *RecyclerString;
@property(strong,nonatomic) NSMutableArray* BusinessListArray;
- (IBAction)SelectYourBusinessButtonClick:(id)sender;
- (IBAction)SubmitButtonClick:(id)sender;
- (IBAction)MenubtnClick:(id)sender;
- (IBAction)segact:(id)sender;
- (IBAction)BackbtnClick:(id)sender;



@end
