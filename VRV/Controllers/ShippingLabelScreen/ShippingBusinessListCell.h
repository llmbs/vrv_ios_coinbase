//
//  ShippingBusinessListCell.h
//  VRV
//
//  Created by Mac 02 on 30/06/17.
//  Copyright © 2017 vnnovate. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShippingBusinessListCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *textbackimageview;
@property (weak, nonatomic) IBOutlet UILabel *textlabel;
@property (weak, nonatomic) IBOutlet UILabel *namelabel;

@end
