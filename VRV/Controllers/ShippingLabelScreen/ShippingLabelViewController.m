

#import "ShippingLabelViewController.h"
#import "Reachability.h"
#import "CommanFunction.h"
#import "AFNetworking.h"
#import "ShippingBusinessListCell.h"
#import "AppDelegate.h"
#import "LeftRightViewController.h"
#import "ReadytoRecycleView.h"
#import "VRV-Swift.h"

@interface ShippingLabelViewController ()
{
    NSUserDefaults *dlf;
    NSString *BusinessIDString;
     AppDelegate *app;
}
@end

@implementation ShippingLabelViewController
@synthesize segout;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UISwipeGestureRecognizer *gestureRecognizerRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandlerRight:)];
    [gestureRecognizerRight setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [self.view addGestureRecognizer:gestureRecognizerRight];

    
    dlf = [NSUserDefaults standardUserDefaults];
    app =(AppDelegate *)[UIApplication sharedApplication].delegate;
    MainScrollview.contentSize = CGSizeMake(self.view.frame.size.width, MainView.frame.size.height);
    MainScrollview.backgroundColor = [UIColor whiteColor];
    selectyourBusinessradius.layer.cornerRadius = 4;
    submitradius.layer.cornerRadius = 4;
    
    BusinessListPopup.hidden = YES;
    SelectBusinessHeader.hidden = YES;
    BrownFooter.hidden = YES;
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark VOID METHOD.

-(void)swipeHandlerRight:(id)sender
{
    
    LeftRightViewController *LeftrightviewScreenObject = [self.storyboard instantiateViewControllerWithIdentifier:@"leftright"];
    UIViewController *newVC = LeftrightviewScreenObject;
    NSMutableArray *vcs =  [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
    [vcs insertObject:newVC atIndex:vcs.count-1];
    [self.navigationController setViewControllers:vcs animated:YES];
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)showAlert:(NSString *)message {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   
                               }];
    [alert addAction:okButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark IBACTION Method.


- (IBAction)SelectYourBusinessButtonClick:(id)sender {
    
    SelectBusinessHeader.hidden = NO;
    BusinessListPopup.hidden = NO;
    BrownFooter.hidden = NO;
    
    if (BusinessListPopup.hidden ==YES) {
        
        SelectBusinessHeader.hidden = YES;
        BrownFooter.hidden = YES;
    }
    
}

- (IBAction)SubmitButtonClick:(id)sender {
    
    
    
    
    int weightintvalue = (weTextField.text).doubleValue;
    int lengthofinches = (leTextField.text).doubleValue;
    int widthofinches = (wiTextField.text).doubleValue;
    int Heightofinches = (heTextField.text).doubleValue;
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    if (remoteHostStatus == NotReachable)
    {
        
        [self showAlert:@"Internet connection offline"];
        
    } else if (SelectyourBusinessTextField.text.length == 0) {
        
        [self SelectYourBusinessButtonClick:self];
        [self showAlert:@"Please select a business"];
        
    } else if (weTextField.text.length == 0) {
        
        
        [self->weTextField becomeFirstResponder];
        [self showAlert:@"Please input weight"];
        
       
    } else if (weightintvalue < 15) {
        
        
        [self->weTextField becomeFirstResponder];
        [self showAlert:@"Weight must be between 15-30 lbs"];
        
        
    } else if (weightintvalue > 30) {
        
        
        [self->weTextField becomeFirstResponder];
        [self showAlert:@"Weight must be between 15-30 lbs"];
        
        
    } else if (leTextField.text.length == 0) {
        
        
        [self->leTextField becomeFirstResponder];
        [self showAlert:@"Please input length"];
      
        
    }   else if (lengthofinches == 0) {
        
        
        [self->leTextField becomeFirstResponder];
        [self showAlert:@"Length must be greater than 0"];
       
        
    }   else if (wiTextField.text.length == 0) {
        
        
        [self->wiTextField becomeFirstResponder];
        [self showAlert:@"Please input width"];
        
        
    }   else if (widthofinches == 0) {
        
        
        [self->wiTextField becomeFirstResponder];
        [self showAlert:@"Width must be greater than 0"];
        
        
    } else if (heTextField.text.length == 0) {
        
        
        [self->heTextField becomeFirstResponder];
         [self showAlert:@"Please input height"];
        
        
    } else if (Heightofinches == 0) {
        
        
        [self->heTextField becomeFirstResponder];
        [self showAlert:@"Height must be greater than 0"];
        
        
    } else if (segout.selectedSegmentIndex == UISegmentedControlNoSegment) {
        
     
     [self showAlert:@"Select a recycler"];
        
    } else if (segout.selectedSegmentIndex == 0) {
        
       // [self showProgressView];
        
    // Send recork email with success and failure api , alert messages and loading indicators needed.
        
        
    } else if (segout.selectedSegmentIndex == 1) {
        
        // [self showProgressView];
        
        // send yemm and heart email with sucesss and failure api , alert messages and loading indicators needed.
        
    } else if (segout.selectedSegmentIndex == 2) {
        
        // [self showProgressView];
        
        // // send cork reharvest email with sucesss and failure api , alert messages and loading indicators needed.
        
    }
    
    
    
}

- (IBAction)MenubtnClick:(id)sender {
    
    LeftRightViewController *LeftrightviewScreenObject = [self.storyboard instantiateViewControllerWithIdentifier:@"leftright"];
    UIViewController *newVC = LeftrightviewScreenObject;
    NSMutableArray *vcs =  [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
    [vcs insertObject:newVC atIndex:vcs.count-1];
    [self.navigationController setViewControllers:vcs animated:YES];
    [self.navigationController popViewControllerAnimated:YES];
    
  
}

- (IBAction)segact:(id)sender {
    switch (self.segout.selectedSegmentIndex)
    {
        case 0:
            [self showAlert:@"You have selected Recork as your recycler"];
            break;
            
        case 1:
            [self showAlert:@"You have selected Yemm & Hart as your recycler"];
            break;
            
        case 2:
            [self showAlert:@"You have selected Cork Reharvest as your recycler"];
            break;
            
        default:
            break;
    }
    
}

- (IBAction)BackbtnClick:(id)sender {
    
    ReadytoRecycleView *ReadytoRecycleScreenObj = [self.storyboard instantiateViewControllerWithIdentifier:@"ReadytorecycleID"];
    [self.navigationController pushViewController:ReadytoRecycleScreenObj animated:YES];
    
    
}

#pragma mark UITABLEVIEW DELEGATE AND DATASOURCE METHOD.

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.BusinessListArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *simpleTableIdentifier = @"ShippingBusinessListCell";
    
    ShippingBusinessListCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil)
    {
        cell = [[ShippingBusinessListCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.textbackimageview.layer.cornerRadius = 27.5;
    cell.textbackimageview.layer.masksToBounds = YES;
    
    cell.namelabel.text = [NSString stringWithFormat:@"%@ - %@ - %@ - %@ ",(self.BusinessListArray)[indexPath.row][@"company_name"],(self.BusinessListArray)[indexPath.row][@"phone"],(self.BusinessListArray)[indexPath.row][@"address1"],(self.BusinessListArray)[indexPath.row][@"address2"]];
    
    
    NSString *firstLetter = [(self.BusinessListArray)[indexPath.row][@"company_name"] substringToIndex:1];
    firstLetter = firstLetter.uppercaseString;
    cell.textlabel.text = firstLetter;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    SelectyourBusinessTextField.text = (self.BusinessListArray)[indexPath.row][@"company_name"];
    PhoneTextField.text = (self.BusinessListArray)[indexPath.row][@"phone"];
    Address1TextField.text = (self.BusinessListArray)[indexPath.row][@"address1"];
    Address2TextField.text = (self.BusinessListArray)[indexPath.row][@"address2"];
    
    
    BusinessListPopup.hidden = YES;
}




@end
