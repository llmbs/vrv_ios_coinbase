
//UITableViewDataSource may need to be added to @interface ScanQRViewController : UIViewController<UITableViewDelegate>

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import <CoreLocation/CoreLocation.h>


@interface ScanQRViewController : UIViewController<UITableViewDelegate>
{
    __weak IBOutlet UILabel *oneminutetimeLabel;  // needed for timer

    __weak IBOutlet UIView *middleIcons;
    __weak IBOutlet UIView *middleBackground;
    __weak IBOutlet UIView *ScanCirclerTimerBaclView;
    __weak IBOutlet UILabel *scanlabel;
    
    __weak IBOutlet UIButton *BackButton; // needed for spontaneous glitch
    
    
    __weak IBOutlet UIImageView *MaterialIcon; //needed for showing which material is being recycled (icon)
    __weak IBOutlet UIImageView *MaterialBg; // needed for showing which material is being recycled ( background)
    
    
    __weak IBOutlet UILabel *HeaderTitle;
    
    __weak IBOutlet UILabel *go_back;
    
    __weak IBOutlet UILabel *green_not_accepting2;
    
    __weak IBOutlet UILabel *tid_accuracy;
    
}

// Get property Value From Privous Class.
@property (strong,nonatomic) NSString *categorytypename;
@property (strong,nonatomic) NSString *categoryID;
@property (nonatomic, assign) NSInteger categorycount;

@property (nonatomic) UIColor *selectedBarColor;
@property (nonatomic) UIColor *unselectedBarColor;
@property (nonatomic) UIColor *markColor;
@property (nonatomic) CGFloat markWidth;
@property (nonatomic, strong) IBOutlet UISlider  *progressView;
@property (nonatomic, assign) BOOL didShowCaptureWarning;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *AllButtonCollections;

@property(assign)BOOL selectedCork;
@property(assign)BOOL selectedClothes;
@property(assign)BOOL selectedPlastic;


- (IBAction)AccuracyButtonClick:(id)sender;
- (IBAction)NotAcceptingButtonClick:(id)sender;
- (IBAction)ReturnButtonClick:(id)sender;

@end
