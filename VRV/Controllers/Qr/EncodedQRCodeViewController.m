//
//  EncodedQRCodeViewController.m
//  Kayoeru
//
//  Created by vnnovate on 29/03/16.
//  Copyright © 2016 Vnnovate. All rights reserved.
//

#import "EncodedQRCodeViewController.h"
#define DOCUMENTS_FOLDER [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"]
@interface EncodedQRCodeViewController ()
{
    
}
@end

@implementation EncodedQRCodeViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
    
    
    _txtName.delegate=self;
    _txtEmail_Id.delegate=self;
    _txtPhoneNumber.delegate=self;
    
    
}
- (void)saveimage: (UIImage*)image1
{
    if (image1 != nil)
    {
        
        NSString *userNmaeStr = _txtName.text;
        
        
        // Give unique nam eto image.
        
        NSString* path = [DOCUMENTS_FOLDER stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", userNmaeStr]];
        
        NSData* data = UIImagePNGRepresentation(image1);
        [data writeToFile:path atomically:YES];
        NSLog(@"%@path",path);
        
    }
}



- (IBAction)btnEncodedAction:(id)sender
{
    [self setUIElementsAsEnabled:NO];
    
    [self.txtName resignFirstResponder];
    [self.txtEmail_Id resignFirstResponder];
    [self.txtPhoneNumber resignFirstResponder];
    
    // Get the string
    
    NSString *stringToEncode = self.txtName.text;
    NSString *stringToEncodeemailId = self.txtEmail_Id.text;
    NSString *stringToEncodPhonenumber = self.txtPhoneNumber.text;
    NSString *combined = [NSString stringWithFormat:@"%@\n%@\n%@", stringToEncode,stringToEncodeemailId,stringToEncodPhonenumber];
    // Generate the image
    
    CIImage *qrCode = [self createQRForString:combined];
    
    // Convert to an UIImage
    
    UIImage *qrCodeImg = [self createNonInterpolatedUIImageFromCIImage:qrCode withScale:2*[UIScreen mainScreen].scale];
    
    // And push the image on to the screen
    
    self.imgviewObject.image = qrCodeImg;
    
    // Re-enable the UI
    [self setUIElementsAsEnabled:YES];
    
    [self saveimage:_imgviewObject.image];
    
    
}
- (void)setUIElementsAsEnabled:(BOOL)enabled
{
    self.btnencoded.enabled = enabled;
    self.txtName.enabled = enabled;
    self.txtEmail_Id.enabled = enabled;
    self.txtPhoneNumber.enabled = enabled;
}
- (CIImage *)createQRForString:(NSString *)qrString
{
    // Need to convert the string to a UTF-8 encoded NSData object
    NSData *stringData = [qrString dataUsingEncoding:NSISOLatin1StringEncoding];
    
    // Create the filter
    CIFilter *qrFilter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
    // Set the message content and error-correction level
    [qrFilter setValue:stringData forKey:@"inputMessage"];
    [qrFilter setValue:@"H" forKey:@"inputCorrectionLevel"];
    
    // Send the image back
    return qrFilter.outputImage;
}
- (UIImage *)createNonInterpolatedUIImageFromCIImage:(CIImage *)image withScale:(CGFloat)scale
{
    // Render the CIImage into a CGImage
    CGImageRef cgImage = [[CIContext contextWithOptions:nil] createCGImage:image fromRect:image.extent];
    
    // Now we'll rescale using CoreGraphics
    UIGraphicsBeginImageContext(CGSizeMake(image.extent.size.width * scale, image.extent.size.width * scale));
    CGContextRef context = UIGraphicsGetCurrentContext();
    // We don't want to interpolate (since we've got a pixel-correct image)
    CGContextSetInterpolationQuality(context, kCGInterpolationNone);
    CGContextDrawImage(context, CGContextGetClipBoundingBox(context), cgImage);
    // Get the image out
    UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    // Tidy up
    UIGraphicsEndImageContext();
    CGImageRelease(cgImage);
    return scaledImage;
    
}
-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

@end
