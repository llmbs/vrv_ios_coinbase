//
//  EncodedQRCodeViewController.h
//  Kayoeru
//
//  Created by vnnovate on 29/03/16.
//  Copyright © 2016 Vnnovate. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EncodedQRCodeViewController : UIViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imgviewObject;
@property (weak, nonatomic) IBOutlet UIButton *btnencoded;
- (IBAction)btnEncodedAction:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtName;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail_Id;
@property (weak, nonatomic) IBOutlet UITextField *txtPhoneNumber;

@end
