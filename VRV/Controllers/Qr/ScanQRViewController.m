

#import "ScanQRViewController.h"
//#import "EncodedQRCodeViewController.h"
#import "AppDelegate.h"
#import "Reachability.h"
#import "AFNetworking.h"
#import "Defaults.h"
#import "CommanFunction.h"
#import "CircularTimerView.h"
#import <CoreLocation/CoreLocation.h>
#import <GoogleMaps/GoogleMaps.h>
//#import <FLAnimatedImage/FLAnimatedImage.h>
#import "LeftRightViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "ReadytoRecycleView.h"
#import "RecylingLocationView.h"
#import "RecylingLocationView.h"
#import "MainTabBarController.h"

#import "VRV-Swift.h"


@interface ScanQRViewController ()
{
    AppDelegate *appDelegate;
    int totalSeconds;
       int totalSeconds1;
    NSTimer *oneMinTimer;
    
    NSUserDefaults *dlf;
    
    AVAudioPlayer *_audioPlayer;
    CLLocation *lastLocation;
}
@property float radius;
@property float internalRadius;
@property (nonatomic, strong) UIColor *backgroundColor;
@property (nonatomic, strong) UIColor *backgroundFadeColor;
@property (nonatomic, strong) UIColor *foregroundColor;
@property (nonatomic, strong) UIColor *foregroundFadeColor;
@property (nonatomic, assign) NSInteger direction;
@property (nonatomic, assign) NSTimeInterval countdownSeconds;

@property (nonatomic, strong) CircularTimerView *circularTimer;




@end

@implementation ScanQRViewController



- (void)viewDidLoad {
    
    [super viewDidLoad];

    [[UIButton appearance] setExclusiveTouch:YES];
  
    if (_selectedCork) {
       
      [self showCorkAlert: @""];
        
       [MaterialIcon setImage:[UIImage imageNamed: @"wine"]];
       [MaterialBg setImage:[UIImage imageNamed: @"corkboard"]];
        
        
    }else if (_selectedClothes) {
        
        [self showClothAlert: @""];
        
        [MaterialIcon setImage:[UIImage imageNamed: @"clothes"]];
        [MaterialBg setImage:[UIImage imageNamed: @"textiles"]];
        
        
    }
    
    else if (_selectedPlastic){

        [self showCrvAlert:@""];

        [MaterialIcon setImage:[UIImage imageNamed: @"plastic_inactive"]];
        [MaterialBg setImage:[UIImage imageNamed: @"beach"]];
    
    }
    
    
  
    
    dlf = [NSUserDefaults standardUserDefaults];
    
    appDelegate =(AppDelegate *) [UIApplication sharedApplication].delegate;
    
    oneminutetimeLabel.text = @"";
    self.progressView.minimumValue = 0;
     self.progressView.maximumValue = 60;
    totalSeconds = 60;  //suppose to be 60 but lower it to test
    totalSeconds1 = 0;
   
    oneMinTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                   target:self
                                                 selector:@selector(timer)
                                                 userInfo:nil
                                                  repeats:YES];
   
   // self.radius = 100;
   //self.internalRadius = 90;
    
     self.radius = 80;
    self.internalRadius = 70;
    
    self.backgroundColor = [UIColor lightGrayColor];
    self.foregroundColor = [UIColor whiteColor ];

    
    NSLog(@"y posotion=%f",scanlabel.frame.size.height+scanlabel.frame.origin.y+ScanCirclerTimerBaclView.frame.origin.y);
    
    CGFloat width   = [UIScreen mainScreen].bounds.size.width;
    
    //double xposition = width -200;
   //xposition = xposition/2;
    
    double xposition = width -160;
    xposition = xposition/2;
    
    self.circularTimer = [[CircularTimerView alloc] initWithPosition:CGPointMake(xposition, scanlabel.frame.size.height+scanlabel.frame.origin.y+ScanCirclerTimerBaclView.frame.origin.y)
                                                              radius:self.radius
                                                      internalRadius:self.internalRadius];
    
    self.circularTimer.backgroundColor = self.backgroundColor;

    self.circularTimer.foregroundColor = self.foregroundColor;

    self.circularTimer.direction = 1;
    self.circularTimer.font = [UIFont systemFontOfSize:22];

    self.circularTimer.fontColor = [UIColor systemGreenColor];
    
    [self.circularTimer setupCountdown:totalSeconds];
    
    [self.view addSubview:self.circularTimer];
    
    [self SliderSetupCode];
    
    for (UIButton *btn in _AllButtonCollections) {
        btn.layer.cornerRadius =4;
    }
    
    HeaderTitle.text = NSLocalizedString(@"ready_to_recycle_timer", "");
    
    
    go_back.text = NSLocalizedString(@"go_back", "");
    
    green_not_accepting2.text = NSLocalizedString(@"green_not_accepting2", "");
    
    tid_accuracy.text = NSLocalizedString(@"tid_accuracy", "");
    
    
    
    
    
    
    middleBackground.layer.cornerRadius = 4;
    middleBackground.hidden = YES;
    middleIcons.hidden = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillResignActive:) name:UIApplicationWillResignActiveNotification object:nil];
    
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
}

// Alert messages for Materials Section
// Material = Corks

- (void)appWillResignActive:(NSNotification*)note {
    //[self materialOkNo];
    
    totalSeconds = 0;
}

- (void)showCorkAlert:(NSString *)message {
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:NSLocalizedString(@"testify_header_cork", "")
                                  message:NSLocalizedString(@"testify_paragraph", "")
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okNo = [UIAlertAction
                           actionWithTitle:NSLocalizedString(@"green_no", "")
                           style:UIAlertActionStyleDefault
                           handler:^(UIAlertAction * action)
                           {
                               [self materialOkNo];
                 
                           }];
    
    
    UIAlertAction* okYes = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"green_yes", "")
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
  
                                 [self materialOkYes];
           
                             }];
                            
                            [alert addAction:okNo];
                            [alert addAction:okYes];
                            [self presentViewController:alert animated:YES completion:nil];
                            
}

// Alert messages for Materials Section
// Material = Clothes

- (void)showClothAlert:(NSString *)message {
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:NSLocalizedString(@"testify_header_clothes", "")
                                  message:NSLocalizedString(@"testify_paragraph", "")
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okNo = [UIAlertAction
                           actionWithTitle:NSLocalizedString(@"green_no", "")
                           style:UIAlertActionStyleDefault
                           handler:^(UIAlertAction * action)
                           {
                            
                               [self materialOkNo];
                               
                           }];
    
    
    UIAlertAction* okYes = [UIAlertAction
                            actionWithTitle:NSLocalizedString(@"green_yes", "")
                            style:UIAlertActionStyleDefault
                            handler:^(UIAlertAction * action)
                            {
                                
                                [self materialOkYes];
                                
                            }];
    
    [alert addAction:okNo];
    [alert addAction:okYes];
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (void)showCrvAlert:(NSString *)message {
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:NSLocalizedString(@"testify_header_crv", "")
                                  message:NSLocalizedString(@"testify_paragraph", "")
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okNo = [UIAlertAction
                           actionWithTitle:NSLocalizedString(@"green_no", "")
                           style:UIAlertActionStyleDefault
                           handler:^(UIAlertAction * action)
                           {
                               
                               [self materialOkNo];
                               
                           }];
    
    
    UIAlertAction* okYes = [UIAlertAction
                            actionWithTitle:NSLocalizedString(@"green_yes", "")
                            style:UIAlertActionStyleDefault
                            handler:^(UIAlertAction * action)
                            {
                                
                                [self materialOkYes];
                                
                            }];
    
    [alert addAction:okNo];
    [alert addAction:okYes];
    [self presentViewController:alert animated:YES completion:nil];
    
}

// Materials alert messages OkNo and OkYes void parameters


- (void)materialOkNo
{
    
    [self backTapped];
    [self->oneMinTimer invalidate];
    self.circularTimer.hidden = YES;
    
    
//    ReadytoRecycleView *ReadytoRecycleScreenObj = [self.storyboard instantiateViewControllerWithIdentifier:@"ReadytorecycleID"];
//    [self.navigationController pushViewController:ReadytoRecycleScreenObj animated:YES];
    
    
    [self.view setUserInteractionEnabled: NO];
    MainTabBarController *tabController = [[MainTabBarController alloc]init];
    [tabController setupTabBar];
    [self.navigationController pushViewController:tabController animated:YES];
    
    
}

- (void)materialOkYes
{
    [self showProgressView];
    
    NSString *material = @"cloth";
    
    if (_selectedCork) {
        material = @"cork";
        
    }
    
    if (_selectedPlastic) {
        material = @"plastic";
        
    }
    
    if (self->totalSeconds > 1) {
        [RecyclingService markAsCompleteWithType:material success:^(NSInteger success) {
            
        [self hideProgressView];

        NSString *path = [[NSBundle mainBundle] pathForResource:@"cheers" ofType:@"mp3"];
        NSURL *fileURL = [NSURL fileURLWithPath:path];
        self->appDelegate.audioPlayer = [AVPlayer playerWithURL:fileURL];
        [self->appDelegate play];


        [self backTapped];
        [self->oneMinTimer invalidate];
        self.circularTimer.hidden = YES;
        [self showAlertCongrats:NSLocalizedString(@"congratulations_para_full", "")];

        
        
        //Handle success
    } failure:^(NSError * _Nullable error) {
        [self->oneMinTimer invalidate];
        self.circularTimer.hidden = YES;
        
        self->middleBackground.hidden = NO;
        self->middleIcons.hidden = NO;
        
        [self handleError:error];
        [self hideProgressView];
        
        
    }];
    

}
}


// Dismisses a UIAlertController so that it does not conflict with another one
                            
- (void)backTapped
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    
    
 
}


// Alert message for -  user selected Accuracy -> Google maps or Improve Coordinates with Location access disabled

- (void)showAlertTutorial:(NSString *)message {
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:NSLocalizedString(@"location_access_para", "")
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* tutorialButton = [UIAlertAction
                                     actionWithTitle:NSLocalizedString(@"recycling_location_tutorial", "")
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         
                                        
                                         
                                         NSURL *recloc = [NSURL URLWithString:@"https://youtu.be/Of9fGR3yBIU"];
                                         
                                         [[UIApplication sharedApplication] openURL: recloc options:@{} completionHandler:^(BOOL success) {
                                             if (success)
                                                 NSLog(@"Opened tutorial");
                                             
                                         }];
                                         
                                         
                                     }];
    
    
    UIAlertAction* SettingsButton = [UIAlertAction
                               actionWithTitle:@"Settings"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   
                                   
                                   
                                   NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                   if (url != nil) {
                                       [[UIApplication sharedApplication] openURL:url options:[NSDictionary new] completionHandler:nil];
                                   }
                                   
                                   
                               }];
    
    
    UIAlertAction*  OKButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                      
                                        
                                    }];
    
    [alert addAction:SettingsButton];
    [alert addAction:tutorialButton];
    [alert addAction:OKButton];
    [self presentViewController:alert animated:YES completion:nil];
    
}


//Time ran out and user did not select yes for Alert messages for Materials Section


- (void)showAlertZeroSecs:(NSString *)message {
    
    
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:NSLocalizedString(@"testify_timepara", "")
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* OkButton = [UIAlertAction
                                actionWithTitle:NSLocalizedString(@"dialog_ok", "")
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                    
                                    [self hideProgressView];
                                    self->middleBackground.hidden = NO;
                                    self->middleIcons.hidden = NO;
                                    
                                }];
    
    
    [alert addAction:OkButton];
    [self presentViewController:alert animated:YES completion:nil];
};

//User selected yes for Alert messages for Materials Section

- (void)showAlertCongrats:(NSString *)message {
    
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction* OkButton = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"dialog_ok", "")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   
                                   
                                   {
                                       
                                       
                                       
                                       self->middleBackground.hidden = NO;
                                       self->middleIcons.hidden = NO;
                                       
                                       
                                   }];
   
    [alert addAction:OkButton];
    [self presentViewController:alert animated:YES completion:nil];
    
}


// IBAction buttons

- (IBAction)ReturnButtonClick:(id)sender {
    
    
    [self.view setUserInteractionEnabled: NO];
    MainTabBarController *tabController = [[MainTabBarController alloc]init];
    [tabController setupTabBar];
    [self.navigationController pushViewController:tabController animated:YES];
    
}


- (IBAction)NotAcceptingButtonClick:(id)sender {
    
    [self showAlertNotAccepting:NSLocalizedString(@"green_not_accepting_para3", "")];
    
}

- (IBAction)AccuracyButtonClick:(id)sender{
 
//    if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied) {
//
//        [self showAlertTutorial:@""];
//
//    } else {
    
        [self showAlertAccuracy:NSLocalizedString(@"tid_accuracy_para2", "")];
    

}



//Accuracy Dialog

- (void)showAlertAccuracy:(NSString *)message {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:NSLocalizedString(@"tid_accuracy", "")
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"dialog_ok", "")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                 
                                   
                               }];
    
    
    UIAlertAction* RecyclingButton = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"recycling_locations", "")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   
                                   RecylingLocationView *RecylinglocationScreenObj = [self.storyboard instantiateViewControllerWithIdentifier:@"ReCycleLocationId"];
                                   [self.navigationController pushViewController:RecylinglocationScreenObj animated:YES];
                                   
                               }];
    
    
    
    [alert addAction:RecyclingButton];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}


//Not accepting Dialog

- (void)showAlertNotAccepting:(NSString *)message {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:NSLocalizedString(@"green_not_accepting", "")
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"dialog_ok", "")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   
                                   
                               }];
    
    
    UIAlertAction* LocationButton = [UIAlertAction
                                    actionWithTitle:NSLocalizedString(@"recycling_locations", "")
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        
                                        RecylingLocationView *RecylinglocationScreenObj = [self.storyboard instantiateViewControllerWithIdentifier:@"ReCycleLocationId"];
                                        [self.navigationController pushViewController:RecylinglocationScreenObj animated:YES];
                                        
                                        
                                    }];
    
    
    
    [alert addAction:LocationButton];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}


-(void)SliderSetupCode{
    
    self.markColor = [UIColor clearColor];
    
    CGRect rect=self.progressView.frame;
    self.markWidth = 1.0;
    CGRect innerRect = CGRectInset(rect, 1.0, 10.0);
    self.selectedBarColor = [UIColor blueColor];
    self.unselectedBarColor = [UIColor lightGrayColor];
    UIGraphicsBeginImageContextWithOptions(innerRect.size, NO, 0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // Selected side
    CGContextSetLineCap(context, kCGLineCapRound);
    CGContextSetLineWidth(context, 12.0);
    CGContextMoveToPoint(context, 6, CGRectGetHeight(innerRect)/2);
    //CGContextAddLineToPoint(context, innerRect.size.width - 10, CGRectGetHeight(innerRect)/2);
    //CGContextSetStrokeColorWithColor(context, (self.selectedBarColor).CGColor);
    CGContextStrokePath(context);
    UIImage *selectedSide = [UIGraphicsGetImageFromCurrentImageContext() resizableImageWithCapInsets:UIEdgeInsetsZero];
    
    // Unselected side
    CGContextSetLineCap(context, kCGLineCapRound);
    CGContextSetLineWidth(context, 12.0);
    CGContextMoveToPoint(context, 6, CGRectGetHeight(innerRect)/2);
    //CGContextAddLineToPoint(context, innerRect.size.width - 10, CGRectGetHeight(innerRect)/2);
    //CGContextSetStrokeColorWithColor(context, (self.unselectedBarColor).CGColor);
    CGContextStrokePath(context);
    UIImage *unselectedSide = [UIGraphicsGetImageFromCurrentImageContext() resizableImageWithCapInsets:UIEdgeInsetsZero];
    
    CGContextSetLineWidth(context, self.markWidth);
    float position = totalSeconds1 * innerRect.size.width / 60.0;
    CGContextMoveToPoint(context, position, CGRectGetHeight(innerRect)/2 - 5);
    //CGContextAddLineToPoint(context, position, CGRectGetHeight(innerRect)/2 + 5);
    //CGContextSetStrokeColorWithColor(context, (self.markColor).CGColor);
    CGContextStrokePath(context);
    [selectedSide drawAtPoint:CGPointMake(0,0)];
    UIImage *selectedStripSide = [UIGraphicsGetImageFromCurrentImageContext() resizableImageWithCapInsets:UIEdgeInsetsZero];
    
    // Set trips on unselected side
    [unselectedSide drawAtPoint:CGPointMake(0,0)];
    
    CGContextSetLineWidth(context, self.markWidth);
    float position1 = totalSeconds1 * innerRect.size.width / 60.0;
    CGContextMoveToPoint(context, position1, CGRectGetHeight(innerRect)/2 - 5);
    //CGContextAddLineToPoint(context, position1, CGRectGetHeight(innerRect)/2 + 5);
    //CGContextSetStrokeColorWithColor(context, (self.markColor).CGColor);
    CGContextStrokePath(context);
    
    UIImage *unselectedStripSide = [UIGraphicsGetImageFromCurrentImageContext() resizableImageWithCapInsets:UIEdgeInsetsZero];
    
    UIGraphicsEndImageContext();
    
    [self.progressView setMinimumTrackImage:selectedStripSide forState:UIControlStateNormal];
    [self.progressView setMaximumTrackImage:unselectedStripSide forState:UIControlStateNormal];

}

- (void)timer {
    totalSeconds--;
    totalSeconds1++;
    NSString *strtime;
    
   
    if (totalSeconds<10) {
        strtime=[NSString stringWithFormat:@"0%d",totalSeconds];
        self.circularTimer.frameBlock = ^(CircularTimerView *circularTimerView){
            circularTimerView.text = strtime;
            
        };

    }else
    {
          strtime=[NSString stringWithFormat:@"%d",totalSeconds];
        self.circularTimer.frameBlock = ^(CircularTimerView *circularTimerView){
            circularTimerView.text = strtime;
            
        };

    }
         self.progressView.value = totalSeconds1;
         oneminutetimeLabel.text = strtime;
    
    
    
    if ( totalSeconds <= 1 ) {
        
      
       
    
            [self backTapped];
            [self->oneMinTimer invalidate];
            self.circularTimer.hidden = YES;
            [self showAlertZeroSecs:@""];
            
            
        
    
        
       
    }
        
    if (totalSeconds == 0) {
        
         [self->oneMinTimer invalidate];
        [self showAlertZeroSecs:@""];

        
    }

}

#pragma mark - Lifecycle



@end
