//
//  ContestWebViewController.m
//  VRV
//
//  Created by Abhishek on 01/11/19.
//  Copyright © 2019 Abhishek Sheth. All rights reserved.
//

#import "ContestWebViewController.h"
#import "ReadytoRecycleView.h"
#import "MainTabBarController.h"
#import <WebKit/WebKit.h>



//CHANGE NUMBER 1:-
@interface ContestWebViewController () <WKNavigationDelegate, WKUIDelegate>

@end

@implementation ContestWebViewController

+ (void)pushContestController:(UIViewController *)from token:(NSString *)token level:(NSInteger)level {
    ContestWebViewController *ContestWebviewScreenObject = [from.storyboard instantiateViewControllerWithIdentifier:@"ContestId"];
    ContestWebviewScreenObject.token = token;
    ContestWebviewScreenObject.level = level;
    [from.navigationController pushViewController:ContestWebviewScreenObject animated:YES];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
   
    
    
    
    _webkitView.navigationDelegate = self;
    _webkitView.UIDelegate = self;
    _webkitView.scrollView.delegate = self;
    [_webkitView setMultipleTouchEnabled:false];
    [_webkitView setContentMode:UIViewContentModeScaleAspectFit];
    
//    NSString *url = [NSString stringWithFormat:@"https://contest.voluntaryrefundvalue.com/?token=%@", self.token];
//    NSLog(@"%@", url);
//    NSURL *nsUrl = [NSURL URLWithString:url];
//    NSURLRequest *request = [NSURLRequest requestWithURL:nsUrl cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:0];
    
    
  //  NSString *url = [NSString stringWithFormat:@"https://www.gtrealtor.com/blank"];
   // NSLog(@"%@", url);
   // NSURL *nsUrl = [NSURL URLWithString:url];
   // NSURLRequest *request = [NSURLRequest requestWithURL:nsUrl cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:0];
    
  //  [_webkitView loadRequest:request];
    
    indBtn.layer.cornerRadius = 4;
    engBtn.layer.cornerRadius = 4;
    vietBtn.layer.cornerRadius = 4;
    
    
    headertitle.text = NSLocalizedString(@"Game_on", "");
    
    [self EngClick:self];
    
}

#pragma mark Void Methods.

-(void)hideLanguage {
    
    indBtn.hidden= YES;
    engBtn.hidden= YES;
    vietBtn.hidden= YES;
    
}

-(void)showLanguage {
    
    indBtn.hidden= NO;
    engBtn.hidden= NO;
    vietBtn.hidden= NO;
    
}




- (void)showAlert:(NSString *)message {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:NSLocalizedString(@"puzzle", "")
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* stayButton = [UIAlertAction
                                 actionWithTitle:NSLocalizedString(@"gameActivity_stay", "")
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
        
    }];
    
    UIAlertAction* reloadButton = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"gameActivity_reload", "")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
        
//        [self showLanguage];
//
//        NSString *url = [NSString stringWithFormat:@"https://www.vrvccoin.com/blank"];
//
//        NSURL *nsUrl = [NSURL URLWithString:url];
//        NSURLRequest *request = [NSURLRequest requestWithURL:nsUrl cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:0];
//
//        [self->_webkitView loadRequest:request];
        
        [self EngClick:self];
        
        
    }];
    
    UIAlertAction* exitButton = [UIAlertAction
                                 actionWithTitle:NSLocalizedString(@"gameActivity_exit", "")
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
        
//        ReadytoRecycleView *ReadytoRecycleScreenObj = [self.storyboard instantiateViewControllerWithIdentifier:@"ReadytorecycleID"];
//        UIViewController *newVC = ReadytoRecycleScreenObj;
//        NSMutableArray *vcs =  [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
//        [vcs insertObject:newVC atIndex:vcs.count-1];
//        [self.navigationController setViewControllers:vcs animated:YES];
//        [self.navigationController popViewControllerAnimated:YES];
        
        
      
        MainTabBarController *tabController = [[MainTabBarController alloc]init];
        [tabController setupTabBar];
        [self.navigationController pushViewController:tabController animated:YES];
        
        
    }];
    
    
    [alert addAction:stayButton];
    [alert addAction:reloadButton];
    [alert addAction:exitButton];
    
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)scrollViewWillBeginZooming:(UIScrollView *)scrollView withView:(UIView *)view{
    [[scrollView pinchGestureRecognizer] setEnabled:false];
}


#pragma mark IBAction Methods.

- (IBAction)BackButtonClick:(id)sender {
    
    [self showAlert:NSLocalizedString(@"rules_contest", "")];
}


- (IBAction)EngClick:(id)sender {
    
   // [self hideLanguage];
     
    
    /*
    
    NSString *url = [NSString stringWithFormat:@"https://www.contest.vrvccoin.com/?token=%@", self.token];
    NSLog(@"%@", url);
    NSURL *nsUrl = [NSURL URLWithString:url];
    NSURLRequest *request = [NSURLRequest requestWithURL:nsUrl cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:0];
    
    [_webkitView loadRequest:request];
    */
    
    
    
    
    
    // Get the path to the e1 folder
    NSString *e1Path = [[NSBundle mainBundle] pathForResource:@"e1" ofType:nil];

    // Create a URL from the path
    NSURL *baseURL = [NSURL fileURLWithPath:e1Path];

    // Get the path to the index.html file in the e1 folder
    NSString *indexPath = [[NSBundle mainBundle] pathForResource:@"index" ofType:@"html" inDirectory:@"e1"];

    // Append the token parameter to the URL
    NSString *indexURLString = [NSString stringWithFormat:@"file://%@?token=%@", indexPath, self.token];

    // Create a URL from the modified string
    NSURL *indexURL = [NSURL URLWithString:indexURLString];
    NSLog(@"Modified index URL: %@", indexURLString);

    // Load the URL in a web view with read access to the e1 directory
    WKWebViewConfiguration *configuration = [[WKWebViewConfiguration alloc] init];
    [configuration.preferences setValue:@(true) forKey:@"allowFileAccessFromFileURLs"];

    CGRect webViewFrame = CGRectMake(0, 69, self.view.frame.size.width, self.view.frame.size.height - 69);
    WKWebView *webView = [[WKWebView alloc] initWithFrame:webViewFrame configuration:configuration];
    [self.view addSubview:webView];
    [webView loadFileURL:indexURL allowingReadAccessToURL:baseURL];

    // Add the tab view as a subview after the web view
    [self.view addSubview:tab];
    
    // Enable WebKit Media Playback
    configuration.allowsInlineMediaPlayback = YES;
    
    webView.UIDelegate = self;
    webView.allowsBackForwardNavigationGestures = true;
    webView.allowsLinkPreview = true;
    webView.navigationDelegate = self;
    
    
}



-(void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    if ([navigationAction.request.URL.absoluteString containsString:@"sms:"]) {
        
        [[UIApplication sharedApplication] openURL:navigationAction.request.URL options:@{} completionHandler:^(BOOL success) {
            if (success) {
                NSLog(@"Opened sms");
            }
        }];
    }
    decisionHandler(WKNavigationActionPolicyAllow);
    return;
}


//CHANGE NUMBER 3:-  Thats all sir.. just 3 changes..
- (void)webView:(WKWebView *)webView runJavaScriptTextInputPanelWithPrompt:(NSString *)prompt defaultText:(NSString *)defaultText initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(NSString *result))completionHandler
{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Digital Wallet"
                                                                   message:prompt
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = prompt;
        textField.secureTextEntry = NO;
        textField.text = defaultText;
    }];
    
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        completionHandler(nil);
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        completionHandler([alert.textFields.firstObject text]);
    }]];
    
    [self presentViewController:alert animated:YES completion:nil];
}



- (IBAction)IndoClick:(id)sender {
    
    [self hideLanguage];
    
    NSString *url = [NSString stringWithFormat:@"https://www.contest.vrvccoin.com/?token=%@", self.token];
    NSLog(@"%@", url);
    NSURL *nsUrl = [NSURL URLWithString:url];
    NSURLRequest *request = [NSURLRequest requestWithURL:nsUrl cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:0];
    
    [_webkitView loadRequest:request];
    
}

- (IBAction)VietClick:(id)sender {
    
    [self hideLanguage];
    
    NSString *url = [NSString stringWithFormat:@"https://www.viet.vrvccoin.com/?token=%@", self.token];
    NSLog(@"%@", url);
    NSURL *nsUrl = [NSURL URLWithString:url];
    NSURLRequest *request = [NSURLRequest requestWithURL:nsUrl cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:0];
    
    [_webkitView loadRequest:request];
    
    
    
}






- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return nil;
}
@end
