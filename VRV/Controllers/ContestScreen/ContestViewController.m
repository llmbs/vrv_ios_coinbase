//
//  ContestViewController.m
//  VRV
//
//  Created by Elisha Dumas on 10/20/19.
//  Copyright © 2019 Abhishek Sheth. All rights reserved.
//

#import "ContestViewController.h"
#import "SweepstakesView.h"

@interface ContestViewController ()

@end

@implementation ContestViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

#pragma mark Void Methods.

- (void)showAlert:(NSString *)message {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:NSLocalizedString(@"puzzle", "")
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* stayButton = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"gameActivity_stay", "")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   
                               }];
    
    UIAlertAction* exitButton = [UIAlertAction
                                 actionWithTitle:NSLocalizedString(@"gameActivity_exit", "")
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     
                                     SweepstakesView *SweepstakesviewScreenObject = [self.storyboard instantiateViewControllerWithIdentifier:@"SweepstakesId"];
                                     UIViewController *newVC = SweepstakesviewScreenObject;
                                     NSMutableArray *vcs =  [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
                                     [vcs insertObject:newVC atIndex:vcs.count-1];
                                     [self.navigationController setViewControllers:vcs animated:YES];
                                     [self.navigationController popViewControllerAnimated:YES];
                                     
                                     
                                 }];
    
    [alert addAction:exitButton];
    [alert addAction:stayButton];
   
    
    [self presentViewController:alert animated:YES completion:nil];
}


#pragma mark IBAction Methods.

- (IBAction)BackButtonClick:(id)sender {
    
    [self showAlert:NSLocalizedString(@"rules_contest", "")];
    

}

@end
