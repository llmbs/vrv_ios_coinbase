//
//  ContestWebViewController.h
//  VRV
//
//  Created by Abhishek on 01/11/19.
//  Copyright © 2019 Abhishek Sheth. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ContestWebViewController : UIViewController<UIScrollViewDelegate,UIWebViewDelegate>

{
    __weak IBOutlet UIButton *backButton;
    
    __weak IBOutlet UIButton *engBtn;
    __weak IBOutlet UIButton *indBtn;
    __weak IBOutlet UIButton *vietBtn;
    
    __weak IBOutlet UIView *centerEmbed;
    
    __weak IBOutlet UILabel *headertitle;
    
    __weak IBOutlet UIView *tab;

}

- (IBAction)BackButtonClick:(id)sender;
- (IBAction)EngClick:(id)sender;
- (IBAction)IndoClick:(id)sender;
- (IBAction)VietClick:(id)sender;



+ (void)pushContestController:(UIViewController *)from token:(NSString *)token level:(NSInteger)level;

@property (strong, nonatomic) IBOutlet WKWebView *webkitView;

@property (strong, nonatomic) NSString *token;
@property (nonatomic) NSInteger level;


@end

NS_ASSUME_NONNULL_END
