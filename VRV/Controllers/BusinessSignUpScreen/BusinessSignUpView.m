

#import "BusinessSignUpView.h"
#import "Reachability.h"
#import "Defaults.h"
#import "CommanFunction.h"
#import "AddressCell.h"
#import "AppDelegate.h"
#import "AFNetworking.h"
#import "LeftRightViewController.h"
#import "UserRoleInfoView.h"
#import "newBusinessView.h"
#import "newBusinessView.h"

#import "VRV-Swift.h"
@import GoogleMaps;
@import GooglePlaces;


@interface BusinessSignUpView () <GMSAutocompleteViewControllerDelegate>
{
    NSString *NaturalWineCorks;
    NSString *Clothes;
    NSString *JobTitle;
    NSString *Plastic;
    
    double businesslatitude;
    double businessLongitude;
    BOOL blnNaturalWineCorks;
    BOOL blnClothess;
    BOOL blnPlastic;
    
    
    NSUserDefaults *dlf;
    
    // int tflag;
    
    AppDelegate *app;
    
    CLLocationCoordinate2D businessLocation;
}

@end


@implementation BusinessSignUpView {
    GMSAutocompleteFilter *_filter;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // flag = 0;
    // tflag = 0;
    
    [[UIButton appearance] setExclusiveTouch:YES];
    
    UIColor *color1 = [UIColor whiteColor];
    CompanyNameTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Company" attributes:@{NSForegroundColorAttributeName: color1}];
    
    UIColor *color2 = [UIColor whiteColor];
    PhoneNumberTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Mobile number" attributes:@{NSForegroundColorAttributeName: color2}];
    
    UIColor *color3 = [UIColor whiteColor];
    Address1TextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Address 1" attributes:@{NSForegroundColorAttributeName: color3}];
    
    UIColor *color4 = [UIColor whiteColor];
    Address2TextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Address 2 (Suite,flr)" attributes:@{NSForegroundColorAttributeName: color4}];
    
    UIColor *color5 = [UIColor whiteColor];
    FirstNameTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"First name" attributes:@{NSForegroundColorAttributeName: color5}];
    
    
    UIColor *color6 = [UIColor whiteColor];
    LastnameTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Last name" attributes:@{NSForegroundColorAttributeName: color6}];
    
    UIColor *color7 = [UIColor whiteColor];
    CompanyEINTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Company EIN" attributes:@{NSForegroundColorAttributeName: color7}];
    
    
    AddNewBusinessSaveButton.layer.cornerRadius= 4;
    AddNewBusinessCancleButton.layer.cornerRadius=4;
    UpdateButton.layer.cornerRadius =4;
    UpdateButton.hidden = YES;
    MenuRad.layer.cornerRadius = 4;
    BackButton.layer.cornerRadius = 4;
    EinButton.layer.cornerRadius = 4;
    
    
    NaturalWineCorks = @"0";
    Clothes = @"0";
    Plastic = @"0";
    JobTitle = @"";
    
    SignUpLabel.text = @"Add a business";
    AddNewBusinessSaveButton.hidden = NO;
    AddNewBusinessCancleButton.hidden=NO;
    
    
    [MainScrollview setScrollEnabled:YES];
    MainScrollview.contentSize = CGSizeMake(self.view.frame.size.width, MainView.frame.size.height);
    
    app =(AppDelegate *)[UIApplication sharedApplication].delegate;
    
    dlf = [NSUserDefaults standardUserDefaults];
    // NSLog(@"key=%@",[dlf objectForKey:@"screenRedirectID"]);
    
    
    
    if (_selectedEdit) {
        
        SignUpLabel.text = @"Edit a Business";
        AddNewBusinessSaveButton.hidden = YES;
        UpdateButton.hidden = NO;
        
        [self usersMaterials];
        
        
        if ([self.selectedBusiness.job_title isEqualToString:@"Manager"]) {
            [self ManagerButtonClick:nil];
        } else if ([self.selectedBusiness.job_title isEqualToString:@"Supervisor"]) {
            [self SupervisorButtonClick:nil];
        }
        
        //2.  UITextfields
        CompanyNameTextField.text = self.selectedBusiness.company_name;
        PhoneNumberTextField.text = self.selectedBusiness.phone;
        Address1TextField.text = self.selectedBusiness.address1;
        Address2TextField.text = self.selectedBusiness.address2;
        FirstNameTextField.text = self.selectedBusiness.first_name;
        LastnameTextField.text = self.selectedBusiness.last_name;
        CompanyEINTextField.text = self.selectedBusiness.company_ein;
        
        double lat = [self.selectedBusiness.lat doubleValue];
        double lng = [self.selectedBusiness.lng doubleValue];
        
        businessLocation = CLLocationCoordinate2DMake(lat, lng);
        
        
        //3. Job title Section string value - "checked"  OR "uncheck"
        
        //      MenagerImageview.image = [UIImage imageNamed:@""];
        //       Supervisorimageview.image = [UIImage imageNamed:@""];
        
        
    }
    
    Address1TextField.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
}


#pragma mark IBaction Method.

- (IBAction)MenuButtonclick:(id)sender {
    
    MainView.hidden = YES;
    
    LeftRightViewController *LeftrightviewScreenObject = [self.storyboard instantiateViewControllerWithIdentifier:@"leftright"];
    UIViewController *newVC = LeftrightviewScreenObject;
    NSMutableArray *vcs =  [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
    [vcs insertObject:newVC atIndex:vcs.count-1];
    [self.navigationController setViewControllers:vcs animated:YES];
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)BackbuttonClick:(id)sender {
    newBusinessView *NewBusinessScreenObj = [self.storyboard instantiateViewControllerWithIdentifier:@"newbusinessID"];
    [self.navigationController pushViewController:NewBusinessScreenObj animated:YES];
    
}

- (IBAction)NaturalWineCorksButtonClick:(id)sender {
    if (blnNaturalWineCorks == false) {
        blnNaturalWineCorks = true;
        NaturalWineCorks = @"1";
        NaturalWineCorksImageview.image =[UIImage imageNamed:@"selected-1"];
    }else{
        blnNaturalWineCorks = false;
        NaturalWineCorks = @"0";
        NaturalWineCorksImageview.image =[UIImage imageNamed:@"material_not_selected"];
    }
}


- (IBAction)ClothesButtonClick:(id)sender {
    if (blnClothess == false) {
        blnClothess = true;
        Clothes = @"1";
        ClothesImageview.image = [UIImage imageNamed:@"selected-1"];
    }else{
        blnClothess = false;
        Clothes = @"0";
        ClothesImageview.image = [UIImage imageNamed:@"material_not_selected"];
    }
    
}
    - (IBAction)PlasticButtonClick:(id)sender {
        if (blnPlastic == false) {
            blnPlastic = true;
            Plastic = @"1";
            PlasticImageview.image = [UIImage imageNamed:@"selected-1"];
        }else{
            blnPlastic = false;
            Plastic = @"0";
            PlasticImageview.image = [UIImage imageNamed:@"material_not_selected"];
        }
    
    
}

- (IBAction)ManagerButtonClick:(id)sender {
    JobTitle = @"Manager";
    MenagerImageview.image = [UIImage imageNamed:@"check"];
    Supervisorimageview.image = [UIImage imageNamed:@"uncheck"];
}

- (IBAction)SupervisorButtonClick:(id)sender {
    JobTitle = @"Supervisor";
    MenagerImageview.image = [UIImage imageNamed:@"uncheck"];
    Supervisorimageview.image = [UIImage imageNamed:@"check"];
}

- (IBAction)UpdateButtonClick:(id)sender {
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    
    if (remoteHostStatus == NotReachable)
    {
        [self showAlert:@"Internet connection offline."];
        
    }else{
        
        [self CheckValidationForBusinessSignUp];
    }
}

- (IBAction)AddNewBusinessSaveButtonClick:(id)sender {
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    
    if (remoteHostStatus == NotReachable)
    {
        [self showAlert:@"Internet connection offline."];
        
    }else{
        
        [self CheckValidationForBusinessSignUp];
    }
}

- (IBAction)AddNewBusinessCancleButtonClick:(id)sender {
    
    newBusinessView *NewBusinessScreenObj = [self.storyboard instantiateViewControllerWithIdentifier:@"newbusinessID"];
    [self.navigationController pushViewController:NewBusinessScreenObj animated:YES];
}

- (IBAction)EinButtonclick:(id)sender {
    
    [self showAlertEin:@"If you have no EIN input 00-0000000"];
}


#pragma mark VOID METHOD.


- (void)showAlertEin:(NSString *)message {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"NO EIN?"
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   
                               }];
    [alert addAction:okButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}


- (void)showAlert:(NSString *)message {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   
                               }];
    [alert addAction:okButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}
- (void)showAlertAddBusiness:(NSString *)message {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   
                                   newBusinessView *NewBusinessScreenObj = [self.storyboard instantiateViewControllerWithIdentifier:@"newbusinessID"];
                                   [self.navigationController pushViewController:NewBusinessScreenObj animated:YES];
                                   
                                   
                               }];
    [alert addAction:okButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}



-(void)CheckValidationForBusinessSignUp{
    
    if ([NaturalWineCorks isEqualToString:@"0"] && [Clothes isEqualToString:@"0"] && [Plastic isEqualToString:@"0"]) {
        
        
        UIScrollView *scrollView = MainScrollview;
        CGRect frame = scrollView.frame;
        frame.origin.y = 0;
        [scrollView scrollRectToVisible:frame animated:YES];
        
        [self showAlert:@"Please select material(s)"];
        
    }else if (CompanyNameTextField.text.length == 0) {
        
        [CompanyNameTextField becomeFirstResponder];
        [self showAlert:@"Input your company name"];
        
    } else if (PhoneNumberTextField.text.length < 1 || PhoneNumberTextField.text.length > 25) {
        
        [PhoneNumberTextField becomeFirstResponder];
        [self showAlert:@"Enter a valid phone number."];
        [PhoneNumberTextField.text isEqualToString:@""];
        
    }else if ([JobTitle isEqualToString:@""]) {
        
        
        UIScrollView *scrollView = MainScrollview;
        CGRect frame = scrollView.frame;
        frame.origin.y = 433;
        [scrollView scrollRectToVisible:frame animated:YES];
        
        [self showAlert:@"Select Job title"];
        
        
    }else if (Address1TextField.text.length == 0) {
        
        [Address1TextField becomeFirstResponder];
        [self showAlert:@"Address cannot be empty"];
    }
    
    else if (Address1TextField.text.length == 0) {
        
        [Address1TextField becomeFirstResponder];
        [self showAlert:@"Address cannot be empty"];
        
    }else if (FirstNameTextField.text.length == 0) {
        
        [FirstNameTextField becomeFirstResponder];
        [self showAlert:@"Input your first name"];
        
        
    }else if (LastnameTextField.text.length == 0) {
        
        [LastnameTextField becomeFirstResponder];
        [self showAlert:@"Input your last name"];
        
    }else if  (CompanyEINTextField.text.length < 10) {
        
        [CompanyEINTextField becomeFirstResponder];
        [self showAlert:@"Please enter a valid company ein"];
        
        
    } else if ([SignUpLabel.text isEqual: @"Add a business"]) {
        
        if ([[Address1TextField text] length] > 0) {
            [self signupBusiness:[NSString stringWithFormat:@"%f", businessLocation.latitude] longitude:[NSString stringWithFormat:@"%f", businessLocation.longitude]];
        }
    }else if ([SignUpLabel.text isEqual: @"Edit a Business"]) {
        //implement api /api/v1/business/save
        [self showProgressView];
        [LocationManager reverseGeoCodeWithAddress:Address1TextField.text success:^(NSString * _Nonnull lat, NSString * _Nonnull longitude) {
            [self updateBusiness:lat longitude:longitude];
        } failure:^(NSError * _Nullable error) {
            [self handleError:error];
        }];
    }

}

-(void)signupBusiness:(NSString *)lat longitude:(NSString *)longitude {
    BOOL isCork = ![NaturalWineCorks isEqualToString:@"0"];
    BOOL isCloth = ![Clothes isEqualToString:@"0"];
    BOOL isPlastic = ![Plastic isEqualToString:@"0"];
    
    [self showProgressView];
    
    [BusinessService addBusinessWithFirst_name:FirstNameTextField.text last_name:LastnameTextField.text company_name:CompanyNameTextField.text cork_business:isCork cloth_business:isCloth plastic_business:isPlastic phone:PhoneNumberTextField.text job_title:JobTitle lat:lat lng:longitude address1:Address1TextField.text address2:Address2TextField.text company_ein:CompanyEINTextField.text success:^(BOOL status) {
        [self hideProgressView];
        [self showAlertAddBusiness:@"Business successfully added, but we must confirm your EIN."];
    } failure:^(NSError * _Nullable error) {
        [self handleError:error];
        [self hideProgressView];
    }];
}

-(void)updateBusiness:(NSString *)lat longitude:(NSString *)longitude {
    BOOL isCork = ![NaturalWineCorks isEqualToString:@"0"];
    BOOL isCloth = ![Clothes isEqualToString:@"0"];
    
    [self showProgressView];
    
    [BusinessService saveBusinessWithFirst_name:FirstNameTextField.text last_name:LastnameTextField.text company_name:CompanyNameTextField.text cork_business:isCork cloth_business:isCloth phone:PhoneNumberTextField.text job_title:JobTitle lat:lat lng:longitude address1:Address1TextField.text address2:Address2TextField.text company_ein:CompanyEINTextField.text business_id:self.selectedBusiness.id success:^(BOOL status) {
        [self hideProgressView];
        [self showAlertAddBusiness:@"Business successfully updated."];
    } failure:^(NSError * _Nullable error) {
        [self handleError:error];
        [self hideProgressView];
    }];
}

-(BOOL)myMobileNumberValidate:(NSString*)number
{
    NSString *numberRegEx = @"[0-9]{10}";
    NSPredicate *numberTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", numberRegEx];
    if ([numberTest evaluateWithObject:number] == YES)
        return TRUE;
    else
        return FALSE;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == CompanyNameTextField) {
        [textField resignFirstResponder];
        [PhoneNumberTextField becomeFirstResponder];
    }
    else if (textField == PhoneNumberTextField) {
        [textField resignFirstResponder];
        [Address1TextField becomeFirstResponder];
    }
    else if (textField == Address1TextField) {
        [textField resignFirstResponder];
        [Address2TextField becomeFirstResponder];
    }
    
    else if ( textField == Address2TextField)
    {
        [textField resignFirstResponder];
        [FirstNameTextField becomeFirstResponder];
        
    }
    else if (textField  == FirstNameTextField) {
        [textField resignFirstResponder];
        [LastnameTextField becomeFirstResponder];
        
    }
    else if (textField == LastnameTextField) {
        [textField resignFirstResponder];
        [CompanyEINTextField becomeFirstResponder];
    }
    
    return YES;
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
    }];
    return [super canPerformAction:action withSender:sender];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    
    
    currenttextField = textField;
    
    if ([string isEqualToString:@" "] && textField.text.length == 0)
    {
        return NO;
        
    }
    
//    if (textField ==  PhoneNumberTextField) {
//
//        int length = (int)[self getLength:textField.text];
//
//
//        if(length == 10)
//        {
//            if(range.length == 0)
//                return NO;
//        }
//
//        if(length == 3)
//        {
//            NSString *num = [self formatNumber:textField.text];
//            textField.text = [NSString stringWithFormat:@"%@-",num];
//
//            if(range.length > 0)
//                textField.text = [NSString stringWithFormat:@"%@",[num substringToIndex:3]];
//        }
//        else if(length == 6)
//        {
//            NSString *num = [self formatNumber:textField.text];
//
//            textField.text = [NSString stringWithFormat:@"%@-%@-",[num  substringToIndex:3],[num substringFromIndex:3]];
//
//            if(range.length > 0)
//                textField.text = [NSString stringWithFormat:@"%@-%@",[num substringToIndex:3],[num substringFromIndex:3]];
//        }
//
//    }
    
    if (textField ==  CompanyEINTextField) {
        
        int length = (int)[self getLength:textField.text];
        
        
        if(length == 9)
        {
            if(range.length == 0)
                return NO;
        }
        
        if(length == 2)
        {
            NSString *num = [self formatNumber:textField.text];
            textField.text = [NSString stringWithFormat:@"%@-",num];
            
            if(range.length > 0)
                textField.text = [NSString stringWithFormat:@"%@",[num substringToIndex:2]];
        }
        
    }
    
    return YES;
}

- (NSString *)formatNumber:(NSString *)mobileNumber
{
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    NSLog(@"%@", mobileNumber);
    
    int length = (int)mobileNumber.length;
    if(length > 10)
    {
        mobileNumber = [mobileNumber substringFromIndex: length-10];
        NSLog(@"%@", mobileNumber);
        
    }
    
    return mobileNumber;
}

- (int)getLength:(NSString *)mobileNumber
{
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    int length = (int)mobileNumber.length;
    
    return length;
}

// This section makes sure the materials the business has is check marked, when we add more material, we go here.

- (void)usersMaterials{
    
    if (self.selectedBusiness.cork_business == 1) {
        [self NaturalWineCorksButtonClick:nil];
        
    }
    
    if (self.selectedBusiness.cloth_business == 1) {
        [self ClothesButtonClick:nil];
        
    }
    
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if (textField == Address1TextField) {
        GMSAutocompleteViewController *acController = [[GMSAutocompleteViewController alloc] init];
        acController.delegate = self;
        
        // Specify a filter.
        _filter = [[GMSAutocompleteFilter alloc] init];
        _filter.type = kGMSPlacesAutocompleteTypeFilterAddress;
        _filter.country = @"US";
        acController.autocompleteFilter = _filter;
        
        // Display the autocomplete view controller.
        [self presentViewController:acController animated:YES completion:nil];
        
        return NO;
    }
    
    return YES;
}

- (void)viewController:(GMSAutocompleteViewController *)viewController didAutocompleteWithPlace:(GMSPlace *)place {
    [self dismissViewControllerAnimated:YES completion:nil];
    // Do something with the selected place.
    NSLog(@"Place name %@", place.name);
    NSLog(@"Place ID %@", place.placeID);
    NSLog(@"Place attributions %@", place.attributions.string);
    
    Address1TextField.text = place.formattedAddress;
    businessLocation = place.coordinate;
}

- (void)viewController:(GMSAutocompleteViewController *)viewController
didFailAutocompleteWithError:(NSError *)error {
    [self dismissViewControllerAnimated:YES completion:nil];
    // TODO: handle the error.
    NSLog(@"Error: %@", [error description]);
}

// User canceled the operation.
- (void)wasCancelled:(GMSAutocompleteViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

// Turn the network activity indicator on and off again.
- (void)didRequestAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)didUpdateAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}


@end
