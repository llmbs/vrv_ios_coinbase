//
//  AddressCell.h
//  VRV
//
//  Created by vnnovate on 2017-02-15.
//  Copyright © 2017 vnnovate. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddressCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *addressnamelabel;
@end
