

#import "BusinessRoleInfoView.h"
#import "BusinessSignUpView.h"
#import "CommanFunction.h"

@interface BusinessRoleInfoView ()
{
    NSUserDefaults *dlf;
}
@end

@implementation BusinessRoleInfoView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    dlf = [NSUserDefaults standardUserDefaults];
    NextRad.layer.cornerRadius= 4;

    [BackButton setBackgroundImage:[CommanFunction imageWithColor:[UIColor grayColor]] forState:UIControlStateHighlighted];
    
    UISwipeGestureRecognizer *gestureRecognizerRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandlerRight:)];
    [gestureRecognizerRight setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [self.view addGestureRecognizer:gestureRecognizerRight];
   }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark IBaction Method.

-(void)swipeHandlerRight:(id)sender {
    
   [self.navigationController popViewControllerAnimated:YES];
    
    
    
}

- (IBAction)NextButtonClick:(id)sender {
    
    
    
    BusinessSignUpView *BusinessSignUpObj = [self.storyboard instantiateViewControllerWithIdentifier:@"Bsignupid"];
    [self.navigationController pushViewController:BusinessSignUpObj animated:YES];
 
    
//    BusinessSignUpView *BusinessSignUpObj = [self.storyboard instantiateViewControllerWithIdentifier:@"Bsignupid"];
//    UIViewController *newVC = BusinessSignUpObj;
//    NSMutableArray *vcs =  [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
//    [vcs insertObject:newVC atIndex:vcs.count-1];
//    [self.navigationController setViewControllers:vcs animated:YES];
//    [self.navigationController popViewControllerAnimated:YES];
    
    
}

- (IBAction)BackButtonClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
