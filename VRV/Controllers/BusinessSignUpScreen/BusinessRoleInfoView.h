//
//  BusinessRoleInfoView.h
//  VRV
//
//  Created by vnnovate on 2017-02-14.
//  Copyright © 2017 vnnovate. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BusinessRoleInfoView : UIViewController
{
    __weak IBOutlet UIImageView *BusinessRoleinfoImageview;
    __weak IBOutlet UIButton *NextRad;
    __weak IBOutlet UIButton *BackButton;
}

- (IBAction)NextButtonClick:(id)sender;
- (IBAction)BackButtonClick:(id)sender;

@end
