

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

@class BusinessData;

@interface BusinessSignUpView : UIViewController<UITextFieldDelegate,UITableViewDelegate,UIAlertViewDelegate>
{
   
    __weak IBOutlet UIScrollView *MainScrollview;
    __weak IBOutlet UIView *MainView;
    
    
    __weak IBOutlet UILabel *SignUpLabel;
    __weak IBOutlet UITextField *CompanyNameTextField;
    __weak IBOutlet UITextField *PhoneNumberTextField;
    __weak IBOutlet UITextField *Address1TextField;
    __weak IBOutlet UITextField *Address2TextField;
    __weak IBOutlet UITextField *CompanyEINTextField;
    
    __weak IBOutlet UITextField *FirstNameTextField;
    __weak IBOutlet UITextField *LastnameTextField;
     __weak IBOutlet UILabel *SelectMaterialsTextField;
    
   
    __weak IBOutlet UIImageView *NaturalWineCorksImageview;
    __weak IBOutlet UIImageView *ClothesImageview;
    __weak IBOutlet UIImageView *PlasticImageview;
    
    __weak IBOutlet UIImageView *MenagerImageview;
    __weak IBOutlet UIImageView *Supervisorimageview;
    
   
    
    
//    NSMutableArray*arrLatlngAddress;
    NSArray *searchResultPlaces;
    
    UITextField *currenttextField;
    
    // int flag;
    

    
    __weak IBOutlet UIButton *AddNewBusinessSaveButton;
    __weak IBOutlet UIButton *AddNewBusinessCancleButton;
    __weak IBOutlet UIButton *UpdateButton;
    __weak IBOutlet UIButton *BackButton;
    __weak IBOutlet UIButton *MenuRad;
    __weak IBOutlet UIButton *EinButton;
    
    __weak IBOutlet UIButton *wineBubbleBusiness;
    __weak IBOutlet UIButton *clothBubbleBusiness;
    __weak IBOutlet UIButton *plasticBubbleBusiness;
}



@property(nonatomic, strong, readwrite) NSString *environment;

@property (nonatomic,strong) NSDictionary *BissnessInfo;
@property(assign)BOOL selectedEdit;
@property (nonatomic,strong) BusinessData *selectedBusiness;

@property (copy) void (^autoCompleteHandler) (NSArray *);

- (IBAction)BackbuttonClick:(id)sender;
-(IBAction)UpdateButtonClick:(id)sender;

- (IBAction)NaturalWineCorksButtonClick:(id)sender;
- (IBAction)ClothesButtonClick:(id)sender;


- (IBAction)ManagerButtonClick:(id)sender;
- (IBAction)SupervisorButtonClick:(id)sender;

- (IBAction)AddNewBusinessSaveButtonClick:(id)sender;
- (IBAction)AddNewBusinessCancleButtonClick:(id)sender;
- (IBAction)MenuButtonclick:(id)sender;
- (IBAction)EinButtonclick:(id)sender;

- (IBAction)PlasticButtonClick:(id)sender;



@end
