//
//  DepositViewController.h
//  VRV
//
//  Created by Elisha Dumas on 11/22/18.
//  Copyright © 2018 Abhishek Sheth. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginView.h"





@interface DepositViewController : UIViewController {
    
    __weak IBOutlet UIImageView *Circle1;
    __weak IBOutlet UIImageView *Circle2;
    __weak IBOutlet UIImageView *Circle3;
    __weak IBOutlet UIImageView *Circle4;
    __weak IBOutlet UIImageView *WhiteArrow;
    
    __weak IBOutlet UIImageView *CenterImage;
    __weak IBOutlet UILabel *Description;
    __weak IBOutlet UILabel *Header;
    __weak IBOutlet UILabel *ContestSub;
    
    __weak IBOutlet UIButton *arrowDepositB;
    __weak IBOutlet UIButton *arrowRecycleB;
    __weak IBOutlet UIButton *arrowAllocationB;
    __weak IBOutlet UIButton *arrowYouTubeB;
    __weak IBOutlet UIButton *CenterImage2B;
    __weak IBOutlet UIButton *Skip;
    
    
    
    
}




- (IBAction)SkipButtonClick:(id)sender;

- (IBAction)ArrowButtonClick:(id)sender;

- (IBAction)ArrowButtonClickR:(id)sender;

- (IBAction)ArrowButtonClickA:(id)sender;

- (IBAction)YoutubeButtonClickArrow:(id)sender;

- (IBAction)YoutubeButtonClick:(id)sender;



@end


