//
//  DepositViewController.m
//  VRV
//
//  Created by Elisha Dumas on 11/22/18.
//  Copyright © 2018 Abhishek Sheth. All rights reserved.
//

#import "DepositViewController.h"
#import "RecycleViewController.h"
#import "AppDelegate.h"





@interface DepositViewController ()

@end

@implementation DepositViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    arrowRecycleB.hidden = YES;
    arrowAllocationB.hidden = YES;
    arrowYouTubeB.hidden = YES;
    CenterImage2B.hidden = YES;
    
    UISwipeGestureRecognizer *gestureRecognizerRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandlerRight:)];
    [gestureRecognizerRight setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [self.view addGestureRecognizer:gestureRecognizerRight];
    
    
    UISwipeGestureRecognizer *gestureRecognizerLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandlerLeft:)];
    [gestureRecognizerLeft setDirection:(UISwipeGestureRecognizerDirectionLeft)];
    [self.view addGestureRecognizer:gestureRecognizerLeft];
    
    
    [[UIButton appearance] setExclusiveTouch:YES];
    
    [Skip setTitle:NSLocalizedString(@"skip_intro", "") forState:UIControlStateNormal];
    
    [arrowYouTubeB setTitle:NSLocalizedString(@"done_intro", "") forState:UIControlStateNormal];
    
    // first slide
    
    Description.text = NSLocalizedString(@"intro_slide_1_text", "");
    
    ContestSub.text = NSLocalizedString(@"intro_2_raas", "");
    
   
    
    
    
    
}




-(void)swipeHandlerLeft:(id)sender {
    
   
    
    if (arrowDepositB.hidden == NO )   {
        
        [self ArrowButtonClick:self];
        
    } else if (arrowRecycleB.hidden == NO )   {
        
        [self ArrowButtonClickR:self];
        
    } else if (arrowAllocationB.hidden == NO )   {
        
        [self ArrowButtonClickA:self];
        
    }
    
}

-(void)swipeHandlerRight:(id)sender {
    
    Skip.hidden = NO;
    
    if (arrowYouTubeB.hidden == NO) {
        
        [self ArrowButtonClickR:self];
        CenterImage2B.hidden = YES;
        arrowYouTubeB.hidden = YES;
        CenterImage.hidden = NO;
        [self->Circle4 setImage:[UIImage imageNamed: @"c-brown"]];
        
        
    } else if (arrowAllocationB.hidden == NO) {
        
        [self ArrowButtonClick:self];
        [self->Circle3 setImage:[UIImage imageNamed: @"c-brown"]];
        arrowYouTubeB.hidden = YES;
        arrowAllocationB.hidden = YES;
    
    } else if (arrowRecycleB.hidden == NO) {
        
        [self->Circle1 setImage:[UIImage imageNamed: @"c-white"]];
        [self->Circle2 setImage:[UIImage imageNamed: @"c-brown"]];
        [self->CenterImage setImage:[UIImage imageNamed: @"vrv_coin"]];
        
        self->Description.text = NSLocalizedString(@"intro_slide_1_text", "");
        
        self->Header.text = @"VRVC Coin";
        
        arrowRecycleB.hidden = YES;
        arrowDepositB.hidden = NO;
        ContestSub.hidden = YES;
        
    }
 
    
    
}

-(IBAction)SkipButtonClick:(id)sender {

   LoginView *loginviewScreenObj  = [self.storyboard instantiateViewControllerWithIdentifier:@"loginid"];
   [self.navigationController pushViewController:loginviewScreenObj animated:YES];
 
}

-(IBAction)ArrowButtonClick:(id)sender {
    
    
    WhiteArrow.hidden = NO;
    Skip.hidden = NO;
   
   
    
    
    [UIView animateWithDuration:0.5 animations:^(void) {
        self->CenterImage.alpha = 0;
        self->CenterImage.alpha = 2;
        self->Description.alpha = 0;
        self->Description.alpha = 2;
        self->Header.alpha = 0;
        self->Header.alpha = 2;
        self->ContestSub.alpha = 0;
        self->ContestSub.alpha = 2;
        
    }];

    
        [self->Circle1 setImage:[UIImage imageNamed: @"c-brown"]];
        [self->Circle2 setImage:[UIImage imageNamed: @"c-white"]];
        [self->CenterImage setImage:[UIImage imageNamed: @"recycleintro"]];
    
        self->Description.text = NSLocalizedString(@"intro_slide_2_text", "");
        self->Header.text = NSLocalizedString(@"recycle", "");
    
    
    arrowDepositB.hidden = YES;
    arrowRecycleB.hidden = NO;
    ContestSub.hidden = NO;
    self->ContestSub.text = NSLocalizedString(@"intro_2_raas", "");
    
    
    
    
   // RecycleViewController *RecycleviewScreenObj  = [self.storyboard instantiateViewControllerWithIdentifier:@"intro2"];
   // [self.navigationController pushViewController:RecycleviewScreenObj animated:YES];
    
     
}

-(IBAction)ArrowButtonClickR:(id)sender {
    
    WhiteArrow.hidden = NO;
    Skip.hidden = NO;
    
    [UIView animateWithDuration:0.5 animations:^(void) {
        self->CenterImage.alpha = 0;
        self->CenterImage.alpha = 2;
        self->Description.alpha = 0;
        self->Description.alpha = 2;
        self->Header.alpha = 0;
        self->Header.alpha = 2;
        
    }];
    
    
    [self->Circle2 setImage:[UIImage imageNamed: @"c-brown"]];
    [self->Circle3 setImage:[UIImage imageNamed: @"c-white"]];
    [self->CenterImage setImage:[UIImage imageNamed: @"controller"]];
    
    self->Description.text = NSLocalizedString(@"intro_slide_3_text", "");
    self->Header.text = NSLocalizedString(@"intro_slide_3_1_text", "");
    
    arrowRecycleB.hidden = YES;
    arrowAllocationB.hidden = NO;
    ContestSub.hidden = NO;
    
    self->ContestSub.text = NSLocalizedString(@"intro_slide_2x1_text", "");

}

-(IBAction)ArrowButtonClickA:(id)sender {
    
    WhiteArrow.hidden = YES;
    CenterImage.hidden = YES;
    CenterImage2B.hidden = NO;
    Skip.hidden = YES;
  
    
    [UIView animateWithDuration:0.5 animations:^(void) {
        self->CenterImage2B.alpha = 0;
        self->CenterImage2B.alpha = 2;
        self->Description.alpha = 0;
        self->Description.alpha = 2;
        self->Header.alpha = 0;
        self->Header.alpha = 2;
        
    }];
    
    
    [self->Circle3 setImage:[UIImage imageNamed: @"c-brown"]];
    [self->Circle4 setImage:[UIImage imageNamed: @"c-white"]];
    
    
    self->Description.text = NSLocalizedString(@"intro_slide_4_text", "");
    self->Header.text = NSLocalizedString(@"vrvtutorial", "");
    
    arrowAllocationB.hidden = YES;
    arrowYouTubeB.hidden = NO;
    ContestSub.hidden = YES;
    
    
}

-(IBAction)YoutubeButtonClickArrow:(id)sender {
    
    LoginView *loginviewScreenObj  = [self.storyboard instantiateViewControllerWithIdentifier:@"loginid"];
    [self.navigationController pushViewController:loginviewScreenObj animated:YES];
}

-(IBAction)YoutubeButtonClick:(id)sender {
    
    NSURL *tutorial = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/apple_vrv_deposit"];
    
    [[UIApplication sharedApplication] openURL: tutorial options:@{} completionHandler:^(BOOL success) {
        if (success)
            NSLog(@"Opened tutorial");
    }];

    
}
    
     
@end
