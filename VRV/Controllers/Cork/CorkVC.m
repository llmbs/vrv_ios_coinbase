//
//  CorkVC.m
//  VRV
//
//  Created by My Mac on 01/09/21.
//  Copyright © 2021 Abhishek Sheth. All rights reserved.
//

#import "CorkVC.h"


#import "Defaults.h"
#import "AFNetworking.h"
#import "CommanFunction.h"
#import "LeftRightViewController.h"
#import "Reachability.h"
#import "VRV-Swift.h"
#import "TurnInView.h"
#import "ContestWebViewController.h"
#import <Lottie/Lottie.h>
#import "ScoreAndRankScreen.h"

@interface CorkVC ()
{
    //  NSUserDefaults *dlf;
     //  NSString *ScreenIdentifier;
       
       double lastAmount;
       double lastQuantity;
       NSString *type;
       BOOL paymentSuceeded;
       BOOL noCoin;
       NSString *usdcString;
       NSString *bnbString;
       BOOL internet;
       BOOL usdcPayidGenerated ;
       
       NSError *lastError;
}
@property (nonatomic, strong) LOTAnimationView *lottieLogo;



@end
@implementation CorkVC


- (void)viewDidLoad {
    [super viewDidLoad];
    
    // [dlf setBool:false forKey:@"scan"];     // [self.menuContainerViewController setPanMode:MFSideMenuPanModeCenterViewController];

    // dlf = [NSUserDefaults standardUserDefaults];
     
     // Do any additional setup after loading the view.
     
     self->MainScrollview.frame=CGRectMake( 0, CGRectGetMaxY(self->tabHeader.frame), self->MainScrollview.frame.size.width, self->MainScrollview.frame.size.height);

     MainScrollview.contentSize=CGSizeMake(MainScrollview.frame.size.width, MainScrollview.frame.size.height);
     MainScrollview.scrollEnabled = YES;
     
     
     
     UISwipeGestureRecognizer* reconizer;
     reconizer = [[UISwipeGestureRecognizer alloc] init];
     reconizer.delaysTouchesBegan = TRUE;
    // [MainScrollview addGestureRecognizer:reconizer];
     
     reconizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(SwipeRecognizer:)];
     reconizer.direction = UISwipeGestureRecognizerDirectionRight; // default
    // [MainScrollview addGestureRecognizer:reconizer];
    // [MainScrollview delaysContentTouches];
     
     reconizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(SwipeRecognizer:)];
     reconizer.direction = UISwipeGestureRecognizerDirectionLeft; // default
     //[MainScrollview addGestureRecognizer:reconizer];
    // [MainScrollview delaysContentTouches];
     

     
  
     
     BNB.hidden = YES;
     USDC.hidden = YES;
     
    
     
     
     for (UIButton *btn in _AllButtonCollections) {
         [btn setBackgroundImage:[CommanFunction imageWithColor:[UIColor whiteColor]] forState:UIControlStateHighlighted];
         btn.layer.cornerRadius = 4;
     }
     
     [[UIButton appearance] setExclusiveTouch:YES];
     
     MaterialDepositTextField.text = @"$2.00";
     MaterialDepositTextField.delegate = self;
     
     
//     self.lottieLogo = [LOTAnimationView animationNamed:@"circle"];
//     self.lottieLogo.frame = CGRectMake(0, 0, 400, 400);
//    self.lottieLogo.center = CGPointMake(gamer.center.x + 22 , 68 + 22);
//
//    self.lottieLogo.center = gamer.center;
//     self.lottieLogo.contentMode = UIViewContentModeScaleAspectFill;
//     self.lottieLogo.loopAnimation = YES;
//     [gamer addSubview:self.lottieLogo];
//     [self.lottieLogo play];
     
    
    /*/*Turn in lottie */
    
    self.lottieLogo = [LOTAnimationView animationNamed:@"circle"];
    [self.view bringSubviewToFront:hourGlass];
    
    self.lottieLogo.frame = CGRectMake(0, 0, 450 , 450);
    
    self.lottieLogo.center = wholeview.center;
    
    hgContainer.center = self.lottieLogo.center;
    
    self.lottieLogo.contentMode = UIViewContentModeScaleAspectFill;
    self.lottieLogo.loopAnimation = YES;
    [self.view addSubview:self.lottieLogo];
    [self.lottieLogo play];

    [self.view insertSubview:hgContainer aboveSubview:self.lottieLogo];
    
    
    /*/*Game on lottie */
     
    self.lottieLogo = [LOTAnimationView animationNamed:@"turn_in"];
    self.lottieLogo.frame = CGRectMake(0, 0, 125 , 125);
   // self.lottieLogo.center = whiteArea.center;
    
    
    self.lottieLogo.center = CGPointMake(whiteArea.center.x,192	);
    
    self.lottieLogo.contentMode = UIViewContentModeScaleAspectFill;
    self.lottieLogo.loopAnimation = YES;
    [self.view addSubview:self.lottieLogo];
    [self.lottieLogo play];
    [self.view bringSubviewToFront:wholeview];
    
    
    
     internet = YES;
     usdcPayidGenerated = NO;
    
    
    /*/Localization storyboard section*/
    
    ready_to_recycle.text = NSLocalizedString(@"ready_to_recycle", "");
    
    [rtr_help setTitle:NSLocalizedString(@"rtr_help", "") forState:(UIControlStateNormal)];
    
    turn_in.text = NSLocalizedString(@"turn_in", "");
    
    rtr_car_keys.text = NSLocalizedString(@"rtr_car_keys", "");
    
    Game_on.text = NSLocalizedString(@"puzzle", "");
    
    corkq.text = NSLocalizedString(@"wcq", "");
    
    input_01.text = NSLocalizedString(@"input_01", "");
    
    vrv_coin.text = NSLocalizedString(@"vrv_coin", "");
    
    [business_access_submit setTitle:NSLocalizedString(@"buy_now", "") forState:(UIControlStateNormal)];
    
    [retrieve_address setTitle:NSLocalizedString(@"retrieve_address", "") forState:(UIControlStateNormal)];
    
    
    MaterialQuantityTxt.placeholder = NSLocalizedString(@"input_01", "");
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark Void Method.

// check internet with bool

-(void)checkInternetConnection {
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
        [reachability startNotifier];
        NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
        if (remoteHostStatus == NotReachable)
        {
            [self showAlert:NSLocalizedString(@"internetActivity_off", "")];
            [self hideProgressView];
            
            
            // internet is off indicator
            internet = NO;
    
        }
    
}

-(void)showAlertGameOn {
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:NSLocalizedString(@"Game_on", "")
                                  message:NSLocalizedString(@"b4_gaming", "")
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* exitButton = [UIAlertAction
                                    actionWithTitle:NSLocalizedString(@"rpf_exit", "")
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        
                                        
                                        }];
    
    UIAlertAction* scoreButton = [UIAlertAction
                                    actionWithTitle:NSLocalizedString(@"score_rank", "")
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        
        [self checkRankWc];
    
                                        }];
    
    UIAlertAction* playButton = [UIAlertAction
                                    actionWithTitle:NSLocalizedString(@"rpf_play", "")
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
        
        [self gamerQualifyApi];
                                        
                                        }];
    
    
    [alert addAction:playButton];
    [alert addAction:scoreButton];
    [alert addAction:exitButton];
    
    
    [self presentViewController:alert animated:YES completion:nil];
    
    
}

// USDC ALERT DIALOG

//-(void)showAlertUsdc:(NSString *)message {
//    
//    
//    UIAlertController * alert=   [UIAlertController
//                                  alertControllerWithTitle:NSLocalizedString(@"usdcwallet", "")
//                                  message:NSLocalizedString(@"cashprize", "")
//                                  preferredStyle:UIAlertControllerStyleAlert];
//    
//    UIAlertAction* submit = [UIAlertAction
//                                   actionWithTitle:NSLocalizedString(@"business_access_submit", "")
//                                   style:UIAlertActionStyleDefault
//                                   handler:^(UIAlertAction * action)
//                                   {
//        
//                                   //check internet connection
//                                    [self checkInternetConnection];
//        
//        
//                                    if (self->internet == NO) {
//                                        
//                                        //debugger log
//                                        NSLog(@"internet is offline.");
//                                        
//                                        
//                                        // hide usdc icon
//                                        self->USDC.hidden = YES;
//                                        self->tabHeader.hidden = NO;
//                                        
//                                        
//                                        //reset internet bool for
//                                        
//                                        self->internet = YES;
//                                        NSLog(@"reset internet bool");
//                                        
//                                    } else {
//        
//                                    
//                                    if ([self->usdcString isEqualToString:@""]) {
//                                        
//                                        self->USDC.hidden = YES;
//                                        self->tabHeader.hidden = NO;
//                                        
//                                        [self quickAlert:NSLocalizedString(@"error_address_empty", "")];
//                                        
//                                    } else if (self->usdcString.length < 42) {
//                                    
//                                        self->USDC.hidden = YES;
//                                        self->tabHeader.hidden = NO;
//                                        
//                                    [self quickAlert:NSLocalizedString(@"inputvalid", "")];
//                                    
//                                    } else {
//                                        
//                                        [self usdcPostApi];
//                                        
//                                    }
//        
//                                }
//        
//        
//                                   }];
//    
//    UIAlertAction* usdchelp = [UIAlertAction
//                                    actionWithTitle:NSLocalizedString(@"getaddress", "")
//                                    style:UIAlertActionStyleDefault
//                                    handler:^(UIAlertAction * action)
//                                    {
//                                        
//                                        NSURL *instantad = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/busd"];
//                                        
//                                        [[UIApplication sharedApplication] openURL: instantad options:@{} completionHandler:^(BOOL success) {
//                                            if (success)
//                                                NSLog(@"Opened busd address support");
//                                            
//                                            self->tabHeader.hidden = NO;
//                                            self->USDC.hidden = YES;
//                                            
//                                        }];
//                                        
//                                    }];
//    
//    
//                        UIAlertAction* cancel = [UIAlertAction
//                                                       actionWithTitle:NSLocalizedString(@"txt_cancel", "")
//                                                       style:UIAlertActionStyleDefault
//                                                       handler:^(UIAlertAction * action)
//                                                       {
//                            
//                                                           self->tabHeader.hidden = NO;
//                                                           self->USDC.hidden = YES;
//                                                           
//                                                       }];
//                        
//    
//    
//    [alert addAction:submit];
//    [alert addAction:usdchelp];
//     [alert addAction:cancel];
//    
//    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
//        textField.delegate = self;
//        textField.tag = 1002;
//        textField.placeholder = NSLocalizedString(@"input_blockchain_address", "");
//        textField.keyboardType = UIKeyboardTypeDefault;
//        
//    }];
//    
//    [self presentViewController:alert animated:YES completion:nil];
//    
//    
//}


// BNB ALERT DIALOG

//-(void)showAlertBnb:(NSString *)message {
//    
//    
//    BNB.hidden = NO;
//    tabHeader.hidden = YES;
//    
//    
//    UIAlertController * alert=   [UIAlertController
//                                  alertControllerWithTitle:NSLocalizedString(@"bnb_current_funds", "")
//                                  message:NSLocalizedString(@"receive_vrvc", "")
//                                  preferredStyle:UIAlertControllerStyleAlert];
//    
//    UIAlertAction* SubmitButton = [UIAlertAction
//                                   actionWithTitle:NSLocalizedString(@"business_access_submit", "")
//                                   style:UIAlertActionStyleDefault
//                                   handler:^(UIAlertAction * action)
//                                   {
//        
//        
//        //check internet connection
//                                    [self checkInternetConnection];
//        
//        
//                                    if (self->internet == NO) {
//                                        
//                                        //debugger log
//                                        NSLog(@"internet is offline.");
//                                        
//                                        // hide bnb icon
//                                        self->BNB.hidden = YES;
//                                        self->tabHeader.hidden = NO;
//                                        
//                                        
//                                        //reset internet bool for
//                                        
//                                        self->internet = YES;
//                                        NSLog(@"reset internet bool");
//                                        
//                                    } else {
//        
//        
//        if ([self->bnbString isEqualToString:@""]) {
//            
//            self->BNB.hidden = YES;
//            self->tabHeader.hidden = NO;
//            
//            [self quickAlert:NSLocalizedString(@"error_address_empty", "")];
//            
//        } else if (self->bnbString.length < 42) {
//            
//            self->BNB.hidden = YES;
//            self->tabHeader.hidden = NO;
//        
//        [self quickAlert:NSLocalizedString(@"inputvalid", "")];
//        
//        
//        }else {
//            
//            [self bnbPostApi];
//            
//        }
//                        
//                                    }
//                                       
//                                   }];
//    
//    
//    
//    UIAlertAction* GetAddressButton = [UIAlertAction
//                                   actionWithTitle:NSLocalizedString(@"getaddress", "")
//                                   style:UIAlertActionStyleDefault
//                                   handler:^(UIAlertAction * action)
//                                   {
//        
//        
//        self->BNB.hidden = YES;
//        self->tabHeader.hidden = NO;
//        
//        
//        NSURL *accepted = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/binance"];
//        
//        [[UIApplication sharedApplication] openURL: accepted options:@{} completionHandler:^(BOOL success) {
//            if (success)
//                NSLog(@"Opened binance wallet support");
//            
//        }];
//        
//                                       
//                                   }];
//    
//    
//    UIAlertAction* CancelButton = [UIAlertAction
//                                    actionWithTitle:NSLocalizedString(@"txt_cancel", "")
//                                    style:UIAlertActionStyleDefault
//                                    handler:^(UIAlertAction * action)
//                                    {
//                                        
//               self->BNB.hidden = YES;
//               self->tabHeader.hidden = NO;
//                                        
//                                        
//                                    }];
//    
//    [alert addAction:SubmitButton];
//    [alert addAction:GetAddressButton];
//    [alert addAction:CancelButton];
//    
//    
//    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
//        textField.delegate = self;
//        textField.tag = 1001;
//        textField.placeholder = NSLocalizedString(@"input_blockchain_address", "");
//        textField.keyboardType = UIKeyboardTypeDefault;
//        
//    }];
//    
//    
//    [self presentViewController:alert animated:YES completion:nil];
//}


////cant send email
//
//-(void)showAlertEmail:(NSString *)message {
//    UIAlertController * alert=   [UIAlertController
//                                  alertControllerWithTitle:@"Restore 'Mail'?"
//                                  message:message
//                                  preferredStyle:UIAlertControllerStyleAlert];
//
//    UIAlertAction* CancelButton = [UIAlertAction
//                               actionWithTitle:@"Cancel"
//                               style:UIAlertActionStyleDefault
//                               handler:^(UIAlertAction * action)
//                               {
//
//                               }];
//
//    UIAlertAction* RestoreButton = [UIAlertAction
//                                   actionWithTitle:@"Restore"
//                                   style:UIAlertActionStyleDefault
//                                   handler:^(UIAlertAction * action)
//                                   {
//
//                                       NSURL *instantad = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/apple_mail"];
//
//                                       [[UIApplication sharedApplication] openURL: instantad options:@{} completionHandler:^(BOOL success) {
//                                           if (success)
//                                               NSLog(@"Opened apple mail in app store");
//
//                                   }];
//
//                                  }];
//
//    [alert addAction:RestoreButton];
//    [alert addAction:CancelButton];
//
//    [self presentViewController:alert animated:YES completion:nil];
//}




// Vrv deposit UIAlert

//- (void)showAlertDeposit:(NSString *)message amount:(double)amount quantity:(double)quantity type:(NSString *)type {
//    
//    UIAlertController * alert=   [UIAlertController
//                                  alertControllerWithTitle:NSLocalizedString(@"vrv_coin", "")
//                                  message:message
//                                  preferredStyle:UIAlertControllerStyleAlert];
//    
//    
//
//    
//    
//    UIAlertAction* dismissButton = [UIAlertAction
//                                    actionWithTitle:NSLocalizedString(@"txt_dismiss", "")
//                                    style:UIAlertActionStyleDefault
//                                    handler:^(UIAlertAction * action)
//                                    {
//                                        
//                                //reset material quantity
//                        self->MaterialQuantityTxt.text = @"";
//                       
//                                        
//                                    }];
//    
//    
//    UIAlertAction* okButton = [UIAlertAction
//                               actionWithTitle:NSLocalizedString(@"dialog_ok", "")
//                               style:UIAlertActionStyleDefault
//                               handler:^(UIAlertAction * action)
//                               {
//    
//        
//                    //reset material quantity
//                     self->MaterialQuantityTxt.text = @"";
//                  
//        
//           
//            [self showProgressView];
//        
//
//            Reachability *reachability = [Reachability reachabilityForInternetConnection];
//            [reachability startNotifier];
//            NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
//            if (remoteHostStatus == NotReachable)
//            {
//                [self showAlert:NSLocalizedString(@"internetActivity_off", "")];
//                [self hideProgressView];
//        
//            } else {
//        
//        
//       
//        [RecyclingService recycleCheckDepositWithSuccess:^(NSInteger code) {
//            
//            if (code == 421) {
//                NSLog(@"%ld",(long)code);
//                [self hideProgressView];
//                [self showAlert:NSLocalizedString(@"already_paid_para", "")];
//                
//                
//            }else{
//                NSLog(@"%ld",(long)code);
//                [self hideProgressView];
//                
//                //coinbase API
//                [self payViaCoinbase:amount quantity:quantity type:type];
//            }
//
//        } failure:^(NSError * _Nullable error) {
//            [self hideProgressView];
//
//        }];
//    
//}
//                                   
//        }];
//    
//    
//    [alert addAction:dismissButton];
//    [alert addAction:okButton];
//    [self presentViewController:alert animated:YES completion:nil];
//    
//}

// UiAlert with no title

- (void)showAlert:(NSString *)message {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"dialog_ok", "")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   
                               }];
    [alert addAction:okButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}



// COINBASE API COPY USDC ADDRESS

- (void)showAlertAddresses:(NSString *)message usdc:(NSString *)usdc {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction* copyButton = [UIAlertAction
                                  actionWithTitle:NSLocalizedString(@"copy", "")
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action)
                                  {

           //  Need function to copy the displayed usdc address for the user.

        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        pasteboard.string = usdc;
        
        [self quickAlert:NSLocalizedString(@"address_copied", "")];

       }];




    [alert addAction:copyButton];
    [self presentViewController:alert animated:YES completion:nil];
}

//- (void)showAlertHourGlass:(NSString *)message {
//    UIAlertController * alert=   [UIAlertController
//                                  alertControllerWithTitle:@"Payment Successful"
//                                  message:message
//                                  preferredStyle:UIAlertControllerStyleAlert];
//
//    UIAlertAction* doneButton = [UIAlertAction
//                               actionWithTitle:@"Done"
//                               style:UIAlertActionStyleDefault
//                               handler:^(UIAlertAction * action)
//                               {
//
//                                    // after user paid and see in the app that payment is done and confirmed
//
//
//
//                               }];
//    [alert addAction:doneButton];
//
//    [self presentViewController:alert animated:YES completion:nil];
//}

//- (void)showAlertMaterials:(NSString *)message {
//    UIAlertController * alert=   [UIAlertController
//                                  alertControllerWithTitle:@"Materials"
//                                  message:message
//                                  preferredStyle:UIAlertControllerStyleAlert];
//
//    UIAlertAction* okButton = [UIAlertAction
//                               actionWithTitle:@"OK"
//                               style:UIAlertActionStyleDefault
//                               handler:^(UIAlertAction * action)
//                               {
//
//                               }];
//
//    UIAlertAction* suggestionButton = [UIAlertAction
//                               actionWithTitle:@"Suggestions"
//                               style:UIAlertActionStyleDefault
//                               handler:^(UIAlertAction * action)
//                               {
                                   
//                                   NSURL *instantad = [NSURL URLWithString:@"https://trello.com/c/LPgt7Lfz"];
//
//                                   [[UIApplication sharedApplication] openURL: instantad options:@{} completionHandler:^(BOOL success) {
//                                       if (success)
//                                           NSLog(@"Opened trello suggestion");
//
//                                   }];
//
//                                   if ([MFMailComposeViewController canSendMail])
//                                   {
//                                           MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
//                                           mail.mailComposeDelegate = self;
//                                           [mail setSubject:@"Material Suggestion"];
//                                           [mail setMessageBody:@"I would like to see 'Type your material' in the app. Thank you." isHTML:NO];
//                                           [mail setToRecipients:@[@"Materials@voluntaryrefundvalue.com"]];
//
//                                           [self presentViewController:mail animated:YES completion:NULL];
//                                   }
//                                   else
//                                   {
//                                          NSLog(@"This device cannot send the email");
//                                       [self showAlertEmail:@"You must restore the app called 'Mail', which is no longer on your iPhone. You can restore it from the App Store by touching Restore. \n\n Furthermore, once you restore the app called 'Mail', you must save and add your email account to it."];
//
//                                   }
//
//
//                               }];
//
//    UIAlertAction* currentMaterialsButton = [UIAlertAction
//                                       actionWithTitle:@"Materials"
//                                       style:UIAlertActionStyleDefault
//                                       handler:^(UIAlertAction * action)
//                                       {
//
//                                           NSURL *instantad = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/current_materials"];
//
//                                           [[UIApplication sharedApplication] openURL: instantad options:@{} completionHandler:^(BOOL success) {
//                                               if (success)
//                                                   NSLog(@"Opened current_materials.pdf");
//
//                                           }];
//
//                                       }];
//
//    [alert addAction:okButton];
//    [alert addAction:currentMaterialsButton];
//    [alert addAction:suggestionButton];
//
//    [self presentViewController:alert animated:YES completion:nil];
//}

- (void)showAlertNoDeposit:(NSString *)message {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:NSLocalizedString(@"contests", "")
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"dialog_ok", "")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   
                               }];
    
   
    [alert addAction:okButton];
    
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)showAlertGameRules:(NSString *)message {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:NSLocalizedString(@"puzzle_promotion", "")
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction* playButton = [UIAlertAction
                                    actionWithTitle:NSLocalizedString(@"rpf_play", "")
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        
                                       // [self checkInternet];
                                     [self startGame];
                                    }];
    
    
   
    UIAlertAction* exitButton = [UIAlertAction
                                      actionWithTitle:NSLocalizedString(@"rpf_exit", "")
                                      style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction * action)
                                      {
                                          
                                        
                                      }];
    
    [alert addAction:playButton];
    [alert addAction:exitButton];
   
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)showAlertInternet:(NSString *)message {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:NSLocalizedString(@"internetActivity_off", "")
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"dialog_ok", "")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                 
                                   [self showAlertGameRules:NSLocalizedString(@"rules_contest_play", "")];
                                   
                               }];
    [alert addAction:okButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}


-(void)viewDidAppear:(BOOL)animated {
   
}

//The event handling method


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
}

-(void) SwipeRecognizer:(UISwipeGestureRecognizer *)sender {                     // left
    
    
    if ( sender.direction == UISwipeGestureRecognizerDirectionLeft) {
        NSLog(@"left");
      
       
        
        
    }

}

#pragma mark - Text Field Delegate

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    
    
    [MaterialQuantityTxt resignFirstResponder];
    [MaterialDepositTextField resignFirstResponder];
    
    
    return YES;
    
   
}

#pragma mark  Void Method.


-(void) quickAlert:(NSString *)Message{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:Message preferredStyle:UIAlertControllerStyleAlert];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [alertController dismissViewControllerAnimated:YES completion:^{
            
        }];
        
    });
    
}



-(void)inputFieldsReset {
    
    self->MaterialQuantityTxt.text = nil;
   // self->MaterialDepositTextField.text = nil;
    
}

//-(void)MaterialIFCheck {
//    
//    if ([MaterialQuantityTxt.text isEqualToString:@""] || ([MaterialQuantityTxt.text isEqualToString:@"0"]) ) {
//    
//        [ MaterialQuantityTxt becomeFirstResponder];
//
//        [self showAlert:NSLocalizedString(@"input_01", "")];
//        
//    }else{
//        
//        [self showAlertBnb:@""];
//        
//    }
//    
//         
//}


-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if (textField == MaterialDepositTextField) {
        

    }
    
}


//- (void) usdcPostApi {
//    [self.view setUserInteractionEnabled: NO];
//    [self showProgressView];
//    [self performSelector:@selector(enableUserinteraction:) withObject:self afterDelay:0.2];
//    
//    [RecyclingService changedTheUSDCOfACustomerWithUsdc: self->usdcString success:^(NSInteger statusCode) {
//        
//        [self hideProgressView];
//        
//        if (statusCode == 200) {
//             
//            
//            NSLog(@"Material selected is cork");
//            
//            //Successful Response
//            NSLog(@"Status code 200-> Usdc for user has been posted cork.");
//            
//            
//            // hide usdc icon
//            self->USDC.hidden = YES;
//            
//            // show tab header
//            self->tabHeader.hidden = NO;
//            
//            //initiate USDC payment dialog
//            
//            [self CoinDialog];
//            
//            
//        } else {
//            
//            [self showAlert:NSLocalizedString(@"error_busd", "")];
//            
//        }
//        
//    } failure:^(NSError * _Nullable error) {
//      //  [self handleError:error];
//        [self hideProgressView];
//        [self showAlert:NSLocalizedString(@"error_busd", "")];
//        
//        
//    }];
//}

//- (void) bnbPostApi {
//    [self.view setUserInteractionEnabled: NO];
//    [self showProgressView];
//    [self performSelector:@selector(enableUserinteraction:) withObject:self afterDelay:0.2];
//    
//    [RecyclingService changedTheBNBOfACustomerWithBnb: self->bnbString success:^(NSInteger statusCode) {
//        
//        [self hideProgressView];
//        
//        if (statusCode == 200) {
//            
//            //Successful Response Log
//            NSLog(@"Status code 200-> Bnb for user has been posted.");
//            
//            
//            // hide bnb icon
//            self->BNB.hidden = YES;
//            
//            //show USDC icon
//            self->USDC.hidden = NO;
//            
//            
//            //Show USDC alert dialog
//            
//            [self showAlertUsdc:@""];
//            
//            
//        } else {
//            
//            // hide bnb icon
//            self->BNB.hidden = YES;
//            
//            // show tab header
//            self->tabHeader.hidden = NO;
//            
//            [self showAlert:NSLocalizedString(@"error_bnb", "")];
//            
//            
//        }
//        
//    } failure:^(NSError * _Nullable error) {
//       // [self handleError:error];
//        [self hideProgressView];
//        
//        // hide bnb icon
//        self->BNB.hidden = YES;
//        
//        // show tab header
//        self->tabHeader.hidden = NO;
//        
//        [self showAlert:NSLocalizedString(@"error_bnb", "")];
//        
//    }];
//}

- (void)createContast {
    //Implement with these response codes so I can do the proper alert dialogs on my end.
    // response code 426
    // response code 425
    // response code 427
    // response code 421
    // if there is no response code 426,425,427,421 then use ELSE and implement  /api/v1/contest/start_game
    [ContestService createContestWithSuccess:^(NSInteger code) {
        if (code == 426) {
            [self showAlert:NSLocalizedString(@"please_wait_para", "")];
            [self hideProgressView];
            
        }else if (code == 425) {
            [self showAlert:NSLocalizedString(@"err_no_contests", "")];
            [self hideProgressView];
            
        }else if (code == 427) {
            [self showAlertNoDeposit:NSLocalizedString(@"err_contest_time", "")];
            [self hideProgressView];
            
        }else if (code == 415) {
                [self showAlert:NSLocalizedString(@"err_already_played", "")];
                [self hideProgressView];
                
            }else if (code == 421) {
                [self showAlert:NSLocalizedString(@"err_already_played", "")];
            [self hideProgressView];
            
        }else if (code == 420) {
                    
                [self showAlert:NSLocalizedString(@"restarting_game", "")];
                [self hideProgressView];
            
        }else {
            
            [self showAlertGameRules:NSLocalizedString(@"rules_contest_play", "")];
            [self hideProgressView];
            
           // [self startGame];
        }
    } failure:^(NSError * _Nullable error) {
        //Handle Error
        [self handleError:error];
        [self hideProgressView];
    }];
}

- (void)startGame {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    //Reachability *reachability = [Reachability reachabilityWithHostName:@"www.google.com"];
    [reachability startNotifier];
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    
    if (remoteHostStatus == NotReachable)
    {
        [[self presentingViewController] dismissViewControllerAnimated:NO completion:nil];
        
        [self showAlertInternet:NSLocalizedString(@"apple_wireless", "")];
        
    } else {
        
        [ContestService startGameWithSuccess:^(NSString * _Nonnull token, NSInteger code) {
            
            [ContestWebViewController pushContestController:self token:token level:code];
            
        } failure:^(NSError * _Nullable error) {
            //Handle Error
        }];
        
    }
    
}

#pragma mark IBAction Method.


- (IBAction)PrizeButtonClick:(id)sender {
    
    NSURL *accepted = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/country_page"];
    
    [[UIApplication sharedApplication] openURL: accepted options:@{} completionHandler:^(BOOL success) {
        if (success)
            NSLog(@"Opened prize page");
        
    }];
    
}



- (IBAction)GameButtonClick:(id)sender {
    
    [self showAlertGameOn];
    
}


- (IBAction)MenuButtonClick:(id)sender {
   
    
    self->MainScrollview.frame=CGRectMake( 0, self->MainScrollview.frame.origin.y, self->MainScrollview.frame.size.width, self->MainScrollview.frame.size.height);
    
    LeftRightViewController *LeftrightviewScreenObject = [self.storyboard instantiateViewControllerWithIdentifier:@"leftright"];
    UIViewController *newVC = LeftrightviewScreenObject;
    NSMutableArray *vcs =  [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
    [vcs insertObject:newVC atIndex:vcs.count-1];
    [self.navigationController setViewControllers:vcs animated:YES];
    [self.navigationController popViewControllerAnimated:YES];
}


//- (IBAction)SubmitMaterial:(id)sender {
//    
//    [self MaterialIFCheck];
//}



-(IBAction)HowtoWinClick:(id)sender {
    
    NSURL *accepted = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/winContestPic"];
    
    [[UIApplication sharedApplication] openURL: accepted options:@{} completionHandler:^(BOOL success) {
        if (success)
            NSLog(@"Opened how to win video");
        
    }];
    
}


- (IBAction)HourGlassButtonClick:(id)sender{
    
  
        [self.view setUserInteractionEnabled: NO];
        [self showProgressView];
        [self performSelector:@selector(enableUserinteraction:) withObject:self afterDelay:0.2 ];
    
    
        [RecyclingService readyToTurnWithSuccess:^(NSInteger cloth, NSInteger cork, NSInteger plastic) {
            
            //add additional materials
            
            if (cork > 0 || cloth > 0 || plastic > 0) {
                NSLog(@"Can be turned in");
                
                [self hideProgressView];
                TurnInView *TurnInScreenObj = [self.storyboard instantiateViewControllerWithIdentifier:@"turnInDialog"];
                TurnInScreenObj.cork = cork;
                TurnInScreenObj.cloth = cloth;
                TurnInScreenObj.plastic = plastic;
                [self.navigationController pushViewController:TurnInScreenObj animated:YES];
              
                
            } else {
                
            NSLog(@"Nothing to turn in");
                [self showAlert:NSLocalizedString(@"madeBUSD_payment", "")];
                [self hideProgressView];
                
            }
            
        } failure:^(NSError * _Nullable error) {
            [self handleError:error];
        }];
   
}


- (IBAction)PaymentAddress:(id)sender {
    

        [self quickAlert:NSLocalizedString(@"burn_contest_para", "")];
        
    
}

- (BOOL)textField:(UITextField* )textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
 
    if (textField == MaterialQuantityTxt) {
        NSLog(@"Range: %@", NSStringFromRange(range));
        return ( range.location < 3 );
    
    } else if (textField == MaterialDepositTextField) {
        NSString *cleanCentString = [[textField.text componentsSeparatedByCharactersInSet: [[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];
        NSInteger centValue = [cleanCentString intValue];
        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
        NSNumber *myNumber = [f numberFromString:cleanCentString];
        NSNumber *result;
    
        
        if (string.length > 0)
        {
            centValue = centValue * 10 + [string intValue];
            double intermediate = [myNumber doubleValue] * 10 +  [[f numberFromString:string] doubleValue];
            result = [[NSNumber alloc] initWithDouble:intermediate];
        } else {
            centValue = centValue / 10;
            double intermediate = [myNumber doubleValue]/10;
            result = [[NSNumber alloc] initWithDouble:intermediate];
        }
        
        myNumber = result;
        NSLog(@"%ld ++++ %@", (long)centValue, myNumber);
        double finalAmount = [myNumber doubleValue] / 100.0f;
        if (finalAmount > 1.00) {
            finalAmount = 1.00;
        }
        NSNumber *formatedValue = [[NSNumber alloc] initWithDouble:finalAmount];
        NSNumberFormatter *_currencyFormatter = [[NSNumberFormatter alloc] init];
        [_currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
        textField.text = [_currencyFormatter stringFromNumber:formatedValue];
        return NO;
    }
    
    return YES;
    
}


// disables pasting for material quantity and amount

-(BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
    }];
    return [super canPerformAction:action withSender:sender];
}




#pragma mark  Void Method.


//disables user interaction for api execution

-(void)enableUserinteraction:(id)sender {
    
    [self.view setUserInteractionEnabled:YES];
}


// COINBASE API

//- (void)payViaCoinbase:(double)amount quantity:(int)quantity type:(NSString *)type {
//
//    lastAmount = amount;
//    lastQuantity = quantity;
//    self->type = type;
//
//    [RecyclingService createRecycleWithType: self->type quantity: [NSString stringWithFormat:@"%f", self->lastQuantity] deposit: [NSString stringWithFormat:@"%f", self->lastAmount] token: @"123" success:^(NSInteger statusCode) {
//
//
//
//        if (statusCode == 423 || statusCode == 429) {
//            [self showAlert:NSLocalizedString(@"no_coins", "")];
//            NSLog(@"Status code 423, no Coins ");
//
//        } else {
//            [self showProgressView];
//            
//            NSDictionary *data = [response valueForKey:@"data"];
//            NSDictionary *addresses = [data valueForKey:@"addresses"];
//            NSString *usdc = [addresses valueForKey:@"usdc"];
//            
//            
//            if ([usdc isEqual:@"usdc"] || statusCode == 427) {
//                
//                [self hideProgressView];
//                
//                NSString *alertString = [[NSString alloc] initWithFormat:NSLocalizedString(@"payBusd", ""), usdc];
//                [self showAlertAddresses:alertString usdc: usdc];
//                
//                
//            } else {
//                
//                
//                [self hideProgressView];
//                [self showAlert:NSLocalizedString(@"error_busd", "")];
//                
//            }
//
//        }
//
//
//    } failure:^(NSError * _Nullable error) {
//
//        [self showAlert:NSLocalizedString(@"error_busd", "")];
//
//    }];
//
//}


//COINBASE API

- (void) checkPaymentStatus:(NSString *) code {

    [RecyclingService checkPaymentStatusWith:code completion:^(NSDictionary* _Nonnull responseDict) {
        NSLog(@"%@", responseDict);

//        [self showAlertHourGlass:@"Touch Turn in for further instructions and go to the current funds screen to view your token."];

    } failure:^(NSError * _Nullable error) {
        NSLog(@"%@", error.localizedDescription);
    }];

}


//- (void)CoinDialog {
//    
//    
//    NSString *MaterialQfield=self->MaterialQuantityTxt.text;
//                 double QuantityValue = MaterialQfield.doubleValue;
//                 
//                 
//           NSString *MaterialDinput = [[self->MaterialDepositTextField.text componentsSeparatedByCharactersInSet: [[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];
//                 NSInteger DepositValue = [MaterialDinput doubleValue];
//           
//           
//           double depositDouble = DepositValue/100.0;
//           [self showAlertDeposit:NSLocalizedString(@"vrv_cc", "") amount:depositDouble quantity:QuantityValue type:@"cork"];
//    
//}


- (void) gamerQualifyApi {
    
    [self.view setUserInteractionEnabled: NO];
    [self showProgressView];
    [self performSelector:@selector(enableUserinteraction:) withObject:self afterDelay:0.2];
    
    [RecyclingService gamerQualifiedWithSuccess:^(NSInteger statusCode) {
        
        [self hideProgressView];
        
        if (statusCode == 422) {
            
            [self showAlert:NSLocalizedString(@"complete_rec_task", "")];
            NSLog(@"422 - No entry in database");
            
            
            
        } else if (statusCode == 423) {
            
            [self showAlert:NSLocalizedString(@"complete_rec_task", "")];
            NSLog(@"423 - Entry in database but recycle is no and location date is blank");
            
            
        } else if (statusCode == 424) {
            
                [self createContast];
                [self showProgressView];
            NSLog(@"424 - Entry in database recycle is yes or location date has data");
            
        } else {
            
        }
        
    } failure: ^(NSError * _Nullable error) {
        
        [self handleError:error];
        [self hideProgressView];
        
    }];
    
    
    
    
}


-(void)checkRankWc {
    
    [self.view setUserInteractionEnabled: NO];
    [self showProgressView];
    [self performSelector:@selector(enableUserinteraction:) withObject:self afterDelay:0.2];
    
    [RecyclingService checkRankWcWithSuccess:^(NSArray<ScoreRankData *>  * _Nonnull responseData, NSInteger statusCode) {
        
        [self hideProgressView];
        
        if (statusCode == 422) {
            

        } else if (statusCode == 200) {
            NSMutableArray<ScoreRankData *> *topData = [NSMutableArray arrayWithArray:responseData];
            ScoreAndRankScreen *nextVC = [[ScoreAndRankScreen alloc] init];
            [nextVC setupArray:topData];
            [self.navigationController pushViewController:nextVC animated:YES];
        } else if (statusCode == 424) {
            
        } else {
            
        }
        
    } failure: ^(NSError * _Nullable error) {
        
        [self handleError:error];
        [self hideProgressView];
        
    }];
   
}



//- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
//{
//    switch (result) {
//        case MFMailComposeResultSent:
//            NSLog(@"You sent the email.");
//            break;
//        case MFMailComposeResultSaved:
//            NSLog(@"You saved a draft of this email");
//            break;
//        case MFMailComposeResultCancelled:
//            NSLog(@"You cancelled sending this email.");
//            break;
//        case MFMailComposeResultFailed:
//            NSLog(@"Mail failed:  An error occurred when trying to compose this email");
//            break;
//        default:
//            NSLog(@"An error occurred when trying to compose this email");
//            break;
//    }
//
//    [self dismissViewControllerAnimated:YES completion:NULL];
//}

#pragma mark UITextFieldDelegate
- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    if (textField.tag == 1001) {
        self->bnbString = textField.text;
    } else if (textField.tag == 1002) {
        self->usdcString = textField.text;
    } else if (textField.tag == 1003) {
        self->bnbString = textField.text;
    } else if (textField.tag == 1004) {
        self->usdcString = textField.text;
    }
    NSLog(@"BNB String ==> %@", self->bnbString);
    NSLog(@"USDC String ==> %@", self->usdcString);
    
}

@end
