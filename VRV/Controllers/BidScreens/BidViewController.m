//
//  BidViewController.m
//  VRV
//
//  Created by Mac 02 on 26/04/17.
//  Copyright © 2017 vnnovate. All rights reserved.
//

#import "BidViewController.h"
#import "Reachability.h"
#import "AFNetworking.h"
#import "Defaults.h"
#import "CommanFunction.h"

@interface BidViewController ()
{
    NSUserDefaults *dlf;
    NSString *strdinnertime;
    NSString *strdinerminut;
    NSString *strdinersecond;
    
    int imagetag;
    // - For live charges, use PayPalEnvironmentProduction (default).
    // - To use the PayPal sandbox, use PayPalEnvironmentSandbox.
    // - For testing, use PayPalEnvironmentNoNetwork.
#define kPayPalEnvironment PayPalEnvironmentSandbox
}
@end

@implementation BidViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    dlf = [NSUserDefaults standardUserDefaults];
    imagetag = 0;
    
     MainScrollview.scrollEnabled = YES;
     MainScrollview.backgroundColor = [UIColor whiteColor];
     MainScrollview.contentSize = CGSizeMake(self.view.frame.size.width, MainView.frame.size.height);
    
    CompanyLogiView.layer.cornerRadius = 10;
    CompanyLogiView.clipsToBounds = YES;
    YoutubelinkView.layer.cornerRadius = 10;
    YoutubelinkView.clipsToBounds = YES;
    
    NoSvgImageLabel.hidden = NO;
    EntiseAmountDollarLabel.hidden = YES;
    AdvertisingAmountDollarLabel.hidden = YES;
    
    if ([_ButtonIdentifier isEqualToString:@"Breakfast"]) {
        MainImageview.image = [UIImage imageNamed:@"breakfast_bg"];
        BarImageview.image = [UIImage imageNamed:@"small_icon_breakfast"];
        BarTitle.text = @"Bid: Breakfast 7:00 am (PST)";
        EntinseDescriptionRedLabel.text = @"These funds will be used as prize money for users that watch your ad and win in the instant ad breakfast sweepstakes. The more you deposit the better. You will only pay if you are the highest bidder.";
    }else if ([_ButtonIdentifier isEqualToString:@"Lunch"]){
        MainImageview.image = [UIImage imageNamed:@"lunch_bg"];
        BarImageview.image = [UIImage imageNamed:@"small_icon_lunch"];
        BarTitle.text = @"Bid: Lunch 12:30 pm (PST)";
        EntinseDescriptionRedLabel.text = @"These funds will be used as prize money for users that watch your ad and win in the instant ad lunch sweepstakes. The more you deposit the better. You will only pay if you are the highest bidder.";
    }else if ([_ButtonIdentifier isEqualToString:@"Dinner"]){
        MainImageview.image = [UIImage imageNamed:@"dinner_bg"];
        BarImageview.image = [UIImage imageNamed:@"small_icon_dinner"];
        BarTitle.text = @"Bid: Dinner 6:00 am (PST)";
        EntinseDescriptionRedLabel.text = @"These funds will be used as prize money for users that watch your ad and win in the instant ad dinner sweepstakes. The more you deposit the better. You will only pay if you are the highest bidder.";
    }
    
   // YoutubeLinkTextField.text = @"";
    youtubelinkimageview.hidden = YES;
    // Remember last inpu link
//    NSString *youtubelinlstring = [dlf objectForKey:@"youtubelink"];
//    if (youtubelinlstring.length == 0) {
//        YoutubeLinkTextField.text = @"";
//        youtubelinkimageview.hidden = YES;
//    }else{
//        YoutubeLinkTextField.text = youtubelinlstring;
//        youtubelinkimageview.hidden = NO;
//    }

    PopUpBackView.hidden = YES;
    YoutubeLinkPopUp.hidden = YES;
    AdvertisingPopup.hidden = YES;
    
    InternetPopupBackView.hidden = YES;
    InternetPopup.hidden = YES;
    
   
    /// View Rounded Corner And Shadow Animation
    (YoutubeLinkPopUp.layer).cornerRadius = 10.0f;
    (YoutubeLinkPopUp.layer).shadowColor = [UIColor blackColor].CGColor;
    (YoutubeLinkPopUp.layer).shadowOpacity = 0.8;
    (YoutubeLinkPopUp.layer).shadowRadius = 3.0;
    (YoutubeLinkPopUp.layer).shadowOffset = CGSizeMake(2.0, 2.0);
    
    [backButton setBackgroundImage:[CommanFunction imageWithColor:[UIColor grayColor]] forState:UIControlStateHighlighted];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark IBACTION Method.

- (IBAction)BackButtonClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)SubmitButtonClick:(id)sender {
    [self ValidationMethod];
    
    
}

- (IBAction)CompanyLogoButtonClick:(id)sender {

    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Option" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // Cameara button tapped.
        NSLog(@"Cameara");
        if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            
            UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                  message:@"Device has no camera"
                                                                 delegate:nil
                                                        cancelButtonTitle:@"OK"
                                                        otherButtonTitles: nil];
            
            [myAlertView show];
            
        }else{
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.allowsEditing = YES;
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            
            [self presentViewController:picker animated:YES completion:NULL];
            
        }
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // Gallary button tapped.
        NSLog(@"Gallary");
       
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self presentViewController:picker animated:YES completion:NULL];
    }]];
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];
}

- (IBAction)YoutubeLinkButtonClick:(id)sender {
   PopUpBackView.hidden = NO;
YoutubeLinkPopUp.hidden = NO;
    
}


- (IBAction)YOutubeLinkSubmitPopupClick:(id)sender {
    if (YoutubeLinkTextField.text.length == 0) {
        [self DisplayAlertMessage:@"Enter youtube link"];
        return;
    }
   // BOOL Valide=[self checkYoutubeLink:YoutubelinkView.text ];
    
    NSString *urlString = YoutubeLinkTextField.text;
    
    NSURL *youURL = [NSURL URLWithString:[urlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]]];
    
    NSString *Encodinurlstring = [NSString stringWithFormat:@"%@",youURL];
    
    BOOL validate = [self validateUrl:Encodinurlstring];
    if (validate) {
        NSLog(@"valid dude");
        [dlf setObject:YoutubeLinkTextField.text forKey:@"youtubelink"];
        youtubelinkimageview.hidden = NO;
        PopUpBackView.hidden = YES;
        YoutubeLinkPopUp.hidden = YES;
        NSLog(@"U r in");

    }else{
        [self DisplayAlertMessage:@"Please Enter Valid Youtube Link"];
        return;
    }
}

- (IBAction)YoutubepopupCancleButtonClick:(id)sender {
   // PopUpBackView.hidden = YES;
   // YoutubeLinkPopUp.hidden = YES;
    
    NSString *urlString = YoutubeLinkTextField.text;
    
    NSURL *youURL = [NSURL URLWithString:[urlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]]];
    
    NSString *Encodinurlstring = [NSString stringWithFormat:@"%@",youURL];
    
    BOOL validate = [self validateUrl:Encodinurlstring];
    if (validate) {
        NSLog(@"valid dude");
         [[UIApplication sharedApplication] openURL:[NSURL URLWithString:YoutubeLinkTextField.text]];
    }else{
        [self DisplayAlertMessage:@"Please Enter Valid Youtube Link"];
        return;
    }

}

- (BOOL) validateUrl: (NSString *) candidate {
    NSString *urlRegEx =
    @"(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+";
    NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
    return [urlTest evaluateWithObject:candidate];
}


- (IBAction)AdvertisingOkButtonClick:(id)sender {
    PopUpBackView.hidden = YES;
    AdvertisingPopup.hidden = YES;
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)InternetPopUpOkButtonClick:(id)sender {
    InternetPopupBackView.hidden = YES;
    InternetPopup.hidden = YES;
    exit(0);
}

#pragma mark VOID METHODS.


-(void)UplaodBidDataOnServer{
    // http://vnnovate.com/voluntary/webservices/webservice.php/webservice.php?action=PostBid&user_id=64&user_type=user&bid_type=breakfast&deposite_amount=100&entice_amount=250&advertizing_amount=300&cmp_name=hfgh&youtube_link=www.youtube.com&cmp_logo=image_base_url

    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus status =[reachability currentReachabilityStatus];
    if (status == NotReachable) {
        
        InternetPopupBackView.hidden = NO;
        InternetPopup.hidden = NO;
        
    }else{
      
         [CommanFunction startanimate:LoderImageview backview:PopUpBackView];
        
        NSMutableDictionary * dictParam = [[NSMutableDictionary alloc]init];
        
        dictParam[@"user_id"] = [dlf objectForKey:KEY_LOGIN_USER_ID];
        dictParam[@"user_type"] = [dlf objectForKey:KEY_USER_Type];
        
        if ([_ButtonIdentifier isEqualToString:@"Breakfast"]) {
            dictParam[@"bid_type"] = @"breakfast";
        }else if ([_ButtonIdentifier isEqualToString:@"Lunch"]){
            dictParam[@"bid_type"] = @"lunch";
        }else if ([_ButtonIdentifier isEqualToString:@"Dinner"]){
           dictParam[@"bid_type"] = @"dinner";
        }
        
        NSString *enticeamountstring = [NSString stringWithFormat:@"%@",EnticeAmountTextField.text];
        NSString *advertisingamountstring = [NSString stringWithFormat:@"%@",AdvertisingAmountTextField.text];
        
        NSString *DepositeAmount = [self RemovePercentageSignFromAmount:DepositeAmountTextField.text];
        
        dictParam[@"deposite_amount"] = DepositeAmount;
        dictParam[@"entice_amount"] = enticeamountstring;
        dictParam[@"advertizing_amount"] = advertisingamountstring;
        dictParam[@"cmp_name"] = CompanyNameTextField.text;
        dictParam[@"youtube_link"] = YoutubeLinkTextField.text;
        
//        NSLog(@"DepositeAmount=%@",DepositeAmountTextField.text);
//        NSLog(@"enticeamount=%@",enticeamountstring);
//        NSLog(@"advertisingamount=%@",advertisingamountstring);
        
        
        NSString *strUrl=[NSString stringWithFormat:@"%@action=PostBid",BASE_URL];
        // the boundary string : a random string, that will not repeat in post data, to separate post data fields.
        NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
        
        // string constant for the post parameter 'file'. My server uses this name: `file`. Your's may differ
        NSString* FileParamConstant = @"cmp_logo";
        
        // the server url to which the image (or the media) is uploaded. Use your server url here
        NSURL* requestURL = [NSURL URLWithString:strUrl];
        
        // create request
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        request.cachePolicy = NSURLRequestReloadIgnoringLocalCacheData;
        [request setHTTPShouldHandleCookies:NO];
        request.timeoutInterval = 30;
        request.HTTPMethod = @"POST";
        
        // set Content-Type in HTTP header
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
        [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        // post body
        NSMutableData *body = [NSMutableData data];
        
        // add params (all params are strings)
        for (NSString *param in dictParam) {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@\r\n", dictParam[param]] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        // add image data
        CGSize newSize = CGSizeMake(200.0f, 200.0f);
        UIGraphicsBeginImageContext(newSize);
        [CompanyLogoImageview.image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
        UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        NSData *imageData = UIImageJPEGRepresentation(newImage, 1.0);
        
        if (imageData) {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", FileParamConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:imageData];
            [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // setting the body of the post to the reqeust
        request.HTTPBody = body;
        
        // set the content-length
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)body.length];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        
        // set URL
        request.URL = requestURL;
        
        NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        
        [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            NSMutableDictionary *dataResponse = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
            NSLog(@"dataResponds=%@",dataResponse);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                 [CommanFunction stopanimate:self->LoderImageview backview:self->PopUpBackView];
                if ([dataResponse[@"status"] isEqualToString:@"success"])
                
                {
                  //  DepositeAmountTextField.text = @"";
                    self->EnticeAmountTextField.text = @"";
                    self->AdvertisingAmountTextField.text = @"";
                    self->CompanyNameTextField.text = @"";
                    self->CompanyNameLabel.text = @"Company name";
                    self->YoutubeLinkTextField.text = @"";
                    self->CompanyLogoImageview.image = [UIImage imageNamed:@"icon_photo"];
                    self->EntiseAmountDollarLabel.hidden = YES;
                    self->AdvertisingAmountDollarLabel.hidden = YES;
                    self->youtubelinkimageview.hidden = YES;
                    [self->dlf setObject:self->YoutubeLinkTextField.text forKey:@"youtubelink"];
                    [self->dlf synchronize];
                    
                    
                    [self DisplayAlertMessage:dataResponse[@"message"]];
                }else{
                      [self DisplayAlertMessage1:dataResponse[@"message"]];
                    
                  
                }
            });
        }] resume];
    
    }
  
}

// http://vnnovate.com/voluntary/webservices/webservice.php?action=payment&user_id=64&user_type=user&first_name=abc&last_name=xyz&email=abc@gmail.com&phone=6549846545&amount=500


-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
    CompanyLogoImageview.image = chosenImage;
    imagetag = 1;
    NoSvgImageLabel.hidden = YES;
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}



-(void)ValidationMethod{
//    NSString *Depositeamountstr = [self RemovePercentageSignFromAmount:DepositeAmountTextField.text];
//    NSInteger Dipositeintstr = [Depositeamountstr integerValue];
//    NSLog(@"amountinteger=%ld",(long)Dipositeintstr);
//    if (Dipositeintstr <= 0){
//        [self DisplayAlertMessage:@"Please enter deposit amount"];
//        return;
//    }
//
//    if (Dipositeintstr < 100){
//        [self DisplayAlertMessage:@"Deposite must be $100"];
//        return;
//    }
//    if (Dipositeintstr > 100){
//        [self DisplayAlertMessage:@"Deposite must be $100"];
//        return;
//    }
//    
    NSString *Enticeamountstr = [self RemovePercentageSignFromAmount:EnticeAmountTextField.text];
    NSInteger Enticeintstr = Enticeamountstr.integerValue;
    NSLog(@"amountinteger=%ld",(long)Enticeintstr);
    if (Enticeintstr <= 0){
        [self DisplayAlertMessage:@"Please enter entice amount"];
        return;
    }
    if (Enticeintstr < 250){
        [self DisplayAlertMessage:@"Minimum entice amount must be $250"];
        return;
    }
    if (Enticeintstr > 5000){
        [self DisplayAlertMessage:@"Maximum entice amount must be $5000"];
        return;
    }
   
    NSString *Advertisingamountstr = [self RemovePercentageSignFromAmount:AdvertisingAmountTextField.text];
    NSInteger Advertisingintstr = Advertisingamountstr.integerValue;
    NSLog(@"amountinteger=%ld",(long)Advertisingintstr);
    if (Advertisingintstr <= 0){
        [self DisplayAlertMessage:@"Please enter advertising amount"];
        return;
    }
    
    NSString *advertisngamount = [self RemovePercentageSignFromAmount:AdvertisingAmountTextField.text];
    NSInteger advertisingstr = advertisngamount.integerValue;
    NSLog(@"amountinteger=%ld",(long)advertisingstr);
    
    if (advertisingstr < 250) {
        [self DisplayAlertMessage:@"Minimum advertising amount must be $250"];
        return;
    }
    
    if (Advertisingintstr > 1000000000){
        [self DisplayAlertMessage:@"Maximum advertising amount must be $1,000,000,000"];
        return;
    }
    
    if ([CompanyNameTextField.text isEqualToString:@""]) {
        [self DisplayAlertMessage:@"Please enter company name"];
        return;
    }
    
    if (imagetag == 0) {
        [self DisplayAlertMessage:@"Please upload company logo"];
        return;
    }
   
    if (YoutubeLinkTextField.text.length == 0) {
        [self DisplayAlertMessage:@"Please Enter Youtube Link"];
        return;
    }
    
    NSLog(@"U R IN");
   //
    [self TimeDifferenceCheckForBreakFastBid];
}

-(void)TimeDifferenceCheckForBreakFastBid{
    
    NSDate *now = [NSDate date];
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
    dateFormatter2.dateFormat = @"HH:mm:ss";
    dateFormatter2.timeZone = [NSTimeZone systemTimeZone];
    NSLog(@"The Current Time is %@",[dateFormatter2 stringFromDate:now]);
    NSString *strdd=[dateFormatter2 stringFromDate:now];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    formatter.dateFormat = @"HH:mm:ss";
    formatter.locale = [[NSLocale alloc]
                          initWithLocaleIdentifier:@"en_US"];
    NSDateFormatter *formatter1 = [[NSDateFormatter alloc] init];
    
    formatter1.dateFormat = @"HH:mm:ss";
    formatter1.locale = [[NSLocale alloc]
                           initWithLocaleIdentifier:@"en_US"];
    NSDate *generatedDate1 = [formatter1 dateFromString:strdd];
    
    NSDate *generatedbrekfast = [formatter dateFromString:@"05:00:00"];
    
    
    NSDateComponents *components3;
    NSInteger days3;
    NSInteger hour3;
    NSInteger minutes3;
    //NSString *durationString3;
    
    
    
    components3 = [[NSCalendar currentCalendar] components: NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond fromDate: generatedDate1 toDate: generatedbrekfast options: 0];
    
    days3 = components3.day;
    hour3 = components3.hour;
    minutes3 = components3.minute;
    NSInteger second=components3.second;
    
    strdinnertime = [NSString stringWithFormat:@"%ld", (long)hour3 ];
    strdinerminut = [NSString stringWithFormat:@"%ld", (long)minutes3];
    strdinersecond = [NSString stringWithFormat:@"%ld", (long)second];
    
    
    NSString *firstLetter = [strdinerminut substringToIndex:1];
    
    NSInteger brek=strdinerminut.integerValue;
    NSInteger brek1=strdinnertime.integerValue;
    
    NSCalendar * calendar = [NSCalendar currentCalendar];
    NSDateComponents * dateComponents = [calendar components: NSDayCalendarUnit | NSWeekdayCalendarUnit fromDate: [NSDate date]];
    
    NSDateComponents * dateComponents1 = [calendar components: NSDayCalendarUnit | NSCalendarUnitHour |  NSCalendarUnitMinute   |  NSCalendarUnitSecond fromDate: [NSDate date]];
    
    
    NSString *daynamestr = calendar.shortWeekdaySymbols[dateComponents.weekday - 1];
    NSLog(@"daynamestring=%@",daynamestr);
    
    if ([firstLetter isEqualToString:@"-"])
    {
        
        if (-1<brek1)
        {
            
            if (-60<=brek)
            {
                NSLog(@"do process");
                 [self PaypalSetupInfoCodeMethod];
               
            }
            
            
        }else
        {
            if (-2<brek1)
            {
                if (-28<=brek)
                {
                    NSLog(@"do process");
                   [self PaypalSetupInfoCodeMethod];
                    
                    
                }else
                {
                    NSLog(@"popup");
                    PopUpBackView.hidden = NO;
                    AdvertisingPopup.hidden = NO;
                    
                }
            }else{
                 NSLog(@"popup");
                PopUpBackView.hidden = NO;
                AdvertisingPopup.hidden = NO;
                
            }
            
            
        }
        
    }else{
        //  PopUpBackGroundView.hidden = NO;
        //  MemberShipSweepstakesPopUp.hidden = NO;
        NSLog(@"u r in");
        
        if ([firstLetter isEqualToString:@"0"])
        {
            NSLog(@"do process");
            [self PaypalSetupInfoCodeMethod];
           
        }
        else
        {
             NSLog(@"popup");
            PopUpBackView.hidden = NO;
            AdvertisingPopup.hidden = NO;
        }
    }
    
    
    
    
}

-(NSString *)RemovePercentageSignFromAmount:(NSString *)Amount{
    NSString *newString;
    NSString *str1=Amount;
    str1 = [str1 stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSRange range = [str1 rangeOfString:@"$" options: NSBackwardsSearch];
    
    NSInteger ranrd=range.length;
    
    // ha toh kbr pads mate to lkhyu htu a
    
   // okkk
    if (ranrd==0) {
        
        newString =str1 ;
    }else{
        newString = [str1 substringFromIndex:(range.location+1)];
    }
    
    NSLog(@"%@",newString);
    return newString;
}

-(void) DisplayAlertMessage:(NSString *)Message{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"VRV" message:Message preferredStyle:UIAlertControllerStyleAlert];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [alertController dismissViewControllerAnimated:YES completion:^{
            
        }];
    });
    // return;
}

-(void) DisplayAlertMessage1:(NSString *)Message{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"VRV" message:Message preferredStyle:UIAlertControllerStyleAlert];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [alertController dismissViewControllerAnimated:YES completion:^{
            [self.navigationController popViewControllerAnimated:YES];
        }];
    });
    // return;
}


#pragma mark UITextField Delegate Method.

- (BOOL)textField:(UITextField* )textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (textField == DepositeAmountTextField) {
        if (DepositeAmountTextField.text.length == 0) {
            DepositeAmountTextField.text = [NSString stringWithFormat:@"$"];
        }else{
            NSString *str = [NSString stringWithFormat:@"%@%@",textField.text,string];
            if (str.length == 1) {
                DepositeAmountTextField.text = @"$";
                return NO;
            }
        }
        
        
    }
    
    if (textField == EnticeAmountTextField) {
//        if (EnticeAmountTextField.text.length == 0) {
//            EnticeAmountTextField.text = [NSString stringWithFormat:@"$"];
//        }else{
//            NSString *str = [NSString stringWithFormat:@"%@%@",textField.text,string];
//            if (str.length == 1) {
//                EnticeAmountTextField.text = @"$";
//                return NO;
//            }
//        }
        
        
    }
    if (textField == AdvertisingAmountTextField) {
//        if (AdvertisingAmountTextField.text.length == 0) {
//            AdvertisingAmountTextField.text = [NSString stringWithFormat:@"$"];
//        }else{
//            NSString *str = [NSString stringWithFormat:@"%@%@",textField.text,string];
//            if (str.length == 1) {
//                AdvertisingAmountTextField.text = @"$";
//                return NO;
//            }
//        }
        
        return ( range.location < 11 );
    }
    
    if (textField ==  DepositeAmountTextField) {
        NSLog(@"Range: %@", NSStringFromRange(range));
        return ( range.location < 4 );
    }
    if (textField ==  EnticeAmountTextField) {
        NSLog(@"Range: %@", NSStringFromRange(range));
        return ( range.location < 5 );
    }
    return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    if (textField == CompanyNameTextField) {
        CompanyNameLabel.text = CompanyNameTextField.text;
    }
    return YES;
}

#pragma mark UITextField Delegate Method.

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    if (textField == EnticeAmountTextField) {
        EntiseAmountDollarLabel.hidden = NO;
    }
    
    if (textField == AdvertisingAmountTextField) {
        AdvertisingAmountDollarLabel.hidden = NO;
    }
}

// --------------

#pragma mark Paypal Delegate Method.
// paypal related code.

-(void)PaypalSetupInfoCodeMethod{
    // PayPal Code
    
//    _paypalConfigue = [[PayPalConfiguration alloc] init];
//
//
//    _paypalConfigue.acceptCreditCards = YES;
//    _paypalConfigue.merchantName = @"IosSetup";
//    _paypalConfigue.merchantPrivacyPolicyURL = [NSURL URLWithString:@"https://www.paypal.com/webapps/mpp/ua/privacy-full"];
//    _paypalConfigue.merchantUserAgreementURL = [NSURL URLWithString:@"https://www.paypal.com/webapps/mpp/ua/useragreement-full"];
//
//    _paypalConfigue.languageOrLocale = [NSLocale preferredLanguages][0];
//
//    _paypalConfigue.payPalShippingAddressOption = PayPalShippingAddressOptionPayPal;
//
//    NSLog(@"PayPal iOS SDK version: %@", [PayPalMobile libraryVersion]);
//
//    self.environment = kPayPalEnvironment;
//
//    [self openpaypal];
}

-(void)openpaypal{
    
}


@end
