//
//  BidViewController.h
//  VRV
//
//  Created by Mac 02 on 26/04/17.
//  Copyright © 2017 vnnovate. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BidViewController : UIViewController<UITextFieldDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate>
{
    __weak IBOutlet UIImageView *MainImageview;
    __weak IBOutlet UIImageView *BarImageview;
    __weak IBOutlet UILabel *BarTitle;
    
   IBOutlet UIScrollView *MainScrollview;
   IBOutlet UIView *MainView;
    
    __weak IBOutlet UITextField *DepositeAmountTextField;
    __weak IBOutlet UITextField *EnticeAmountTextField;
    __weak IBOutlet UITextField *AdvertisingAmountTextField;
    __weak IBOutlet UITextField *CompanyNameTextField;
    
    __weak IBOutlet UIView *CompanyLogiView;
    __weak IBOutlet UIView *YoutubelinkView;
    
    __weak IBOutlet UIImageView *CompanyLogoImageview;
    __weak IBOutlet UIView *PopUpBackView;
    __weak IBOutlet UIView *YoutubeLinkPopUp;
    __weak IBOutlet UILabel *CompanyNameLabel;
    
    __weak IBOutlet UITextField *YoutubeLinkTextField;
    
    __weak IBOutlet UIView *AdvertisingPopup;
    __weak IBOutlet UIImageView *LoderImageview;
    
    __weak IBOutlet UIView *InternetPopupBackView;
    __weak IBOutlet UIView *InternetPopup;
    
    __weak IBOutlet UIImageView *youtubelinkimageview;
    
    __weak IBOutlet UIButton *backButton;
    
     __weak IBOutlet UILabel *NoSvgImageLabel;
    
    __weak IBOutlet UILabel *EntiseAmountDollarLabel;
    __weak IBOutlet UILabel *AdvertisingAmountDollarLabel;
    
    __weak IBOutlet UILabel *EntinseDescriptionRedLabel;
    
}

@property(nonatomic, strong, readwrite) NSString *environment;
@property(strong,nonatomic) NSString *ButtonIdentifier;

- (IBAction)BackButtonClick:(id)sender;
- (IBAction)SubmitButtonClick:(id)sender;
- (IBAction)CompanyLogoButtonClick:(id)sender;
- (IBAction)YoutubeLinkButtonClick:(id)sender;
- (IBAction)YOutubeLinkSubmitPopupClick:(id)sender;
- (IBAction)YoutubepopupCancleButtonClick:(id)sender;
- (IBAction)AdvertisingOkButtonClick:(id)sender;
- (IBAction)InternetPopUpOkButtonClick:(id)sender;

@end
