//
//  TurnInView.h
//  VRV
//
//  Created by Elisha Dumas on 11/21/18.
//  Copyright © 2018 Abhishek Sheth. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "ScanQRViewController.h"




@interface TurnInView : UIViewController {
    CLPlacemark *placemark;
    __weak IBOutlet UIView *MainView;
    __weak IBOutlet UILabel *countdownLabel;
    NSTimer *countdownTimer;
    int secondsCount;
}

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *AllButtonCollections;
@property (nonatomic, weak) IBOutlet UITextField *lblCork;
@property (nonatomic, weak) IBOutlet UITextField *lblCloth;
@property (nonatomic, weak) IBOutlet UITextField *lblPlastic;

@property (nonatomic) NSInteger cloth;
@property (nonatomic) NSInteger cork;
@property (nonatomic) NSInteger plastic;

@property(assign)BOOL materialCork;
@property(assign)BOOL materialClothes;
@property(assign)BOOL materialPlastic;

- (IBAction)MenuButtonClick:(id)sender;
- (IBAction)RecButtonClick:(id)sender;
- (IBAction)PerButtonClick:(id)sender;
- (IBAction)RulButtonClick:(id)sender;
- (IBAction)LocButtonClick:(id)sender;
- (IBAction)AccButtonClick:(id)sender;

- (IBAction)WineButtonClick:(id)sender;
- (IBAction)ClotheButtonClick:(id)sender;
- (IBAction)PlasticButtonClick:(id)sender;





- (IBAction)CoordinateApiClick:(id)sender;


@end


