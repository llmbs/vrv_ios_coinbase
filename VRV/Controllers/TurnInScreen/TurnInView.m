//
//  TurnInView.m
//  VRV
//
//  Created by Elisha Dumas on 11/21/18.
//  Copyright © 2018 Abhishek Sheth. All rights reserved.
//

#import "TurnInView.h"
#import "LeftRightViewController.h"
#import "RecylingLocationView.h"
#import <CoreLocation/CoreLocation.h>
#import "VRV-Swift.h"
#import "Reachability.h"
#import "AFNetworking.h"
#import "ScanQRViewController.h"
#import "ReadytoRecycleView.h"
#import "INTULocationManager.h"
#import "VRV-Swift.h"

@interface TurnInView () {
    CLLocation *lastLocation;
    NSInteger locationRequestId;
}

@end

@implementation TurnInView

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[UIButton appearance] setExclusiveTouch:YES];
    
    for (UIButton *btn in _AllButtonCollections) {
        btn.layer.cornerRadius =4;
    }
    
    
    UISwipeGestureRecognizer *gestureRecognizerRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandlerRight:)];
    [gestureRecognizerRight setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [self.view addGestureRecognizer:gestureRecognizerRight];
    
    _lblCork.text = [NSString stringWithFormat:@"%ld", (long)self.cork];
    _lblCloth.text = [NSString stringWithFormat:@"%ld", (long)self.cloth];
    _lblPlastic.text = [NSString stringWithFormat:@"%ld", (long)self.plastic];
    
    
    //abhi location access
    
//    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
//    self->locationRequestId = [locMgr requestLocationWithDesiredAccuracy:INTULocationAccuracyBlock
//                                       timeout:5.0
//                          delayUntilAuthorized:YES    // This parameter is optional, defaults to NO if omitted
//                                         block:^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
//                                             if (status == INTULocationStatusSuccess) {
//                                                 // Request succeeded, meaning achievedAccuracy is at least the requested accuracy, and
//                                                 // currentLocation contains the device's current location.
//                                                 self->lastLocation = currentLocation;
//                                             }
//                                             else if (status == INTULocationStatusTimedOut) {
//                                                 // Wasn't able to locate the user with the requested accuracy within the timeout interval.
//                                                 // However, currentLocation contains the best location available (if any) as of right now,
//                                                 // and achievedAccuracy has info on the accuracy/recency of the location in currentLocation.
//                                                 self->lastLocation = currentLocation;
//                                             }
//                                             else {
//                                                 // An error occurred, more info is available by looking at the specific status returned.
//                                                 [self showAlertTutorial:@""];
//                                             }
//                                         }];
    countdownLabel.hidden = YES;
}

-(void)viewWillAppear:(BOOL)animated {
    [self getCurrentLatLong];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    [locMgr cancelLocationRequest:self->locationRequestId];
}

-(void)timerRun{
    countdownLabel.hidden = NO;
    
    secondsCount = secondsCount - 1;
    int minuts = secondsCount / 60;
    int seconds = secondsCount - (minuts * 60);
    
    NSString *timerOutput = [NSString stringWithFormat:@"%2d", seconds];
    countdownLabel.text = timerOutput;
    
    if (seconds == 0) {
        
        [countdownTimer invalidate];
        countdownTimer = nil;
        countdownLabel.hidden = YES;
    }
}

-(void)setTimer {
    
    secondsCount = 5;
    countdownTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerRun) userInfo:nil repeats:YES];
    
}

-(void)getCoordinatesApi {
    
    [self setTimer];
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    self->locationRequestId = [locMgr requestLocationWithDesiredAccuracy:INTULocationAccuracyBlock
                                                                 timeout:5.0
                                                    delayUntilAuthorized:YES    // This parameter is optional, defaults to NO if omitted
                                                                   block:^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
                                                                       if (status == INTULocationStatusSuccess) {
                                                                           // Request succeeded, meaning achievedAccuracy is at least the requested accuracy, and
                                                                           // currentLocation contains the device's current location.
                                                                           
                                                                           self->lastLocation = currentLocation;
                                                                           [self->countdownTimer invalidate];
                                                                           self->countdownTimer = nil;
                                                                           self->countdownLabel.hidden = YES;
                                                                           
                                                                           [self executeCoordinateApi];
                                          
                                                                           
                                                                       }
                                                                       else if (status == INTULocationStatusTimedOut) {
                                                                           // Wasn't able to locate the user with the requested accuracy within the timeout interval.
                                                                           // However, currentLocation contains the best location available (if any) as of right now,
                                                                           // and achievedAccuracy has info on the accuracy/recency of the location in currentLocation.
                                                                           
                                                                           self->lastLocation = currentLocation;
                                                                           [self->countdownTimer invalidate];
                                                                           self->countdownTimer = nil;
                                                                           self->countdownLabel.hidden = YES;
                                                                           
                                                                           [self executeCoordinateApi];
              
                                                                       }
                                                                       else {
                                                                           // An error occurred, more info is available by looking at the specific status returned.
                                                                           [self hideProgressView];
                                                                           [self showAlertTutorial:@""];
                                                                           [self->countdownTimer invalidate];
                                                                           self->countdownTimer = nil;
                                                                           self->countdownLabel.hidden = YES;
                                                                           
                                                                       }
                                                                   }];
}

-(void)getGpsCoordinates {
    
    [self setTimer];
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    self->locationRequestId = [locMgr requestLocationWithDesiredAccuracy:INTULocationAccuracyBlock
                                                                 timeout:5.0
                                                    delayUntilAuthorized:YES    // This parameter is optional, defaults to NO if omitted
                                                                   block:^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
                                                                       if (status == INTULocationStatusSuccess) {
                                                                           // Request succeeded, meaning achievedAccuracy is at least the requested accuracy, and
                                                                           // currentLocation contains the device's current location.
                                                                           
                                                                           self->lastLocation = currentLocation;
                                                                           [self->countdownTimer invalidate];
                                                                           self->countdownTimer = nil;
                                                                           self->countdownLabel.hidden = YES;
                                                                           
                                                                           if (self->_materialCork == YES) {
                                                                               [self wineLocationApi];
                                                                           }else if (self->_materialClothes == YES) {
                                                                               [self clothLocationApi];
                                                                               
                                                                           } else if (self->_materialPlastic ==YES){
                                                                               [self plasticLocationApi];
                                                                           }
                                                                           
                                                                           
                                                                       }
                                                                       else if (status == INTULocationStatusTimedOut) {
                                                                           // Wasn't able to locate the user with the requested accuracy within the timeout interval.
                                                                           // However, currentLocation contains the best location available (if any) as of right now,
                                                                           // and achievedAccuracy has info on the accuracy/recency of the location in currentLocation.
                                                                           
                                                                           self->lastLocation = currentLocation;
                                                                           [self->countdownTimer invalidate];
                                                                           self->countdownTimer = nil;
                                                                           self->countdownLabel.hidden = YES;
                                                                           
                                                                           if (self->_materialCork == YES) {
                                                                               [self wineLocationApi];
                                                                           }else if (self->_materialClothes == YES) {
                                                                               [self clothLocationApi];
                                                                               
                                                                           } else if (self->_materialPlastic ==YES){
                                                                           [self plasticLocationApi];
                                                                       }
                                                                           
                                                                       }
                                                                       else {
                                                                           // An error occurred, more info is available by looking at the specific status returned.
                                                                           [self hideProgressView];
                                                                           [self showAlertTutorial:@""];
                                                                           [self->countdownTimer invalidate];
                                                                           self->countdownTimer = nil;
                                                                           self->countdownLabel.hidden = YES;
                                                                           
                                                                       }
                                                                   }];
}

-(void)getCurrentLatLong {
        
        INTULocationManager *locMgr = [INTULocationManager sharedInstance];
        [locMgr requestLocationWithDesiredAccuracy:INTULocationAccuracyBlock
                                                                     timeout:5.0
                                                        delayUntilAuthorized:YES    // This parameter is optional, defaults to NO if omitted
                                                                       block:^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
                 if (status == INTULocationStatusSuccess) {
                     // Request succeeded, meaning achievedAccuracy is at least the requested accuracy, and
                     // currentLocation contains the device's current location.
                     
                     self->lastLocation = currentLocation;
                 }
                 else if (status == INTULocationStatusTimedOut) {
                     
                     self->lastLocation = currentLocation;
                 }
                 else {
                 }
             }];
}

-(void)enableUserinteraction:(id)sender {
    
    [self.view setUserInteractionEnabled:YES];
    
}

-(void)swipeHandlerRight:(id)sender {
     [self.navigationController popViewControllerAnimated:YES];
}

- (void)showAlertTutorial:(NSString *)message {
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:@"Location access is required. Touch tutorial to learn how to enable location access and make sure your phone is not in airplane mode. If this message continues, touch reset network and follow the instructions."
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* tutorialButton = [UIAlertAction
                                     actionWithTitle:@"Tutorial"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         [self.navigationController popViewControllerAnimated:YES];
                                         
                                         NSURL *recloc = [NSURL URLWithString:@"https://youtu.be/uhrfDnaxH6c"];
                                         
                                         [[UIApplication sharedApplication] openURL: recloc options:@{} completionHandler:^(BOOL success) {
                                             if (success)
                                                 NSLog(@"Opened tutorial");
                                             
                                         }];
                                         
                                         
                                     }];
    
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"Settings"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   [self.navigationController popViewControllerAnimated:YES];
                                   
                                   NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                   if (url != nil) {
                                       [[UIApplication sharedApplication] openURL:url options:[NSDictionary new] completionHandler:nil];
                                   }
                                   
                                   
                               }];
    
    UIAlertAction* resetButton = [UIAlertAction
                                  actionWithTitle:@"Reset network"
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action)
                                  {
                                      [self MenuButtonClick:self];
                                      
                                      NSURL *resetTutorial = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/reset_network"];
                                      
                                      [[UIApplication sharedApplication] openURL: resetTutorial options:@{} completionHandler:^(BOOL success) {
                                          if (success)
                                              NSLog(@"Opened reset tutorial");
                                          
                                      }];
                                      
                                      
                                  }];
    
    
    UIAlertAction*  cancelButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        [self.navigationController popViewControllerAnimated:YES];
                                        
                                    }];
    
    [alert addAction:okButton];
    [alert addAction:tutorialButton];
    [alert addAction:resetButton];
    [alert addAction:cancelButton];
    [self presentViewController:alert animated:YES completion:nil];
    

}

- (void)showAlertCoordinate:(NSString *)message {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Coordinate Api"
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction* learnButton = [UIAlertAction
                               actionWithTitle:@"Learn More"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                
        
        NSURL *instantad = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/ti_coordinate_api"];
        
        [[UIApplication sharedApplication] openURL: instantad options:@{} completionHandler:^(BOOL success) {
            if (success)
                NSLog(@"Opened coordinate api pdf");
            
        
                                   
                               }];
    
    }];
    
    UIAlertAction* disButton = [UIAlertAction
                               actionWithTitle:@"Dismiss"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   
                                   
                               }];
    
    UIAlertAction* apiButton = [UIAlertAction
    actionWithTitle:@"Submit Coordinates"
    style:UIAlertActionStyleDefault
    handler:^(UIAlertAction * action)
    {
            
             [self.view setUserInteractionEnabled: NO];
             [self performSelector:@selector(enableUserinteraction:) withObject:self afterDelay:0.2 ];
            
           
            Reachability *reachability = [Reachability reachabilityForInternetConnection];
            [reachability startNotifier];
            NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
            if (remoteHostStatus == NotReachable)
            {
                
                [self showAlert:@"Internet connection offline."];
                
                
            } else {
                
               
                [self showProgressView];
                [self getCoordinatesApi];
                
                
                
            }
        
        
        
        
    }];
    
    [alert addAction:apiButton];
    [alert addAction:learnButton];
    [alert addAction:disButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    
}
- (void)showAlertFinishedCoordinate:(NSString *)message {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   [self.navigationController popViewControllerAnimated:YES];
                                   
                               }];
    [alert addAction:okButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    
}

- (void)showAlert:(NSString *)message {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   
                                   
                               }];
    [alert addAction:okButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    
}
- (void)showAlertRec:(NSString *)message {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Recycle Now"
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   
                                   
                               }];
    [alert addAction:okButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    
}
- (void)showAlertRules:(NSString *)message {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Recycling Rules"
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   
                                   
                               }];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
    
    
    
}

- (void)showAlertDistance:(NSString *)message {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Recycling Rules"
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    
    
    UIAlertAction* learnButton = [UIAlertAction
                               actionWithTitle:@"Learn More"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   
        
        
        NSURL *instantad = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/ti_recycle"];
        
        [[UIApplication sharedApplication] openURL: instantad options:@{} completionHandler:^(BOOL success) {
            if (success)
                NSLog(@"Opened recycling pdf");
            
        
                                   
                               }];
    
    }];
        

    
    UIAlertAction* locButton = [UIAlertAction
                               actionWithTitle:@"Locations"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   RecylingLocationView *RecylinglocationScreenObj = [self.storyboard instantiateViewControllerWithIdentifier:@"ReCycleLocationId"];
                                   [self.navigationController pushViewController:RecylinglocationScreenObj animated:YES];
                                   
                               }];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   
                               }];
    
    [alert addAction:locButton];
    [alert addAction:learnButton];
    [alert addAction:okButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    
    
    
}
- (void)showAlertAcc:(NSString *)message {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Accuracy"
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   
                                   
                               }];
    [alert addAction:okButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    
}

- (IBAction)CoordinateApiClick:(id)sender {
    
    
    [self showAlertCoordinate:@"If you don't have material to recycle or a recycling facility is greater than 10 mins away then touch submit coordinates near your favorite area to generate business leads.\n\nIn the future, recycling will be required 100% of the time. For more information, touch learn more. Thank you!"];
    
    
}




- (IBAction)MenuButtonClick:(id)sender {
    
    
    
        LeftRightViewController *LeftrightviewScreenObject = [self.storyboard instantiateViewControllerWithIdentifier:@"leftright"];
        UIViewController *newVC = LeftrightviewScreenObject;
        NSMutableArray *vcs =  [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
        [vcs insertObject:newVC atIndex:vcs.count-1];
        [self.navigationController setViewControllers:vcs animated:YES];
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    
- (IBAction)RecButtonClick:(id)sender {
    
    
    [self showAlertRec:@"Please familiarize yourself with the rules. Touch the rules icon. Thank you."];
    
    
}

- (IBAction)PerButtonClick:(id)sender {
    
    
    
    NSURL *permission = [NSURL URLWithString:@"https://youtu.be/uhrfDnaxH6c"];
    
    [[UIApplication sharedApplication] openURL: permission options:@{} completionHandler:^(BOOL success) {
        if (success)
            NSLog(@"Opened permission youtube");
        
    }];
   
}


- (IBAction)RulButtonClick:(id)sender {
    
   
    [self showAlertRules:@"Recycle your material or submit coordinate api between Saturday 12 AM PST to Friday before 11:30 PM PST.\n\nTo recycle your material(s), you must be within 1056 ft (0.2 miles) from a recycling location. \n\nIf you don't have material to recycle or a location is too far then touch coordinate api as temporary alternative."];
    
    
}

- (IBAction)LocButtonClick:(id)sender {
    
   
    
    RecylingLocationView *RecylinglocationScreenObj = [self.storyboard instantiateViewControllerWithIdentifier:@"ReCycleLocationId"];
    [self.navigationController pushViewController:RecylinglocationScreenObj animated:YES];
   
}

- (IBAction)AccButtonClick:(id)sender {
    
   [self showAlertAcc:@"Sometimes GPS won't place you directly at a recycling location for a variety of reasons. \n\nHowever, you can improve the GPS coordinates of a recycling location after you successfully recycle your materials by touching the accuracy icon."];
    
}

- (void)showAlertCheckMaterials:(NSString *)message {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   [self.navigationController popViewControllerAnimated:YES];
                                   
                               }];
    [alert addAction:okButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}


- (void)wineLocationApi {
    
    
    
    NSString *lat = [NSString stringWithFormat:@"%.8f", self->lastLocation.coordinate.latitude];
    NSString *lng = [NSString stringWithFormat:@"%.8f", self->lastLocation.coordinate.longitude];
    
    [BusinessService hasAnyBusiness:@"cork" lat:lat lng:lng success:^(BOOL success) {
        
        [self hideProgressView];
        
        if (success == YES) {
        ScanQRViewController *ScanqrScreenObj = [self.storyboard instantiateViewControllerWithIdentifier:@"qrcode"];
        [self.navigationController pushViewController:ScanqrScreenObj animated:YES];
        ScanqrScreenObj.selectedCork=YES;
        }  else {
            
        
            [self showAlertDistance:@"To recycle your material, you must be within 1056 ft (0.2 miles) from a recycling location for that material. \n\nTo check your position, touch locations, touch the material your trying to recycle and then touch the closest Google maps icon.\n\nEnable Wi-fi to increase your GPS accuracy. For more information touch learn more."];
        }
        
    } failure:^(NSError * _Nullable error) {
        
        [self hideProgressView];
        [self showAlert:@"Error, please try again."];
        NSLog(@"Failed to call hasAnyBusiness Api");
        
    }];
    
}
- (void)clothLocationApi {
    
    
    
    NSString *lat = [NSString stringWithFormat:@"%.8f", self->lastLocation.coordinate.latitude];
    NSString *lng = [NSString stringWithFormat:@"%.8f", self->lastLocation.coordinate.longitude];
    
    [BusinessService hasAnyBusiness:@"cloth" lat:lat lng:lng success:^(BOOL success) {
        
        [self hideProgressView];
        
        if (success == YES) {
            ScanQRViewController *ScanqrScreenObj = [self.storyboard instantiateViewControllerWithIdentifier:@"qrcode"];
            [self.navigationController pushViewController:ScanqrScreenObj animated:YES];
            ScanqrScreenObj.selectedClothes=YES;
        } else {
            
            [self showAlertDistance:@"To recycle your material, you must be within 1056 ft (0.2 miles) from a recycling location for that material. \n\nTo check your position, touch locations, touch the material your trying to recycle and then touch the closest Google maps icon.\n\nEnable Wi-fi to increase your GPS accuracy. For more information touch learn more."];
        }
    } failure:^(NSError * _Nullable error) {
        
       
        [self hideProgressView];
        [self showAlert:@"Error, please try again."];
         NSLog(@"Failed to call hasAnyBusiness Api");
        

    }];
    
}

- (void)plasticLocationApi {
    
    
    
    NSString *lat = [NSString stringWithFormat:@"%.8f", self->lastLocation.coordinate.latitude];
    NSString *lng = [NSString stringWithFormat:@"%.8f", self->lastLocation.coordinate.longitude];
    
    [BusinessService hasAnyBusiness:@"plastic" lat:lat lng:lng success:^(BOOL success) {
        
        [self hideProgressView];
        
        if (success == YES) {
            ScanQRViewController *ScanqrScreenObj = [self.storyboard instantiateViewControllerWithIdentifier:@"qrcode"];
            [self.navigationController pushViewController:ScanqrScreenObj animated:YES];
            ScanqrScreenObj.selectedPlastic=YES;
        } else {
            
            [self showAlertDistance:@"To recycle your material, you must be within 1056 ft (0.2 miles) from a recycling location for that material. \n\nTo check your position, touch locations, touch the material your trying to recycle and then touch the closest Google maps icon.\n\nEnable Wi-fi to increase your GPS accuracy. For more information touch learn more."];
        }
    } failure:^(NSError * _Nullable error) {
        
       
        [self hideProgressView];
        [self showAlert:@"Error, please try again."];
         NSLog(@"Failed to call hasAnyBusiness Api");
        

    }];
    
}

- (void)executeCoordinateApi {
    
    
    NSString *lat = [NSString stringWithFormat:@"%.8f", self->lastLocation.coordinate.latitude];
    NSString *lng = [NSString stringWithFormat:@"%.8f", self->lastLocation.coordinate.longitude];
    
    NSInteger userId = [ConfigurationManager shared].currentUser.getUserId;
    
    [AuthenticationService cordinateConfirmationWith:userId longitude:lng latitude:lat success:^(BOOL success) {
        
        if (success == YES) {
            [self hideProgressView];
            
            [self showAlertFinishedCoordinate:@"Good job! You can now touch game on between Saturday 12 AM pst to Friday before 11:30 Pm pst to enter the contest if you desire."];
        }
        
    } failure:^(NSError * _Nullable error) {
        
        [self showAlert:@"Could not submit coordinates. Try again."];
        
        [self hideProgressView];
        
    }];
    
    
}


- (IBAction)WineButtonClick:(id)sender {
    
    _materialCork = YES;
    
     [self.view setUserInteractionEnabled: NO];
     [self performSelector:@selector(enableUserinteraction:) withObject:self afterDelay:0.2 ];
    
   
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    if (remoteHostStatus == NotReachable)
    {
        
        [self showAlert:@"Internet connection offline."];
        
    }
    
    else [RecyclingService readyToTurnWithSuccess:^(NSInteger cloth, NSInteger cork, NSInteger plastic) {
        
        [self showProgressView];
       
        // when we add a material, we must include it in the if statement &&
        if (cork == 0 && cloth == 0 && plastic == 0) {
         
            NSLog(@"Friday delete crons job occured");
            [self hideProgressView];
            [self showAlertCheckMaterials:@"You must buy a VRVC coin to recycle this materials. \n\n Currently, you can only buy one VRVC coin once per week."];
            
        }
        
        
        else if (cork < 1) {
            
            NSLog(@"Nothing to turn in");
            [self hideProgressView];
            [self showAlert:@"You must buy a VRVC coin to recycle this materials. \n\n Currently, you can only buy one VRVC coin once per week."];
            
        } else if (cork > 0) {
            
            NSLog(@"Can be turned in");
            [self getGpsCoordinates];
            //[self wineLocationApi];
      }
        
    } failure:^(NSError * _Nullable error) {
        
        [self handleError:error];
        NSLog(@"Failed to call ready to turn in");
        [self hideProgressView];
        
    }];
    

    
}

    
    



- (IBAction)ClotheButtonClick:(id)sender {
    
    _materialClothes = YES;
    
    [self.view setUserInteractionEnabled: NO];
     [self performSelector:@selector(enableUserinteraction:) withObject:self afterDelay:0.2 ];
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    if (remoteHostStatus == NotReachable)
    {
   
        [self showAlert:@"Internet connection offline."];
        
    }
    
    
    else [RecyclingService readyToTurnWithSuccess:^(NSInteger cloth, NSInteger cork, NSInteger plastic) {
        
        [self showProgressView];
      
        
        
        // when we add a material, we must include it in the if statement &&
        
        if (cork == 0 && cloth == 0 && plastic == 0) {
          
            NSLog(@"Friday delete crons job occured");
            [self hideProgressView];
            [self showAlertCheckMaterials:@"You must buy a VRVC coin to recycle this materials. \n\n Currently, you can only buy one VRVC coin once per week."];
            
        }
        
        
        else if (cloth < 1) {
           
            NSLog(@"Nothing to turn in");
            [self hideProgressView];
            [self showAlert:@"You must buy a VRVC coin to recycle this materials. \n\n Currently, you can only buy one VRVC coin once per week."];
            
        } else if (cloth > 0) {
            
            NSLog(@"Can be turned in");
            [self getGpsCoordinates];
            //[self clothLocationApi];
        }
        
    } failure:^(NSError * _Nullable error) {
       
        [self handleError:error];
        NSLog(@"Failed to call ready to turn in");
        [self hideProgressView];
    }];
    
    
    
}

- (IBAction)PlasticButtonClick:(id)sender {
    
    _materialPlastic = YES;
    
    [self.view setUserInteractionEnabled: NO];
     [self performSelector:@selector(enableUserinteraction:) withObject:self afterDelay:0.2 ];
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    if (remoteHostStatus == NotReachable)
    {
   
        [self showAlert:@"Internet connection offline."];
        
    }
    
    
    else [RecyclingService readyToTurnWithSuccess:^(NSInteger cloth, NSInteger cork, NSInteger plastic) {
        
        [self showProgressView];
      
        
        
        // when we add a material, we must include it in the if statement &&
        
        if (cork == 0 && cloth == 0 && plastic == 0) {
          
            NSLog(@"Friday delete crons job occured");
            [self hideProgressView];
            [self showAlertCheckMaterials:@"You must buy a VRVC coin to recycle this materials. \n\n Currently, you can only buy one VRVC coin once per week."];
            
        }
        
        
        else if (plastic < 1) {
           
            NSLog(@"Nothing to turn in");
            [self hideProgressView];
            [self showAlert:@"You must buy a VRVC coin to recycle this materials. \n\n Currently, you can only buy one VRVC coin once per week."];
            
        } else if (plastic > 0) {
            
            NSLog(@"Can be turned in");
            [self getGpsCoordinates];
            
        }
        
    } failure:^(NSError * _Nullable error) {
       
        [self handleError:error];
        NSLog(@"Failed to call ready to turn in");
        [self hideProgressView];
    }];
    
    
    
}



@end
