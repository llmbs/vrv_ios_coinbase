//
//  FriestScreenAnimation.m
//  VRV
//
//  Created by Mac 02 on 27/04/17.
//  Copyright © 2017 vnnovate. All rights reserved.
//

#import "FriestScreenAnimation.h"
#import "CommanFunction.h"
#import "LoginView.h"
#import "VRV-Swift.h"
#import "LeftRightViewController.h"
#import"DepositViewController.h"

@interface FriestScreenAnimation ()

@end

@implementation FriestScreenAnimation

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
   
    
    [NSTimer scheduledTimerWithTimeInterval:3.0f
                                     target:self
                                   selector:@selector(tutorialScreen)
                                   userInfo:nil
                                    repeats:NO];

   
}



-(void)tutorialScreen {
    
    if (![@"1" isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:@"tutorialScreen"]]) {
        
        [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"tutorialScreen"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        DepositViewController *DepositviewScreenObject = [self.storyboard instantiateViewControllerWithIdentifier:@"intro1"];
        UIViewController *newVC = DepositviewScreenObject;
        NSMutableArray *vcs =  [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
        [vcs insertObject:newVC atIndex:vcs.count-1];
        [self.navigationController setViewControllers:vcs animated:YES];
        [self.navigationController popViewControllerAnimated:YES];
        
    }else {
        
        [self loginLeftRight];
}
}



-(void)loginLeftRight {

   
    
    if ([[ConfigurationManager shared] isLoggedIn] == YES) {
        
        LeftRightViewController *LeftrightviewScreenObject = [self.storyboard instantiateViewControllerWithIdentifier:@"leftright"];
        UIViewController *newVC = LeftrightviewScreenObject;
        NSMutableArray *vcs =  [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
        [vcs insertObject:newVC atIndex:vcs.count-1];
        [self.navigationController setViewControllers:vcs animated:YES];
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        
        
        
        LoginView *loginviewScreenObj = [self.storyboard instantiateViewControllerWithIdentifier:@"loginid"];
        UIViewController *newVC = loginviewScreenObj;
        NSMutableArray *vcs =  [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
        [vcs insertObject:newVC atIndex:vcs.count-1];
        [self.navigationController setViewControllers:vcs animated:YES];
        [self.navigationController popViewControllerAnimated:YES];
        
        
        
    }
    

//    DepositViewController *DepositviewScreenObject = [self.storyboard instantiateViewControllerWithIdentifier:@"intro1"];
//    UIViewController *newVC = DepositviewScreenObject;
//    NSMutableArray *vcs =  [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
//    [vcs insertObject:newVC atIndex:vcs.count-1];
//    [self.navigationController setViewControllers:vcs animated:YES];
//    [self.navigationController popViewControllerAnimated:YES];


}

    




-(void)displayChoiceTimerFired{
    
    
    if ([[ConfigurationManager shared] isLoggedIn] == YES) {
        
       
        
        LeftRightViewController *LeftrightviewScreenObject = [self.storyboard instantiateViewControllerWithIdentifier:@"leftright"];
        UIViewController *newVC = LeftrightviewScreenObject;
        NSMutableArray *vcs =  [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
        [vcs insertObject:newVC atIndex:vcs.count-1];
        [self.navigationController setViewControllers:vcs animated:YES];
        [self.navigationController popViewControllerAnimated:YES];
        
        
    } else {
      
     

        
        LoginView *loginviewScreenObj = [self.storyboard instantiateViewControllerWithIdentifier:@"loginid"];
        UIViewController *newVC = loginviewScreenObj;
        NSMutableArray *vcs =  [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
        [vcs insertObject:newVC atIndex:vcs.count-1];
        [self.navigationController setViewControllers:vcs animated:YES];
        [self.navigationController popViewControllerAnimated:YES];
        
        
        
    }
    
    
    
    
    
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
