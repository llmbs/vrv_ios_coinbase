//
//  ScoreAndRankScreen.m
//  VRV
//
//  Created by My Mac on 19/08/21.
//  Copyright © 2021 Abhishek Sheth. All rights reserved.
//

#import "ScoreAndRankScreen.h"

@interface ScoreAndRankScreen () {
    NSMutableArray<ScoreRankData *> *arrOfScoresRank;
}

@end

@implementation ScoreAndRankScreen

#pragma mark - View Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupTableView];
}

#pragma mark - Methods
- (void) setupArray: (NSMutableArray<ScoreRankData *> *) topData {
    self->arrOfScoresRank = [[NSMutableArray alloc] initWithArray:topData];
}

- (void) setupTableView {
    self.tblScoreRank.delegate = self;
    self.tblScoreRank.dataSource = self;
    self.tblScoreRank.tableFooterView = [[UIView alloc] init];
    [self.tblScoreRank registerNib:[BusinessCustomeCell Nib] forCellReuseIdentifier:@"BusinessCustomeCell"];
    [self.tblScoreRank reloadData];
}

#pragma mark - IBActions
- (IBAction)onBtnBackAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableViewDataSource, UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrOfScoresRank.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ScoreRankData *scoreData = arrOfScoresRank[indexPath.row];
    BusinessCustomeCell *cell = (BusinessCustomeCell *)[tableView dequeueReusableCellWithIdentifier:@"BusinessCustomeCell" forIndexPath:indexPath];
    cell.tag = indexPath.row;
    [cell setupConstraints];
    [cell configureForScoreAndRankWithData:scoreData];
    return cell;
}

@end
