//
//  ScoreAndRankScreen.h
//  VRV
//
//  Created by My Mac on 19/08/21.
//  Copyright © 2021 Abhishek Sheth. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VRV-Swift.h"
#import "BusinessCustomecell.h"

NS_ASSUME_NONNULL_BEGIN

@interface ScoreAndRankScreen : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tblScoreRank;

- (void) setupArray: (NSMutableArray<ScoreRankData *> *) topData;

@end

NS_ASSUME_NONNULL_END
