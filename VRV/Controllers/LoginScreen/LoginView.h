

#import <UIKit/UIKit.h>


@interface LoginView : UIViewController<UITextFieldDelegate>
{
        
    __weak IBOutlet UITextField *EmailTextField;
    __weak IBOutlet UITextField *PasswordTextField;
    __weak IBOutlet UITextField *ForgotPasswordTextField;
    
    
   
    
    
    
   
    __weak IBOutlet UIView *Container;
    
    __weak IBOutlet UIButton *LoginBtn;
    __weak IBOutlet UIButton *ActBtn;
    __weak IBOutlet UIButton *ForBtn;
    __weak IBOutlet UIButton *TutorialBtn;
   
    
    
}
- (IBAction)ForgotPasswordButtonClick:(id)sender;
- (IBAction)LoginButtonClick:(id)sender;
- (IBAction)SignUpButtonClick:(id)sender;



@end
