
#import "LoginView.h"
#import "Reachability.h"
#import "AFNetworking.h"
#import "Defaults.h"
#import "CommanFunction.h"
#import "UserRoleInfoView.h"
#import "AppDelegate.h"
#import "LeftRightViewController.h"
#import "SignUpView.h"
#import "VRV-Swift.h"
#import "INTULocationManager.h"

@interface LoginView ()
{
    NSUserDefaults *dlf;
    AppDelegate *app;
}
@end

@implementation LoginView

 - (void)viewDidLoad {
  [super viewDidLoad];
    
// Do any additional setup after loading the view
     
     INTULocationManager *locMgr = [INTULocationManager sharedInstance];
     [locMgr requestLocationWithDesiredAccuracy:INTULocationAccuracyCity
                                        timeout:5.0
                           delayUntilAuthorized:YES    // This parameter is optional, defaults to NO if omitted
                                          block:^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
                                              if (status == INTULocationStatusSuccess) {
                                                  // Request succeeded, meaning achievedAccuracy is at least the requested accuracy, and
                                                  // currentLocation contains the device's current location.
                                              }
                                              else if (status == INTULocationStatusTimedOut) {
                                                  // Wasn't able to locate the user with the requested accuracy within the timeout interval.
                                                  // However, currentLocation contains the best location available (if any) as of right now,
                                                  // and achievedAccuracy has info on the accuracy/recency of the location in currentLocation.
                                              }
                                              else {
                                                  // An error occurred, more info is available by looking at the specific status returned.
                                              }
                                          }];
     
     
     UIColor *color = [UIColor colorWithRed:0.35 green:0.29 blue:0.27 alpha:1.0];
     EmailTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"alemail", "") attributes:@{NSForegroundColorAttributeName: color}];
     
     
     UIColor *color2 = [UIColor colorWithRed:0.35 green:0.29 blue:0.27 alpha:1.0];
     PasswordTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"alpassword", "") attributes:@{NSForegroundColorAttributeName: color2}];
     
     // for testing
     
    // EmailTextField.text = @"test@voluntaryrefundvalue.com";
    // PasswordTextField.text = @"password123";
    
    app =(AppDelegate *)[[UIApplication sharedApplication]delegate];
    dlf = [NSUserDefaults standardUserDefaults];
    
 
     //old webservices
  //   if ([dlf boolForKey:KEY_USER_IS_LOGIN] ) {
  //  LeftRightViewController *LeftRightScreenObj = [self.storyboard instantiateViewControllerWithIdentifier:@"leftright"];
  //  [self.navigationController pushViewController:LeftRightScreenObj animated:NO];
    
//}
     
    
    LoginBtn.layer.cornerRadius = 4;
    ActBtn.layer.cornerRadius = 4;
    ForBtn.layer.cornerRadius = 4;
     TutorialBtn.layer.cornerRadius =4;
    Container.layer.cornerRadius = 4;
     
    [[UIButton appearance] setExclusiveTouch:YES];
     
     [LoginBtn setTitle:NSLocalizedString(@"allogin", "") forState:UIControlStateNormal];
     
     [ActBtn setTitle:NSLocalizedString(@"al_acct", "") forState:UIControlStateNormal];
     
     [ForBtn setTitle:NSLocalizedString(@"al_forgot", "") forState:UIControlStateNormal];
     
     [TutorialBtn setTitle:NSLocalizedString(@"vrvtutorial", "") forState:UIControlStateNormal];
     
     
     
}
// Dispose of any resources that can be recreated.
- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
   
}


#pragma mark IBAction Method.
- (IBAction)TutorialButtonClick:(id)sender {
    
    
    NSURL *quantis = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/winContestPic"];
    
    [[UIApplication sharedApplication] openURL: quantis options:@{} completionHandler:^(BOOL success) {
        if (success)
            NSLog(@"Opened tutorial");
        
    }];
    
    
}


- (IBAction)ForgotPasswordButtonClick:(id)sender {
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:NSLocalizedString(@"al_forgot", "")
                                  message:NSLocalizedString(@"label_enter_email_address", "")
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* submit = [UIAlertAction actionWithTitle:NSLocalizedString(@"business_access_submit", "") style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action) {
                                                   //Do Some action here
                                                   
                                                 
                                                   
                                                   if (self->ForgotPasswordTextField.text.length == 0) {
                                                       
                                                       
                                                       UIAlertController * alert=   [UIAlertController
                                                                                     alertControllerWithTitle:@""
                                                                                     message:NSLocalizedString(@"error_invalid_email_address", "")
                                                                                     preferredStyle:UIAlertControllerStyleAlert];
                                                       
                                                       UIAlertAction* okButton = [UIAlertAction
                                                                                  actionWithTitle:NSLocalizedString(@"dialog_ok", "")
                                                                                  style:UIAlertActionStyleDefault
                                                                                  handler:^(UIAlertAction * action)
                                                                                  
                                                                                  {
                                                                                     
                                                                                      
                                                                                      
                                                                                  }];
                                                       [alert addAction:okButton];
                                                       
                                                       [self presentViewController:alert animated:YES completion:nil];
                               
                                                    }
                                                   
                                                    if ([self IsValidEmail:self->ForgotPasswordTextField.text]!=true)
                                                    {
                            
                                                        UIAlertController * alert=   [UIAlertController
                                                                                      alertControllerWithTitle:@""
                                                                                      message:NSLocalizedString(@"error_invalid_email_address", "")
                                                                                      preferredStyle:UIAlertControllerStyleAlert];
                                                        
                                                        UIAlertAction* okButton = [UIAlertAction
                                                                                   actionWithTitle:NSLocalizedString(@"dialog_ok", "")
                                                                                   style:UIAlertActionStyleDefault
                                                                                   handler:^(UIAlertAction * action)
                                                                                   
                                                                                   {
                                                                                       
                                                                                       
                                                                                   }];
                                                        [alert addAction:okButton];
                                                        
                                                        [self presentViewController:alert animated:YES completion:nil];
                                                   
                                                    }
                                                   
                                                    else {
                                                   
                                                    [self CallWebServiceForForgotPassword];
                                                    }
                                               
                                               }];
                            UIAlertAction* cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"txt_cancel", "") style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action) {
                                                   [alert dismissViewControllerAnimated:YES completion:nil];
                                                   }];
    
    
                            [alert addAction:cancel];
                            [alert addAction:submit];
    
                            [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
                                
                                self->ForgotPasswordTextField = textField;
                                textField.placeholder = NSLocalizedString(@"label_enter_email_address", "");
                                textField.keyboardType = UIKeyboardTypeEmailAddress;
                            }];
    
                            [self presentViewController:alert animated:YES completion:nil];
    
}

- (IBAction)LoginButtonClick:(id)sender {
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    if (remoteHostStatus == NotReachable) {
        
       
        
        [self showAlert:NSLocalizedString(@"internetActivity_off", "")];
        
        
    } else
        
        [self CheckLoginValidation];
}


- (IBAction)SignUpButtonClick:(id)sender {
   
    [self showAlertSignUp:@""];
    
    
    
}


#pragma mark VOID Method.

-(void)CheckLoginValidation{
    
   
    
    if ([self IsValidEmail:EmailTextField.text]!=true)
{
        [EmailTextField becomeFirstResponder];
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@""
                                      message:NSLocalizedString(@"error_invalid_email_address", "")
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* okButton = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"dialog_ok", "")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       
                                   }];
        [alert addAction:okButton];
        [self presentViewController:alert animated:YES completion:nil];
    
    
}
    
    
    else if (EmailTextField.text.length > 0 && PasswordTextField.text.length == 0 )  {
        
        [PasswordTextField becomeFirstResponder];
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@""
                                      message:NSLocalizedString(@"error_password_empty", "")
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* okButton = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"dialog_ok", "")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       
                                   }];
        [alert addAction:okButton];
        [self presentViewController:alert animated:YES completion:nil];
    
    } else {
        
   
        
        [self showProgressView];
        
        [AuthenticationService loginWith:EmailTextField.text password:PasswordTextField.text success:^(UserDetails * _Nonnull user) {
            
            //TODO : Handle Login
            
            [self->PasswordTextField resignFirstResponder];
            [self->EmailTextField resignFirstResponder];
            
            
            LeftRightViewController *LeftrightviewScreenObject = [self.storyboard instantiateViewControllerWithIdentifier:@"leftright"];
            UIViewController *newVC = LeftrightviewScreenObject;
            NSMutableArray *vcs =  [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
            [vcs insertObject:newVC atIndex:vcs.count-1];
            [self.navigationController setViewControllers:vcs animated:YES];
            [self.navigationController popViewControllerAnimated:YES];
            
            
        } failure:^(NSError * _Nullable error) {
            
            
            [self hideProgressView];
            
            
            
            [self showAlert:NSLocalizedString(@"error_login_failed", "")];
            
           
            // default api fail message
            // [self showAlert:error.localizedDescription];
        }];
    }
   
}

- (void)showAlert:(NSString *)message {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"dialog_ok", "")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   
                               }];
    [alert addAction:okButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)showAlertSignUp:(NSString *)message {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:NSLocalizedString(@"user_registration", "")
                                  message:NSLocalizedString(@"age_terms", "")
                                  
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* YesButton = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"sign_up", "")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   SignUpView *SignUpScreenObj = [self.storyboard instantiateViewControllerWithIdentifier:@"signupid"];
                                   [self.navigationController pushViewController:SignUpScreenObj animated:YES];
                                   
                               }];
    UIAlertAction* TermsButton = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"terms", "")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   NSURL *terms = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/pp_tos"];
                                   
                                   [[UIApplication sharedApplication] openURL: terms options:@{} completionHandler:^(BOOL success) {
                                       if (success)
                                           NSLog(@"Opened terms");
                                       
                                   }];
                                   
                               }];
    UIAlertAction* PrivacyButton = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"privacy_txt", "")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   NSURL *privacy = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/pp_tos"];
                                   
                                   [[UIApplication sharedApplication] openURL: privacy options:@{} completionHandler:^(BOOL success) {
                                       if (success)
                                           NSLog(@"Opened privacy");
                                       
                                   }];
                                   
                               }];
    UIAlertAction* CookieButton = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"cookie_txt", "")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   NSURL *cookie = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/cookie_use"];
                                   
                                   [[UIApplication sharedApplication] openURL: cookie options:@{} completionHandler:^(BOOL success) {
                                       if (success)
                                           NSLog(@"Opened cookie");
                                       
                                   }];
                                   
                               }];
    UIAlertAction* NoButton = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"loginActivity_leave", "")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   
                               }];
    
    [alert addAction:YesButton];
    [alert addAction:TermsButton];
    [alert addAction:PrivacyButton];
    [alert addAction:CookieButton];
    [alert addAction:NoButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    
}


-(BOOL)IsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

-(void)CallWebServiceForForgotPassword{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus status =[reachability currentReachabilityStatus];
    if (status == NotReachable) {
        
        [self showAlert:NSLocalizedString(@"internetActivity_off", "")];
        
    }else{
       
        [self showProgressView];
        
      
        
        [AuthenticationService forgotPasswordWith:ForgotPasswordTextField.text success:^(BOOL success) {
            //TODO : Success
            if (success == YES) {
                
                [self hideProgressView];
                
                [self showAlert:NSLocalizedString(@"info_check_email_for_password_reset_email", "")];
                
                
            } else {
                
                //Email doesnt exist, this may not show because its not a localizedDescription
              //  [self showAlert:NSLocalizedString(@"xxxxxxx", "")@"Email doesnt exist."];
                
               [self hideProgressView];;
                
            }
        } failure:^(NSError * _Nullable error) {
            
            [self showAlert:NSLocalizedString(@"error_enter_valid_email", "")];
            
            [self hideProgressView];
            
             //default api fail message
            // [self showAlert:error.localizedDescription];
        }];
       

                 
             }
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == EmailTextField) {
        [textField resignFirstResponder];
        [PasswordTextField becomeFirstResponder];
    }else if (textField == PasswordTextField) {
        
        [textField resignFirstResponder];
    }
    
    return NO;
}



@end
