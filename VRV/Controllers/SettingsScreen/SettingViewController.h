

#import <UIKit/UIKit.h>
#import "OTPSendView.h"



@interface SettingViewController : UIViewController{
    __weak IBOutlet UITextField *txtName;
    __weak IBOutlet UITextField *txtEmail;
    __weak IBOutlet UITextField *txtMobileNumber;
    __weak IBOutlet UITextField *txtBirthday;
    __weak IBOutlet UITextField *txtGender;
    __weak IBOutlet UIButton *delaccradius;
    __weak IBOutlet UIButton *security;
    __weak IBOutlet UIButton *notification;
    __weak IBOutlet UIButton *share;
    __weak IBOutlet UIButton *ratebtn;
    
    __weak IBOutlet UIScrollView *MainScrollview;
    __weak IBOutlet UIView *MainView;
   
    
    __weak IBOutlet UILabel *settings;

    __weak IBOutlet UILabel *settings_name;
    
    __weak IBOutlet UILabel *settings_email;
    
    __weak IBOutlet UILabel *settings_mobile;
    
    __weak IBOutlet UILabel *settings_birthday;
    
    __weak IBOutlet UILabel *settings_gender;
    
    __weak IBOutlet UILabel *security2;
    
    __weak IBOutlet UILabel *settings_notifications;
    
    __weak IBOutlet UILabel *settings_share;
    
    __weak IBOutlet UILabel *settings_rate_app;
    
   // settings_delete_account;
    
    
    

    
}
- (IBAction)RightMenubuttonClick:(id)sender;
- (IBAction)btnSecurityClick:(id)sender;
- (IBAction)btnAccountClick:(id)sender;
- (IBAction)btnNotificationClick:(id)sender;

- (IBAction)btnShareClick:(id)sender;
- (IBAction)btnRateClick:(id)sender;

- (IBAction)changeEmail:(id)sender;
- (IBAction)changemobile:(id)sender;







@end
