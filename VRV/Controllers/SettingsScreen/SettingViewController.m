

#import "SettingViewController.h"
#import "LeftRightViewController.h"
#import "Reachability.h"
#import "AFNetworking.h"
#import "Defaults.h"
#import "CommanFunction.h"
#import "Reachability.h"
#import <StoreKit/StoreKit.h>
#import "VRV-Swift.h"

@interface SettingViewController (){
    NSUserDefaults *dlf;
}

@end

@implementation SettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [MainScrollview setScrollEnabled:YES];
    
    MainScrollview.contentSize = CGSizeMake(self.view.frame.size.width, MainView.frame.size.height);
    MainScrollview.backgroundColor = [UIColor whiteColor];
    
    UISwipeGestureRecognizer *gestureRecognizerRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandlerRight:)];
    [gestureRecognizerRight setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [self.view addGestureRecognizer:gestureRecognizerRight];
    
    
    dlf = [NSUserDefaults standardUserDefaults];
    self->txtName.userInteractionEnabled=false;
    self->txtEmail.userInteractionEnabled=false;
    self->txtBirthday.userInteractionEnabled=false;
    self->txtGender.userInteractionEnabled=false;
    self->txtMobileNumber.userInteractionEnabled=false;
    delaccradius.layer.cornerRadius = 4;
    security.layer.cornerRadius = 4;
    notification.layer.cornerRadius = 4;
    share.layer.cornerRadius = 4;
    ratebtn.layer.cornerRadius = 4;
    
    [self fetchUserData];
    [[UIButton appearance] setExclusiveTouch:YES];
    
    
    
    /*/Storyboard localization*/
    
    
    settings.text = NSLocalizedString(@"settings", "");
    
    settings_name.text = NSLocalizedString(@"settings_name", "");
    
    settings_email.text = NSLocalizedString(@"settings_email", "");
    
    settings_mobile.text = NSLocalizedString(@"settings_mobile", "");
    
    settings_birthday.text = NSLocalizedString(@"settings_birthday", "");
    
    settings_gender.text = NSLocalizedString(@"settings_gender", "");
    
    security2.text = NSLocalizedString(@"security2", "");
    
    settings_notifications.text = NSLocalizedString(@"settings_notifications", "");
    
    settings_share.text = NSLocalizedString(@"settings_share", "");
    
    settings_rate_app.text = NSLocalizedString(@"settings_rate_app", "");
    
    [delaccradius setTitle:NSLocalizedString(@"delaccradius", "") forState:UIControlStateNormal];
    
    
    
    
    
    
}

- (void)fetchUserData {
    [self showProgressView];
    [UserService getCurrentUserWithSuccess:^(UserDetails * _Nonnull user) {
        [self hideProgressView];
        
        self->txtName.text = [NSString stringWithFormat:@"%@ %@", user.first_name, user.last_name];
        self->txtEmail.text = user.email;
       self->txtGender.text = ([user.gender  isEqual: NSLocalizedString(@"at_m", "")] ? NSLocalizedString(@"at_male", "") : NSLocalizedString(@"at_female", ""));
        self->txtMobileNumber.text = user.mobile;
       self->txtBirthday.text = user.birth_day;
        
        
        if ([user.gender isEqual:NSLocalizedString(@"at_m", "")]) {
            self ->txtGender.text = NSLocalizedString(@"male_gender", "");
        } else {
            
            self ->txtGender.text = NSLocalizedString(@"female_gender", "");
            
        }
        
        
    } failure:^(NSError * _Nullable error) {
        [self handleError:error];
        [self hideProgressView];
    }];
}


-(void)DrawRoundRecyCornerFroUiview:(UIView *)MyView{
    MyView.layer.cornerRadius = 20;
    MyView.layer.masksToBounds = YES;
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)enableUserinteraction:(id)sender {
    
    [self.view setUserInteractionEnabled:YES];
    
}

- (void)rateAppMsg:(NSString *)message {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:NSLocalizedString(@"rate_h", "")
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* dismissButton = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"txt_dismiss", "")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   
                                   
                               }];
  
    
    UIAlertAction* okButton = [UIAlertAction
                                  actionWithTitle:NSLocalizedString(@"dialog_ok", "")
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action)
                                  {
//
//                                      [self.view setUserInteractionEnabled: NO];
//                                      [self performSelector:@selector(enableUserinteraction:) withObject:self afterDelay:2.2 ];
                                      
                                      // [AppReviewService.sharedInstance showImmediateReviewPopup];
                                      
                                     // [SKStoreReviewController requestReview];
        
   
        NSURL *cookie = [NSURL URLWithString:@"https://apps.apple.com/us/app/vrvc-coin/id1458954523"];
        
        [[UIApplication sharedApplication] openURL: cookie options:@{} completionHandler:^(BOOL success) {
            if (success)
                NSLog(@"Opened appstore");
        }];
        
    }];
    
    
    [alert addAction:dismissButton];
    [alert addAction:okButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}


- (IBAction)RightMenubuttonClick:(id)sender {
    LeftRightViewController *LeftrightviewScreenObject = [self.storyboard instantiateViewControllerWithIdentifier:@"leftright"];
    UIViewController *newVC = LeftrightviewScreenObject;
    NSMutableArray *vcs =  [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
    [vcs insertObject:newVC atIndex:vcs.count-1];
    [self.navigationController setViewControllers:vcs animated:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnSecurityClick:(id)sender{
   
    NSURL *sec = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/security_app"];
    
    [[UIApplication sharedApplication] openURL: sec options:@{} completionHandler:^(BOOL success) {
        if (success)
            NSLog(@"Opened sec");
         }];
    
}
     
- (IBAction)btnNotificationClick:(id)sender{
   
    NSURL *not = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/notification_app"];
    
    [[UIApplication sharedApplication] openURL: not options:@{} completionHandler:^(BOOL success) {
        if (success)
            NSLog(@"Opened not");
         }];
    
}

- (IBAction)btnAccountClick:(id)sender{
    
    OTPSendView *otpsendViewObj = [self.storyboard instantiateViewControllerWithIdentifier:@"otpid"];
    otpsendViewObj.isDeleteuser=YES;
    [self.navigationController pushViewController:otpsendViewObj animated:YES];
}

- (IBAction)changeEmail:(id)sender{
    
    OTPSendView *otpsendViewObj = [self.storyboard instantiateViewControllerWithIdentifier:@"otpid"];
    otpsendViewObj.isEmail=YES;
    [self.navigationController pushViewController:otpsendViewObj animated:YES];
}

- (IBAction)changemobile:(id)sender {
    
    OTPSendView *otpsendViewObj = [self.storyboard instantiateViewControllerWithIdentifier:@"otpid"];
    otpsendViewObj.isMobile=YES;
    [self.navigationController pushViewController:otpsendViewObj animated:YES];
}

- (IBAction)btnRateClick:(id)sender {
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus status =[reachability currentReachabilityStatus];
    if (status == NotReachable) {
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@""
                                      message:NSLocalizedString(@"internetActivity_off", "")
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* okButton = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"dialog_ok", "")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       
                                   }];
        [alert addAction:okButton];
        [self presentViewController:alert animated:YES completion:nil];
        
    }else{
    
        [self rateAppMsg:NSLocalizedString(@"rate_p", "")];
    }
}

- (IBAction)btnShareClick:(id)sender {
    NSURL *vrv = [NSURL URLWithString:@"https://itunes.apple.com/us/app/myapp/id1458954523?ls=1&mt=8"];
    NSString *shareMessage = NSLocalizedString(@"shareAppPara", "");
    
    NSArray* sharedObjects=[NSArray arrayWithObjects:shareMessage, vrv, nil];
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:sharedObjects applicationActivities:nil];
    activityViewController.popoverPresentationController.sourceView = self.view;
    [self presentViewController:activityViewController animated:YES completion:nil];
}

- (void)GetuserDetails{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus status =[reachability currentReachabilityStatus];
    if (status == NotReachable) {
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@""
                                      message:NSLocalizedString(@"internetActivity_off", "")
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* okButton = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"dialog_ok", "")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       
                                   }];
        [alert addAction:okButton];
        [self presentViewController:alert animated:YES completion:nil];
        
        
    }else{ // execute api/v1/customers/{id}
        
        }
    
}

-(void)NoInternent
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    if (remoteHostStatus == NotReachable)
    {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@""
                                      message:NSLocalizedString(@"internetActivity_off", "")
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* okButton = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"dialog_ok", "")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       
                                   }];
        [alert addAction:okButton];
        [self presentViewController:alert animated:YES completion:nil];
        
        
     
    }
}

-(void)swipeHandlerRight:(id)sender
{
    
    LeftRightViewController *LeftrightviewScreenObject = [self.storyboard instantiateViewControllerWithIdentifier:@"leftright"];
    UIViewController *newVC = LeftrightviewScreenObject;
    NSMutableArray *vcs =  [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
    [vcs insertObject:newVC atIndex:vcs.count-1];
    [self.navigationController setViewControllers:vcs animated:YES];
    [self.navigationController popViewControllerAnimated:YES];
    
}

@end
