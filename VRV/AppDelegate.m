//
//  AppDelegate.m
//  VRV
//
//  Created by vnnovate on 2017-02-13.
//  Copyright © 2017 vnnovate. All rights reserved.
//

#import "AppDelegate.h"
#import <GoogleMaps/GoogleMaps.h>
#import "LoginView.h"
#import "VRV-Swift.h"

@import Firebase;
//@import Stripe;

@import GooglePlaces;

#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)


@import GoogleMaps;

@interface AppDelegate ()
{
   // NSTimer *winnersintimecount;
    NSUserDefaults *dlf;
    
}
@end

@implementation AppDelegate

//com.VoluntaryRefundValues.app
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    dlf = [NSUserDefaults standardUserDefaults];
    
    [self initDependencies];
    NSLog(@"%ld",(long)[FIRConfiguration version]);
    [GMSPlacesClient provideAPIKey:@"AIzaSyC9pk1RjkGEQP-pq5HW7JIFXDC0x8BoCyI"];
    [GMSServices provideAPIKey:@"AIzaSyBNj5g7HQSqh4NojpuPG-7cC7DeYZ44V3A"];
    
    //pk_live_SkGWcO1JeWxnck5nzxMB90oi live
    //pk_test_NCaFrDK8paIUZjchV9Az69Yb test
   // [[STPPaymentConfiguration sharedConfiguration] setPublishableKey:@"pk_live_SkGWcO1JeWxnck5nzxMB90oi"];
  //  [[STPPaymentConfiguration sharedConfiguration] setAppleMerchantIdentifier:@"merchant.com.voluntaryrefundvalue.vrv"];

    //push notification
//    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)])
//    {
//        UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound);
//        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes categories:nil];
//        [application registerUserNotificationSettings:settings];
//        
//        [application registerForRemoteNotifications];
//    }
//    else
//    {
//        [application registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound)];
//    }
    
//     [dlf setObject:@"x" forKey:@"Miles"];
//     [dlf synchronize];
    
    [FIRApp configure];
    [FIRMessaging messaging].delegate = self;
    [FIRCrashlytics crashlytics];

    if ([UNUserNotificationCenter class] != nil) {
        // iOS 10 or later
        // For iOS 10 display notification (sent via APNS)
        [UNUserNotificationCenter currentNotificationCenter].delegate = self;
        UNAuthorizationOptions authOptions = UNAuthorizationOptionAlert |
        UNAuthorizationOptionSound | UNAuthorizationOptionBadge;
        [[UNUserNotificationCenter currentNotificationCenter]
         requestAuthorizationWithOptions:authOptions
         completionHandler:^(BOOL granted, NSError * _Nullable error) {
             // ...
         }];
    } else {
        // iOS 10 notifications aren't available; fall back to iOS 8-9 notifications.
        UIUserNotificationType allNotificationTypes =
        (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings =
        [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
        [application registerUserNotificationSettings:settings];
    }
    
    [application registerForRemoteNotifications];
    
    
    self.winnersintimecount = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                  target:self
                                                selector:@selector(timerfortimedifference)
                                                userInfo:nil
                                                 repeats:YES];
    

    
    return YES;
}


- (void)timerfortimedifference {
    
    NSCalendar *gregorian = [[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar];
    NSDate *date = [NSDate date];
    NSDateComponents *weekdayComponents = [gregorian components:NSWeekdayCalendarUnit fromDate:date];
    NSInteger todayWeekday = weekdayComponents.weekday;
    
    enum Weeks {
        SUNDAY = 1,
        MONDAY,
        TUESDAY,
        WEDNESDAY,
        THURSDAY,
        FRIDAY,
        SATURDAY
    };
    
    NSInteger moveDays=SATURDAY-todayWeekday;
    if (moveDays<=0)
    {
        moveDays+=7;
    }
    
    NSDateComponents *components = [NSDateComponents new];
    components.day=moveDays;
    
    NSCalendar *calendar=[[NSCalendar alloc] initWithCalendarIdentifier: NSGregorianCalendar];
    NSDate* NextSatDate = [calendar dateByAddingComponents:components toDate:date options:0];
  //  NSLog(@"%@",NextSatDate);
    
    
    //    // Set Date Formate of Next Saturday
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd";
    NSString *formattedDateString = [formatter stringFromDate: NextSatDate];
    
    
    NSString *time =[NSString stringWithFormat:@"%@ 05.59.00 pm",formattedDateString] ;
    
    NSDate *date1;
    NSDate *date2;
    {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"yyyy-MM-dd hh.mm.ss a";
        date1 = [formatter dateFromString:time];
        date2 = [formatter dateFromString:[formatter stringFromDate:[NSDate date]]];
        
    }
    NSTimeInterval interval = [date1 timeIntervalSinceDate: date2];//[date1 timeIntervalSince1970] - [date2 timeIntervalSince1970];
    int hour = interval / 3600;
    int minute = (int)interval % 3600 / 60;
    int seconds = floor(interval - (minute * 60) - (hour * 60 * 60));
    //NSLog(@"minute=%d",minute);
    
    self.Hourstring = [NSString stringWithFormat:@"%d",hour];
    if ( minute == 59 || minute == 60) {
        self.minutestring = [NSString stringWithFormat:@"%d",minute];
    }else{
        self.minutestring = [NSString stringWithFormat:@"%d",minute+2];
    }
    
    self.secondstring = [NSString stringWithFormat:@"%d",seconds];
   // NSLog(@"time=%@",self.secondstring);
    
}


- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
//    [GPPURLHandler handleURL:url
//                     sourceApplication:sourceApplication
//                            annotation:annotation];
    
    return YES;
}


-(void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    NSString *devToken = [[[deviceToken.description
                            stringByReplacingOccurrencesOfString:@"<"withString:@""]
                           stringByReplacingOccurrencesOfString:@">" withString:@""]
                          stringByReplacingOccurrencesOfString: @" " withString: @""];
    
//    _strDeviceTokenId = devToken;
//    // _strDeviceTokenIds = devToken;
//    NSLog(@"AppDelegatetoken=%@",devToken);
//    NSUserDefaults * preafs=[NSUserDefaults standardUserDefaults];
//
//    [preafs setObject:devToken forKey:@"devToken"];
//    [preafs synchronize];
    
    [FIRMessaging messaging].APNSToken = deviceToken;
}


#pragma mark Custome Method.

- (float)getDuration
{
    AVPlayerItem *currentItem = _audioPlayer.currentItem;
    CMTime duration = currentItem.duration;
    float DurationSeconds = CMTimeGetSeconds(duration);
    return DurationSeconds;
}

- (void)play
{
    [_audioPlayer play];
}

- (NSTimeInterval)getMinushTime;
{
    
    AVPlayerItem *currentItem = _audioPlayer.currentItem;
    CMTime duration = currentItem.duration;
    float DurationSeconds = CMTimeGetSeconds(duration);
    
    
    
    
    NSTimeInterval currentTime = CMTimeGetSeconds(currentItem.currentTime);
    
    double value=DurationSeconds-currentTime;
    
    
    return value;
}

- (void)pause
{
    [_audioPlayer pause];
}

- (void)stop
{
    [_audioPlayer pause];
    
}

- (float)getCurrentTime
{
    
    AVPlayerItem *currentItem = _audioPlayer.currentItem;
    NSTimeInterval currentTime = CMTimeGetSeconds(currentItem.currentTime);
    
    return currentTime;
}

- (float)getloadDuration
{
    AVPlayerItem * item = _audioPlayer.currentItem;
    float loadedDuration;
    if (item.status == AVPlayerItemStatusReadyToPlay)
    {
        NSArray * timeRangeArray = item.loadedTimeRanges;
        
        CMTimeRange aTimeRange = [timeRangeArray[0] CMTimeRangeValue];
        
        double startTime = CMTimeGetSeconds(aTimeRange.start);
        loadedDuration = CMTimeGetSeconds(aTimeRange.duration);
        
        // FIXME: shoule we sum up all sections to have a total playable duration,
        // or we just use first section as whole?
        
        NSLog(@"get time range, its start is %f seconds, its duration is %f seconds.", startTime, loadedDuration);
        
        
        // return (NSTimeInterval)(startTime + loadedDuration);
    }
    else
    {
        return(CMTimeGetSeconds(kCMTimeInvalid));
    }
    
    
    
    return loadedDuration;
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {
    NSDictionary *userInfo = notification.request.content.userInfo;
    
    // With swizzling disabled you must let Messaging know about the message, for Analytics
    // [[FIRMessaging messaging] appDidReceiveMessage:userInfo];
    
    // Print message ID.
//    if (userInfo[kGCMMessageIDKey]) {
//        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
//    }
    
    // Print full message.
    NSLog(@"%@", userInfo);
    
    // Change this to your preferred presentation option
    completionHandler(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge);
}



//Called to let your app know which action was selected by the user for a given notification.
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
didReceiveNotificationResponse:(UNNotificationResponse *)response
         withCompletionHandler:(void(^)(void))completionHandler {
    NSDictionary *userInfo = response.notification.request.content.userInfo;
//    if (userInfo[kGCMMessageIDKey]) {
//        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
//    }
    
    // Print full message.
    NSLog(@"%@", userInfo);
    
    completionHandler();
}


-(void) application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void
                                                                                                                               (^)(UIBackgroundFetchResult))completionHandler
{
    
    [[FIRMessaging messaging] appDidReceiveMessage:userInfo];
    
    // Print message ID.
//    if (userInfo[kGCMMessageIDKey]) {
//        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
//    }
    
    // Print full message.
    NSLog(@"%@", userInfo);
    
    
    NSDictionary *dicaps=[userInfo valueForKey:@"aps"];
    
    NSDictionary *server=[userInfo valueForKey:@"server"];
    
    NSString *unread_msg_count=[server valueForKey:@"unread_msg_count"];
    NSUInteger COUNT=unread_msg_count.integerValue;
    
    NSString *unread_msg_countS =[NSString stringWithFormat:@"%lu",(unsigned long)COUNT];
    
    
    NSString *stralert=dicaps[@"alert"];
    NSString *strMesssage=[NSString stringWithFormat:@"%@",stralert];
    
    if (([stralert isEqualToString:@""])||(stralert==nil)||([stralert isKindOfClass:[NSNull class]]))
    {
        
    }
    else
    {
        
        NSUserDefaults *preafs=[NSUserDefaults standardUserDefaults];
        [preafs setObject:unread_msg_countS forKey:@"MessageCountT"];
        [preafs synchronize];
        
        //  [self startSoundAndVibrate];
        
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"MessageFound" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"MessageFoundPush" object:self];
        
        
    }
    // custom code to handle notification content
    
    if( [UIApplication sharedApplication].applicationState == UIApplicationStateInactive )
    {
        NSLog( @"INACTIVE" );
        completionHandler( UIBackgroundFetchResultNewData );
        NSLog( @"BACKGROUND" );
        completionHandler( UIBackgroundFetchResultNewData );
        
        UNMutableNotificationContent *content = [[UNMutableNotificationContent alloc] init];
        content.title = [NSString localizedUserNotificationStringForKey:@"" arguments:nil];
        content.body = [NSString localizedUserNotificationStringForKey:strMesssage
                                                             arguments:nil];
        
        //  [content setValue:@(YES) forKeyPath:@"shouldAlwaysAlertWhileAppIsForeground"];
        
        
        content.sound = [UNNotificationSound defaultSound];
        //content.categoryIdentifier = @"";
        /// 4. update application icon badge number
        content.badge = @([UIApplication sharedApplication].applicationIconBadgeNumber + 1);
        content.launchImageName = @"any string is ok,such as 微博@iOS程序犭袁";
        // Deliver the notification in five seconds.
        //*** Terminating app due to uncaught exception 'NSInternalInconsistencyException', reason: 'time interval must be at least 60 if repeating'
        UNTimeIntervalNotificationTrigger *trigger = [UNTimeIntervalNotificationTrigger
                                                      triggerWithTimeInterval:60.0f repeats:NO];
        UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:@"OneSecond"
                                                                              content:content trigger:trigger];
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        /// 3. schedule localNotification,The delegate must be set before the application returns from applicationDidFinishLaunching:.
        // center.delegate = self;
        [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
            if (!error) {
                NSLog(@"add NotificationRequest succeeded!");
                
            }
        }];
        
    }
    else if( [UIApplication sharedApplication].applicationState == UIApplicationStateBackground )
    {
        NSLog( @"BACKGROUND" );
        completionHandler( UIBackgroundFetchResultNewData );
        
        UNMutableNotificationContent *content = [[UNMutableNotificationContent alloc] init];
        content.title = [NSString localizedUserNotificationStringForKey:@"" arguments:nil];
        content.body = [NSString localizedUserNotificationStringForKey:strMesssage
                                                             arguments:nil];
        
        //  [content setValue:@(YES) forKeyPath:@"shouldAlwaysAlertWhileAppIsForeground"];
        
        
        content.sound = [UNNotificationSound defaultSound];
        //content.categoryIdentifier = @"";
        /// 4. update application icon badge number
        content.badge = @([UIApplication sharedApplication].applicationIconBadgeNumber + 1);
        content.launchImageName = @"any string is ok,such as 微博@iOS程序犭袁";
        // Deliver the notification in five seconds.
        //*** Terminating app due to uncaught exception 'NSInternalInconsistencyException', reason: 'time interval must be at least 60 if repeating'
        UNTimeIntervalNotificationTrigger *trigger = [UNTimeIntervalNotificationTrigger
                                                      triggerWithTimeInterval:60.0f repeats:NO];
        UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:@"OneSecond"
                                                                              content:content trigger:trigger];
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        /// 3. schedule localNotification,The delegate must be set before the application returns from applicationDidFinishLaunching:.
        // center.delegate = self;
        [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
            if (!error) {
                NSLog(@"add NotificationRequest succeeded!");
                
            }
        }];
        
    }
    else
    {
        NSLog( @"FOREGROUND" );
        
        UNMutableNotificationContent *content = [[UNMutableNotificationContent alloc] init];
        content.title = [NSString localizedUserNotificationStringForKey:@"" arguments:nil];
        content.body = [NSString localizedUserNotificationStringForKey:strMesssage
                                                             arguments:nil];
        
        [content setValue:@(YES) forKeyPath:@"shouldAlwaysAlertWhileAppIsForeground"];
        
        
        content.sound = [UNNotificationSound defaultSound];
        // content.categoryIdentifier = @"";
        /// 4. update application icon badge number
        content.badge = @([UIApplication sharedApplication].applicationIconBadgeNumber + 1);
        content.launchImageName = @"any string is ok,such as 微博@iOS程序犭袁";
        // Deliver the notification in five seconds.
        //*** Terminating app due to uncaught exception 'NSInternalInconsistencyException', reason: 'time interval must be at least 60 if repeating'
        UNTimeIntervalNotificationTrigger *trigger = [UNTimeIntervalNotificationTrigger
                                                      triggerWithTimeInterval:60.0f repeats:NO];
        UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:@"OneSecond"
                                                                              content:content trigger:trigger];
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        /// 3. schedule localNotification,The delegate must be set before the application returns from applicationDidFinishLaunching:.
        // center.delegate = self;
        [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
            if (!error) {
                NSLog(@"add NotificationRequest succeeded!");
                
            }
        }];
        
        completionHandler( UIBackgroundFetchResultNewData );
        
        
        
        
    }
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    NSDictionary *dicaps=[userInfo valueForKey:@"aps"];
    
    NSDictionary *server=[userInfo valueForKey:@"server"];
    
    NSString *unread_msg_count=[server valueForKey:@"unread_msg_count"];
    NSUInteger COUNT=unread_msg_count.integerValue;
    
    NSString *unread_msg_countS =[NSString stringWithFormat:@"%lu",(unsigned long)COUNT];
    
    
    NSString *stralert=dicaps[@"alert"];
    NSString *strMesssage=[NSString stringWithFormat:@"%@",stralert];
    
    if (([stralert isEqualToString:@""])||(stralert==nil)||([stralert isKindOfClass:[NSNull class]]))
    {
        
    }
    else
    {
        
        NSUserDefaults *preafs=[NSUserDefaults standardUserDefaults];
        [preafs setObject:unread_msg_countS forKey:@"MessageCountT"];
        [preafs synchronize];
        
        // [self startSoundAndVibrate];
        
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"MessageFound" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"MessageFoundPush" object:self];
        
        
    }
    //    aps =     {
    //        alert = "notification message";
    //        badge = 1;
    //        sound = default;
    //    };
    //    server =     {
    //        "unread_msg_count" = "";
    //    };
    
    
    
    //  NSDictionary *dic=[userInfo valueForKey:@"server"];
    
    //  NSString *strdevice_name=[dic objectForKey:@"device_name"];
    //  NSString *stralarm_type=[dic objectForKey:@"alarm_type"];
    
    
    
    
    
    
    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
    // localNotification.userInfo = userInfo;
    
    localNotification.soundName = UILocalNotificationDefaultSoundName;
    localNotification.alertBody = strMesssage;
    localNotification.fireDate = [NSDate date];
    
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    
    UNMutableNotificationContent *content = [[UNMutableNotificationContent alloc] init];
    content.title = [NSString localizedUserNotificationStringForKey:@"" arguments:nil];
    content.body = [NSString localizedUserNotificationStringForKey:strMesssage
                                                         arguments:nil];
    
    //  [content setValue:@(YES) forKeyPath:@"shouldAlwaysAlertWhileAppIsForeground"];
    
    
    content.sound = [UNNotificationSound defaultSound];
    //content.categoryIdentifier = @"";
    /// 4. update application icon badge number
    content.badge = @([UIApplication sharedApplication].applicationIconBadgeNumber + 1);
    content.launchImageName = @"any string is ok,such as 微博@iOS程序犭袁";
    // Deliver the notification in five seconds.
    //*** Terminating app due to uncaught exception 'NSInternalInconsistencyException', reason: 'time interval must be at least 60 if repeating'
    UNTimeIntervalNotificationTrigger *trigger = [UNTimeIntervalNotificationTrigger
                                                  triggerWithTimeInterval:60.0f repeats:NO];
    UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:@"OneSecond"
                                                                          content:content trigger:trigger];
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    /// 3. schedule localNotification,The delegate must be set before the application returns from applicationDidFinishLaunching:.
    // center.delegate = self;
    [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
        if (!error) {
            NSLog(@"add NotificationRequest succeeded!");
            
        }
    }];
    
    /*
     aps =     {
     alert = PUSH;
     badge = 5;
     sound = default;
     };
     server =     {
     name = test;
     serverId = 1;
     };
     */
    //    if (application.applicationState == UIApplicationStateActive)
    //    {
    //        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Did receive a Remote Notification" message:[NSString stringWithFormat:@"%@",[[userInfo objectForKey:@"aps"] objectForKey:@"alert"]]delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    //        [alertView show];
    //    }
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)messaging:(FIRMessaging *)messaging didReceiveRegistrationToken:(NSString *)fcmToken {
    NSLog(@"FCM registration token: %@", fcmToken);
    // Notify about received token.
    NSDictionary *dataDict = [NSDictionary dictionaryWithObject:fcmToken forKey:@"token"];
    [[NSNotificationCenter defaultCenter] postNotificationName:
     @"FCMToken" object:nil userInfo:dataDict];
    
    _strDeviceTokenId = fcmToken;
    // _strDeviceTokenIds = devToken;
    NSLog(@"AppDelegatetoken=%@",fcmToken);
    NSUserDefaults * preafs=[NSUserDefaults standardUserDefaults];
    
    [preafs setObject:fcmToken forKey:@"devToken"];
    [preafs synchronize];
    
    // TODO: If necessary send token to application server.
    // Note: This callback is fired at each app startup and whenever a new token is generated.
}

- (void)messaging:(FIRMessaging *)messaging didReceiveMessage:(FIRMessagingRemoteMessage *)remoteMessage {
    NSLog(@"Received data message: %@", remoteMessage.appData);
}

@end
