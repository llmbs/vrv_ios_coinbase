//
//  ConfigurationManager.swift
//
//
//  Created by Abhishek Sheth on 19/02/18.
//  Copyright © 2018 Abhishek Sheth. All rights reserved.
//

import UIKit

class ConfigurationManager: NSObject {
    enum UserDefaultKey: String {
        case user = "User"
    }
    
    @objc static let shared = ConfigurationManager()
    
    private override init() {
         super.init()
    }
    
    @objc var currentUser : UserDetails? {
        set {
            if let user = newValue {
                UserDefaults.standard.set(user.toDictionary(), forKey: UserDefaultKey.user.rawValue)
            } else {
                UserDefaults.standard.removeObject(forKey: UserDefaultKey.user.rawValue)
            }
        } get {
            var user : UserDetails? = nil
            if let data = UserDefaults.standard.value(forKey: UserDefaultKey.user.rawValue) as? NSDictionary {
                user = UserDetails(dictionary: data)
            }
            
            return user
        }
    }
    
    
    @objc func isLoggedIn() -> Bool {
        return !(self.currentUser == nil)
    }
}
