//
//  BeaconManager.swift
//  SwiftBeaconManager
//
//  Created by Duncan MacDonald on 5/2/18.
//  Copyright © 2018 Duncan MacDonald. All rights reserved.
//

import Foundation
import CoreLocation
import GoogleMaps
import EVReflection

@objc public class LocationManager: NSObject, CLLocationManagerDelegate {
    
    static let shared = LocationManager()
    private override init() { }
    
    var locationManager: CLLocationManager!
    var delegate : LocationManagerDelegate? = nil
    
    var isLocationEnabled : Bool! = false
    
    private var lastLocation: CLLocation? {
        didSet {
            if let location = lastLocation {
                delegate?.locationManager(sender: self, didTrace: location)
            }
        }
    }
    
    private func startMonitoring() {
        locationManager = CLLocationManager()
        locationManager.allowsBackgroundLocationUpdates = false
        locationManager.delegate = self
        
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        PrintOptions.Active = .None
    }
    
    public func stopMonitoring() {
        locationManager.stopUpdatingLocation()
    }
    
    @objc func getLastLocationCordinates() -> CLLocationCoordinate2D {
        if let location = self.lastLocation {
            return location.coordinate
        } else {
            return CLLocationCoordinate2D(latitude: 0, longitude: 0)
        }
    }
    
    public func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            isLocationEnabled = false
            locationManager.requestAlwaysAuthorization()
            break
        case .authorizedWhenInUse:
            isLocationEnabled = true
            break
        case .authorizedAlways:
            isLocationEnabled = true
            break
        case .restricted:
            isLocationEnabled = false
            // restricted by e.g. parental controls. User can't enable Location Services
            break
        case .denied:
            isLocationEnabled = false
            // user denied your app access to Location Services, but can grant access from Settings.app
            break
        }
    }
    
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.lastLocation = locations.first
    }
    
    @objc class func reverseGeoCode(address : String!, success : @escaping (String, String) -> Void, failure : @escaping (Error?) -> Void) {
        let addressEncoded : String! = address.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        let url = "https://maps.googleapis.com/maps/api/geocode/json?&address=\(addressEncoded!)&key=AIzaSyALRh6SG5DKjPi4A-HNEDBfT7Vo92DmWp8"
        
        NetworkManager.shared.customget1(url: url, success: { (response) in
            if let data = response["results"] as? [NSDictionary], let firstObj = data.first, let geometry = firstObj["geometry"] as? [String : Any], let location = geometry["location"] as? [String : Any] {
                
                let lat = location["lat"] as! NSNumber
                let lng = location["lng"] as! NSNumber
                
                success(lat.stringValue, lng.stringValue)
            } else {
                failure(nil)
            }
        }, failure:failure)
    }
    
    @objc class func geocode(_ coordinate : CLLocationCoordinate2D, success : @escaping ([GMSAddress]) -> Void, failure : @escaping (Error) -> Void) {
        GMSGeocoder().reverseGeocodeCoordinate(coordinate) { (result, error) in
            if let error = error {
                failure(error)
            } else {
                if let results = result?.results() {
                    success(results)
                }
            }
        }
    }
    
    @objc class func openInGoogleMaps(_ location: CLLocationCoordinate2D, address : String) {
        let latlong = "\(location.latitude),\(location.longitude)"
        let addressEncoded = address.replacingOccurrences(of: " ", with: "+")
        //let str = "comgooglemaps://?daddr=\(latlong)&zoom=14&views=traffic&directionsMode=driving"
        let str = "https://maps.google.com/?q=\(addressEncoded)@\(latlong)"
        if let url = URL(string: str), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url)
        } else {
            
        }
    }
}

public protocol LocationManagerDelegate: class {
    func locationManager(sender: LocationManager, didTrace location: CLLocation)
}
