//
//  NetworkManager.swift
//  Focus
//
//  Created by Abhishek Sheth on 23/03/17.
//  Copyright © 2017 ITCan. All rights reserved.
//

import UIKit
import Alamofire

extension NSError {
    class func error(code : Int!, errorMessage : String!) -> Error {
        let userInfo: [String : Any] =
            [
                NSLocalizedDescriptionKey : errorMessage,
                NSLocalizedFailureReasonErrorKey : errorMessage
        ]
        
        let err = NSError(domain: "", code: code, userInfo: userInfo)
        return err
    }
}

class NetworkManager: NSObject {
    static let shared = NetworkManager()
    private override init () {
        super.init()
    }
    
    func get(baseURL : String!,path : String!, params : [String : String]? = nil, logOutput : Bool = true, success : @escaping (Any?) -> Void, failure : @escaping (Error) -> Void) {
        var finalPath : String! = path
        if let params = params {
            var components = URLComponents()
            components.queryItems = params.map {
                URLQueryItem(name: $0, value: $1)
            }
            if let queryString = components.url?.absoluteString {
                finalPath = finalPath.appending(queryString)
            }
        }
        self.apiCall(baseURL: baseURL, path: finalPath, method: .get, logOutput : logOutput, success: success, failure: failure)
    }
    
    func getString(baseURL : String!,path : String!, params : [String : String]? = nil, logOutput : Bool = true, success : @escaping (Any?) -> Void, failure : @escaping (Error) -> Void) {
        var finalPath : String! = path
        if let params = params {
            var components = URLComponents()
            components.queryItems = params.map {
                URLQueryItem(name: $0, value: $1)
            }
            if let queryString = components.url?.absoluteString {
                finalPath = finalPath.appending(queryString)
            }
        }
        self.apiCallForStringResponse(baseURL: baseURL, path: finalPath, method: .get, logOutput : logOutput, success: success, failure: failure)
    }
    
    func post(baseURL : String!, path : String!, params : [String : Any], success : @escaping (Any?) -> Void, failure : @escaping (Error) -> Void) {
        self.apiCall(baseURL: baseURL, path: path, method: .post, params: params, success: success, failure: failure)
    }
    
    func postWithStatus(baseURL : String!, path : String!, params : [String : Any], success : @escaping (Any?,Int) -> Void, failure : @escaping (Error,Int) -> Void) {
        self.apiCallWithStatus(baseURL: baseURL, path: path, method: .post, params: params) { (suc, statusCode) in
            success(suc,statusCode)
        } failure: { (fail, statusCode) in
            failure(fail,statusCode)
        }
    }
    
    func postString(baseURL : String!, path : String!, params : [String : Any], success : @escaping (Any?) -> Void, failure : @escaping (Error) -> Void) {
        self.apiCallForStringResponse(baseURL: baseURL, path: path, method: .post, params: params, success: success, failure: failure)
    }
    
    func put(baseURL : String!, path : String!, params : [String : Any], success : @escaping (Any?) -> Void, failure : @escaping (Error) -> Void) {
        self.apiCall(baseURL: baseURL, path: path, method: .put, params: params, success: success, failure: failure)
    }
    
    func apiCallForStringResponse(baseURL : String!, path : String!, method : HTTPMethod, params : [String : Any]? = nil, logOutput : Bool = true, success : @escaping (Any?) -> Void, failure : @escaping (Error) -> Void) {
        
        var headers: HTTPHeaders = [
            "Accept": "application/json"
        ]
        
        if let token = ConfigurationManager.shared.currentUser?.access_token {
            headers["Authorization"] = token
        }
        
        let parameters = params
        
        let finalPath = baseURL + path
        debugPrint("API : ", finalPath)
        debugPrint("Params : ", parameters)
        
        AF.request(finalPath, method: method, parameters: parameters, encoding: JSONEncoding.default, headers : headers).responseString(completionHandler: { (response) in
            print(response.request)
            print(response.request?.allHTTPHeaderFields)
            
            if (logOutput) {
                debugPrint("Response String : ", response.value)
            }
            
            self.handleStringResponse(path: finalPath, response: response, success: success, failure: failure)
        })
    }
    
    func apiCall(baseURL : String!, path : String!, method : HTTPMethod, params : [String : Any]? = nil, logOutput : Bool = true, success : @escaping (Any?) -> Void, failure : @escaping (Error) -> Void) {
        
        var headers: HTTPHeaders = [
            "Accept": "application/json"
        ]
        
        if let token = ConfigurationManager.shared.currentUser?.access_token {
            headers["Authorization"] = token
        }
        
        let parameters = params
        
        let finalPath = baseURL + path
        debugPrint("API : ", finalPath)
        debugPrint("Params : ", parameters as Any)
        
        AF.request(finalPath, method: method, parameters: parameters, encoding: JSONEncoding.default, headers : headers).responseString(completionHandler: { (response) in
            if (logOutput) {
                debugPrint("Response String : ", response.value ?? "")
            }
        }).responseJSON { response in
            //print(response.request)
            //print(response.request?.allHTTPHeaderFields)
            self.handleResponse(path: finalPath, response: response, success: success, failure: failure)
        }
    }
    
    func apiCallWithStatus(baseURL : String!, path : String!, method : HTTPMethod, params : [String : Any]? = nil, logOutput : Bool = true, success : @escaping (Any?,Int) -> Void, failure : @escaping (Error,Int) -> Void) {
        
        var headers: HTTPHeaders = [
            "Accept": "application/json"
        ]
        
        if let token = ConfigurationManager.shared.currentUser?.access_token {
            headers["Authorization"] = token
        }
        
        let parameters = params
        
        let finalPath = baseURL + path
        debugPrint("API : ", finalPath)
        debugPrint("Params : ", parameters as Any)
        
        AF.request(finalPath, method: method, parameters: parameters, encoding: JSONEncoding.default, headers : headers).responseString(completionHandler: { (response) in
            if (logOutput) {
                debugPrint("Response String : ", response.value ?? "")
            }
        }).responseJSON { response in
            //print(response.request)
            //print(response.request?.allHTTPHeaderFields)
            self.handleResponseWithStatus(path: finalPath, response: response) { (suc, statusCode) in
                success(suc,statusCode)
            } failure: { (fail, statusCode) in
                success(fail,statusCode)
            }
        }
    }
    
    func downloadFile(path : String!, destinationPath : URL!, success : @escaping (URL?) -> Void, failure : @escaping (Error) -> Void) {
        
        AF.download(path, method: .get, to: { (url, response) -> (destinationURL: URL, options: DownloadRequest.Options) in
            return (destinationPath, [.removePreviousFile, .createIntermediateDirectories])
        }).response(completionHandler: { (downloadResponse) in
            debugPrint(downloadResponse)
            success(downloadResponse.fileURL)
        })
    }
    
    func customget1(url : String!, success : @escaping (Dictionary<String,Any>) -> Void, failure : @escaping (Error) -> Void) {
        let headers: HTTPHeaders = [
            "Accept": "application/json"
        ]
        
        AF.request(url, method: HTTPMethod.get, parameters: nil, encoding: JSONEncoding.default, headers : headers).responseString(completionHandler: { (response) in
            debugPrint("Response String : ", response)
            if var response = response.value {
                //response = response.replacingOccurrences(of: "root", with: "\"root\"")
                //response = response.replacingOccurrences(of: "vehicledata", with: "\"vehicledata\"")
                
                //response = self.stringByRemovingControlCharacters2(string: response)
                
                if let data = response.data(using: .utf8) {
                    do {
                        if let jsonArray = try JSONSerialization.jsonObject(with: data, options : []) as? Dictionary<String,Any> {
                            success(jsonArray)
                        } else {
                            print("bad json")
                            let error = NSError.error(code: -1, errorMessage: "Invalid Response")
                            failure(error)
                        }
                    } catch let error as NSError {
                        print(error)
                        failure(error)
                    }
                }
            }
        })
    }
    
    func apiCallwithStatusResponse(baseURL : String!, path : String!, method : HTTPMethod, params : [String : Any]? = nil, logOutput : Bool = true, success : @escaping (Int) -> Void, failure : @escaping (Error) -> Void) {
        
        var headers: HTTPHeaders = [
            "Accept": "application/json"
        ]
        
        if let token = ConfigurationManager.shared.currentUser?.access_token {
            headers["Authorization"] = token
        }
        
        let parameters = params
        
        let finalPath = baseURL + path
        debugPrint("API : ", finalPath)
        debugPrint("Params : ", parameters as Any)
        
        AF.request(finalPath, method: method, parameters: parameters, encoding: JSONEncoding.default, headers : headers).responseString(completionHandler: { (response) in
            if (logOutput) {
                debugPrint("Response String : ", response.value ?? "")
            }
        }).responseJSON { response in
            //print(response.request)
            //print(response.request?.allHTTPHeaderFields)
            success(response.response?.statusCode ?? 0)
            //self.handleResponse(path: finalPath, response: response, success: success, failure: failure)
        }
    }
    
    func apiCallwithResponse(baseURL : String!, path : String!, method : HTTPMethod, params : [String : Any]? = nil, logOutput : Bool = true, success : @escaping (Int,String) -> Void, failure : @escaping (Error) -> Void) {
        
        var headers: HTTPHeaders = [
            "Accept": "application/json"
        ]
        
        if let token = ConfigurationManager.shared.currentUser?.access_token {
            headers["Authorization"] = token
        }
        
        let parameters = params
        
        let finalPath = baseURL + path
        debugPrint("API : ", finalPath)
        debugPrint("Params : ", parameters as Any)
        
        AF.request(finalPath, method: method, parameters: parameters, encoding: JSONEncoding.default, headers : headers).responseString(completionHandler: { (response) in
            if (logOutput) {
                debugPrint("Response String : ", response.value ?? "")
            }
            
        }).responseJSON { response in
                        
            if let data = response.data {
                do {
                    if let jsonArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [Dictionary<String,Any>]
                    {
                       print(jsonArray) // use the json here
                    } else {
                        print("bad json")
                    }
                } catch let error as NSError {
                    print(error)
                }
            }

            var responseString = ""
            if let responseValue = response.value as? String {
                responseString = responseValue
            }
            
            success(response.response?.statusCode ?? 0, responseString)
        }
    }
    
    func handleResponse(path : String!, response : AFDataResponse<Any>, success : @escaping (Any?) -> Void, failure : @escaping (Error) -> Void) {
        switch response.result {
        case .success(let result):
            if let responseCode = response.response?.statusCode, responseCode == 401  {
                let serverError = NSError.error(code: responseCode, errorMessage: "Session expired")
                failure(serverError)
            } else {
                success(result)
            }
        case .failure(let error):
            let responseCode : Int! = response.response?.statusCode ?? 0
            let serverError = NSError.error(code: responseCode, errorMessage: error.localizedDescription)
            failure(serverError)
        }
    }
    
    func handleResponseWithStatus(path : String!, response : AFDataResponse<Any>, success : @escaping (Any?,Int) -> Void, failure : @escaping (Error,Int) -> Void) {
        switch response.result {
        case .success(let result):
            if let responseCode = response.response?.statusCode, responseCode == 401  {
                let serverError = NSError.error(code: responseCode, errorMessage: "Session expired")
                failure(serverError,response.response?.statusCode ?? 0)
            } else {
                success(result, response.response?.statusCode ?? 0)
            }
        case .failure(let error):
            let responseCode : Int! = response.response?.statusCode ?? 0
            let serverError = NSError.error(code: responseCode, errorMessage: error.localizedDescription)
            failure(serverError, response.response?.statusCode ?? 0)
        }
    }
    
    func handleStringResponse(path : String!, response : AFDataResponse<String>, success : @escaping (Any?) -> Void, failure : @escaping (Error) -> Void) {
        switch response.result {
        case .success(let result):
            if let responseCode = response.response?.statusCode, responseCode == 401  {
                let serverError = NSError.error(code: responseCode, errorMessage: "Session expired")
                failure(serverError)
            } else {
                success(result)
            }
        case .failure(let error):
            let responseCode : Int! = response.response?.statusCode ?? 0
            let serverError = NSError.error(code: responseCode, errorMessage: error.localizedDescription)
            failure(serverError)
        }
    }
    
    
    func apiCallWithStatusCodeAndResponse(baseURL : String!, path : String!, method : HTTPMethod, params : [String : Any]? = nil, logOutput : Bool = true, success : @escaping (Any?, Int?) -> Void, failure : @escaping (Error) -> Void) {
        
        var headers: HTTPHeaders = [
            "Accept": "application/json"
        ]
        
        if let token = ConfigurationManager.shared.currentUser?.access_token {
            headers["Authorization"] = token
        }
        
        let parameters = params
        
        let finalPath = baseURL + path
        debugPrint("API : ", finalPath)
        debugPrint("Params : ", parameters as Any)
        
        AF.request(finalPath, method: method, parameters: parameters, encoding: JSONEncoding.default, headers : headers).responseString(completionHandler: { (response) in
            if (logOutput) {
                debugPrint("Response String : ", response.value ?? "")
            }
        }).responseJSON { response in
            //print(response.request)
            //print(response.request?.allHTTPHeaderFields)
            self.handleResponseWithStatusCode(path: finalPath, response: response, success: success, failure: failure)
        }
    }
    
    func handleResponseWithStatusCode(path : String!, response : AFDataResponse<Any>, success : @escaping (Any?, Int?) -> Void, failure : @escaping (Error) -> Void) {
        switch response.result {
        case .success(let result):
            if let responseCode = response.response?.statusCode, responseCode == 401  {
                let serverError = NSError.error(code: responseCode, errorMessage: "Session expired")
                failure(serverError)
            } else {
                success(result, response.response?.statusCode)
            }
        case .failure(let error):
            let responseCode : Int! = response.response?.statusCode ?? 0
            let serverError = NSError.error(code: responseCode, errorMessage: error.localizedDescription)
            failure(serverError)
        }
    }
    
}
