//
//  ViewController+Extension.swift
//  VRV
//
//  Created by baps on 26/09/18.
//  Copyright © 2018 Abhishek Sheth. All rights reserved.
//

import UIKit
import MBProgressHUD

extension UIViewController {
    @objc internal func showProgressView() {
        DispatchQueue.main.async(execute: {
            let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
            self.view.bringSubviewToFront(hud)
        })
    }
    
    @objc internal func hideProgressView() {
        DispatchQueue.main.async(execute: {
            MBProgressHUD.hide(for: self.view, animated: true)
        })
    }
    
    @objc func handleError(_ error : Error?) {
        self.handleError(error, errorMessage: nil, handler: nil)
    }
    
    @objc func handleError(_ error : Error?, errorMessage : String? = nil, handler : ((UIAlertAction) -> Swift.Void)? = nil) {
        hideProgressView()
        if let error = error as NSError?, error.code == 401 {
            //Handle Logout
            ConfigurationManager.shared.currentUser = nil
            
            if let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "loginid") {
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
        } else if let message = error?.localizedDescription {
            showErrorAlertView(message)
        } else if let errorMessage = errorMessage {
            showErrorAlertView(errorMessage)
        }
    }
    
    @objc func showErrorAlertView(_ message : String) {
        showAlertView(message, title: "")
    }
    
    @objc func showAlertView(_ message : String, title : String = "", okString : String! = "Ok", handler : ((UIAlertAction) -> Swift.Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle:.alert)
        let ok : String! = okString
        
        alert.addAction(UIAlertAction(title: ok, style: .default, handler: handler))
        present(alert, animated: true, completion: nil)
    }
}
