//
//  UIViewExtension.swift
//  Smart City Vadodara
//
//  Created by BAPS on 11/05/18.
//  Copyright © 2018 Amor Innovations. All rights reserved.
//

import UIKit

extension UIView {
    func applyCornerRadius(radius : CGFloat!) {
        self.layer.masksToBounds = true
        self.layer.cornerRadius = radius
    }
    
    class func transparentView(width : CGFloat, height : CGFloat) -> UIView! {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: width, height: height))
        view.backgroundColor = UIColor.clear
        return view
    }
    
    @objc class func Nib() -> UINib? {
        let hasNib: Bool = Bundle.main.path(forResource: self.nameOfClass, ofType: "nib") != nil
        guard hasNib else {
            assert(!hasNib, "Invalid parameter") // assert
            return UINib()
        }
        
        return UINib(nibName: self.nameOfClass, bundle:nil)
    }
}

