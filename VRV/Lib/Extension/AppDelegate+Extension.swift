//
//  AppDelegate+Extension.swift
//  VRV
//
//  Created by Abhishek on 06/04/19.
//  Copyright © 2019 Abhishek Sheth. All rights reserved.
//

import Foundation
import AppRating

extension AppDelegate {
    @objc func initDependencies() {
        AppReviewService.sharedInstance.initialize()
        AppReviewService.sharedInstance.addSignificantEvent()
    }
}

class AppReviewService: NSObject {
    let appId = "1458954523"
    
    static let sharedInstance: AppReviewService = {
        let instance = AppReviewService()
        return instance
    }()
    
    override init() {
        super.init()
    }
    
    func initialize() {
        //Rating
        AppRating.appID(self.appId)
        //AppRating.debugEnabled(true)
        AppRating.daysUntilPrompt(1000)
        AppRating.significantEventsUntilPrompt(10000);
    }
    
    func addSignificantEvent() {
        //AppRating.userDidSignificantEvent(canPromptForRating: true)
    }
    
    func showImmediateReviewPopup() {
        AppRating.showRatingAlert()
        AppRating.resetAllCounters()
    }
}
