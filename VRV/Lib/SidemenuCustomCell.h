//
//  SidemenuCustomCell.h
//  VRV
//
//  Created by vnnovate on 2017-02-16.
//  Copyright © 2017 vnnovate. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SidemenuCustomCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *MenuItemName;
@property (weak, nonatomic) IBOutlet UIImageView *MenuItemImage;
@property (weak, nonatomic) IBOutlet UILabel *LastLabelforTableview;

@property (weak, nonatomic) IBOutlet UIImageView *redsirenimage;
@property (weak, nonatomic) IBOutlet UIView *cellviewforcontent;

@end
