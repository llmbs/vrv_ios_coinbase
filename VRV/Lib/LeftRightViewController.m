//
//  LeftRightViewController.m
//  VRV
//
//  Created by Mac 02 on 31/03/17.
//  Copyright © 2017 vnnovate. All rights reserved.
//

#import "LeftRightViewController.h"
#import "SidemenuCustomCell.h"
#import "LoginView.h"
#import "Defaults.h"
#import "RecylingLocationView.h"
#import "SocialNetworkingView.h"
#import "WinnersInView.h"
#import "SweepstakesView.h"
#import "AdvertisingView.h"
#import "AboutUsView.h"
#import "ReadytoRecycleView.h"
#import "CurrentFundsView.h"
#import "AFNetworking.h"
#import "CommanFunction.h"
#import "Reachability.h"
#import "RecycleItemCustomeCell.h"
#import "ScanQRViewController.h"
#import "SettingViewController.h"
#import "VRV-Swift.h"
#import <AVFoundation/AVFoundation.h>
#import <Lottie/Lottie.h>
#import "MainTabBarController.h"

@interface LeftRightViewController ()
{
  
    
    __weak IBOutlet UIView *NormalView;
    
    
}

@property (nonatomic, strong) LOTAnimationView *lottieLogo;

@end


@implementation LeftRightViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
   // [[AppReviewService sharedInstance] addSignificantEvent];

//    [self DrawRoundRecyCornerFroUiview:View1];
//    [self DrawRoundRecyCornerFroUiview:View2];
//    [self DrawRoundRecyCornerFroUiview:View3];
//    [self DrawRoundRecyCornerFroUiview:View4];
//    [self DrawRoundRecyCornerFroUiview:View5];
//    [self DrawRoundRecyCornerFroUiview:View6];
//    [self DrawRoundRecyCornerFroUiview:View7];
//    [self DrawRoundRecyCornerFroUiview:View8];
//    [self DrawRoundRecyCornerFroUiview:settingbackview];
//    [self DrawRoundRecyCornerFroUiview:View10];
    

    /*LOCALIZATION STORYBOARD*/
    
    app_description.text = NSLocalizedString(@"app_description", "");
    
    recycling_locations.text = NSLocalizedString(@"recycling_locations", "");
    
    current_funds.text = NSLocalizedString(@"current_funds", "");
    
    social_networking.text = NSLocalizedString(@"social_networking", "");
    
    ready_to_recycle.text = NSLocalizedString(@"ready_to_recycle", "");
    
    winners_in.text = NSLocalizedString(@"winners_in", "");
    
    sweepstakes.text = NSLocalizedString(@"sweepstakes", "");
    
    advertising.text = NSLocalizedString(@"advertising", "");
    
    about_us.text = NSLocalizedString(@"about_us", "");
    
    settings.text = NSLocalizedString(@"settings", "");
    
    logout.text = NSLocalizedString(@"logout", "");
    
    Main_menu.text = NSLocalizedString(@"Main_menu", "");
    
    puzzle.text = NSLocalizedString(@"puzzle", "");
    
    
    
    
    
    
    
    [Button1 setBackgroundImage:[CommanFunction imageWithColor:[UIColor grayColor]] forState:UIControlStateHighlighted];
    [Button2 setBackgroundImage:[CommanFunction imageWithColor:[UIColor grayColor]] forState:UIControlStateHighlighted];
    [Button3 setBackgroundImage:[CommanFunction imageWithColor:[UIColor grayColor]] forState:UIControlStateHighlighted];
    [Button4 setBackgroundImage:[CommanFunction imageWithColor:[UIColor grayColor]] forState:UIControlStateHighlighted];
    [Button5 setBackgroundImage:[CommanFunction imageWithColor:[UIColor grayColor]] forState:UIControlStateHighlighted];
    [Button6 setBackgroundImage:[CommanFunction imageWithColor:[UIColor grayColor]] forState:UIControlStateHighlighted];
    [Button7 setBackgroundImage:[CommanFunction imageWithColor:[UIColor grayColor]] forState:UIControlStateHighlighted];
    [Button8 setBackgroundImage:[CommanFunction imageWithColor:[UIColor grayColor]] forState:UIControlStateHighlighted];
    [Button9 setBackgroundImage:[CommanFunction imageWithColor:[UIColor grayColor]] forState:UIControlStateHighlighted];
    [Button10 setBackgroundImage:[CommanFunction imageWithColor:[UIColor grayColor]] forState:UIControlStateHighlighted];
    
    NormalView.hidden = NO;
  //  [[UIButton appearance] setExclusiveTouch:YES];
   
    PuzzleButton.layer.cornerRadius=4;
    

//    self.lottieLogo = [LOTAnimationView animationNamed:@"circle"];
//    self.lottieLogo.frame = CGRectMake(0, 0, 300 , 300);
//    self.lottieLogo.center = PuzzleButton.center;
//    self.lottieLogo.contentMode = UIViewContentModeScaleAspectFill;
//    self.lottieLogo.loopAnimation = YES;
//    [puzzleView addSubview:self.lottieLogo];
//    [self.lottieLogo play];
    
    
    self.lottieLogo = [LOTAnimationView animationNamed:@"circle"];
    self.lottieLogo.frame = CGRectMake(0, 0, 450 , 450);
    self.lottieLogo.center = puzzleView.center;
    self.lottieLogo.contentMode = UIViewContentModeScaleAspectFill;
    self.lottieLogo.loopAnimation = YES;
    [self.view addSubview:self.lottieLogo];
    [self.lottieLogo play];
    [self.view bringSubviewToFront:puzzleView];
    
}

-(void)DrawRoundRecyCornerFroUiview:(UIView *)MyView{
    MyView.layer.cornerRadius = 20;
    MyView.layer.masksToBounds = YES;

}

- (IBAction)SettingButtonClick:(id)sender {
    [self.view setUserInteractionEnabled: NO];
    SettingViewController *SettingViewScreenobj = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingViewController"];
    [self.navigationController pushViewController:SettingViewScreenobj animated:YES];
}
- (IBAction)LogoutButtonClick:(id)sender {
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Ecology Realtor"
                                  message:NSLocalizedString(@"logout_message", "")
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"dialog_ok", "")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   [AuthenticationService logoutWith:[ConfigurationManager shared].currentUser.getUserId success:^(BOOL success) {
                                       [self logout];
                                   } failure:^(NSError * _Nullable error) {
                                       [self logout];
                                   }];
                               }];
    
    
    UIAlertAction* cancelButton = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"txt_cancel", "")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   
                               }];
    
     [alert addAction:cancelButton];
    [alert addAction:okButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (void)logout {
    [ConfigurationManager shared].currentUser = nil;
    
    LoginView *LoginScreenObj = [self.storyboard instantiateViewControllerWithIdentifier:@"loginid"];
    [self.navigationController pushViewController:LoginScreenObj animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark IBACTION Methods.

- (IBAction)RecyclingLocationButtonClick:(id)sender {
    [self.view setUserInteractionEnabled: NO];
   RecylingLocationView *RecylinglocationScreenObj = [self.storyboard instantiateViewControllerWithIdentifier:@"ReCycleLocationId"];
    [self.navigationController pushViewController:RecylinglocationScreenObj animated:YES];
   
    //for testing turn in screen code screen
    
   // ScanQRViewController *ScanqrScreenObj = [self.storyboard instantiateViewControllerWithIdentifier:@"qrcode"];
  //  [self.navigationController pushViewController:ScanqrScreenObj animated:YES];
    
    
    
  
    
}


- (IBAction)CurrentFundsButtonClick:(id)sender {
    [self.view setUserInteractionEnabled: NO];
    CurrentFundsView *CurrentFundsScreenObj = [self.storyboard instantiateViewControllerWithIdentifier:@"CurrentFundID"];
    [self.navigationController pushViewController:CurrentFundsScreenObj animated:YES];
}

- (IBAction)SocialNetworking:(id)sender {
    [self.view setUserInteractionEnabled: NO];
    SocialNetworkingView *SocailNetworkingScreenObj = [self.storyboard instantiateViewControllerWithIdentifier:@"SocialnetworkingID"];
    [self.navigationController pushViewController:SocailNetworkingScreenObj animated:YES];
}

- (IBAction)ReadytoRecycleButtonClick:(id)sender {
    [self.view setUserInteractionEnabled: NO];
    MainTabBarController *tabController = [[MainTabBarController alloc]init];
    [tabController setupTabBar];
    [self.navigationController pushViewController:tabController animated:YES];
}

- (IBAction)WinnersInButtonClick:(id)sender {
    [self.view setUserInteractionEnabled: NO];
    WinnersInView *WinnersInScreenObj = [self.storyboard instantiateViewControllerWithIdentifier:@"winnersinid"];
    [self.navigationController pushViewController:WinnersInScreenObj animated:YES];
    
}

- (IBAction)SweepstakesButtonClick:(id)sender {
    [self.view setUserInteractionEnabled: NO];
    SweepstakesView *SweepstakesScreenObj = [self.storyboard instantiateViewControllerWithIdentifier:@"SweepstakesId"];
    [self.navigationController pushViewController:SweepstakesScreenObj animated:YES];
}

- (IBAction)AdvertisingButtonClick:(id)sender {
    [self.view setUserInteractionEnabled: NO];
    AdvertisingView *AdvertisingScreenObj = [self.storyboard instantiateViewControllerWithIdentifier:@"AdvertisingID"];
    [self.navigationController pushViewController:AdvertisingScreenObj animated:YES];
}

- (IBAction)ABoutUsButtonClick:(id)sender {
    [self.view setUserInteractionEnabled: NO];
    AboutUsView *AboutUsScreenObj = [self.storyboard instantiateViewControllerWithIdentifier:@"AboutUsViewId"];
    [self.navigationController pushViewController:AboutUsScreenObj animated:YES];
}

- (IBAction)VrvWebsiteButtonClick:(id)sender {
    
    NSURL *VrvWebsite = [NSURL URLWithString:@"https://www.vrvccoin.com"];
    
    [[UIApplication sharedApplication] openURL: VrvWebsite options:@{} completionHandler:^(BOOL success) {
        if (success)
            NSLog(@"Opened Vrv website");
    }];
    
}

- (IBAction)PuzzleButtonClick:(id)sender {
    [self.view setUserInteractionEnabled: NO];
    MainTabBarController *tabController = [[MainTabBarController alloc]init];
    [tabController setupTabBar];
    [self.navigationController pushViewController:tabController animated:YES];
}


@end
