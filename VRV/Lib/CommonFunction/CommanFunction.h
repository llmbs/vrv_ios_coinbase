
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Defaults.h"
#import "AnimatedGif.h"
#import "UIImageView+AnimatedGif.h"


@interface CommanFunction : NSObject

+(UIColor *)colorFromHexString:(NSString *)hexString;
+ (CGSize) calculateScrollViewSize:(UIScrollView*) scrollView;


+ (void)displayAlertView : (NSString *)alertMessageString;
+ (void)showLoading : (UIView *)view;
+ (void)hideLoading;


//+(BOOL)CheckinternetConnection;
+(void)BordertoView:(UIView *)view;
+(void)BOrdertoButton:(UIButton *)button;
+(void)ScrollViewMethod:(UIScrollView *)scrollview viewfloatsize:(CGFloat *)viewsize viewscreen:(UIView *)view;

+ (BOOL) isiPad;
+ (BOOL)validateEmailWithString:(NSString*)email;
+ (NSString *)changeDateFormate : (NSString *)dateString;

+ (void)startanimate:(UIImageView *)Loadingimage backview:(UIView *)loadingBackView;
+ (void)stopanimate:(UIImageView *)Loadingimage backview:(UIView *)loadingBackView;

//+ (CGSize) calculateScrollViewSize:(UIScrollView*) scrollView;
//+ (CGSize) calculateScrollViewSize:(UIScrollView*) scrollView;
//+ (void)showLoading : (UIView *)view;
//+ (void)hideLoading;
//+ (UIColor *)colorFromHexString:(NSString *)hexString;

+ (UIImage *)imageWithColor:(UIColor *)color;

@end
