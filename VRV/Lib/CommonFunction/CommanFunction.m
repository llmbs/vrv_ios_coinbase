

#import "CommanFunction.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "Reachability.h"


@implementation CommanFunction
static MBProgressHUD *loadinView;

+ (UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    scanner.scanLocation = 1; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}


//DISPLAY ALERT
+ (void)displayAlertView : (NSString *)alertMessageString{
//    UIAlertView *commanAllert = [[UIAlertView alloc]initWithTitle:@"Alert" message:alertMessageString delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//    [commanAllert show];
}

+ (void)showLoading : (UIView *)view
{
    loadinView = [MBProgressHUD showHUDAddedTo:view animated:YES];
    loadinView.mode = MBProgressHUDModeIndeterminate;
    loadinView.labelText = @"Loading";
}

+ (void)hideLoading
{
    loadinView.hidden = YES;
}

//+(void)GestureInisilaztionMethod:(UIView *)view{
//   }




/* THIS FUNCTION CALCULATES THE SIZE OF THE SCROLLVIEW BASED ON NUMBER OF VIEWS IT CURRENTLY HOLDS. */
+ (CGSize) calculateScrollViewSize:(UIScrollView*) scrollView {
    CGRect contentRect = CGRectZero;
    for (UIView *view in scrollView.subviews)
    {
        contentRect = CGRectUnion(contentRect, view.frame);
    }
    contentRect.size.width = 260;
    return contentRect.size;
}



+(void)BordertoView:(UIView *)view{
    view.layer.borderColor = [UIColor whiteColor].CGColor;
    view.layer.borderWidth = 1.0f;
    view.layer.cornerRadius=14.0f;
}

+(void)BOrdertoButton:(UIButton *)button{
    button.layer.cornerRadius = 14.0f;
    [button.layer setMasksToBounds:YES];
    button.layer.borderWidth = 1.0f;
    button.layer.borderColor = [UIColor clearColor].CGColor;
}

+(void)ScrollViewMethod:(UIScrollView *)scrollview viewfloatsize:(CGFloat *)viewsize viewscreen:(UIView *)view{
    [scrollview setScrollEnabled:YES];
    scrollview.contentSize = CGSizeMake(view.frame.size.width, *viewsize);
}

+ (BOOL) isiPad{
#if (__IPHONE_OS_VERSION_MAX_ALLOWED >= 30200)
    if ([[UIDevice currentDevice] respondsToSelector: @selector(userInterfaceIdiom)])
        return ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad);
#endif
    return NO;
}

/* THIS FUNCTION CALCULATES THE SIZE OF THE SCROLLVIEW BASED ON NUMBER OF VIEWS IT CURRENTLY HOLDS. */

#pragma mark - EMAIL VALIDATION METHOD
+ (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}



+ (NSString *)changeDateFormate : (NSString *)dateString
{
    NSDateFormatter *df = [[NSDateFormatter alloc]init];
    df.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    
    NSDate *bdate = [df dateFromString:dateString];
    
    df.dateFormat = @"dd/MMM/yyyy";
    NSString *newDateString = [df stringFromDate:bdate];
    return newDateString;
}

+ (void)startanimate:(UIImageView *)Loadingimage backview:(UIView *)loadingBackView{
    Loadingimage.hidden = NO;
    loadingBackView.hidden =NO;
    NSData * animationData = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"vrv.gif" ofType:nil]];
    AnimatedGif * animation = [AnimatedGif getAnimationForGifWithData:animationData];
    [Loadingimage setAnimatedGif:animation startImmediately:YES];
}

+ (void)stopanimate:(UIImageView *)Loadingimage backview:(UIView *)loadingBackView;
{
    NSData * animationData = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"vrv.gif" ofType:nil]];
    AnimatedGif * animation = [AnimatedGif getAnimationForGifWithData:animationData];
    [Loadingimage setAnimatedGif:animation startImmediately:NO];
    Loadingimage.hidden = YES;
     loadingBackView.hidden =YES;
}

+ (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, color.CGColor);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}


@end









