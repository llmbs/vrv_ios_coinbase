

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>


@interface LeftRightViewController : UIViewController

//UICollectionViewDelegate,UICollectionViewDataSource

{

    //UIlabel
    
    __weak IBOutlet UILabel *app_description;
    
    __weak IBOutlet UILabel *recycling_locations;
    
    __weak IBOutlet UILabel *current_funds;
    
    __weak IBOutlet UILabel *social_networking;
    
    __weak IBOutlet UILabel *ready_to_recycle;
    
    __weak IBOutlet UILabel *winners_in;
    
    __weak IBOutlet UILabel *sweepstakes;
    
    __weak IBOutlet UILabel *advertising;
    
    __weak IBOutlet UILabel *about_us;
    
    __weak IBOutlet UILabel *settings;
    
    __weak IBOutlet UILabel *logout;
    
    __weak IBOutlet UILabel *Main_menu;
    
    __weak IBOutlet UILabel *puzzle;
    
   
    
    
    
    
    // UiVIew Round Rect
    __weak IBOutlet UIView *View1;
    __weak IBOutlet UIView *View2;
    __weak IBOutlet UIView *View3;
    __weak IBOutlet UIView *View4;
    __weak IBOutlet UIView *View5;
    __weak IBOutlet UIView *View6;
    __weak IBOutlet UIView *View7;
    __weak IBOutlet UIView *View8;
    __weak IBOutlet UIView *View10;
    
    __weak IBOutlet UIButton *Button1;
    __weak IBOutlet UIButton *Button2;
    __weak IBOutlet UIButton *Button3;
    __weak IBOutlet UIButton *Button4;
    __weak IBOutlet UIButton *Button5;
    __weak IBOutlet UIButton *Button6;
    __weak IBOutlet UIButton *Button7;
    __weak IBOutlet UIButton *Button8;
    __weak IBOutlet UIButton *Button9;
    __weak IBOutlet UIButton *Button10;
    
    __weak IBOutlet UIView *puzzleView;
    __weak IBOutlet UIButton *PuzzleButton;
    

    __weak IBOutlet UIView *settingbackview;
    
   

}
- (IBAction)SettingButtonClick:(id)sender;
- (IBAction)LogoutButtonClick:(id)sender;
-(IBAction)PuzzleButtonClick:(id)sender;


// Menu Button CLicked Events
- (IBAction)RecyclingLocationButtonClick:(id)sender;
- (IBAction)CurrentFundsButtonClick:(id)sender;
- (IBAction)SocialNetworking:(id)sender;
- (IBAction)ReadytoRecycleButtonClick:(id)sender;
- (IBAction)WinnersInButtonClick:(id)sender;
- (IBAction)SweepstakesButtonClick:(id)sender;
- (IBAction)AdvertisingButtonClick:(id)sender;
- (IBAction)ABoutUsButtonClick:(id)sender;
- (IBAction)VrvWebsiteButtonClick:(id)sender;





@end
