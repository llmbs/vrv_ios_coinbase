

#import <Foundation/Foundation.h>

/*  OS DETECTION CODE. */
#define SYSTEM_VERSION_EQUAL_TO(v)([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)
#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

/*  DEVICE DETECTION CODE.*/
#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_6 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )667 ) < DBL_EPSILON )
#define IS_IPHONE_6P ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )736 ) < DBL_EPSILON )

//#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
//#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
//#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
//#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
#define LOCALIZED_FILE @"Localizable"

//NSUSER DEFAULAT KEYS
#define KEY_USER_LOGIN_INFO @"user_login_info"
#define KEY_USER_IS_LOGIN @"is_user_login"

#define KEY_USER_ADDRESS_INFO @"user_address"
#define KEY_USER_Name @"user_name"
#define KEY_USER_Contact_no @"contact_no"
#define KEY_USER_Customer_Code @"Cus_code"
#define KEY_USER_Email_id @"Email_id"
#define KEY_USER_Type @"Business/user"

#define KEY_TOKEN_ID @"device_token_id"
#define KEY_LOGIN_USER_ID @"user_id"
#define KEY_USER_LOGIN_OPTION @"login_option"
#define KEY_USER_LANGUAGE @"user_language"
#define KEY_REGISTER_STATUS @"register status"

#define KEY_IS_USER_SET_PIN @"is_user_set_pin"
#define KEY_USER_PIN @"user_pin"
#define SELECTED_STORE_ID @"store_id"

//http://vnnovate.com/voluntary/
//Replace to
//http://voluntaryrefundvalue.com/admin/

//#define BASE_URL @"http://vnnovate.com/voluntary/webservices/webservice.php?" // OLD
#define BASE_URL @"http://voluntaryrefundvalue.com/admin/webservices/webservice.php?" // NEW
//#define BASE_URL_NEW @"http://staging.voluntaryrefundvalue.com/api/"
#define BASE_URL_NEW @"https://uat.voluntaryrefundvalue.com/api/"
#define CONTEST_URL @"https://uat.voluntaryrefundvalue.com/link/contest_game"

//REGISTRATION SCREEN TEXTFIELD VALIDATION

#define REGEX_USER_NAME_LIMIT @"^.{3,10}$"
#define REGEX_USER_NAME @"[A-Za-z0-9]{3,10}"
#define REGEX_EMAIL @"[A-Z0-9a-z._%+-]{3,}+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
#define REGEX_PASSWORD_LIMIT @"^.{6,20}$"
#define REGEX_PASSWORD @"[A-Za-z0-]{6,20}"
#define REGEX_PHONE_DEFAULT @"[0-9]{10}"
#define REGEX_CITY_LIMIT @"^.{1,100}$"
#define REGEX_CITY @"[A-Za-z0-9]{1,100}"

#define TACK_SHOP ((int)1)
#define FOOD_STOCK ((int)2)
#define SANITARY_PRODUCT ((int)3)

@interface Defaults : NSObject

@end
