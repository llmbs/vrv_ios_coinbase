//
//  SidemenuCustomCell.m
//  VRV
//
//  Created by vnnovate on 2017-02-16.
//  Copyright © 2017 vnnovate. All rights reserved.
//

#import "SidemenuCustomCell.h"

@implementation SidemenuCustomCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
