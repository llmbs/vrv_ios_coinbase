//
//  SPGooglePlacesAutocompleteUtilities.h
//  SPGooglePlacesAutocomplete
//
//  Created by Stephen Poletto on 7/18/12.
//  Copyright (c) 2012 Stephen Poletto. All rights reserved.
//
#import <UIKit/UIKit.h>
// AIzaSyAlijttedTIQlhvibcPJ1IstC7TV9g9i6o

#define kGoogleAPIKey @"AIzaSyAlijttedTIQlhvibcPJ1IstC7TV9g9i6o"
#define kGoogleAPINSErrorCode 42

@class CLPlacemark;

typedef NS_ENUM(unsigned int, SPGooglePlacesAutocompletePlaceType) {
    SPPlaceTypeGeocode = 0,
    SPPlaceTypeEstablishment
};

typedef void (^SPGooglePlacesPlacemarkResultBlock)(CLPlacemark *placemark, NSString *addressString, NSError *error);
typedef void (^SPGooglePlacesAutocompleteResultBlock)(NSArray *places, NSError *error);
typedef void (^SPGooglePlacesPlaceDetailResultBlock)(NSDictionary *placeDictionary, NSError *error);

extern SPGooglePlacesAutocompletePlaceType SPPlaceTypeFromDictionary(NSDictionary *placeDictionary);
extern NSString *SPBooleanStringForBool(BOOL boolean);
extern NSString *SPPlaceTypeStringForPlaceType(SPGooglePlacesAutocompletePlaceType type);
extern BOOL SPEnsureGoogleAPIKey();
extern void SPPresentAlertViewWithErrorAndTitle(NSError *error, NSString *title);
extern BOOL SPIsEmptyString(NSString *string);

@interface NSArray(SPFoundationAdditions)
@property (NS_NONATOMIC_IOSONLY, readonly, strong) id onlyObject;
@end
