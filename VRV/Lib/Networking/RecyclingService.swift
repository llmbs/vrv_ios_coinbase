//
//  RecyclingService.swift
//  VRV
//
//  Created by Abhishek on 09/12/18.
//  Copyright © 2018 Abhishek Sheth. All rights reserved.
//

import Foundation

class RecyclingService : NSObject {
    @objc class func createRecycle(type : String, quantity : String, deposit : String, token : String, success : @escaping (Int,String) -> Void, failure : @escaping (Error?) -> Void) {
        let url = String(format: "v1/recycle/create")
        
        var params : [String:Any] = [:]
        params["customer_id"] = ConfigurationManager.shared.currentUser?.id ?? 0
        params["type"] = type
        params["quantity"] = quantity
        params["vrv_deposit"] = deposit
        params["token"] = token
        
        /* // Older code
        NetworkManager.shared.post(baseURL: BASE_URL_NEW, path: url, params: params, success: { (response) in
            success(true)
        }) { (error) in
            failure(error)
        }*/
        
        NetworkManager.shared.apiCallwithResponse(baseURL: BASE_URL_NEW, path: url, method: .post, params: params, success: { (responseCode, responseString) in
            success(responseCode,responseString)
        }) { (error) in
                failure(error);
        }
        
//        NetworkManager.shared.apiCallWithStatusCodeAndResponse(baseURL: BASE_URL_NEW, path: url, method: .post, params: params, success: { (response, statusCode) in
//            if let response = response as? [String:Any], let statusCode = statusCode {
//                let dictionary: NSDictionary = NSDictionary(dictionary: response)
//                success(dictionary, statusCode)
//            }
//        }) { (error) in
//            failure(error)
//        }
        
    }
    
    @objc class func readyToTurn(success : @escaping (Int, Int, Int) -> Void, failure : @escaping (Error?) -> Void) {
        let url = String(format: "v1/recycle/readyToTurn")
        
        var params : [String:Any] = [:]
        params["customer_id"] = ConfigurationManager.shared.currentUser?.id ?? 0
        
        NetworkManager.shared.post(baseURL: BASE_URL_NEW, path: url, params: params, success: { (response) in
            print(response)
            if let response = response as? [String:Any] {
                if let cloth = response["cloth"] as? Int, let cork = response["cork"] as? Int, let plastic = response["plastic"] as? Int {
                    success(cloth, cork, plastic)
                }
            }
        }) { (error) in
            failure(error);
        }
    }
    
    @objc class func markAsComplete(type : String, success : @escaping (Int) -> Void, failure : @escaping (Error?) -> Void) {
        let url = String(format: "v1/recycle/markAllComplete")
        
        var params : [String:Any] = [:]
        params["customer_id"] = ConfigurationManager.shared.currentUser?.id ?? 0
        params["type"] = type
        
        NetworkManager.shared.post(baseURL: BASE_URL_NEW, path: url, params: params, success: { (response) in
            if let response = response as? [String:Any], let code = response["code"] as? Int {
                success(code);
            }
        }) { (error) in
            failure(error);
        }
    }
    
    @objc class func recycleCheckDeposit(success : @escaping (Int) -> Void, failure : @escaping (Error?) -> Void) {
        let url = String(format: "v1/recycle/depositCheck")
        
        var params : [String:Any] = [:]
        params["customer_id"] = ConfigurationManager.shared.currentUser?.id ?? 0
 
        NetworkManager.shared.apiCallwithStatusResponse(baseURL: BASE_URL_NEW, path: url, method: .post, params: params, success: { (responseCode) in
            success(responseCode)
        }) { (error) in
            failure(error);
        }
    }
    
    @objc class func checkPaymentStatus(with code: String, completion: @escaping (NSDictionary) -> Void, failure: @escaping (Error?) -> Void) {
        let url = String(format: "v1/coinbase/status/\(code)")
        
        NetworkManager.shared.post(baseURL: BASE_URL_NEW, path: url, params: [:], success: { (response) in
            if let response = response as? [String:Any] {
                let dictionary: NSDictionary = NSDictionary(dictionary: response)
                completion(dictionary)
            }
        }) { (error) in
            failure(error)
        }
        
    }
    
    @objc class func changedTheUSDCOfACustomer(usdc: String, success: @escaping (Int) -> Void, failure: @escaping (Error?) -> Void) {
        let url = String(format: "v1/customers/usdc")
        
        var params : [String:Any] = [:]
        params["id"] = ConfigurationManager.shared.currentUser?.id ?? 0
        params["usdc"] = usdc
        
        NetworkManager.shared.apiCallwithStatusResponse(baseURL: BASE_URL_NEW, path: url, method: .post, params: params, success: { (responseCode) in
            success(responseCode);
        }) { (error) in
            failure(error);
        }
    }
    
    @objc class func changedTheBNBOfACustomer(bnb: String, success: @escaping (Int) -> Void, failure: @escaping (Error?) -> Void) {
        let url = String(format: "v1/customers/bnb")
        
        var params : [String:Any] = [:]
        params["id"] = ConfigurationManager.shared.currentUser?.id ?? 0
        params["bnb"] = bnb
        
        NetworkManager.shared.apiCallwithStatusResponse(baseURL: BASE_URL_NEW, path: url, method: .post, params: params, success: { (responseCode) in
            success(responseCode);
        }) { (error) in
            failure(error);
        }
    }
    
    @objc class func gamerQualified(success: @escaping (Int) -> Void, failure: @escaping (Error?) -> Void) {
        let url = String(format: "v1/gamerqualified")
        
        var params : [String:Any] = [:]
        params["id"] = ConfigurationManager.shared.currentUser?.id ?? 0
        
        NetworkManager.shared.apiCallwithStatusResponse(baseURL: BASE_URL_NEW, path: url, method: .post, params: params, success: { (responseCode) in
            success(responseCode);
        }) { (error) in
            failure(error);
        }
        
    }
    
    @objc class func checkRankWc(success: @escaping ([ScoreRankData], Int) -> Void, failure: @escaping (Error?) -> Void) {
        let url = String(format: "v1/checkRankWc")
        
        var params : [String:Any] = [:]
        params["customer_id"] = ConfigurationManager.shared.currentUser?.id ?? 0
        
        NetworkManager.shared.apiCallWithStatusCodeAndResponse(baseURL: BASE_URL_NEW, path: url, method: .post, params: params) { (response, statusCode) in
            if let response = response as? [String:Any], let statusCode = statusCode {
                let dictionary: NSDictionary = NSDictionary(dictionary: response)
                if let topData = dictionary.value(forKey: "top") as? NSArray, let data = dictionary.value(forKey: "data") as? NSArray {
                    var dataArray: [ScoreRankData] = []
                    for (index, _) in topData.enumerated() {
                        let scoreRankData = ScoreRankData(dictionary: topData[index] as! NSDictionary)
                        dataArray.append(scoreRankData)
                    }
                    
                    if dataArray.contains(where: {$0.customer_id == ConfigurationManager.shared.currentUser?.id ?? 0}) {
                        dataArray = dataArray.sorted(by: { $0.rank < $1.rank})
                        success(dataArray, statusCode)
                        return
                    }
                    
                    
                    for (index, _) in data.enumerated() {
                        let scoreRankData = ScoreRankData(dictionary: data[index] as! NSDictionary)
                        dataArray.append(scoreRankData)
                    }
                    
                    dataArray = dataArray.sorted(by: { $0.rank < $1.rank})
                    success(dataArray, statusCode)
                }
            }
        } failure: { (error) in
            failure(error);
        }

        
    }

    
    
    
    
    
}
