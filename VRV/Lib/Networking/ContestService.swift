//
//  ContestService.swift
//  VRV
//
//  Created by Abhishek on 01/11/19.
//  Copyright © 2019 Abhishek Sheth. All rights reserved.
//

import Foundation

class ContestService : NSObject {
    @objc class func createContest(success : @escaping (Int) -> Void, failure : @escaping (Error?) -> Void) {
        let url = String(format: "v1/contest/create")
        
        var params : [String:Any] = [:]
        params["customer_id"] = ConfigurationManager.shared.currentUser?.id ?? 0
        
        NetworkManager.shared.post(baseURL: BASE_URL_NEW, path: url, params: params, success: { (response) in
            
            if let response = response as? [String:Any] {
                if let code = response["code"] as? Int {
                    success(code)
                } else if let message = response["message"] as? String {
                    let error = NSError.error(code: 427, errorMessage: message)
                    failure(error)
                }
            }
            
            failure(nil)
        }) { (error) in
            failure(error)
        }
    }
    
    @objc class func startGame(success : @escaping (String, Int) -> Void, failure : @escaping (Error?) -> Void) {
        let url = String(format: "v1/contest/start_game")
        
        var params : [String:Any] = [:]
        params["customer_id"] = ConfigurationManager.shared.currentUser?.id ?? 0
        params["play_started"] = true
        
        NetworkManager.shared.post(baseURL: BASE_URL_NEW, path: url, params: params, success: { (response) in
            if let response = response as? [String:Any] {
                if let code = response["data"] as? String {
                    success(code, 0)
                } else if let message = response["message"] as? String {
                    let error = NSError.error(code: 427, errorMessage: message)
                    failure(error)
                }
            }
            
            failure(nil)
        }) { (error) in
            failure(error)
        }
    }
}
