//
//  AuthenticationService.swift
//  VRV
//
//  Created by baps on 06/09/18.
//  Copyright © 2018 vnnovate. All rights reserved.
//

import UIKit

@objc class AuthenticationService: NSObject {
    @objc class func login(with email : String!, password : String!, success : @escaping (UserDetails) -> Void, failure : @escaping (Error?) -> Void) {
        let params : NSMutableDictionary = NSMutableDictionary()
        params["email"] = email
        params["password"] = password
        
        let url = String(format: "%@v1/login", BASE_URL_NEW)
        
        let manger = AFHTTPRequestOperationManager.init()
        manger.post(url, parameters: params, success: { (operation, response) in
            print(response)
            if let responseObject = response as? NSDictionary {
                if let message = responseObject["message"] as? String {
                    let error = NSError.error(code: -1, errorMessage: message)
                    failure(error)
                } else {
                    let user = UserDetails(dictionary: responseObject)
                    if user.is_active == 1 {
                        ConfigurationManager.shared.currentUser = user
                        if let token = UserDefaults.standard.string(forKey: "devToken") {
                            self.updateDevice(with: user.id, success: { (successData) in
                                success(user)
                            }, failure: failure)
                        } else {
                            success(user)
                        }
                    } else {
                        let error = NSError.error(code: -1, errorMessage: "Inactive Account")
                        failure(error)
                    }
                }
            } else {
                failure(nil)
            }
        }) { (operation, error) in
            print(error)
            failure(error)
        }
    }
    
    @objc class func register(with email : String!, password : String!, first_name : String!, last_name : String!, mobile : String!, gender : String!, birth_day : String!, success : @escaping (UserDetails) -> Void, failure : @escaping (Error?) -> Void) {
        let params : NSMutableDictionary = NSMutableDictionary()
        params["email"] = email
        params["password"] = password
        params["first_name"] = first_name
        params["last_name"] = last_name
        params["mobile"] = mobile
        params["gender"] = gender
        params["birth_day"] = birth_day
        
        let url = String(format: "%@v1/register", BASE_URL_NEW)
        
        let manger = AFHTTPRequestOperationManager.init()
        manger.post(url, parameters: params, success: { (operation, response) in
            print(response)
            if let responseObject = response as? NSDictionary {
                let user = UserDetails(dictionary: responseObject)
                success(user)
            } else {
                failure(nil)
            }
        }) { (operation, error) in
            print(error)
            failure(error)
        }
    }
    
    @objc class func forgotPassword(with email : String!, success : @escaping (Bool) -> Void, failure : @escaping (Error?) -> Void) {
        let params : NSMutableDictionary = NSMutableDictionary()
        params["email"] = email
        
        let url = String(format: "%@v1/customers/forgot", BASE_URL_NEW)
        
        let manger = AFHTTPRequestOperationManager.init()
        manger.post(url, parameters: params, success: { (operation, response) in
            print(response)
            if operation?.response.statusCode == 404 {
                success(false)
            } else {
                success(true)
            }
        }) { (operation, error) in
            print(error)
            failure(error)
        }
    }
    
    @objc class func updateDevice(with customerId : Int, success : @escaping (Bool) -> Void, failure : @escaping (Error?) -> Void) {
        var params : [String : Any] = [:]
        params["customer_id"] = customerId
        params["token"] = UserDefaults.standard.string(forKey: "devToken")
        params["device_id"] = UIDevice.current.identifierForVendor?.uuidString
        params["device_type"] = "iOS"
        
        NetworkManager.shared.post(baseURL: BASE_URL_NEW, path: "v1/devices", params: params, success: { (response) in
            success(true)
        }) { (error) in
            failure(error)
        }
    }
    
    @objc class func logout(with customerId : Int, success : @escaping (Bool) -> Void, failure : @escaping (Error?) -> Void) {
        var params : [String : Any] = [:]
        params["customer_id"] = customerId
        params["token"] = UserDefaults.standard.string(forKey: "devToken")
        params["device_id"] = UIDevice.current.identifierForVendor?.uuidString
        params["device_type"] = "iOS"
        
        NetworkManager.shared.post(baseURL: BASE_URL_NEW, path: "v1/logout", params: params, success: { (response) in
            success(true)
        }) { (error) in
            failure(error)
        }
    }
    
    //::
    @objc class func cordinateConfirmation(with id: Int, longitude: String, latitude: String, success : @escaping (Bool) -> Void, failure : @escaping (Error?) -> Void) {
        var params : [String : Any] = [:]
        params["id"] = "\(id)"
        params["lat"] = latitude
        params["lng"] = longitude
       params["location_date"] = "\(Date())"
        
        NetworkManager.shared.post(baseURL: BASE_URL_NEW, path: "v1/recycle/coordinate/confirmation", params: params, success: { (response) in
            success(true)
        }) { (error) in
            failure(error)
        }
    }
    //::
    
    
}
