//
//  BusinessService.swift
//  VRV
//
//  Created by baps on 26/09/18.
//  Copyright © 2018 Abhishek Sheth. All rights reserved.
//

import UIKit

@objc class BusinessService: NSObject {
    @objc class func getBusiness(_ key : NSString, lat : Double, lng : Double, success : @escaping ([BusinessData]) -> Void, failure : @escaping (Error?) -> Void) {
        let url = String(format: "v1/business/name_limit?key=%@&lat=%f&lng=%f&count=10", key, lat, lng)
        
        NetworkManager.shared.get(baseURL: BASE_URL_NEW, path: url, success: { (response) in
            if let responseObject = response as? [NSDictionary] {
                var data : [BusinessData] = []
                for businessDict in responseObject {
                    let business = BusinessData(dictionary: businessDict)
                    data.append(business)
                }
                success(data)
            }
        }) { (error) in
            failure(error);
        }
    }
    
    @objc class func getAllBusiness(success : @escaping ([BusinessData]) -> Void, failure : @escaping (Error?) -> Void) {
        let url = String(format: "v1/business/all")
        
        var params : [String:String] = [:]
        if let id = ConfigurationManager.shared.currentUser?.id {
            params["customer_id"] = "\(id)"
        }
        
        
        NetworkManager.shared.get(baseURL: BASE_URL_NEW, path: url, params: params, logOutput: false, success: { (response) in
            if let responseObject = response as? [NSDictionary] {
                var data : [BusinessData] = []
                for businessDict in responseObject {
                    let business = BusinessData(dictionary: businessDict)
                    if business.is_active == 1 {
                        data.append(business)
                    }
                }
                success(data)
            }
        }) { (error) in
            failure(error);
        }
    }
    
    @objc class func deleteAllBusiness(success : @escaping (Bool) -> Void, failure : @escaping (Error?) -> Void) {
        let url = String(format: "v1/business/delete_all")
        
        var params : [String:String] = [:]
        params["customer_id"] = "\(ConfigurationManager.shared.currentUser?.id ?? 0)"
        
        NetworkManager.shared.getString(baseURL: BASE_URL_NEW, path: url, params:params, success: { (response) in
            if let responseObject = response as? String {
                success(true)
            }
        }) { (error) in
            failure(error)
        }
    }
    
    @objc class func deleteSingleBusiness(business_id : Int, success : @escaping (Bool) -> Void, failure : @escaping (Error?) -> Void) {
        let url = String(format: "v1/business/delete_one")
        
        var params : [String:String] = [:]
        params["customer_id"] = "\(ConfigurationManager.shared.currentUser?.id ?? 0)"
        params["business_id"] = "\(business_id)"
        
        NetworkManager.shared.getString(baseURL: BASE_URL_NEW, path: url, params:params, success: { (response) in
            if let responseObject = response as? String {
                success(true)
            }
        }) { (error) in
            failure(error)
        }
    }
    
    @objc class func reportBusiness(_ id : Int, type : String, success : @escaping (Bool) -> Void, failure : @escaping (Error?) -> Void) {
        let url = String(format: "v1/business/not_accepting")
        
        var params : [String:Any] = [:]
        params["id"] = id
        params["type"] = type
        
        NetworkManager.shared.post(baseURL: BASE_URL_NEW, path: url, params:params, success: { (response) in
            if let responseObject = response as? NSDictionary {
                success(true)
            }
        }) { (error) in
            failure(error)
        }
    }
    
    @objc class func addBusiness(first_name : String, last_name : String, company_name : String, cork_business : Bool, cloth_business :Bool, plastic_business : Bool, phone : String, job_title : String, lat : String, lng : String, address1 : String, address2 : String, company_ein : String, success : @escaping (Bool) -> Void, failure : @escaping (Error?) -> Void) {
        let url = String(format: "v1/business")
        
        var params : [String:Any] = [:]
        params["customer_id"] = ConfigurationManager.shared.currentUser?.id ?? 0
        params["first_name"] = first_name
        params["last_name"] = last_name
        params["company_name"] = company_name
        params["cork_business"] = cork_business
        params["cloth_business"] = cloth_business
        params["plastic_business"] = plastic_business
        params["phone"] = phone
        params["job_title"] = job_title
        params["lat"] = lat
        params["lng"] = lng
        params["address1"] = address1
        params["address2"] = address2
        params["company_ein"] = company_ein
        
        NetworkManager.shared.post(baseURL: BASE_URL_NEW, path: url, params:params, success: { (response) in
            if let responseObject = response as? NSDictionary {
                success(true)
            }
        }) { (error) in
            failure(error)
        }
    }
    
    @objc class func hasAnyBusiness(_ key : String, lat : String, lng : String, success : @escaping (Bool) -> Void, failure : @escaping (Error?) -> Void) {
        let url = String(format: "v1/business/has_any", key)
        
        var params : [String:String] = [:]
        params["key"] = key
        params["lat"] = lat
        params["lng"] = lng
        
        NetworkManager.shared.getString(baseURL: BASE_URL_NEW, path: url, params: params, success: { (response) in
            if let responseObject = response as? String {
                if responseObject == "0" {
                    success(false)
                } else {
                    success(true)
                }
                
            }
        }) { (error) in
            failure(error);
        }
    }
    
    @objc class func improveCoordinates(forBusiness business_id : Int, lat : String, lng : String, success : @escaping (Bool) -> Void, failure : @escaping (Error?) -> Void) {
        let url = String(format: "v1/business/improve_coordinates")
        
        var params : [String:Any] = [:]
        params["business_id"] = business_id
        params["customer_id"] = ConfigurationManager.shared.currentUser?.id ?? 0
        params["lat"] = lat
        params["lng"] = lng
        
        NetworkManager.shared.post(baseURL: BASE_URL_NEW, path: url, params: params, success: { (response) in
            if let responseObject = response as? NSDictionary {
                success(true)
            }
        }) { (error) in
            failure(error);
        }
    }
    
    @objc class func saveBusiness(first_name : String, last_name : String, company_name : String, cork_business : Bool, cloth_business : Bool, phone : String, job_title : String, lat : String, lng : String, address1 : String, address2 : String, company_ein : String, business_id : Int, success : @escaping (Bool) -> Void, failure : @escaping (Error?) -> Void) {
        let url = String(format: "v1/business/save")
        
        var params : [String:Any] = [:]
        params["id"] = business_id
        params["customer_id"] = ConfigurationManager.shared.currentUser?.id ?? 0
        params["first_name"] = first_name
        params["last_name"] = last_name
        params["company_name"] = company_name
        params["cork_business"] = cork_business
        params["cloth_business"] = cloth_business
        params["phone"] = phone
        params["job_title"] = job_title
        params["lat"] = lat
        params["lng"] = lng
        params["address1"] = address1
        params["address2"] = address2
        params["company_ein"] = company_ein
        
        NetworkManager.shared.post(baseURL: BASE_URL_NEW, path: url, params:params, success: { (response) in
            if let responseObject = response as? NSDictionary {
                success(true)
            }
        }) { (error) in
            failure(error)
        }
    }
}
