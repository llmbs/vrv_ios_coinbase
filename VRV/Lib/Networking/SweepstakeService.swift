//
//  SweepstakeService.swift
//  VRV
//
//  Created by baps on 17/11/18.
//  Copyright © 2018 Abhishek Sheth. All rights reserved.
//

import UIKit

@objc class SweepstakeService: NSObject {
    @objc class func getSweepstakeStatus(success : @escaping (Bool) -> Void, failure : @escaping (Error?) -> Void) {
        let url = String(format: "v1/customers/check_sweepstakes")
        
        var params : [String:Any] = [:]
        params["customer_id"] = ConfigurationManager.shared.currentUser?.id ?? 0
        
        NetworkManager.shared.post(baseURL: BASE_URL_NEW, path: url, params: params, success: { (response) in
            if let responseObject = response as? NSDictionary, let sweepStakes = responseObject["membership_sweepstakes"] as? NSNumber {
                success(sweepStakes.boolValue)
            }
        }) { (error) in
            failure(error);
        }
    }
    
    @objc class func updateCustomerSweepstakes(success : @escaping (Bool) -> Void, failure : @escaping (Error?) -> Void) {
        let url = String(format: "v1/customers/sweepstakes")
        
        var params : [String:Any] = [:]
        params["customer_id"] = ConfigurationManager.shared.currentUser?.id ?? 0
        params["membership_sweepstakes"] = 1
        
        NetworkManager.shared.post(baseURL: BASE_URL_NEW, path: url, params: params, success: { (response) in
            if let responseObject = response as? NSDictionary {
                success(true)
            }
        }) { (error) in
            failure(error);
        }
    }
}
