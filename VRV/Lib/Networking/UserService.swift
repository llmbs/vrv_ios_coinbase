//
//  UserService.swift
//  VRV
//
//  Created by baps on 22/11/18.
//  Copyright © 2018 Abhishek Sheth. All rights reserved.
//

import UIKit

class UserService: NSObject {
    @objc class func updateUserEmail(email : String, password : String, success : @escaping (Int) -> Void, failure : @escaping (Error?) -> Void) {
        let url = String(format: "v1/customers/email")
        
        var params : [String:Any] = [:]
        params["id"] = ConfigurationManager.shared.currentUser?.id ?? 0
        params["email"] = email
        params["password"] = password
        
        NetworkManager.shared.postWithStatus(baseURL: BASE_URL_NEW, path: url, params: params, success: { (response,StatusCode)  in
            if let responseObject = response as? NSDictionary {
                if let id = responseObject["first_name"] as? String {
                    let user = UserDetails(dictionary: responseObject)
                    ConfigurationManager.shared.currentUser = user
                    success(StatusCode)
                } else if let message = responseObject["message"] as? String {
                    let error = NSError.error(code: 0, errorMessage: message)
                    failure(error)
                } else {
                    let error = NSError.error(code: 0, errorMessage: "Email already exists.")
                    failure(error)
                }
            } else {
                let error = NSError.error(code: 0, errorMessage: "Update failed.")
                failure(error)
            }
        }) { (error,StatusCode)  in
            failure(error);
        }
    }
    
    @objc class func updateUserMobile(mobile : String, password : String, success : @escaping (Int) -> Void, failure : @escaping (Error?) -> Void) {
        let url = String(format: "v1/customers/mobile")
        
        var params : [String:Any] = [:]
        params["id"] = ConfigurationManager.shared.currentUser?.id ?? 0
        params["mobile"] = mobile
        params["password"] = password
        
        NetworkManager.shared.postWithStatus(baseURL: BASE_URL_NEW, path: url, params: params, success: { (response,StatusCode)  in
            if let responseObject = response as? NSDictionary {
                if let id = responseObject["first_name"] as? String {
                    let user = UserDetails(dictionary: responseObject)
                    ConfigurationManager.shared.currentUser = user
                    success(StatusCode)
                } else if let message = responseObject["message"] as? String {
                    let error = NSError.error(code: 0, errorMessage: message)
                    failure(error)
                } else {
                    let error = NSError.error(code: 0, errorMessage: "Update failed.")
                    failure(error)
                }
            } else {
                let error = NSError.error(code: 0, errorMessage: "Update failed.")
                failure(error)
            }
        }) { (error,StatusCode)  in
            failure(error);
        }
    }
    
    @objc class func deleteUser(email : String, password : String, success : @escaping (Bool) -> Void, failure : @escaping (Error?) -> Void) {
        let url = String(format: "v1/customers/delete")
        
        var params : [String:Any] = [:]
        //params["id"] = ConfigurationManager.shared.currentUser?.id ?? 0
        params["email"] = email
        params["password"] = password
        
        NetworkManager.shared.postString(baseURL: BASE_URL_NEW, path: url, params: params, success: { (response) in
            if let responseObject = response as? String {
                if let data = responseObject.convertToDictionary() {
                    success(true)
                } else {
                    let error = NSError.error(code: 0, errorMessage: "Password is incorrect.")  // responseObject
                    failure(error)
                }
            } else {
                let error = NSError.error(code: 0, errorMessage: "Delete failed.")
                failure(error)
            }
        }) { (error) in
            failure(error)
        }
    }
    
    @objc class func getCurrentUser(success : @escaping (UserDetails) -> Void, failure : @escaping (Error?) -> Void) {
        let id : Int = ConfigurationManager.shared.currentUser?.id ?? 0
        
        let url = String(format: "v1/customers/\(id)")
        
        NetworkManager.shared.get(baseURL: BASE_URL_NEW, path: url, success: { (response) in
            if let responseObject = response as? NSDictionary {
                if let id = responseObject["first_name"] as? String {
                    let user = UserDetails(dictionary: responseObject)
                    ConfigurationManager.shared.currentUser = user
                    success(user)
                } else if let message = responseObject["message"] as? String {
                    let error = NSError.error(code: 0, errorMessage: message)
                    failure(error)
                } else {
                    let error = NSError.error(code: 0, errorMessage: "Update failed.")
                    failure(error)
                }
            } else {
                let error = NSError.error(code: 0, errorMessage: "Update failed.")
                failure(error)
            }
        }) { (error) in
            failure(error);
        }
    }
}
