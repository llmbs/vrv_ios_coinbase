//
//  ContributionService.swift
//  VRV
//
//  Created by baps on 26/09/18.
//  Copyright © 2018 Abhishek Sheth. All rights reserved.
//

import UIKit

@objc class ContributionService: NSObject {
    @objc class func makeContribution(customerId : Int,paymentId : String,countributionAmount : Double, success : @escaping (Bool) -> Void, failure : @escaping (Error?) -> Void) {
        var params : [String:Any] = [:]
        params["customer_id"] = customerId
        params["token"] = paymentId
        params["contribution_amount"] = countributionAmount
        
        NetworkManager.shared.post(baseURL: BASE_URL_NEW, path: "v1/contribution", params: params, success: { (response) in
            success(true)
        }) { (error) in
            failure(error)
        }
    }
    
    @objc class func getContribution(success : @escaping (String) -> Void, failure : @escaping (Error?) -> Void) {
        
        NetworkManager.shared.get(baseURL: BASE_URL_NEW, path: "v1/contribution", success: { (response) in
            if let response = response as? NSDictionary, let amount = response["contribution_amount"] as? String {
                success(amount)
            }
        }) { (error) in
            failure(error)
        }
    }
}
