//
//  TimerService.swift
//  VRV
//
//  Created by baps on 26/09/18.
//  Copyright © 2018 Abhishek Sheth. All rights reserved.
//

import UIKit

@objc class TimerService: NSObject {
    @objc class func getTimer(success : @escaping (TimerData) -> Void, failure : @escaping (Error?) -> Void) {
        let url = String(format: "v1/time")
        
        NetworkManager.shared.get(baseURL: BASE_URL_NEW, path: url, success: { (response) in
            if let responseObject = response as? NSDictionary {
                let tData = TimerData(dictionary: responseObject)
                success(tData)
            }
        }) { (error) in
            failure(error);
        }
    }
}
