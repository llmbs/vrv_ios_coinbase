

#import "AboutUsView.h"
#import "LeftRightViewController.h"
#import "OTPSendView.h"
#import "CommanFunction.h"

@interface AboutUsView ()


@end

@implementation AboutUsView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[UIButton appearance] setExclusiveTouch:YES];
    
    UISwipeGestureRecognizer *gestureRecognizerRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandlerRight:)];
    [gestureRecognizerRight setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [self.view addGestureRecognizer:gestureRecognizerRight];
    
    
    for (UIButton *btn in _ButtonCollectons) {
        [btn setBackgroundImage:[CommanFunction imageWithColor:[UIColor grayColor]] forState:UIControlStateHighlighted];
        
        about.layer.cornerRadius = 4;
        
    }
    
    
    about_us.text = NSLocalizedString(@"about_us", "");
    
    about_us2.text = NSLocalizedString(@"about_us2", "");
    
    information_txt.text = NSLocalizedString(@"information_txt", "");
    
    about_us_screen_title.text = NSLocalizedString(@"about_us_screen_title", "");
    
    email_txt.text = NSLocalizedString(@"email_txt", "");
    
    faq_txt.text = NSLocalizedString(@"faq_txt", "");
    
    eula_txt.text = NSLocalizedString(@"eula_txt", "");
    
    privacy_txt.text = NSLocalizedString(@"privacy_txt", "");
    
    cookie_txt.text = NSLocalizedString(@"cookie_txt", "");
    
    scroll_txt.text = NSLocalizedString(@"scroll_txt", "");
    
    brief_txt.text = NSLocalizedString(@"brief_txt", "");
    
    business_txt.text = NSLocalizedString(@"business_txt", "");
    
    vrvtutorial.text = NSLocalizedString(@"vrvtutorial", "");
    
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    

}

#pragma mark IBAction Method.

- (IBAction)Aboutbuttonclick:(id)sender {
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:NSLocalizedString(@"about_us", "")
                                  message:NSLocalizedString(@"about_txt", "")
                                  
                                  
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"dialog_ok", "")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   
                               }];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)Menubuttonclick:(id)sender {
    
    LeftRightViewController *LeftrightviewScreenObject = [self.storyboard instantiateViewControllerWithIdentifier:@"leftright"];
    UIViewController *newVC = LeftrightviewScreenObject;
    NSMutableArray *vcs =  [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
    [vcs insertObject:newVC atIndex:vcs.count-1];
    [self.navigationController setViewControllers:vcs animated:YES];
    [self.navigationController popViewControllerAnimated:YES];

}

- (IBAction)EmailUsButtonclick:(id)sender {
    
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:NSLocalizedString(@"about_us_screen_title", "")
                                  message:NSLocalizedString(@"about_us_screen_para", "")
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                                    actionWithTitle:NSLocalizedString(@"dialog_ok", "")
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        // dismisses dialog by default
                                        
                                        }];
    
    UIAlertAction* emailButton = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"about_us_screen_title", "")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   
                                   if ([MFMailComposeViewController canSendMail])
                                   {
                                           MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
                                           mail.mailComposeDelegate = self;
                                           [mail setSubject:NSLocalizedString(@"about_us", "")];
                                           [mail setMessageBody:@"???????" isHTML:NO];
                                           [mail setToRecipients:@[@"contest@vrvccoin.com"]];
                                       
                                           [self presentViewController:mail animated:YES completion:NULL];
                                   }
                                   else
                                   {
                                          NSLog(@"This device cannot send the email");
                                       [self showAlertEmail:@"Iphone 'Mail' error."];
                                       
                                   }
                                   
                               }];
    
    [alert addAction:okButton];
    [alert addAction:emailButton];
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (IBAction)FAQButtonClick:(id)sender {
    
    NSURL *faq = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/smart_contract"];
    
    [[UIApplication sharedApplication] openURL: faq options:@{} completionHandler:^(BOOL success) {
        if (success)
            NSLog(@"Opened binance smart contract");
    }];
}

- (IBAction)TremsofServiceButtonClick:(id)sender {
    
    NSURL *tos = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/country_page"];
    
    [[UIApplication sharedApplication] openURL: tos options:@{} completionHandler:^(BOOL success) {
        if (success)
            NSLog(@"Opened tos");
        }];
}

- (IBAction)PrivcyButtonClick:(id)sender {
    
    NSURL *privacy = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/country_page"];
    
    [[UIApplication sharedApplication] openURL: privacy options:@{} completionHandler:^(BOOL success) {
        if (success)
            NSLog(@"Opened privacy");
    }];
}

- (IBAction)CookieButtonClick:(id)sender {
    
    NSURL *cookie = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/country_page"];
    
    [[UIApplication sharedApplication] openURL: cookie options:@{} completionHandler:^(BOOL success) {
        if (success)
            NSLog(@"Opened cookie");
    }];
}

- (IBAction)SweeptakesRuleButtonClick:(id)sender {
    
    NSURL *sweepstakerules = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/country_page"];
    
    [[UIApplication sharedApplication] openURL: sweepstakerules options:@{} completionHandler:^(BOOL success) {
        if (success)
            NSLog(@"Opened sweepstake rules");
    }];
}

- (IBAction)BusinessSummary:(id)sender {

    NSURL *bsummary = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/country_page"];
    
    [[UIApplication sharedApplication] openURL: bsummary options:@{} completionHandler:^(BOOL success) {
        if (success)
            NSLog(@"Opened business summary");
    }];
}

- (IBAction)YourBusinessButtonClick:(id)sender {

    
    // go to business screen. Future update.
    
//    OTPSendView *otpsendViewObj = [self.storyboard instantiateViewControllerWithIdentifier:@"otpid"];
//    otpsendViewObj.isAccessBusinesses=YES;
//    [self.navigationController pushViewController:otpsendViewObj animated:YES];
    
    
    [self quickAlert:NSLocalizedString(@"dropoffPoint", "")];
    
}

- (IBAction)WatchTutorialButtonClick:(id)sender {
    
    NSURL *youtube = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/winContestPic"];
    
    [[UIApplication sharedApplication] openURL: youtube options:@{} completionHandler:^(BOOL success) {
        if (success)
            NSLog(@"youtube tutorial");
        
    }];
    
    
}
#pragma mark Void Method.

-(void)swipeHandlerRight:(id)sender
{
    
    LeftRightViewController *LeftrightviewScreenObject = [self.storyboard instantiateViewControllerWithIdentifier:@"leftright"];
    UIViewController *newVC = LeftrightviewScreenObject;
    NSMutableArray *vcs =  [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
    [vcs insertObject:newVC atIndex:vcs.count-1];
    [self.navigationController setViewControllers:vcs animated:YES];
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result) {
        case MFMailComposeResultSent:
            NSLog(@"You sent the email.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"You saved a draft of this email");
            break;
        case MFMailComposeResultCancelled:
            NSLog(@"You cancelled sending this email.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed:  An error occurred when trying to compose this email");
            break;
        default:
            NSLog(@"An error occurred when trying to compose this email");
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}
-(void)showAlertEmail:(NSString *)message {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"'Mail'?"
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* CancelButton = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"txt_cancel", "")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       
                                   }];
    
    UIAlertAction* RestoreButton = [UIAlertAction
                                    actionWithTitle:@"Mail"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        
                                        NSURL *instantad = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/apple_mail"];
                                        
                                        [[UIApplication sharedApplication] openURL: instantad options:@{} completionHandler:^(BOOL success) {
                                            if (success)
                                                NSLog(@"Opened apple mail in app store");
                                            
                                        }];
                                        
                                    }];
    
    [alert addAction:RestoreButton];
    [alert addAction:CancelButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

-(void) quickAlert:(NSString *)Message{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:Message preferredStyle:UIAlertControllerStyleAlert];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [alertController dismissViewControllerAnimated:YES completion:^{
            
        }];
        
    });
    
    
}

@end
