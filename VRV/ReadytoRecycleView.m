//
//
//#import "ReadytoRecycleView.h"
//#import "Defaults.h"
//#import "AFNetworking.h"
//#import "CommanFunction.h"
//#import "Redaytorecyclecustoecell.h"
//#import "ScanQRViewController.h"
//#import "LeftRightViewController.h"
//#import "Reachability.h"
//#import "RecylingLocationView.h"
//#import "VRV-Swift.h"
//#import "newBusinessData.h"
//#import "ShippingLabelViewController.h"
//#import "TurnInView.h"
//#import "ContestWebViewController.h"
//#import <Lottie/Lottie.h>
//#import "CurrentFundsView.h"
//#import "SettingViewController.h"
//#import "ScoreAndRankScreen.h"
//
////@import Stripe;
////@interface ReadytoRecycleView () <PKPaymentAuthorizationViewControllerDelegate, UITextFieldDelegate> {
//
//@interface ReadytoRecycleView () <UITextFieldDelegate> {
//    NSUserDefaults *dlf;
//    NSString *ScreenIdentifier;
//
//    double lastAmount;
//    double lastQuantity;
//    NSString *type;
//    BOOL paymentSuceeded;
//    BOOL noCoin;
//    NSString *usdcString;
//    NSString *bnbString;
//    BOOL internet;
//    BOOL usdcPayidGenerated ;
//
//    NSError *lastError;
//}
//
//@property (nonatomic, strong) LOTAnimationView *lottieLogo;
//@property (nonatomic, strong) LOTAnimationView *lottieLogo2;
//
//@end
//
//@implementation ReadytoRecycleView
//
//
//
//- (void)viewDidLoad {
//    [super viewDidLoad];
//
//    [dlf setBool:false forKey:@"scan"];     // [self.menuContainerViewController setPanMode:MFSideMenuPanModeCenterViewController];
//
//    dlf = [NSUserDefaults standardUserDefaults];
//
//    // Do any additional setup after loading the view.
//
//    MainScrollview.contentSize=CGSizeMake(MainScrollview.frame.size.width, MainScrollview.frame.size.height);
//    MainScrollview.scrollEnabled = YES;
//
//    UISwipeGestureRecognizer* reconizer;
//    reconizer = [[UISwipeGestureRecognizer alloc] init];
//    reconizer.delaysTouchesBegan = TRUE;
//    [MainScrollview addGestureRecognizer:reconizer];
//
//    reconizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(SwipeRecognizer:)];
//    reconizer.direction = UISwipeGestureRecognizerDirectionRight; // default
//    [MainScrollview addGestureRecognizer:reconizer];
//    [MainScrollview delaysContentTouches];
//
//    reconizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(SwipeRecognizer:)];
//    reconizer.direction = UISwipeGestureRecognizerDirectionLeft; // default
//    [MainScrollview addGestureRecognizer:reconizer];
//    [MainScrollview delaysContentTouches];
//
//    loadingIndicator.hidden = NO;
//
//
//    ScreenIdentifier = @"";
//
//    submitradius.layer.cornerRadius=4;
//    submitradiusc.layer.cornerRadius=4;
//    hourglass.layer.cornerRadius = 4;
//    puzzle.layer.cornerRadius = 4;
//
//    Corklabel.hidden = NO;
//    Clotheslabel.hidden = YES;
//
//    BNB.hidden = YES;
//    USDC.hidden = YES;
//
//    payAdd.layer.cornerRadius=4;
//    payAddCork.layer.cornerRadius=4;
//
//
//    for (UIButton *btn in _AllButtonCollections) {
//        [btn setBackgroundImage:[CommanFunction imageWithColor:[UIColor whiteColor]] forState:UIControlStateHighlighted];
//        btn.layer.cornerRadius = 4;
//    }
//
//    [[UIButton appearance] setExclusiveTouch:YES];
//
//    VrvDepositeTextFiled.text = @"$5.00";
//    ClothesVRVDepositTxt.text = @"$5.00";
//
//    VrvDepositeTextFiled.delegate = self;
//    ClothesVRVDepositTxt.delegate = self;
//
//    self.lottieLogo = [LOTAnimationView animationNamed:@"arrow"];
//    self.lottieLogo.frame = CGRectMake(0, 0, 88, 88);
//    self.lottieLogo.center = CGPointMake(puzzle.center.x + 22 , 68 + 22);
//    self.lottieLogo.contentMode = UIViewContentModeScaleAspectFill;
//    self.lottieLogo.loopAnimation = YES;
//    [puzzleView addSubview:self.lottieLogo];
//    [self.lottieLogo play];
//
//    self.lottieLogo2 = [LOTAnimationView animationNamed:@"arrow"];
//    self.lottieLogo2.frame = CGRectMake(0, 0, 88, 88);
//    self.lottieLogo2.center = CGPointMake(puzzle.center.x + 22 , 68 + 22);
//    self.lottieLogo2.contentMode = UIViewContentModeScaleAspectFill;
//    self.lottieLogo2.loopAnimation = YES;
//    [clothPuzzleView addSubview:self.lottieLogo2];
//    [self.lottieLogo2 play];
//
//    internet = YES;
//    usdcPayidGenerated = NO;
//}
//
//- (void)didReceiveMemoryWarning {
//    [super didReceiveMemoryWarning];
//    // Dispose of any resources that can be recreated.
//}
//
//
//
//#pragma mark Void Method.
//
//// check internet with bool
//
//-(void)checkInternetConnection {
//
//    Reachability *reachability = [Reachability reachabilityForInternetConnection];
//        [reachability startNotifier];
//        NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
//        if (remoteHostStatus == NotReachable)
//        {
//            [self showAlert:@"Internet connection offline"];
//            [self hideProgressView];
//
//
//            // internet is off indicator
//            internet = NO;
//
//        }
//
//}
//
//
//-(void)showAlertGameOn {
//
//    UIAlertController * alert=   [UIAlertController
//                                  alertControllerWithTitle:@"Game On!"
//                                  message:@"You must touch turn in and complete your recycling task before gaming.\n\nTouch play if you completed your recycling task.\n\nTouch score and rank to see who is winning."
//                                  preferredStyle:UIAlertControllerStyleAlert];
//
//    UIAlertAction* exitButton = [UIAlertAction
//                                    actionWithTitle:@"Exit"
//                                    style:UIAlertActionStyleDefault
//                                    handler:^(UIAlertAction * action)
//                                    {
//
//
//                                        }];
//
//    UIAlertAction* scoreButton = [UIAlertAction
//                                    actionWithTitle:@"Score & Rank"
//                                    style:UIAlertActionStyleDefault
//                                    handler:^(UIAlertAction * action)
//                                    {
//
//        [self checkRankWc];
//        /*Implement /api/v1/checkRankWc --> parameter to pass is  customer_id*/
//
//                              // use status code 422. Ill put my own alert in.
//                               // use status code 200 for success
//
//
//
//                                        }];
//
//    UIAlertAction* playButton = [UIAlertAction
//                                    actionWithTitle:@"Play"
//                                    style:UIAlertActionStyleDefault
//                                    handler:^(UIAlertAction * action)
//                                    {
//
//        [self gamerQualifyApi];
//
//                                        }];
//
//
//    [alert addAction:playButton];
//    [alert addAction:scoreButton];
//    [alert addAction:exitButton];
//
//
//    [self presentViewController:alert animated:YES completion:nil];
//
//
//}
//
//
//
//// USDC ALERT DIALOG
//
//-(void)showAlertUsdc:(NSString *)message {
//
//
//    UIAlertController * alert=   [UIAlertController
//                                  alertControllerWithTitle:@"USDC Wallet"
//                                  message:@"To receive a cash prize if you win."
//                                  preferredStyle:UIAlertControllerStyleAlert];
//
//    UIAlertAction* submit = [UIAlertAction
//                                   actionWithTitle:@"Submit"
//                                   style:UIAlertActionStyleDefault
//                                   handler:^(UIAlertAction * action)
//                                   {
//
//                                   //check internet connection
//                                    [self checkInternetConnection];
//
//
//                                    if (self->internet == NO) {
//
//                                        //debugger log
//                                        NSLog(@"internet is offline.");
//
//
//                                        // hide usdc icon
//                                        self->USDC.hidden = YES;
//                                        self->tabHeader.hidden = NO;
//
//
//                                        //reset internet bool for
//
//                                        self->internet = YES;
//                                        NSLog(@"reset internet bool");
//
//                                    } else {
//
//
//                                    if ([self->usdcString isEqualToString:@""]) {
//
//                                        self->USDC.hidden = YES;
//                                        self->tabHeader.hidden = NO;
//
//                                        [self quickAlert:@"Address can't be empty."];
//
//                                    } else if (self->usdcString.length < 42) {
//
//                                        self->USDC.hidden = YES;
//                                        self->tabHeader.hidden = NO;
//
//                                    [self quickAlert:@"Input a valid address."];
//
//                                    } else {
//
//                                        [self usdcPostApi];
//
//                                    }
//
//                                }
//
//
//                                   }];
//
//    UIAlertAction* usdchelp = [UIAlertAction
//                                    actionWithTitle:@"Get An Address"
//                                    style:UIAlertActionStyleDefault
//                                    handler:^(UIAlertAction * action)
//                                    {
//
//                                        NSURL *instantad = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/usdc"];
//
//                                        [[UIApplication sharedApplication] openURL: instantad options:@{} completionHandler:^(BOOL success) {
//                                            if (success)
//                                                NSLog(@"Opened usdc address support");
//
//                                            self->tabHeader.hidden = NO;
//                                            self->USDC.hidden = YES;
//
//                                        }];
//
//                                    }];
//
//
//                        UIAlertAction* cancel = [UIAlertAction
//                                                       actionWithTitle:@"Cancel"
//                                                       style:UIAlertActionStyleDefault
//                                                       handler:^(UIAlertAction * action)
//                                                       {
//
//                                                           self->tabHeader.hidden = NO;
//                                                           self->USDC.hidden = YES;
//
//                                                       }];
//
//
//
//    [alert addAction:submit];
//    [alert addAction:usdchelp];
//     [alert addAction:cancel];
//
//    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
//        textField.delegate = self;
//        textField.tag = 1002;
//        textField.placeholder = @"Input address";
//        textField.keyboardType = UIKeyboardTypeDefault;
//        //::USDC String Cork
//    }];
//
//    [self presentViewController:alert animated:YES completion:nil];
//
//
//}
//
//
//// BNB ALERT DIALOG
//
//-(void)showAlertBnb:(NSString *)message {
//
//
//    BNB.hidden = NO;
//    tabHeader.hidden = YES;
//
//
//    UIAlertController * alert=   [UIAlertController
//                                  alertControllerWithTitle:@"Smart Chain (BNB)"
//                                  message:@"To receive a VRVC coin."
//                                  preferredStyle:UIAlertControllerStyleAlert];
//
//    UIAlertAction* SubmitButton = [UIAlertAction
//                                   actionWithTitle:@"Submit"
//                                   style:UIAlertActionStyleDefault
//                                   handler:^(UIAlertAction * action)
//                                   {
//
//
//        //check internet connection
//                                    [self checkInternetConnection];
//
//
//                                    if (self->internet == NO) {
//
//                                        //debugger log
//                                        NSLog(@"internet is offline.");
//
//                                        // hide bnb icon
//                                        self->BNB.hidden = YES;
//                                        self->tabHeader.hidden = NO;
//
//
//                                        //reset internet bool for
//
//                                        self->internet = YES;
//                                        NSLog(@"reset internet bool");
//
//                                    } else {
//
//
//        if ([self->bnbString isEqualToString:@""]) {
//
//            self->BNB.hidden = YES;
//            self->tabHeader.hidden = NO;
//
//            [self quickAlert:@"Address can't be empty."];
//
//        } else if (self->bnbString.length < 42) {
//
//            self->BNB.hidden = YES;
//            self->tabHeader.hidden = NO;
//
//        [self quickAlert:@"Input a valid address."];
//
//
//        }else {
//
//            [self bnbPostApi];
//
//        }
//
//                                    }
//
//                                   }];
//
//
//
//    UIAlertAction* GetAddressButton = [UIAlertAction
//                                   actionWithTitle:@"Get An Address"
//                                   style:UIAlertActionStyleDefault
//                                   handler:^(UIAlertAction * action)
//                                   {
//
//
//        self->BNB.hidden = YES;
//        self->tabHeader.hidden = NO;
//
//
//        NSURL *accepted = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/binance"];
//
//        [[UIApplication sharedApplication] openURL: accepted options:@{} completionHandler:^(BOOL success) {
//            if (success)
//                NSLog(@"Opened binance wallet support");
//
//        }];
//
//
//                                   }];
//
//
//    UIAlertAction* CancelButton = [UIAlertAction
//                                    actionWithTitle:@"Cancel"
//                                    style:UIAlertActionStyleDefault
//                                    handler:^(UIAlertAction * action)
//                                    {
//
//               self->BNB.hidden = YES;
//               self->tabHeader.hidden = NO;
//
//
//                                    }];
//
//    [alert addAction:SubmitButton];
//    [alert addAction:GetAddressButton];
//    [alert addAction:CancelButton];
//
//
//    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
//        textField.delegate = self;
//        textField.tag = 1001;
//        textField.placeholder = @"Input your address";
//        textField.keyboardType = UIKeyboardTypeDefault;
//        //::BNBStringCork
//    }];
//
//
//    [self presentViewController:alert animated:YES completion:nil];
//}
//
//
////cant send email
//
//-(void)showAlertEmail:(NSString *)message {
//    UIAlertController * alert=   [UIAlertController
//                                  alertControllerWithTitle:@"Restore 'Mail'?"
//                                  message:message
//                                  preferredStyle:UIAlertControllerStyleAlert];
//
//    UIAlertAction* CancelButton = [UIAlertAction
//                               actionWithTitle:@"Cancel"
//                               style:UIAlertActionStyleDefault
//                               handler:^(UIAlertAction * action)
//                               {
//
//                               }];
//
//    UIAlertAction* RestoreButton = [UIAlertAction
//                                   actionWithTitle:@"Restore"
//                                   style:UIAlertActionStyleDefault
//                                   handler:^(UIAlertAction * action)
//                                   {
//
//                                       NSURL *instantad = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/apple_mail"];
//
//                                       [[UIApplication sharedApplication] openURL: instantad options:@{} completionHandler:^(BOOL success) {
//                                           if (success)
//                                               NSLog(@"Opened apple mail in app store");
//
//                                   }];
//
//                                  }];
//
//    [alert addAction:RestoreButton];
//    [alert addAction:CancelButton];
//
//    [self presentViewController:alert animated:YES completion:nil];
//}
//
//// Cork type UIAlert
//
//- (void)showAlertCorkType:(NSString *)message {
//    UIAlertController * alert=   [UIAlertController
//                                  alertControllerWithTitle:@"Cork Type"
//                                  message:message
//                                  preferredStyle:UIAlertControllerStyleAlert];
//
//    UIAlertAction* okButton = [UIAlertAction
//                               actionWithTitle:@"OK"
//                               style:UIAlertActionStyleDefault
//                               handler:^(UIAlertAction * action)
//                               {
//
//                               }];
//
//
//    UIAlertAction* LearnButton = [UIAlertAction
//                               actionWithTitle:@"Learn more"
//                               style:UIAlertActionStyleDefault
//                               handler:^(UIAlertAction * action)
//                               {
//
//                                   NSURL *instantad = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/natural_cork"];
//
//                                   [[UIApplication sharedApplication] openURL: instantad options:@{} completionHandler:^(BOOL success) {
//                                       if (success)
//                                           NSLog(@"Opened patrick spencer");
//
//                                   }];
//
//                               }];
//
//
//    [alert addAction:LearnButton];
//    [alert addAction:okButton];
//
//    [self presentViewController:alert animated:YES completion:nil];
//}
//
//
//
//// Vrv deposit UIAlert
//
//- (void)showAlertDeposit:(NSString *)message amount:(double)amount quantity:(double)quantity type:(NSString *)type {
//
//    UIAlertController * alert=   [UIAlertController
//                                  alertControllerWithTitle:@"VRVC COIN"
//                                  message:message
//                                  preferredStyle:UIAlertControllerStyleAlert];
//
//
//
//
//
//    UIAlertAction* dismissButton = [UIAlertAction
//                                    actionWithTitle:@"Dismiss"
//                                    style:UIAlertActionStyleDefault
//                                    handler:^(UIAlertAction * action)
//                                    {
//
//                                //reset material quantity
//                        self->NaturalCorkQuantityTxt.text = @"";
//                        self->ClothesQuantityTxt.text = @"";
//
//                                    }];
//
//
//    UIAlertAction* okButton = [UIAlertAction
//                               actionWithTitle:@"Ok"
//                               style:UIAlertActionStyleDefault
//                               handler:^(UIAlertAction * action)
//                               {
//
//
//                        //set bool for usdc has been generated
//
//                    self->usdcPayidGenerated = true;
//
//
//                    //reset material quantity
//                     self->NaturalCorkQuantityTxt.text = @"";
//                     self->ClothesQuantityTxt.text = @"";
//
//
//            [self showProgressView];
//
//
//            Reachability *reachability = [Reachability reachabilityForInternetConnection];
//            [reachability startNotifier];
//            NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
//            if (remoteHostStatus == NotReachable)
//            {
//                [self showAlert:@"Internet connection offline"];
//                [self hideProgressView];
//
//            } else {
//
//
//        // IMPLEMENT --->    api/v1/recycle/depositCheck     ---> Use if error code 421 else. I will do the if else commands.
//
//        [RecyclingService recycleCheckDepositWithSuccess:^(NSInteger code) {
//
//            if (code == 421) {
//                NSLog(@"%ld",(long)code);
//                [self hideProgressView];
//                [self showAlert:@"You may only set your schedule once per week. Try again on Saturday at 12:00 AM PST."];
//
//
//            }else{
//                NSLog(@"%ld",(long)code);
//                [self hideProgressView];
//                [self payViaCoinbase:amount quantity:quantity type:type];
//            }
//
//        } failure:^(NSError * _Nullable error) {
//            [self hideProgressView];
//
//        }];
//
//}
//
//        }];
//
//
//    [alert addAction:dismissButton];
//    [alert addAction:okButton];
//    [self presentViewController:alert animated:YES completion:nil];
//
//}
//
//// UiAlert with no title
//
//- (void)showAlert:(NSString *)message {
//    UIAlertController * alert=   [UIAlertController
//                                  alertControllerWithTitle:@""
//                                  message:message
//                                  preferredStyle:UIAlertControllerStyleAlert];
//
//    UIAlertAction* okButton = [UIAlertAction
//                               actionWithTitle:@"OK"
//                               style:UIAlertActionStyleDefault
//                               handler:^(UIAlertAction * action)
//                               {
//
//                               }];
//    [alert addAction:okButton];
//
//    [self presentViewController:alert animated:YES completion:nil];
//}
//
//- (void)showAlertAddresses:(NSString *)message usdc:(NSString *)usdc {
//    UIAlertController * alert=   [UIAlertController
//                                  alertControllerWithTitle:@""
//                                  message:message
//                                  preferredStyle:UIAlertControllerStyleAlert];
//
//    UIAlertAction* copyButton = [UIAlertAction
//                                  actionWithTitle:@"COPY"
//                                  style:UIAlertActionStyleDefault
//                                  handler:^(UIAlertAction * action)
//                                  {
//
//           //  Need function to copy the displayed usdc address for the user.
//
//        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
//        pasteboard.string = usdc;
//
//       }];
//
//
//
//
//    [alert addAction:copyButton];
//    [self presentViewController:alert animated:YES completion:nil];
//}
//
//- (void)showAlertHourGlass:(NSString *)message {
//    UIAlertController * alert=   [UIAlertController
//                                  alertControllerWithTitle:@"Payment Successful"
//                                  message:message
//                                  preferredStyle:UIAlertControllerStyleAlert];
//
//    UIAlertAction* doneButton = [UIAlertAction
//                               actionWithTitle:@"Done"
//                               style:UIAlertActionStyleDefault
//                               handler:^(UIAlertAction * action)
//                               {
//
//                                    // after user paid and see in the app that payment is done and confirmed
//
//
//
//                               }];
//    [alert addAction:doneButton];
//
//    [self presentViewController:alert animated:YES completion:nil];
//}
//
//- (void)showAlertMaterials:(NSString *)message {
//    UIAlertController * alert=   [UIAlertController
//                                  alertControllerWithTitle:@"Materials"
//                                  message:message
//                                  preferredStyle:UIAlertControllerStyleAlert];
//
//    UIAlertAction* okButton = [UIAlertAction
//                               actionWithTitle:@"OK"
//                               style:UIAlertActionStyleDefault
//                               handler:^(UIAlertAction * action)
//                               {
//
//                               }];
//
//    UIAlertAction* suggestionButton = [UIAlertAction
//                               actionWithTitle:@"Suggestions"
//                               style:UIAlertActionStyleDefault
//                               handler:^(UIAlertAction * action)
//                               {
//
////                                   NSURL *instantad = [NSURL URLWithString:@"https://trello.com/c/LPgt7Lfz"];
////
////                                   [[UIApplication sharedApplication] openURL: instantad options:@{} completionHandler:^(BOOL success) {
////                                       if (success)
////                                           NSLog(@"Opened trello suggestion");
////
////                                   }];
//
//                                   if ([MFMailComposeViewController canSendMail])
//                                   {
//                                           MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
//                                           mail.mailComposeDelegate = self;
//                                           [mail setSubject:@"Material Suggestion"];
//                                           [mail setMessageBody:@"I would like to see 'Type your material' in the app. Thank you." isHTML:NO];
//                                           [mail setToRecipients:@[@"Materials@voluntaryrefundvalue.com"]];
//
//                                           [self presentViewController:mail animated:YES completion:NULL];
//                                   }
//                                   else
//                                   {
//                                          NSLog(@"This device cannot send the email");
//                                       [self showAlertEmail:@"You must restore the app called 'Mail', which is no longer on your iPhone. You can restore it from the App Store by touching Restore. \n\n Furthermore, once you restore the app called 'Mail', you must save and add your email account to it."];
//
//                                   }
//
//
//                               }];
//
//    UIAlertAction* currentMaterialsButton = [UIAlertAction
//                                       actionWithTitle:@"Materials"
//                                       style:UIAlertActionStyleDefault
//                                       handler:^(UIAlertAction * action)
//                                       {
//
//                                           NSURL *instantad = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/current_materials"];
//
//                                           [[UIApplication sharedApplication] openURL: instantad options:@{} completionHandler:^(BOOL success) {
//                                               if (success)
//                                                   NSLog(@"Opened current_materials.pdf");
//
//                                           }];
//
//                                       }];
//
//    [alert addAction:okButton];
//    [alert addAction:currentMaterialsButton];
//    [alert addAction:suggestionButton];
//
//    [self presentViewController:alert animated:YES completion:nil];
//}
//
//- (void)showAlertNoDeposit:(NSString *)message {
//    UIAlertController * alert=   [UIAlertController
//                                  alertControllerWithTitle:@"Contests"
//                                  message:message
//                                  preferredStyle:UIAlertControllerStyleAlert];
//
//    UIAlertAction* okButton = [UIAlertAction
//                               actionWithTitle:@"OK"
//                               style:UIAlertActionStyleDefault
//                               handler:^(UIAlertAction * action)
//                               {
//
//                               }];
//
//
//    [alert addAction:okButton];
//
//
//    [self presentViewController:alert animated:YES completion:nil];
//}
//
//- (void)showAlertGameRules:(NSString *)message {
//    UIAlertController * alert=   [UIAlertController
//                                  alertControllerWithTitle:@"Game On Promotion!"
//                                  message:message
//                                  preferredStyle:UIAlertControllerStyleAlert];
//
//
//    UIAlertAction* playButton = [UIAlertAction
//                                    actionWithTitle:@"Play"
//                                    style:UIAlertActionStyleDefault
//                                    handler:^(UIAlertAction * action)
//                                    {
//
//                                       // [self checkInternet];
//                                     [self startGame];
//                                    }];
//
//
//
//    UIAlertAction* exitButton = [UIAlertAction
//                                      actionWithTitle:@"Exit"
//                                      style:UIAlertActionStyleDefault
//                                      handler:^(UIAlertAction * action)
//                                      {
//
//
//                                      }];
//
//    [alert addAction:playButton];
//    [alert addAction:exitButton];
//
//
//    [self presentViewController:alert animated:YES completion:nil];
//}
//
//- (void)showAlertInternet:(NSString *)message {
//    UIAlertController * alert=   [UIAlertController
//                                  alertControllerWithTitle:@"Internet Connection Offline"
//                                  message:message
//                                  preferredStyle:UIAlertControllerStyleAlert];
//
//    UIAlertAction* okButton = [UIAlertAction
//                               actionWithTitle:@"Retry"
//                               style:UIAlertActionStyleDefault
//                               handler:^(UIAlertAction * action)
//                               {
//
//                                   [self showAlertGameRules:@"You have between Saturday 12 AM PST to Friday before 11:30 PM PST to complete your contest entry.\n\nMake sure your connection is wireless and strong. Use Wi-fi for faster loading and please read the rules on the rules screen."];
//
//                               }];
//    [alert addAction:okButton];
//
//    [self presentViewController:alert animated:YES completion:nil];
//}
//
//
//-(void)viewDidAppear:(BOOL)animated {
//
//}
//
////The event handling method
//
//
//-(void)viewWillAppear:(BOOL)animated{
//    [super viewWillAppear:YES];
//
//}
//
//-(void) SwipeRecognizer:(UISwipeGestureRecognizer *)sender {                     // left
//
//
//    if ( sender.direction == UISwipeGestureRecognizerDirectionLeft) {
//        NSLog(@"left");
//        [self ClothesButtsButtonClick:self];
//        [self->loadingIndicator startAnimating];
//
//
//    }
//
//
//    else if ( sender.direction == UISwipeGestureRecognizerDirectionRight && loadingIndicator.isAnimating == YES) {
//
//        [self-> loadingIndicator stopAnimating];
//
//          NSLog(@"Right");
//        [self CorkButtonClick:self];
//
//
//    }
//
//
//    else if ( loadingIndicator.isAnimating == NO && sender.direction == UISwipeGestureRecognizerDirectionRight  ) {
//
//        NSLog(@"Right");
//        [self MenuButtonClick:self];
//
//    }
//
//
//
//}
//
//#pragma mark - Text Field Delegate
//
//- (BOOL) textFieldShouldReturn:(UITextField *)textField {
//
////    if (textField == NaturalCorkQuantityTxt) {
////        [textField resignFirstResponder];
////        [VrvDepositeTextFiled becomeFirstResponder];
////    }
//
//    [NaturalCorkQuantityTxt resignFirstResponder];
//    [VrvDepositeTextFiled resignFirstResponder];
//    [ClothesQuantityTxt resignFirstResponder];
//    [ClothesVRVDepositTxt resignFirstResponder];
//
//    return YES;
//
//
//}
//
//#pragma mark  Void Method.
//
//
//-(void) quickAlert:(NSString *)Message{
//    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:Message preferredStyle:UIAlertControllerStyleAlert];
//
//    [self presentViewController:alertController animated:YES completion:nil];
//
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//
//        [alertController dismissViewControllerAnimated:YES completion:^{
//
//        }];
//
//    });
//
//}
//
//
//
//-(void)inputFieldsReset {
//
//    self->NaturalCorkQuantityTxt.text = nil;
//   // self->VrvDepositeTextFiled.text = nil;
//    self->ClothesQuantityTxt.text = nil;
//   // self->ClothesVRVDepositTxt.text = nil;
//
//}
//
//-(void)CorkInputFieldsCheck {
//
//    if ([NaturalCorkQuantityTxt.text isEqualToString:@""] || ([NaturalCorkQuantityTxt.text isEqualToString:@"0"]) ) {
//
//        [ NaturalCorkQuantityTxt becomeFirstResponder];
//
//        [self showAlert:@"Input 01 if you currently have none."];
//
//    }else{
//
//        [self showAlertBnb:@""];
//
//    }
//
//
//}
//
//-(void)ClothInputFieldsCheck {
//
//if ([ClothesQuantityTxt.text isEqualToString:@""] || ([ClothesQuantityTxt.text isEqualToString:@"0"]) ) {
//
//    [ ClothesQuantityTxt becomeFirstResponder];
//
//    [self showAlert:@"Input 01 if you currently have none."];
//
//}else{
//
//    [self showAlertBnb:@""];
//
//    }
//}
//
//
//-(void)ClothesValidationInputFields {
//
//
//    // cloth quantity
//
//    NSString *CorkQfield=ClothesQuantityTxt.text;
//    double QuantityValue = CorkQfield.doubleValue;
//
//    // cloth deposit
//
//    NSString *CorkDinput = [[ClothesVRVDepositTxt.text componentsSeparatedByCharactersInSet: [[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];
//    NSInteger DepositValue = [CorkDinput doubleValue];
//
//    // Validation parameters
//
//    if (QuantityValue < 1) {
//
//        [ ClothesQuantityTxt becomeFirstResponder];
//
//        [self showAlert:@"Misc quantity must be greater than 0."];
//
//    }else if (DepositValue <= 99) {
//
//        [ ClothesVRVDepositTxt becomeFirstResponder];
//        self->ClothesVRVDepositTxt.text = nil;
//
//        [self showAlert:@"Allowed minimum deposit is $1.00."];
//
//    } else {
//
//        BNB.hidden = NO;
//        tabHeader.hidden = YES;
//
//        UIAlertController * alert=   [UIAlertController
//                                         alertControllerWithTitle:@"Smart Chain (BNB)"
//                                         message:@"To be assigned a VRV coin."
//                                         preferredStyle:UIAlertControllerStyleAlert];
//
//           UIAlertAction* submit = [UIAlertAction actionWithTitle:@"Submit" style:UIAlertActionStyleDefault
//                                                          handler:^(UIAlertAction * action) {
//
//               self->BNB.hidden = YES;
//               self->USDC.hidden = NO;
//
//
//
//               UIAlertController * alert=   [UIAlertController
//                                                                 alertControllerWithTitle:@"USDC Wallet"
//                                                                 message:@"To receive a cash prize if you win."
//                                                                 preferredStyle:UIAlertControllerStyleAlert];
//
//                                   UIAlertAction* submit = [UIAlertAction
//                                                                  actionWithTitle:@"Submit"
//                                                                  style:UIAlertActionStyleDefault
//                                                                  handler:^(UIAlertAction * action)
//                                                                  {
//
//                                                                      self->tabHeader.hidden = NO;
//                                                                      self->USDC.hidden = YES;
//
//                                                        double depositDouble = DepositValue/100.0;
//                                                      [self showAlertDeposit:@"Users residing in states CO, CT, MD, NE, AZ and ND are not eligible.\n\nTo receive a VRVC coin, touch ok and make a $5.00 + gas fee payment to the displayed USDC address.\n\nYou will be able to copy the address." amount:depositDouble quantity:QuantityValue type:@"cloth"];
//
//
//
//                                                                  }];
//
//                                   UIAlertAction* usdchelp = [UIAlertAction
//                                                                   actionWithTitle:@"Get An Address"
//                                                                   style:UIAlertActionStyleDefault
//                                                                   handler:^(UIAlertAction * action)
//                                                                   {
//
//                                                                       NSURL *instantad = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/usdc"];
//
//                                                                       [[UIApplication sharedApplication] openURL: instantad options:@{} completionHandler:^(BOOL success) {
//                                                                           if (success)
//                                                                               NSLog(@"Opened usdc address support");
//
//                                                                           self->tabHeader.hidden = NO;
//                                                                           self->USDC.hidden = YES;
//
//                                                                       }];
//
//                                                                   }];
//
//
//                                                       UIAlertAction* cancel = [UIAlertAction
//                                                                                      actionWithTitle:@"Cancel"
//                                                                                      style:UIAlertActionStyleDefault
//                                                                                      handler:^(UIAlertAction * action)
//                                                                                      {
//
//                                                                                          self->tabHeader.hidden = NO;
//                                                                                          self->USDC.hidden = YES;
//
//                                                                                      }];
//
//
//
//                                   [alert addAction:submit];
//                                   [alert addAction:usdchelp];
//                                    [alert addAction:cancel];
//
//               [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
//                   textField.delegate = self;
//                   textField.tag = 1004;
//                   textField.placeholder = @"Input address";
//                   textField.keyboardType = UIKeyboardTypeDefault;
//                   //::USDC Clothes String
//               }];
//
//                                   [self presentViewController:alert animated:YES completion:nil];
//
//
//                                                                              }];
//
//
//           UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
//                                                          handler:^(UIAlertAction * action) {
//                                                              [alert dismissViewControllerAnimated:YES completion:nil];
//
//                                   self->BNB.hidden = YES;
//                                   self->tabHeader.hidden = NO;
//
//                                                          }];
//
//        UIAlertAction* binance = [UIAlertAction actionWithTitle:@"Get An Address" style:UIAlertActionStyleDefault
//                                                   handler:^(UIAlertAction * action) {
//                                                       [alert dismissViewControllerAnimated:YES completion:nil];
//
//                            NSURL *accepted = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/binance"];
//
//                            [[UIApplication sharedApplication] openURL: accepted options:@{} completionHandler:^(BOOL success) {
//                                if (success)
//                                    NSLog(@"Opened binance wallet support");
//
//                            }];
//
//                        self->BNB.hidden = YES;
//                        self->tabHeader.hidden = NO;
//
//                                                   }];
//
//
//
//           [alert addAction:submit];
//            [alert addAction:binance];
//        [alert addAction:cancel];
//
//           [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
//               textField.delegate = self;
//               textField.tag = 1003;
//               textField.placeholder = @"Input address";
//               textField.keyboardType = UIKeyboardTypeDefault;
//               //::BNB Clothes String
//           }];
//
//           [self presentViewController:alert animated:YES completion:nil];
//
//
//
//
//
//
//    }
//
//}
//
//-(void)textFieldDidBeginEditing:(UITextField *)textField{
//    if (textField == VrvDepositeTextFiled) {
//
//
//    }
//
//    if (textField == ClothesVRVDepositTxt) {
//
//
//    }
//}
//
//#pragma mark API Call
//
//- (void) usdcPostApi {
//    [self.view setUserInteractionEnabled: NO];
//    [self showProgressView];
//    [self performSelector:@selector(enableUserinteraction:) withObject:self afterDelay:0.2];
//
//    [RecyclingService changedTheUSDCOfACustomerWithUsdc: self->usdcString success:^(NSInteger statusCode) {
//
//        [self hideProgressView];
//
//        if (statusCode == 200 && self->loadingIndicator.isAnimating == NO) {
//
//            //reset material cork back to default
//           // self->materialCork = NO;
//            NSLog(@"Material selected is cork");
//
//            //Successful Response
//            NSLog(@"Status code 200-> Usdc for user has been posted 'cork'.");
//
//
//            // hide usdc icon
//            self->USDC.hidden = YES;
//
//            // show tab header
//            self->tabHeader.hidden = NO;
//
//            //initiate USDC payment dialog
//            [self corkVrvcCoinDialog];
//
//
//        } else {
//
//
//            NSLog(@"Material selected is cloth");
//
//            //Successful Response
//            NSLog(@"Status code 200-> Usdc for user has been posted 'cloth'.");
//
//            // hide usdc icon
//            self->USDC.hidden = YES;
//
//            // show tab header
//            self->tabHeader.hidden = NO;
//
//            //initiate USDC payment dialog
//            [self clothVrvcCoinDialog];
//
//
//
//        }
//
//    } failure:^(NSError * _Nullable error) {
//      //  [self handleError:error];
//        [self hideProgressView];
//        [self showAlert:@"Server error. Please try again."];
//
//
//    }];
//}
//
//- (void) bnbPostApi {
//    [self.view setUserInteractionEnabled: NO];
//    [self showProgressView];
//    [self performSelector:@selector(enableUserinteraction:) withObject:self afterDelay:0.2];
//
//    [RecyclingService changedTheBNBOfACustomerWithBnb: self->bnbString success:^(NSInteger statusCode) {
//
//        [self hideProgressView];
//
//        if (statusCode == 200) {
//
//            //Successful Response Log
//            NSLog(@"Status code 200-> Bnb for user has been posted.");
//
//
//            // hide bnb icon
//            self->BNB.hidden = YES;
//
//            //show USDC icon
//            self->USDC.hidden = NO;
//
//
//            //Show USDC alert dialog
//
//            [self showAlertUsdc:@""];
//
//
//        } else {
//
//            // hide bnb icon
//            self->BNB.hidden = YES;
//
//            // show tab header
//            self->tabHeader.hidden = NO;
//
//
//        }
//
//    } failure:^(NSError * _Nullable error) {
//       // [self handleError:error];
//        [self hideProgressView];
//
//        // hide bnb icon
//        self->BNB.hidden = YES;
//
//        // show tab header
//        self->tabHeader.hidden = NO;
//
//        [self showAlert:@"Server error. Please try again."];
//
//    }];
//}
//
//#pragma mark IBAction Method.
//
//- (void)createContast {
//    //Implement with these response codes so I can do the proper alert dialogs on my end.
//    // response code 426
//    // response code 425
//    // response code 427
//    // response code 421
//    // if there is no response code 426,425,427,421 then use ELSE and implement  /api/v1/contest/start_game
//    [ContestService createContestWithSuccess:^(NSInteger code) {
//        if (code == 426) {
//            [self showAlert:@"You must wait between 8:00 am to 4:59 pm (PST)."];
//            [self hideProgressView];
//
//        }else if (code == 425) {
//            [self showAlert:@"Currently, there are no contests."];
//            [self hideProgressView];
//
//        }else if (code == 427) {
//            [self showAlertNoDeposit:@"To enter in the contest you must submit a non-refundable $1.00 recycling schedule for one material."];
//            [self hideProgressView];
//
//        }else if (code == 415) {
//                [self showAlert:@"Your entry has been recorded for the week. A new week begins every Saturday at 12:00 AM PST"];
//                [self hideProgressView];
//
//            }else if (code == 421) {
//                [self showAlert:@"Your entry has been recorded for the week. A new week begins every Saturday at 12:00 AM PST"];
//            [self hideProgressView];
//
//        }else if (code == 420) {
//
//                [self showAlert:@"Restarting. In several seconds, touch game on again."];
//                [self hideProgressView];
//
//        }else {
//
//            [self showAlertGameRules:@"You have between Saturday 12 AM PST to Friday before 11:30 PM PST to complete your contest entry.\n\nMake sure your connection is wireless and strong. Use Wi-fi for faster loading and please read the rules on the rules screen."];
//            [self hideProgressView];
//
//           // [self startGame];
//        }
//    } failure:^(NSError * _Nullable error) {
//        //Handle Error
//        [self handleError:error];
//        [self hideProgressView];
//    }];
//}
//
//- (void)startGame {
//    Reachability *reachability = [Reachability reachabilityForInternetConnection];
//    //Reachability *reachability = [Reachability reachabilityWithHostName:@"www.google.com"];
//    [reachability startNotifier];
//    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
//
//    if (remoteHostStatus == NotReachable)
//    {
//        [[self presentingViewController] dismissViewControllerAnimated:NO completion:nil];
//
//  //::      [self showAlertInternet:@"Touch retry when you have a good internet connection via wireless only.\n\nIf you exit the app your entry will be invalid for today! Please read the contest rules in the sweepstake screen."];
//        [self showAlertInternet:@"Touch retry when you have a good internet connection via wireless only. Please read the contenst rules on the rules screen."];
//
//    } else {
//
//        [ContestService startGameWithSuccess:^(NSString * _Nonnull token, NSInteger code) {
//
//            [ContestWebViewController pushContestController:self token:token level:code];
//
//        } failure:^(NSError * _Nullable error) {
//            //Handle Error
//        }];
//
//    }
//
//}
//
//
//- (IBAction)PuzzleButtonClick:(id)sender {
//
//
//    [self showAlertGameOn];
//
//
//
//}
//
//- (IBAction)WebGlButtonClick:(id)sender {
//
//    NSURL *accepted = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/prize_page"];
//
//    [[UIApplication sharedApplication] openURL: accepted options:@{} completionHandler:^(BOOL success) {
//        if (success)
//            NSLog(@"Opened vehicle page on website");
//
//    }];
//
//}
//
//
//- (IBAction)MenuButtonClick:(id)sender {
//   // Corkview.hidden = YES;
//   // clothesview.hidden = YES;
//
//    self->MainScrollview.frame=CGRectMake( 0, self->MainScrollview.frame.origin.y, self->MainScrollview.frame.size.width, self->MainScrollview.frame.size.height);
//
//    LeftRightViewController *LeftrightviewScreenObject = [self.storyboard instantiateViewControllerWithIdentifier:@"leftright"];
//    UIViewController *newVC = LeftrightviewScreenObject;
//    NSMutableArray *vcs =  [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
//    [vcs insertObject:newVC atIndex:vcs.count-1];
//    [self.navigationController setViewControllers:vcs animated:YES];
//    [self.navigationController popViewControllerAnimated:YES];
//}
//
//- (IBAction)CorkButtonClick:(id)sender {
//
//
//
//
//   //  MainScrollview.scrollEnabled = YES;
//
//    [self textFieldShouldReturn:ClothesQuantityTxt];
//    [self textFieldShouldReturn:ClothesVRVDepositTxt];
//
//        [UIView animateWithDuration:0.3 animations:^{
//
//
//
//        self->Corklabel.backgroundColor = [UIColor colorWithRed:(0.35) green:0.29 blue:0.27 alpha:1.0];
//        self->Clotheslabel.backgroundColor = [UIColor colorWithRed:(0.55) green:0.80 blue:0.53 alpha:1.0];
//        self->CorkText.textColor = [UIColor whiteColor];
//        self->ClothesText.textColor = [UIColor colorWithRed:(0.35) green:0.29 blue:0.27 alpha:1.0];
//
//        self->Corklabel.hidden = NO;
//        self->Clotheslabel.hidden = YES;
//
//        self->MainScrollview.frame=CGRectMake( 0, self->MainScrollview.frame.origin.y, self->MainScrollview.frame.size.width, self->MainScrollview.frame.size.height);
//
//    }];
//
//}
//
//
//- (IBAction)ClothesButtsButtonClick:(id)sender {
//
//
//
//    [self->loadingIndicator startAnimating];
//
//  //   MainScrollview.scrollEnabled = YES;
//
//        [self textFieldShouldReturn:NaturalCorkQuantityTxt];
//        [self textFieldShouldReturn:VrvDepositeTextFiled];
//
//
//        [UIView animateWithDuration:0.3 animations:^{
//
//        self->Clotheslabel.backgroundColor = [UIColor colorWithRed:(0.35) green:0.29 blue:0.27 alpha:1.0];
//        self->Corklabel.backgroundColor = [UIColor colorWithRed:(0.55) green:0.80 blue:0.53 alpha:1.0];
//        self->ClothesText.textColor = [UIColor whiteColor];
//        self->CorkText.textColor = [UIColor colorWithRed:(0.35) green:0.29 blue:0.27 alpha:1.0];
//
//        self->Corklabel.hidden = YES;
//        self->Clotheslabel.hidden = NO;
//
//
//
//        CGFloat width = -1 * self.view.frame.size.width;
//
//        self->MainScrollview.frame=CGRectMake( width, self->MainScrollview.frame.origin.y, self->MainScrollview.frame.size.width, self->MainScrollview.frame.size.height);
//
//    }];
//
//
//}
//
//- (IBAction)SubmitCork:(id)sender {
//
//    [self CorkInputFieldsCheck];
//}
//
//- (IBAction)CorkAdvertiseClick:(id)sender {
//
//    NSURL *accepted = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/Vehicle_page"];
//
//    [[UIApplication sharedApplication] openURL: accepted options:@{} completionHandler:^(BOOL success) {
//        if (success)
//            NSLog(@"Opened recycle ad");
//
//    }];
//
//}
//
//- (IBAction)ClothesAdvertiseClick:(id)sender {
//
//    NSURL *accepted = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/Vehicle_page"];
//
//    [[UIApplication sharedApplication] openURL: accepted options:@{} completionHandler:^(BOOL success) {
//        if (success)
//            NSLog(@"Opened recycle ad");
//
//    }];
//
//}
//
//
//- (IBAction) corkMapButtonClick:(id)sender{
//
//    RecylingLocationView *RecylinglocationScreenObj = [self.storyboard instantiateViewControllerWithIdentifier:@"ReCycleLocationId"];
//    [self.navigationController pushViewController:RecylinglocationScreenObj animated:YES];
//
//
//}
//
//- (IBAction) ClothesMapButtonClick:(id)sender {
//
//    RecylingLocationView *RecylinglocationScreenObj = [self.storyboard instantiateViewControllerWithIdentifier:@"ReCycleLocationId"];
//    [self.navigationController pushViewController:RecylinglocationScreenObj animated:YES];
//
//
//
//}
//
//
//- (IBAction)ClothesSubmitButtonClick:(id)sender {
//
//    [self ClothInputFieldsCheck];
//
//}
//
//
//-(IBAction)GameListClick:(id)sender {
//
//    NSURL *accepted = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/winContestPic"];
//
//    [[UIApplication sharedApplication] openURL: accepted options:@{} completionHandler:^(BOOL success) {
//        if (success)
//            NSLog(@"Opened how to win video");
//
//    }];
//
//}
//
//
//
//
//- (IBAction)HourGlassButtonClick:(id)sender{
//
//
//        [self.view setUserInteractionEnabled: NO];
//        [self showProgressView];
//        [self performSelector:@selector(enableUserinteraction:) withObject:self afterDelay:0.2 ];
//
//
//        [RecyclingService readyToTurnWithSuccess:^(NSInteger cloth, NSInteger cork, NSInteger plastic) {
//
//            //add additional materials
//
//            if (cork > 0 || cloth > 0 || plastic > 0) {
//                NSLog(@"Can be turned in");
//
//                [self hideProgressView];
//                TurnInView *TurnInScreenObj = [self.storyboard instantiateViewControllerWithIdentifier:@"turnInDialog"];
//                TurnInScreenObj.cork = cork;
//                TurnInScreenObj.cloth = cloth;
//                TurnInScreenObj.plastic = plastic;
//                [self.navigationController pushViewController:TurnInScreenObj animated:YES];
//
//
//            } else {
//
//            NSLog(@"Nothing to turn in");
//                [self showAlert:@"If you made a USDC payment you can touch turn in when your payment clears.\n\nOtherwise, buy a VRVC Coin."];
//                [self hideProgressView];
//
//            }
//
//        } failure:^(NSError * _Nullable error) {
//            [self handleError:error];
//        }];
//
//}
//
//
//- (IBAction)MaterialClick:(id)sender {
//    [self showAlertMaterials:@"Touch materials to view the current materials. Touch suggestions to email us materials you would like for us to add. \n\nWe will do our best to update the app with your material suggestions. Thank you."];
//}
//
//
//- (BOOL)textField:(UITextField* )textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
//
//    if (textField == NaturalCorkQuantityTxt) {
//        NSLog(@"Range: %@", NSStringFromRange(range));
//        return ( range.location < 3 );
//    } else if (textField == ClothesQuantityTxt){
//        NSLog(@"Range: %@", NSStringFromRange(range));
//        return ( range.location < 3 );
//    } else if (textField == VrvDepositeTextFiled || textField == ClothesVRVDepositTxt) {
//        NSString *cleanCentString = [[textField.text componentsSeparatedByCharactersInSet: [[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];
//        NSInteger centValue = [cleanCentString intValue];
//        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
//        NSNumber *myNumber = [f numberFromString:cleanCentString];
//        NSNumber *result;
//
//
//        if (string.length > 0)
//        {
//            centValue = centValue * 10 + [string intValue];
//            double intermediate = [myNumber doubleValue] * 10 +  [[f numberFromString:string] doubleValue];
//            result = [[NSNumber alloc] initWithDouble:intermediate];
//        } else {
//            centValue = centValue / 10;
//            double intermediate = [myNumber doubleValue]/10;
//            result = [[NSNumber alloc] initWithDouble:intermediate];
//        }
//
//        myNumber = result;
//        NSLog(@"%ld ++++ %@", (long)centValue, myNumber);
//        double finalAmount = [myNumber doubleValue] / 100.0f;
//        if (finalAmount > 1.00) {
//            finalAmount = 1.00;
//        }
//        NSNumber *formatedValue = [[NSNumber alloc] initWithDouble:finalAmount];
//        NSNumberFormatter *_currencyFormatter = [[NSNumberFormatter alloc] init];
//        [_currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
//        textField.text = [_currencyFormatter stringFromNumber:formatedValue];
//        return NO;
//    }
//
//    return YES;
//
//}
//
//- (IBAction)PaymentAddress:(id)sender {
//
//
//
//    [self showProgressView];
//
//    if (usdcPayidGenerated == false) {
//
//        [self hideProgressView];
//    }
//
//    if (usdcPayidGenerated == true) {
//
//
//
//        [self hideProgressView];
//
//        [self corkVrvcCoinDialog];
//
//
//    // amount:depositDouble quantity:QuantityValue type:@"cork"];
//
//    } else {
//
//        [self showAlert:@"Touch submit and follow the instructions."];
//
//    }
//
//
//}
//
//// disables pasting for material quantity and amount
//
//-(BOOL)canPerformAction:(SEL)action withSender:(id)sender {
//    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
//        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
//    }];
//    return [super canPerformAction:action withSender:sender];
//}
//
////disables user interaction for api execution
//
//-(void)enableUserinteraction:(id)sender {
//
//    [self.view setUserInteractionEnabled:YES];
//}
//
////- (void)payViaCoinbase:(double)amount quantity:(int)quantity type:(NSString *)type {
////
////    lastAmount = amount;
////    lastQuantity = quantity;
////    self->type = type;
////
////    [RecyclingService createRecycleWithType: self->type quantity: [NSString stringWithFormat:@"%f", self->lastQuantity] deposit: [NSString stringWithFormat:@"%f", self->lastAmount] token: @"123" success:^(NSInteger statusCode) {
////
////
////
////        if (statusCode == 423) {
////            [self showAlert:@"No more coins. Try on Saturday at 12:00 AM PST."];
////            NSLog(@"Status code 423, no Coins ");
////
////        } else {
////            NSDictionary *data = [response valueForKey:@"data"];
////            if ([data isKindOfClass: [NSDictionary class]]) {
////                NSString *code = [data valueForKey:@"code"];
////                NSDictionary *addresses = [data valueForKey:@"addresses"];
////
////                if ([code isKindOfClass: [NSString class]] && ![code isEqualToString:@""]) {
////
////                    if ([addresses isKindOfClass: [NSDictionary class]]) {
////                        NSString *usdc = [addresses valueForKey:@"usdc"];
////
////                        if ([usdc isKindOfClass: [NSString class]] && ![usdc isEqualToString:@""]) {
////
////                            // Generate USDC address to pay to.
////
////                            NSString *alertString = [[NSString alloc] initWithFormat:@"To receive a VRVC coin, touch copy and make a $5.00 + gas fee payment to the displayed USDC address with your preferred method. \n\n %@", usdc];
////                            [self showAlertAddresses:alertString usdc: usdc];
////
////                        } else {
////
////
////                        }
////
////                    } else {
////
////                    }
////
////                } else {
////
////                }
////
////            } else {
////
////            }
////        }
////
////
////    } failure:^(NSError * _Nullable error) {
////
////        [self showAlert:@"Server error. Please try again."];
////
////    }];
////
////}
//
//
//- (void) checkPaymentStatus:(NSString *) code {
//
//    [RecyclingService checkPaymentStatusWith:code completion:^(NSDictionary* _Nonnull responseDict) {
//        NSLog(@"%@", responseDict);
//
//        [self showAlertHourGlass:@"Touch Turn in for further instructions and go to the current funds screen to view your token."];
//
//    } failure:^(NSError * _Nullable error) {
//        NSLog(@"%@", error.localizedDescription);
//    }];
//
//}
//
//
//- (void)corkVrvcCoinDialog {
//
//
//    NSString *CorkQfield=self->NaturalCorkQuantityTxt.text;
//                 double QuantityValue = CorkQfield.doubleValue;
//                 // Cork deposit
//
//           NSString *CorkDinput = [[self->VrvDepositeTextFiled.text componentsSeparatedByCharactersInSet: [[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];
//                 NSInteger DepositValue = [CorkDinput doubleValue];
//
//
//           double depositDouble = DepositValue/100.0;
//           [self showAlertDeposit:@"Users residing in states CO, CT, MD, NE, AZ and ND are not eligible.\n\nTo receive a VRVC coin, touch ok and make a $5.00 + gas fee payment to the displayed USDC address.\n\nYou will be able to copy the address." amount:depositDouble quantity:QuantityValue type:@"cork"];
//
//}
//
//- (void)clothVrvcCoinDialog {
//
//    NSString *CorkQfield=ClothesQuantityTxt.text;
//    double QuantityValue = CorkQfield.doubleValue;
//
//    // cloth deposit
//
//    NSString *CorkDinput = [[ClothesVRVDepositTxt.text componentsSeparatedByCharactersInSet: [[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];
//    NSInteger DepositValue = [CorkDinput doubleValue];
//
//
//            double depositDouble = DepositValue/100.0;
//            [self showAlertDeposit:@"Users residing in states CO, CT, MD, NE, AZ and ND are not eligible.\n\nTo receive a VRVC coin, touch ok and make a $5.00 + gas fee payment to the displayed USDC address.\n\nYou will be able to copy the address." amount:depositDouble quantity:QuantityValue type:@"cloth"];
//
//}
//
//- (void) gamerQualifyApi {
//
//    [self.view setUserInteractionEnabled: NO];
//    [self showProgressView];
//    [self performSelector:@selector(enableUserinteraction:) withObject:self afterDelay:0.2];
//
//    [RecyclingService gamerQualifiedWithSuccess:^(NSInteger statusCode) {
//
//        [self hideProgressView];
//
//        if (statusCode == 422) {
//
//            [self showAlert:@"You must touch turn in and complete your recycling task before gaming."];
//            NSLog(@"422 - No entry in database");
//
//
//
//        } else if (statusCode == 423) {
//
//            [self showAlert:@"You must touch turn in and complete your recycling task before gaming."];
//            NSLog(@"423 - Entry in database but recycle is no and location date is blank");
//
//
//        } else if (statusCode == 424) {
//
//                [self createContast];
//                [self showProgressView];
//            NSLog(@"424 - Entry in database recycle is yes or location date has data");
//
//        } else {
//
//        }
//
//    } failure: ^(NSError * _Nullable error) {
//
//        [self handleError:error];
//        [self hideProgressView];
//
//    }];
//
//
//
//
//}
//
//
//-(void)checkRankWc {
//
//    [self.view setUserInteractionEnabled: NO];
//    [self showProgressView];
//    [self performSelector:@selector(enableUserinteraction:) withObject:self afterDelay:0.2];
//
//    [RecyclingService checkRankWcWithSuccess:^(NSArray<ScoreRankData *>  * _Nonnull responseData, NSInteger statusCode) {
//
//        [self hideProgressView];
//
//        if (statusCode == 422) {
//
//
//        } else if (statusCode == 200) {
//            NSMutableArray<ScoreRankData *> *topData = [NSMutableArray arrayWithArray:responseData];
//            ScoreAndRankScreen *nextVC = [[ScoreAndRankScreen alloc] init];
//            [nextVC setupArray:topData];
//            [self.navigationController pushViewController:nextVC animated:YES];
//        } else if (statusCode == 424) {
//
//        } else {
//
//        }
//
//    } failure: ^(NSError * _Nullable error) {
//
//        [self handleError:error];
//        [self hideProgressView];
//
//    }];
//
//}
//
//
//
//- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
//{
//    switch (result) {
//        case MFMailComposeResultSent:
//            NSLog(@"You sent the email.");
//            break;
//        case MFMailComposeResultSaved:
//            NSLog(@"You saved a draft of this email");
//            break;
//        case MFMailComposeResultCancelled:
//            NSLog(@"You cancelled sending this email.");
//            break;
//        case MFMailComposeResultFailed:
//            NSLog(@"Mail failed:  An error occurred when trying to compose this email");
//            break;
//        default:
//            NSLog(@"An error occurred when trying to compose this email");
//            break;
//    }
//
//    [self dismissViewControllerAnimated:YES completion:NULL];
//}
//
//#pragma mark UITextFieldDelegate
//- (void)textFieldDidEndEditing:(UITextField *)textField {
//
//    if (textField.tag == 1001) {
//        self->bnbString = textField.text;
//    } else if (textField.tag == 1002) {
//        self->usdcString = textField.text;
//    } else if (textField.tag == 1003) {
//        self->bnbString = textField.text;
//    } else if (textField.tag == 1004) {
//        self->usdcString = textField.text;
//    }
//    NSLog(@"BNB String ==> %@", self->bnbString);
//    NSLog(@"USDC String ==> %@", self->usdcString);
//
//}
//
//@end
