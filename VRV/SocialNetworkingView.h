

#import <UIKit/UIKit.h>

@interface SocialNetworkingView : UIViewController{
    
    __weak IBOutlet UIImageView *IntercomImageview;
    
    __weak IBOutlet UILabel *social_networking;
    
    __weak IBOutlet UILabel *live_txt;
    
    __weak IBOutlet UILabel *intercom_txt;
    
    __weak IBOutlet UILabel *platform_txt;
    
    
    
}

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *AllButtonCollections;



- (IBAction)MenuButtonClick:(id)sender;
- (IBAction)FacebokButtonClick:(id)sender;
- (IBAction)AppleButtonClick:(id)sender;
- (IBAction)TwitterButtonClick:(id)sender;
- (IBAction)YoutubeButtonClick:(id)sender;
- (IBAction)intercomButtonClick:(id)sender;
-(IBAction)InstagramButtonClick:(id)sender;
-(IBAction)PinterestButtonClick:(id)sender;
@end
