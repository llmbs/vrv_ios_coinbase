

#import "AdvertisingView.h"
#import "LeftRightViewController.h"






@implementation AdvertisingView

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UISwipeGestureRecognizer *gestureRecognizerRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandlerRight:)];
    [gestureRecognizerRight setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [self.view addGestureRecognizer:gestureRecognizerRight];
    
    [[UIButton appearance] setExclusiveTouch:YES];
    
    advertising.text = NSLocalizedString(@"advertising", "");
    
    Whoadvertise.text = NSLocalizedString(@"Whoadvertise", "");
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
   

}

#pragma mark IBAction Method.

- (IBAction)MenuButtonClick:(id)sender {
    LeftRightViewController *LeftrightviewScreenObject = [self.storyboard instantiateViewControllerWithIdentifier:@"leftright"];
    UIViewController *newVC = LeftrightviewScreenObject;
    NSMutableArray *vcs =  [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
    [vcs insertObject:newVC atIndex:[vcs count]-1];
    [self.navigationController setViewControllers:vcs animated:YES];
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction)IconButtonClick:(id)sender {
    
    NSURL *accepted = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/recycle_ad"];
    
    [[UIApplication sharedApplication] openURL: accepted options:@{} completionHandler:^(BOOL success) {
        if (success)
            NSLog(@"Opened recycle ad");
        
    }];
    
}

#pragma mark Void Method.

-(void)swipeHandlerRight:(id)sender
{
    
    LeftRightViewController *LeftrightviewScreenObject = [self.storyboard instantiateViewControllerWithIdentifier:@"leftright"];
    UIViewController *newVC = LeftrightviewScreenObject;
    NSMutableArray *vcs =  [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
    [vcs insertObject:newVC atIndex:vcs.count-1];
    [self.navigationController setViewControllers:vcs animated:YES];
    [self.navigationController popViewControllerAnimated:YES];
    
}

@end
