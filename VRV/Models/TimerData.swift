//
//  TimerData.swift
//  VRV
//
//  Created by baps on 26/09/18.
//  Copyright © 2018 Abhishek Sheth. All rights reserved.
//

import UIKit

class TimerData: BaseModel {
    var timestamp : String!
    var date : String!
    var time : String!
    var date_now : String!
    var day : String!
    var time_now : String!
    var remaining_days : Int = 0
    var remaining_hours : Int = 0
    var remaining_minutes : Int = 0
    var remaining_seconds : Int = 0
}

