//
//  BusinessData.swift
//  VRV
//
//  Created by baps on 26/09/18.
//  Copyright © 2018 Abhishek Sheth. All rights reserved.
//

@objc class BusinessData: BaseModel {
    var id : Int = 0
    var customer_id : Int = 0
    var first_name : String!
    var last_name : String!
    var company_name : String? = nil
    var accepting_cloth : Int = 0
    var accepting_cork : Int = 0
    var phone : String!
    var address1 : String? = nil
    var address2 : String? = nil
    var city : String!
    var state : String!
    var country : String!
    var zip_code : String!
    var job_title : String!
    var company_ein : String!
    var lat : String!
    var lng : String!
    var cork_business : Int = 0
    var cloth_business : Int = 0
    var is_active : Int = 0
    var is_legit : Int = 0
    var distance : NSNumber? = nil
}
