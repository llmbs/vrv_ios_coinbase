//
//  UserDetails.swift
//  VRV
//
//  Created by baps on 06/09/18.
//  Copyright © 2018 vnnovate. All rights reserved.
//

class UserDetails: BaseModel {
    var access_token : String!
   var birth_day : String!
    var email : String!
    var first_name : String!
   var gender : String!
    var id : Int = 0
    var last_name : String!
    var membership_sweepstakes : Int = 0
    var mobile : String!
    var verified : Int = 0
    var is_active : Int = 0
    
    @objc func getUserId() -> Int {
        return id
    }
}
