//
//  ScoreRankData.swift
//  VRV
//
//  Created by My Mac on 19/08/21.
//  Copyright © 2021 Abhishek Sheth. All rights reserved.
//

import Foundation

class ScoreRankData: BaseModel {
    var customer_id: Int64 = 0
    var first_name: String = ""
    var last_name: String = ""
    var rank: Int64 = 0
    var score: Int64 = 0
}
