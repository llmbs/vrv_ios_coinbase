

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <WebKit/WebKit.h>

@interface SweepstakesView : UIViewController
{
    
    __weak IBOutlet UIScrollView *MainScrollview;
    __weak IBOutlet UIView *MainView;
    __weak IBOutlet UIView *BrownTitleBar;
    
    __weak IBOutlet UIButton *getinButton;
    __weak IBOutlet UIButton *yourinButton;
    __weak IBOutlet UIButton *instantmorebutton;
    __weak IBOutlet UIButton *daycaremorebuttton;
    __weak IBOutlet UIButton *workerbonusButton;
    __weak IBOutlet UIButton *HomelessButton;
    __weak IBOutlet UIButton *ConstenstsMoreInfoButton;
    __weak IBOutlet UIButton *ConstenstsWinnersInButton;
    __weak IBOutlet UIButton *MsMoreInfoButton;
    __weak IBOutlet UIButton *CheckPuzzleButton;
    
    __weak IBOutlet UIButton *RefundButton;
    __weak IBOutlet UIButton *instantWinnerBtn;
    
    __weak IBOutlet UIButton *DaycareWinnerBtn;
    
    __weak IBOutlet UIButton *WorkerWinnerBtn;
    
    __weak IBOutlet UIButton *HomlessWinnerBtn;
    
    __weak IBOutlet UIButton *DismissPdf;
    __weak IBOutlet UIImageView *DismissPdfArrow;
    
    __weak IBOutlet UILabel *HeaderTitle;
    
    __weak IBOutlet UILabel *worker_bonus;
    
    __weak IBOutlet UILabel *homeless;
    
    __weak IBOutlet UILabel *contests;
    
    
    
   
    CLPlacemark *placemark;
    
    __weak IBOutlet UIImageView *notepadimageview;
    __weak IBOutletCollection(UIButton) NSArray *WinnersButtonCollections;
    
    

}

@property (weak, nonatomic) IBOutlet WKWebView *webkitView;

- (IBAction)MenuButtonClick:(id)sender;
- (IBAction)GetinButttonClick:(id)sender;
- (IBAction)InstantMoreInfoButtonClick:(id)sender;
- (IBAction)DayCareMoreInfoButtonClick:(id)sender;
- (IBAction)WorkoutMoreInfoButtonClick:(id)sender;
- (IBAction)HomelessMoreInfoButtonClick:(id)sender;


- (IBAction)InstantadSweepWinnersButtonClick:(id)sender;
- (IBAction)DaycareWinnersButtonClick:(id)sender;
- (IBAction)WorkerBonusWinnersClick:(id)sender;
- (IBAction)HomelessWinnersButonClick:(id)sender;
- (IBAction)ContestsMoreInfoButtonClick:(id)sender;
- (IBAction)WinnersMoreInfoButtonClick:(id)sender;
- (IBAction)MsMoreInfoButtonClick:(id)sender;
- (IBAction)CheckPuzzleButtonClick:(id)sender;
- (IBAction)RefundButtonClick:(id)sender;
-(IBAction)BackButtonClick:(id)sender;

@end
