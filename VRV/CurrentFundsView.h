

#import <UIKit/UIKit.h>


@interface CurrentFundsView : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
{
    __weak IBOutlet UIImageView *MoneyImageview;
    __weak IBOutlet UIImageView *CoinImageview;
    __weak IBOutlet UITableView *Refereshtabelview;
    __weak IBOutlet UILabel *BelensLabel;
    __weak IBOutlet UIButton *ContributeButton;
    __weak IBOutlet UIButton *MoneyBtnOutlet;
    __weak IBOutlet UIButton *LearnBtnOutlet;
    __weak IBOutlet UIButton *addressBtnOutlet;
    __weak IBOutlet UILabel *PercentIncrease;
    __weak IBOutlet UILabel *VrvCoinValue;
    __weak IBOutlet UITextField *smartContract;
    
    
    __weak IBOutlet UILabel *current_funds;
    
    __weak IBOutlet UILabel *contest_balance;
    
    __weak IBOutlet UILabel *bnb_current_funds;
    
    __weak IBOutlet UIView *whiteArea;
    
    __weak IBOutlet UIView *wholeview;
    
    

    
   
    __weak IBOutlet UITextField *MoneyTextField;
 
}


@property(nonatomic, strong, readwrite) NSString *environment;

@property(strong,nonatomic) UIRefreshControl *refreshControl;
- (IBAction)MenuButtonClick:(id)sender;
- (IBAction)ContributeButtonClick:(id)sender;
- (IBAction)moneybuttonclick:(id)sender;
 -(IBAction)learnbuttonclick:(id)sender;
 -(IBAction)addressbuttonclick:(id)sender;



@end
