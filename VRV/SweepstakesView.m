
#import "SweepstakesView.h"
#import "LeftRightViewController.h"
#import "Defaults.h"
#import "CommanFunction.h"
#import "Reachability.h"
#import "AFNetworking.h"
#import "AnimatedGif.h"
#import "UIImageView+AnimatedGif.h"
#import "VRV-Swift.h"
#import "ContestViewController.h"
#import "ContestWebViewController.h"
#import "ReadytoRecycleView.h"
#import <PDFKit/PDFKit.h>

@interface SweepstakesView () {
    NSString *Timesting1;
    NSDateFormatter *formatter;
    
    NSString *strdinnertime;
    NSString *strdinerminut;
    NSString *strdinersecond;
    
    NSString *userCurrentCity;
    
    NSUserDefaults *dlf;
    
    CLLocation *lastLocation;
}
@end

@implementation SweepstakesView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    

    
    [[UIButton appearance] setExclusiveTouch:YES];
    
    UISwipeGestureRecognizer *gestureRecognizerRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandlerRight:)];
    [gestureRecognizerRight setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [self.view addGestureRecognizer:gestureRecognizerRight];


    dlf = [NSUserDefaults standardUserDefaults];
    
    [MainScrollview setScrollEnabled:YES];
    
    MainScrollview.contentSize = CGSizeMake(self.view.frame.size.width, MainView.frame.size.height);
    MainScrollview.backgroundColor = [UIColor whiteColor];
    

    
    
    // Button RoundCorner
   
    
    getinButton.layer.cornerRadius = 4;
    getinButton.clipsToBounds = YES;
    MsMoreInfoButton.layer.cornerRadius = 4;
    MsMoreInfoButton.clipsToBounds = YES;
    
    
    yourinButton.layer.cornerRadius = 4;
    yourinButton.clipsToBounds = YES;
    yourinButton.hidden = YES;
    
    instantmorebutton.layer.cornerRadius = 4;
    instantmorebutton.clipsToBounds = YES;
    daycaremorebuttton.layer.cornerRadius = 4;
    daycaremorebuttton.clipsToBounds = YES;
    workerbonusButton.layer.cornerRadius = 4;
    workerbonusButton.clipsToBounds = YES;
    HomelessButton.layer.cornerRadius = 4;
    HomelessButton.clipsToBounds = YES;
    
    ConstenstsMoreInfoButton.layer.cornerRadius = 4;
    ConstenstsMoreInfoButton.clipsToBounds = YES;
    ConstenstsWinnersInButton.layer.cornerRadius = 4;
    ConstenstsWinnersInButton.clipsToBounds = YES;
    CheckPuzzleButton.clipsToBounds=YES;
    CheckPuzzleButton.layer.cornerRadius = 4;
    
    RefundButton.clipsToBounds=YES;
    RefundButton.layer.cornerRadius = 4;
    
    instantWinnerBtn.layer.cornerRadius = 4;
     instantWinnerBtn.clipsToBounds = 4;
    
    DaycareWinnerBtn.layer.cornerRadius = 4;
    DaycareWinnerBtn.clipsToBounds = 4;
    
    WorkerWinnerBtn.layer.cornerRadius = 4;
    WorkerWinnerBtn.clipsToBounds = 4;
    
    HomlessWinnerBtn.layer.cornerRadius = 4;
    HomlessWinnerBtn.clipsToBounds = 4;
    
    

    for (UIButton *btn in WinnersButtonCollections) {
        btn.layer.cornerRadius = 4;
        btn.clipsToBounds = YES;
    }
    
    DismissPdf.hidden = YES;
    DismissPdfArrow.hidden = YES;
    
    
    
    /*/LOCALIZATION STORYBOARD*/
    
    HeaderTitle.text = NSLocalizedString(@"sweepstakes", "");
    
    worker_bonus.text = NSLocalizedString(@"worker_bonus", "");
    
    homeless.text = NSLocalizedString(@"homeless", "");
    
    contests.text = NSLocalizedString(@"contests", "");
    
    
    [workerbonusButton setTitle:NSLocalizedString(@"more_info", "") forState:UIControlStateNormal];
    
    [WorkerWinnerBtn setTitle:NSLocalizedString(@"sw_winners", "") forState:UIControlStateNormal];
    
    [HomelessButton setTitle:NSLocalizedString(@"more_info", "") forState:UIControlStateNormal];
    
    [HomlessWinnerBtn setTitle:NSLocalizedString(@"sw_winners", "") forState:UIControlStateNormal];
    
    [ConstenstsMoreInfoButton setTitle:NSLocalizedString(@"more_info", "") forState:UIControlStateNormal];

    
    
    // User is automatically in the sweepstake.
    
//    [self.view setUserInteractionEnabled: NO];
//    [self showProgressView];
//    [self performSelector:@selector(enableUserinteraction:) withObject:self afterDelay:0.2 ];
//
    // User is automatically in the sweepstake.
    
//    [SweepstakeService getSweepstakeStatusWithSuccess:^(BOOL response) {
//        [self hideProgressView];
//        [self updateSweepstakeStatus:response];
//
//
//    } failure:^(NSError * _Nullable error) {
//
//       [self handleError:error];
//        [self hideProgressView];
//
//
//    }];
//}
//
//-(void)updateSweepstakeStatus:(BOOL)status {
//    self->getinButton.hidden = status;
//    self->yourinButton.hidden = !status;
//}

#pragma mark Location Manager Delegate Method.
    
}

-(void)enableUserinteraction:(id)sender {
    
    [self.view setUserInteractionEnabled:YES];
    
}


- (void)showAlertTutorial:(NSString *)message {
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:@"Location access is required. Touch Tutorial to learn how to enable location access."
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* tutorialButton = [UIAlertAction
                                     actionWithTitle:@"Tutorial"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         
                                         NSURL *recloc = [NSURL URLWithString:@"apple_location_access"];
                                         
                                         [[UIApplication sharedApplication] openURL: recloc options:@{} completionHandler:^(BOOL success) {
                                             if (success)
                                                 NSLog(@"Opened tutorial");
                                             
                                         }];
                                         
                                         
                                     }];
    
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"Settings"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   
                                   NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                   if (url != nil) {
                                       [[UIApplication sharedApplication] openURL:url options:[NSDictionary new] completionHandler:nil];
                                   }
                                   
                                   
                               }];
    
    
    UIAlertAction*  cancelButton = [UIAlertAction
                                    actionWithTitle:@"Cancel"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        
                                        
                                    }];
    
    [alert addAction:okButton];
    [alert addAction:tutorialButton];
    [alert addAction:cancelButton];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)swipeHandlerRight:(id)sender {
    LeftRightViewController *LeftrightviewScreenObject = [self.storyboard instantiateViewControllerWithIdentifier:@"leftright"];
    UIViewController *newVC = LeftrightviewScreenObject;
    NSMutableArray *vcs =  [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
    [vcs insertObject:newVC atIndex:vcs.count-1];
    [self.navigationController setViewControllers:vcs animated:YES];
    [self.navigationController popViewControllerAnimated:YES];
    
}


#pragma mark VOID METHOD.

- (void)showAlertInternet:(NSString *)message {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Internet Connection Offline"
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"Retry"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   
                                   [self showAlertGameRules:@"You have between Saturday 12 AM PST to Friday before 11:30 PM PST to complete your contest entry.\n\nOur sweepstakes game on promotion is available to all users for FREE. Make sure your connection is strong and wireless.\n\nTouch reload if the game does not load. Please read the rules on the rules screen."];
                                   
                               }];
    [alert addAction:okButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)showAlert:(NSString *)message {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"dialog_ok", "")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   
                               }];
    [alert addAction:okButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)showAlertSweepstakes:(NSString *)message {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                
                                   
                                   
                               }];
    [alert addAction:okButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)showAlertContest:(NSString *)message {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   
                                   
                                   
                               }];
    [alert addAction:okButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}



-(void)MakeRoundedCornerAndShadowForUIView:(UIView*)myView{
    
    (myView.layer).cornerRadius = 10.0f;
    
    (myView.layer).shadowColor = [UIColor blackColor].CGColor;
    (myView.layer).shadowOpacity = 0.8;
    (myView.layer).shadowRadius = 3.0;
    (myView.layer).shadowOffset = CGSizeMake(2.0, 2.0);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark IBAction Methods.

-(IBAction)BackButtonClick:(id)sender {
    
    
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"PDF"
                                  message:@"If the Pdf does not load, make sure you have a good connection and touch refresh."
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   
                               }];
    
    UIAlertAction* contestButton = [UIAlertAction
                              actionWithTitle:@"Refresh contest"
                              style:UIAlertActionStyleDefault
                              handler:^(UIAlertAction * action)
                              {
                                
                                      [self ContestRulesPdf];
                                  self->HeaderTitle.text = @"Contest Rules";
                            
                                  
                              }];
    
   

    
    UIAlertAction* cButton = [UIAlertAction
                              actionWithTitle:@"Close Pdf"
                              style:UIAlertActionStyleDefault
                              handler:^(UIAlertAction * action)
                              {
                                  
                                  self->_webkitView.hidden= YES;
                                  self->MainView.hidden=NO;
                                  
                                  self->DismissPdf.hidden = YES;
                                  self->DismissPdfArrow.hidden = YES;
                                  self->HeaderTitle.text = @"Rules";
                                  
                                      
                                  }];
                                  
    
    
    
    [alert addAction:contestButton];
    [alert addAction:cButton];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];

    
}

- (IBAction)MenuButtonClick:(id)sender {
    LeftRightViewController *LeftrightviewScreenObject = [self.storyboard instantiateViewControllerWithIdentifier:@"leftright"];
    UIViewController *newVC = LeftrightviewScreenObject;
    NSMutableArray *vcs =  [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
    [vcs insertObject:newVC atIndex:vcs.count-1];
    [self.navigationController setViewControllers:vcs animated:YES];
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)GetinButttonClick:(id)sender {
    
    [self showAlertSweepstakes:@"Unavailable."];
    
    // User is automatically in the sweepstake.
    
    //    updateSweepstakeStatusForUser;
    
//    Reachability *reachability = [Reachability reachabilityForInternetConnection];
//    [reachability startNotifier];
//    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
//
//    if (remoteHostStatus == NotReachable)
//    {
//         [self showAlertInternet:@"Internet connection offline."];
//
//    }
//
//
//    else {
//
//        [self.view setUserInteractionEnabled: NO];
//        [self showProgressView];
//        [self performSelector:@selector(enableUserinteraction:) withObject:self afterDelay:0.2 ];
//        [TimerService getTimerWithSuccess:^(TimerData * _Nonnull times) {
//            [self hideProgressView];
//
//
//            // Get weekday 1 = Sunday, 2 = Monday, etc.
//           // if (timeNowDouble > 100000 && timeNowDouble < 175959 && weekday == 1 )
//
//            NSInteger weekday = [[NSCalendar currentCalendar] component:NSCalendarUnitWeekday
//            fromDate:[NSDate date]];
//
//
//            NSString *timeNow = [[times time_now] stringByReplacingOccurrencesOfString:@":" withString:@""];
//            double timeNowDouble = [timeNow doubleValue];
//            if (timeNowDouble > 100000 && timeNowDouble < 175959 && weekday == 1 ) {
//                [self updateSweepstakeStatusForUser];
//            } else {
//                [self showAlert:@"You must wait until Sunday between 10:00am to 5:59 pm (PST)."];
//            }
//        } failure:^(NSError * _Nullable error) {
//            [self handleError:error];
//            [self hideProgressView];
//
//        }];
//    }
}



//- (void)updateSweepstakeStatusForUser {
//    [self.view setUserInteractionEnabled: NO];
//    [self showProgressView];
//    [self performSelector:@selector(enableUserinteraction:) withObject:self afterDelay:0.2];
//
//    [SweepstakeService updateCustomerSweepstakesWithSuccess:^(BOOL status) {
//        [self hideProgressView];
//        [self updateSweepstakeStatus:status];
//        [self showAlertSweepstakes:@"Your entry has been added."];
//
//    } failure:^(NSError * _Nullable error) {
//       [self handleError:error];
//        [self hideProgressView];
//
//    }];
//}


- (IBAction)YourinButtonClick:(id) sender {
    
    // can put an alert message, but lets leave blank, like android
    
    
}

- (IBAction)InstantMoreInfoButtonClick:(id)sender {
    
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Instant Ad Sweepstakes"
                                  message:@"Instant ad sweepstakes may occur.\n\nApple is not a sponsor."
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {

                                   
                               }];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)DayCareMoreInfoButtonClick:(id)sender {
    
    NSURL *instantad = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/sunday_jackpot_form"];
    
    [[UIApplication sharedApplication] openURL: instantad options:@{} completionHandler:^(BOOL success) {
        if (success)
            NSLog(@"Opened jackpot submission form");
        
    }];
}

- (IBAction)WorkoutMoreInfoButtonClick:(id)sender {
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:NSLocalizedString(@"worker_bonus", "")
                                  message:NSLocalizedString(@"worker_para", "")
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"dialog_ok", "")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   
                                   
                               }];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)HomelessMoreInfoButtonClick:(id)sender {
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:NSLocalizedString(@"homeless", "")
                                  message:NSLocalizedString(@"homeless_para", "")
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"dialog_ok", "")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   
                                   
                               }];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
    
}


- (IBAction)InstantadSweepWinnersButtonClick:(id)sender {
    
    NSURL *instantad = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/instant_ad_winners"];
    
    [[UIApplication sharedApplication] openURL: instantad options:@{} completionHandler:^(BOOL success) {
        if (success)
            NSLog(@"Opened iasweep.pdf");
        
    }];
    
}

- (IBAction)DaycareWinnersButtonClick:(id)sender {
    
    NSURL *daycare = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/sunday_jackpot"];
    
    [[UIApplication sharedApplication] openURL: daycare options:@{} completionHandler:^(BOOL success) {
        if (success)
            NSLog(@"jackpot.pdf");
        
    }];
    
}

- (IBAction)WorkerBonusWinnersClick:(id)sender {
    
    NSURL *workerbonus = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/country_page"];
    
    [[UIApplication sharedApplication] openURL: workerbonus options:@{} completionHandler:^(BOOL success) {
        if (success)
            NSLog(@"Opened workerbonus.pdf");
        
    }];
}

- (IBAction)HomelessWinnersButonClick:(id)sender {
    
    NSURL *homeless = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/country_page"];
    
    [[UIApplication sharedApplication] openURL: homeless options:@{} completionHandler:^(BOOL success) {
        if (success)
            NSLog(@"homelessshelter.pdf");
        
    }];
    
}

- (IBAction)ContestsMoreInfoButtonClick:(id)sender {
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:NSLocalizedString(@"contests", "")
                                  message:NSLocalizedString(@"rules_contest_mi", "")
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"dialog_ok", "")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   
                                   
                               }];
    
    UIAlertAction* ContestRulesButton = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"read_rules", "")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   
                                   Reachability *reachability = [Reachability reachabilityForInternetConnection];
                                   [reachability startNotifier];
                                   NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
                                   if (remoteHostStatus == NotReachable)
                                   {

                                       [reachability stopNotifier];
                                       [self showAlert:NSLocalizedString(@"internetActivity_off", "")];
                                       
                                   } else {
                                       
                                       [reachability stopNotifier];
                                       
                                       [self ContestRulesPdf];
                                       self->DismissPdf.hidden = NO;
                                       self->DismissPdfArrow.hidden = NO;
                                       //PasswordTextField.text = @"password123";
                                       self->HeaderTitle.text = NSLocalizedString(@"scroll_txt", "");
                                   }
            
                               }];
    
    
    UIAlertAction* ContestWinnersButton = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"sw_winners", "")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   
                                   NSURL *contest = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/country_page"];
                                   
                                   [[UIApplication sharedApplication] openURL: contest options:@{} completionHandler:^(BOOL success) {
                                       if (success)
                                           NSLog(@"Opened contest winners.pdf");
                                       
                                       
                                       
                                   }];
                                   
                                   
                               }];
    
    UIAlertAction* RefundButton = [UIAlertAction
    actionWithTitle:NSLocalizedString(@"rules_refund", "")
    style:UIAlertActionStyleDefault
    handler:^(UIAlertAction * action)
    {
        
       NSURL *refund = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/country_page"];
        
        [[UIApplication sharedApplication] openURL: refund options:@{} completionHandler:^(BOOL success) {
            if (success)
                NSLog(@"refund.pdf");
            
        }];
        
        
    }];
    
    
    [alert addAction:ContestRulesButton];
    [alert addAction:ContestWinnersButton];
    [alert addAction:RefundButton];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)ContestGetInButtonClick:(id)sender  {
    
    //-------- /api/v1/contest/create ---------//
    [self createContast];
    //Implement with these response codes so I can do the proper alert dialogs on my end.
    // response code 426
    // response code 425
    // response code 427
    // response code 421
    // if there is no response code 426,425,427,421 then use ELSE and implement  /api/v1/contest/start_game
    
    
    //-------- /api/v1/contest/start_game ---------//
    
    // We need response code 200 ELSE I will implement an alert dialog.
    
    
    
    
    
    //-------- Your code section---------//
    
    
    
    
    
    
    
    
    //-------- My code section---------//
    //    [self showAlertSweepstakes:@"Currently, there are no contests."];
    //    ContestViewController *ContestviewScreenObject = [self.storyboard instantiateViewControllerWithIdentifier:@"ContestId"];
    //    UIViewController *newVC = ContestviewScreenObject;
    //    NSMutableArray *vcs =  [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
    //    [vcs insertObject:newVC atIndex:vcs.count-1];
    //    [self.navigationController setViewControllers:vcs animated:YES];
    //    [self.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction)MsMoreInfoButtonClick:(id)sender  {
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Membership Sweepstakes"
                                  message:@"Play and finish the weekly word puzzle for cash prizes. Touch game on under the contest section.\n\nApple is not a sponsor."
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   
                                   
                               }];
    
    UIAlertAction* rButton = [UIAlertAction
                                         actionWithTitle:@"Read Rules"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action)
                                         {

                                             Reachability *reachability = [Reachability reachabilityForInternetConnection];
                                             [reachability startNotifier];
                                             NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
                                             if (remoteHostStatus == NotReachable)
                                             {

                                                 [reachability stopNotifier];
                                                 [self showAlert:@"Internet connection offline."];

                                             } else {

                                                 [reachability stopNotifier];

                                                 [self SweepstakeRulesPdf];
                                                 self->DismissPdf.hidden = NO;
                                                 self->DismissPdfArrow.hidden = NO;
                                                 self->HeaderTitle.text = @"Sweepstakes Rules";

                                             }

                                         }];


    UIAlertAction* wButton = [UIAlertAction
                               actionWithTitle:@"Winners"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {

                                   NSURL *membership = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/sweepstake_winners"];

                                   [[UIApplication sharedApplication] openURL: membership options:@{} completionHandler:^(BOOL success) {
                                       if (success)
                                           NSLog(@"Opened msw.pdf");

                                   }];

                               }];
    
  
    [alert addAction:rButton];
    [alert addAction:wButton];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)CheckPuzzleButtonClick:(id)sender {
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Device Check"
                                  message:@"Check your device to see if your browser supports WebGL. Make sure you update your browser.\n\nIf your device does not support WebGL, you may not be able to play the contest game."
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   
                                   
                                   
                               }];
    
    
    UIAlertAction* CheckButton = [UIAlertAction
                               actionWithTitle:@"Check Device"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   
                                   NSURL *recloc = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/web_gl"];
                                   
                                   [[UIApplication sharedApplication] openURL: recloc options:@{} completionHandler:^(BOOL success) {
                                       if (success)
                                           NSLog(@"Opened webgl check");
                                       
                                   }];
                                   
                                   
                               }];
    
    
    [alert addAction:CheckButton];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (IBAction)RefundButtonClick:(id)sender {
    
    NSURL *homeless = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/contest_refund"];
    
    [[UIApplication sharedApplication] openURL: homeless options:@{} completionHandler:^(BOOL success) {
        if (success)
            NSLog(@"refund.pdf");
        
    }];
    
}

- (void)createContast {
    //Implement with these response codes so I can do the proper alert dialogs on my end.
    // response code 426
    // response code 425
    // response code 427
    // response code 421
    // if there is no response code 426,425,427,421 then use ELSE and implement  /api/v1/contest/start_game
    [ContestService createContestWithSuccess:^(NSInteger code) {
        if (code == 426) {
            [self showAlert:@"You must wait between 8:00 am to 4:59 pm (PST)."];
            
        }else if (code == 425) {
            [self showAlert:@"Currently, there are no contests."];
            
        }else if (code == 427) {
            [self showAlertNoDeposit:@"To enter in the contest you must submit a $1.00 Vrv deposit for at least one material.\n\nMake sure you check device to see if your device is compatible."];
            
        }else if (code == 421) {
            [self showAlert:@"You have already played the game for today. You can try again tomorrow."];
            
        }else if (code == 404) {
                
            [self showAlert:@"Restarting. In several seconds, touch game on again."];
            
        }else {
            
            [self showAlertGameRules:@"You have between Saturday 12 AM PST to Friday before 11:30 PM PST to complete your contest entry.\n\nMake sure your connection is wireless and strong. Use Wi-fi for faster loading and please read the rules on the rules screen."];
            
            // [self startGame];
        }
    } failure:^(NSError * _Nullable error) {
        //Handle Error
        [self handleError:error];
    }];
}

- (void)startGame {
   
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    //Reachability *reachability = [Reachability reachabilityWithHostName:@"www.google.com"];
    [reachability startNotifier];
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    
    if (remoteHostStatus == NotReachable)
    {
        [reachability stopNotifier];
        
        [[self presentingViewController] dismissViewControllerAnimated:NO completion:nil];
        
   //::     [self showAlertInternet:@"Touch retry when you have a good internet connection via wireless only.\n\nIf you exit the app your entry will be invalid for today! Please read the contest rules in the sweepstake screen."];
        
        [self showAlertInternet:@"Touch retry when you have a good internet connection via wireless only. Please read the contenst rules in the sweepstake screen."];
        
    } else {
        
        [reachability stopNotifier];
        
        [ContestService startGameWithSuccess:^(NSString * _Nonnull token, NSInteger code) {
            
            [ContestWebViewController pushContestController:self token:token level:code];
            
        } failure:^(NSError * _Nullable error) {
            //Handle Error
        }];
        
    }
       
        
}
    
    
   

- (void)showAlertNoDeposit:(NSString *)message {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Contests"
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   
                               }];
    
    UIAlertAction* recycleButton = [UIAlertAction
                                    actionWithTitle:@"Recycle"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        ReadytoRecycleView *ReadytoRecycleScreenObj = [self.storyboard instantiateViewControllerWithIdentifier:@"ReadytorecycleID"];
                                        [self.navigationController pushViewController:ReadytoRecycleScreenObj animated:YES];
                                    }];
    
    
    [alert addAction:recycleButton];
    [alert addAction:okButton];
    
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)showWebGl:(NSString *)message {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"WebGL"
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"Continue"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   
                                   [self showAlertGameRules:@"Your internet connection must be good and your device must support WebGL before you can play.\n\nIf you exit the app now, your entry will be invalid for today!\n\nPlease read the contest rules in the sweepstake screen."];
                                   
                               }];
    [alert addAction:okButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)showAlertGameRules:(NSString *)message {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Game On Promotion!"
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction* playButton = [UIAlertAction
                                 actionWithTitle:@"Play"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     
                                     //[self checkInternet];
                                     [self startGame];
                                     
                                 }];
    
    
    UIAlertAction* exitButton = [UIAlertAction
                                      actionWithTitle:@"Exit"
                                      style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction * action)
                                      {
                                          
                                        
                                      }];
    
    [alert addAction:exitButton];
    [alert addAction:playButton];
    [self presentViewController:alert animated:YES completion:nil];
}


- (void)ContestRulesPdf {
    
   _webkitView.hidden= NO;
    MainView.hidden=YES;

    [_webkitView setMultipleTouchEnabled:false];
    [_webkitView setContentMode:UIViewContentModeScaleAspectFit];
    [self.view addSubview:_webkitView];

    NSString *url = [NSString stringWithFormat:@"https://uat.voluntaryrefundvalue.com/link/contest_rules"];
    
    NSURL *nsUrl = [NSURL URLWithString:url];
    NSURLRequest *request = [NSURLRequest requestWithURL:nsUrl cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:0];
    
    [_webkitView loadRequest:request];
    
}

- (void)SweepstakeRulesPdf {
    
    _webkitView.hidden= NO;
    MainView.hidden=YES;
    
    [_webkitView setMultipleTouchEnabled:false];
    [_webkitView setContentMode:UIViewContentModeScaleAspectFit];
    [self.view addSubview:_webkitView];
    
    NSString *url = [NSString stringWithFormat:@"https://uat.voluntaryrefundvalue.com/link/sweepstake_rules"];
    
    NSURL *nsUrl = [NSURL URLWithString:url];
    NSURLRequest *request = [NSURLRequest requestWithURL:nsUrl cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:0];
    
    [_webkitView loadRequest:request];
    
}

//- (void)checkInternet {
//    Reachability *reachability = [Reachability reachabilityForInternetConnection];
//    [reachability startNotifier];
//    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
//    if (remoteHostStatus == NotReachable)
//    {
//
//          [reachability stopNotifier];
//
//    } else {
//
//          [reachability stopNotifier];
//
//    }
//}

@end
