

#import <UIKit/UIKit.h>
#import <UserNotifications/UserNotifications.h>
#import <MapKit/MapKit.h>
#import <AVFoundation/AVFoundation.h>
#include <AudioToolbox/AudioToolbox.h>
#import <AudioToolbox/AudioServices.h>
#import <MediaPlayer/MediaPlayer.h>

@import GooglePlaces;


@interface AppDelegate : UIResponder <UIApplicationDelegate, UNUserNotificationCenterDelegate, AVAudioSessionDelegate>

@property (strong, nonatomic) AVAudioPlayer *mp3AudioPlay;
@property (strong, nonatomic) AVPlayer *audioPlayer;
@property (strong, nonatomic)  NSTimer *winnersintimecount;
@property (strong, nonatomic) NSString *strDeviceTokenId;
@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) NSString *Hourstring;
@property (strong, nonatomic) NSString *minutestring;
@property (strong, nonatomic) NSString *secondstring;

@property (NS_NONATOMIC_IOSONLY, getter=getDuration, readonly) float duration;

- (void)play;
- (void)pause;
- (void)stop;
@property (NS_NONATOMIC_IOSONLY, getter=getCurrentTime, readonly) float currentTime;

@property (NS_NONATOMIC_IOSONLY, getter=getMinushTime, readonly) NSTimeInterval minushTime;

@property (NS_NONATOMIC_IOSONLY, getter=getloadDuration, readonly) float loadDuration;




@end

