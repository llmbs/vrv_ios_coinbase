

#import "CurrentFundsView.h"
#import <QuartzCore/QuartzCore.h>
#import "Reachability.h"
#import "AFNetworking.h"
#import "Defaults.h"
#import "CommanFunction.h"
#import <Lottie/Lottie.h>
#import "LeftRightViewController.h"
#import "VRV-Swift.h"
//@import Stripe;
//@interface CurrentFundsView () <PKPaymentAuthorizationViewControllerDelegate>

@interface CurrentFundsView ()
{
    double lastAmount;
    BOOL paymentSuceeded;
    NSError *lastError;
}

@property (nonatomic, strong) LOTAnimationView *lottieLogo;

@end

@implementation CurrentFundsView

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UISwipeGestureRecognizer *gestureRecognizerRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandlerRight:)];
    [gestureRecognizerRight setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [self.view addGestureRecognizer:gestureRecognizerRight];
    
    
    ContributeButton.layer.cornerRadius = 4;
    ContributeButton.clipsToBounds = YES;
    
    MoneyBtnOutlet.layer.cornerRadius = 4;
    MoneyBtnOutlet.clipsToBounds = YES;
    
    LearnBtnOutlet.layer.cornerRadius = 4;
    LearnBtnOutlet.clipsToBounds = YES;
    
    addressBtnOutlet.layer.cornerRadius = 4;
    addressBtnOutlet.clipsToBounds = YES;
   
    

    MoneyTextField.tintColor = [UIColor clearColor];
    [[UIButton appearance] setExclusiveTouch:YES];
    
    smartContract.text = @"0x5ef6622b3842c1f7a5e96e89c81a96b8f27bd8ae";
    
    
    [MoneyBtnOutlet setTitle:NSLocalizedString(@"refresh_current", "") forState:(UIControlStateNormal)];
    
    [LearnBtnOutlet setTitle:NSLocalizedString(@"refresh_current", "") forState:(UIControlStateNormal)];
    
    
    
    current_funds.text = NSLocalizedString(@"current_funds", "");
    
    contest_balance.text = NSLocalizedString(@"contest_balance", "");
    
    bnb_current_funds.text = NSLocalizedString(@"bnb_current_funds", "");
    
    PercentIncrease.text = NSLocalizedString(@"dollar_sign", "");
    
    
    /*/*Game on lottie */
     
    self.lottieLogo = [LOTAnimationView animationNamed:@"bitcoin_l"];
    self.lottieLogo.frame = CGRectMake(0, 0, 125 , 125);
   // self.lottieLogo.center = whiteArea.center;
    
    
    self.lottieLogo.center = CGPointMake(whiteArea.center.x,192    );
    
    self.lottieLogo.contentMode = UIViewContentModeScaleAspectFill;
    self.lottieLogo.loopAnimation = YES;
    [self.view addSubview:self.lottieLogo];
    [self.lottieLogo play];
    [self.view bringSubviewToFront:wholeview];
    
    
    
   
    
    
}

- (void)getConfiguration {
    
    [self.view setUserInteractionEnabled: NO];
    [self performSelector:@selector(enableUserinteraction:) withObject:self afterDelay:0.2 ];
   // [self showProgressView];
    
    [ContributionService getContributionWithSuccess:^(NSString *balance) {
        [self hideProgressView];
//        NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
//        NSNumber *number = [numberFormatter numberFromString:balance];
//        [numberFormatter setNumberStyle: NSNumberFormatterCurrencyStyle];
//
//        numberFormatter.currencySymbol = @"$";
//        NSString *numberString = [numberFormatter stringFromNumber:number];
//        self->BelensLabel.text = numberString;
        
        
        self->BelensLabel.text = balance;
        
        
    } failure:^(NSError * _Nullable error) {
        
        
        [self handleError:error];
        [self hideProgressView];
     
        
    }];
    
    
//    [RecyclingService recycleCheckDepositWithSuccess:^(NSInteger code) {
//
//                if (code == 421) {
//                    NSLog(@"%ld",(long)code);
//                    [self showAlert:@"You have a VRVC coin."];
//                    self->VrvCoinValue.text = @"1";
//
//                } else {
//                    NSLog(@"%ld",(long)code);
//                    [self showAlert:@"You can buy a VRVC coin."];
//                }
//
//            } failure:^(NSError * _Nullable error) {
//
//
//            }];
        
    }


-(void)FadeinOutAnimation{
    
    [UIView animateWithDuration:0.5
                        delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                        animations:(void (^)(void)) ^{
                        self->MoneyImageview.transform=CGAffineTransformMakeScale(1.2, 1.2);
                     }
                         completion:^(BOOL finished){
                             self->MoneyImageview.transform=CGAffineTransformIdentity;
            
                     }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)MenuButtonClick:(id)sender {
    MoneyTextField.text = @"";
    LeftRightViewController *LeftrightviewScreenObject = [self.storyboard instantiateViewControllerWithIdentifier:@"leftright"];
    UIViewController *newVC = LeftrightviewScreenObject;
    NSMutableArray *vcs =  [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
    [vcs insertObject:newVC atIndex:vcs.count-1];
    [self.navigationController setViewControllers:vcs animated:YES];
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction)learnbuttonclick:(id)sender {
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:NSLocalizedString(@"faq_txt", "")
                                  message:NSLocalizedString(@"vrvCoin_para", "")
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction* copyButton = [UIAlertAction
                                  actionWithTitle:NSLocalizedString(@"copy", "")
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action)
                                  {
                                      
        
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        pasteboard.string = self->smartContract.text;
        
        [self quickAlert:NSLocalizedString(@"address_copied", "")];
        
                                  }];
    
        
                                    
    [alert addAction:copyButton];
    [self presentViewController:alert animated:YES completion:nil];
    
    
}

- (IBAction)addressbuttonclick:(id)sender {
    
    // add api and upon success I'll make an alert dialog
    
    [self quickAlert:NSLocalizedString(@"address_copied", "")];
    
}


- (IBAction)ContributeButtonClick:(id)sender {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Card Statements"
                                  message:@"When you submit a Vrv contribution, the name that will appear on your card statements will be VRV LLC. \n\nWe use Apple Pay for your protection. For your convenience, the tos has been provided below. Touch ok to continue."
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    
    
    
    
    
    UIAlertAction*DismissButton  = [UIAlertAction
                                   actionWithTitle:@"Dismiss"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       
                                           
                                      
                                   }];
    
    UIAlertAction*TosButton  = [UIAlertAction
                                    actionWithTitle:@"Tos"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        NSURL *instantad = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/terms_of_service"];
                                        
                                        [[UIApplication sharedApplication] openURL: instantad options:@{} completionHandler:^(BOOL success) {
                                            if (success)
                                                NSLog(@"Opened tos.pdf");
                                            
                                        }];
                                        
                                    }];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"Ok"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   UIAlertController * alert=   [UIAlertController
                                                                 alertControllerWithTitle:@"Contribute"
                                                                 message:@""
                                                                 preferredStyle:UIAlertControllerStyleAlert];
                                   
                                   UIAlertAction* submit = [UIAlertAction actionWithTitle:@"Submit" style:UIAlertActionStyleDefault
                                                                                  handler:^(UIAlertAction * action) {
                                                                                      //Do Some action here
                                                                                      NSLog(@"amt = %@",self->MoneyTextField.text);
                                                                                      
                                                                                      double moneyint = [self getCentValue];
                                                                                      
                                                                                      if (moneyint < 1.00) {
                                                                                          [self->MoneyTextField becomeFirstResponder];
                                                                                          [self showAlert:@"Allowed minimum contribution is $1.00."];
                                                                                      } else if (moneyint > 1.00) {
                                                                                          [self->MoneyTextField becomeFirstResponder];
                                                                                          [self showAlert:@"Maximum contribution is $1.00"];
                                                                                      } else {
                                                                                          //google pay api
                                                                                          //api/vi/contribution
                                                                                        //  [self payViaApplePay:moneyint];
                                                                                      }
                                                                                  }];
                                   
                                   UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                                                  handler:^(UIAlertAction * action) {
                                                                                      [alert dismissViewControllerAnimated:YES completion:nil];
                                                                                  }];
                                   
                                   
                                   [alert addAction:cancel];
                                   [alert addAction:submit];
                                   
                                   [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
                                       textField.delegate = self;
                                       textField.placeholder = @"How much?";
                                       self->MoneyTextField = textField;
                                       self->MoneyTextField.keyboardType = UIKeyboardTypeDecimalPad;
                                       [self configureMoneyTextField];
                                   }];
                                   
                                   [self presentViewController:alert animated:YES completion:nil];
                                   
                                   
                               }];
    
    [alert addAction:TosButton];
    [alert addAction:DismissButton];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)configureMoneyTextField {
    
    
    
}

- (void)enableMoney {
      MoneyBtnOutlet.enabled = true;
}

- (void)showBusdalert:(NSString *)message {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:NSLocalizedString(@"vrvCoin_Header", "")
                                  message:NSLocalizedString(@"eth_para", "")
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"dialog_ok", "")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   
                                   
                               }];
    
    UIAlertAction* usdcButton = [UIAlertAction
                                  actionWithTitle:@"OpenNode"
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action)
                                  {
                                      
        
        NSURL *instantad = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/bsc_cf"];
        
        [[UIApplication sharedApplication] openURL: instantad options:@{} completionHandler:^(BOOL success) {
            if (success)
                NSLog(@"Opened bscscan.com");
            
           
            
        }];
                                    
        
                                      
                                  }];
    
    [alert addAction:usdcButton];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}



- (void)showAlert:(NSString *)message {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"dialog_ok", "")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   
                                   
                               }];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}



-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (textField == MoneyTextField) {
    NSString *cleanCentString = [[textField.text componentsSeparatedByCharactersInSet: [[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];
    NSInteger centValue = [cleanCentString intValue];
    NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
    NSNumber *myNumber = [f numberFromString:cleanCentString];
    NSNumber *result;
    
    if([textField.text length] < 16) {
        if (string.length > 0)
        {
            centValue = centValue * 10 + [string intValue];
            double intermediate = [myNumber doubleValue] * 10 +  [[f numberFromString:string] doubleValue];
            result = [[NSNumber alloc] initWithDouble:intermediate];
        } else {
            centValue = centValue / 10;
            double intermediate = [myNumber doubleValue]/10;
            result = [[NSNumber alloc] initWithDouble:intermediate];
        }
        
        myNumber = result;
        NSLog(@"%ld ++++ %@", (long)centValue, myNumber);
        double finalAmount = [myNumber doubleValue] / 100.0f;
        if (finalAmount > 1.00) {
            finalAmount = 1.00;
        }
        NSNumber *formatedValue = [[NSNumber alloc] initWithDouble:finalAmount];
        NSNumberFormatter *_currencyFormatter = [[NSNumberFormatter alloc] init];
        [_currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
        textField.text = [_currencyFormatter stringFromNumber:formatedValue];
        return NO;
    } else {
        NSNumberFormatter *_currencyFormatter = [[NSNumberFormatter alloc] init];
        [_currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
        textField.text = [_currencyFormatter stringFromNumber:0];
        
        
          //  [self showAlert:@"Maximum contribution is $100.00."];
        
        
        return NO;
    }
    }
    return YES;
}



- (IBAction)moneybuttonclick:(id)sender {
  
    //disables moneybuttonclick
    
    MoneyBtnOutlet.enabled = false;
    
    //enables moneybuttonclick after 10 secds

    [self performSelector:@selector(enableMoney) withObject:self afterDelay:10.0 ];
    
    
    [UIView animateWithDuration:3.0 animations:^(void) {
        self->MoneyImageview.alpha = 0;
        self->MoneyImageview.alpha = 2;
        
        
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus status =[reachability currentReachabilityStatus];
        if (status == NotReachable) {
            
            
            
                UIAlertController * alert=   [UIAlertController
                                              alertControllerWithTitle:@""
                                              message:NSLocalizedString(@"internetActivity_off", "")
                                              preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* okButton = [UIAlertAction
                                           actionWithTitle:NSLocalizedString(@"dialog_ok", "")
                                           style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction * action)
                                           {
                                                [self hideProgressView];
                                               
                                           }];
                [alert addAction:okButton];
                [self presentViewController:alert animated:YES completion:nil];
            
        }else{
            
            [self getConfiguration];
            
            [self showBusdalert:@""];
            
        }
        
    }];
    
  
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell;
    return cell;
}

-(void)swipeHandlerRight:(id)sender
{
    
    LeftRightViewController *LeftrightviewScreenObject = [self.storyboard instantiateViewControllerWithIdentifier:@"leftright"];
    UIViewController *newVC = LeftrightviewScreenObject;
    NSMutableArray *vcs =  [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
    [vcs insertObject:newVC atIndex:vcs.count-1];
    [self.navigationController setViewControllers:vcs animated:YES];
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(void)enableUserinteraction:(id)sender {
    
    [self.view setUserInteractionEnabled:YES];
}

- (double)getCentValue {
    NSString *cleanCentString = [[MoneyTextField.text componentsSeparatedByCharactersInSet: [[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];
    NSInteger centValue = [cleanCentString intValue];
    NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
    NSNumber *myNumber = [f numberFromString:cleanCentString];
    
    double amountValue = [myNumber doubleValue] / 100;
    return amountValue;
}

-(void) quickAlert:(NSString *)Message{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:Message preferredStyle:UIAlertControllerStyleAlert];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [alertController dismissViewControllerAnimated:YES completion:^{
            
        }];
        
    });
    
}



@end
