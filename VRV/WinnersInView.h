

#import <UIKit/UIKit.h>

@interface WinnersInView : UIViewController
{
    
    
    __weak IBOutlet UIView *MainView;
    
    __weak IBOutlet UIButton *LearnMoreButton;
    __weak IBOutlet UILabel *HourLabel;
    __weak IBOutlet UILabel *MinuteLabel;
    __weak IBOutlet UILabel *SecondLabel;
    
    
    __weak IBOutlet UILabel *winners_in;
    
    __weak IBOutlet UILabel *hrs;
    
    __weak IBOutlet UILabel *minutes;
    
    __weak IBOutlet UILabel *secs;
    
    __weak IBOutlet UILabel *winners;
    
    
    
    
    
    
    __weak IBOutlet UIImageView *ClockImageview;
}
- (IBAction)leftMenuButtonClick:(id)sender;
- (IBAction)LearnMoreButtonClick:(id)sender;
@end
