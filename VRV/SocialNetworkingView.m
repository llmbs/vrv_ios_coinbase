

#import "SocialNetworkingView.h"
#import "LeftRightViewController.h"
#import "CommanFunction.h"

@interface SocialNetworkingView ()
@end

@implementation SocialNetworkingView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UISwipeGestureRecognizer *gestureRecognizerRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandlerRight:)];
    [gestureRecognizerRight setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [self.view addGestureRecognizer:gestureRecognizerRight];
  
    for (UIButton *btn in _AllButtonCollections) {
    [btn setBackgroundImage:[CommanFunction imageWithColor:[UIColor grayColor]] forState:UIControlStateHighlighted];
    }
    [[UIButton appearance] setExclusiveTouch:YES];
    
    social_networking.text = NSLocalizedString(@"social_networking", "");

    live_txt.text = NSLocalizedString(@"live_txt", "");

    intercom_txt.text = NSLocalizedString(@"intercom_txt", "");

    platform_txt.text = NSLocalizedString(@"platform_txt", "");
    
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)MenuButtonClick:(id)sender {
    LeftRightViewController *LeftrightviewScreenObject = [self.storyboard instantiateViewControllerWithIdentifier:@"leftright"];
    UIViewController *newVC = LeftrightviewScreenObject;
    NSMutableArray *vcs =  [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
    [vcs insertObject:newVC atIndex:vcs.count-1];
    [self.navigationController setViewControllers:vcs animated:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)InstagramButtonClick:(id)sender {
    
    //if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"http://www.instagram.com/vrvllc"]])
    
    NSURL *instaUrl = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/instagram_social"];
    
        [[UIApplication sharedApplication] openURL: instaUrl options:@{} completionHandler:^(BOOL success) {
            if (success)
                NSLog(@"Opened instagram");
            
         }];
    
    }


- (IBAction)FacebokButtonClick:(id)sender {
   
    NSURL *instaUrl = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/facebook_social"];
    
    [[UIApplication sharedApplication] openURL: instaUrl options:@{} completionHandler:^(BOOL success) {
        if (success)
            NSLog(@"Opened facebook");
        
    }];
    
    
}

- (IBAction)AppleButtonClick:(id)sender {
    
    NSURL *instaUrl = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/CurrentFundsScreen"];
    
    [[UIApplication sharedApplication] openURL: instaUrl options:@{} completionHandler:^(BOOL success) {
        if (success)
            NSLog(@"Opened opennode website");
        
    }];
    
   
}

- (IBAction)TwitterButtonClick:(id)sender {

    NSURL *instaUrl = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/twitter_social"];
    
    [[UIApplication sharedApplication] openURL: instaUrl options:@{} completionHandler:^(BOOL success) {
        if (success)
            NSLog(@"Opened twitter");
        
    }];
    
}

- (IBAction)YoutubeButtonClick:(id)sender {
    
    NSURL *instaUrl = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/youtube_social"];
    
    [[UIApplication sharedApplication] openURL: instaUrl options:@{} completionHandler:^(BOOL success) {
        if (success)
            NSLog(@"Opened youtube");
        
    }];
    
    
}

- (IBAction)PinterestButtonClick:(id)sender {
    
    
    NSURL *instaUrl = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/pinterest_social"];
    
    [[UIApplication sharedApplication] openURL: instaUrl options:@{} completionHandler:^(BOOL success) {
        if (success)
            NSLog(@"Opened pinterest");
        
    }];
    
}

- (IBAction)intercomButtonClick:(id)sender {
    
    NSURL *instaUrl = [NSURL URLWithString:@"https://uat.voluntaryrefundvalue.com/link/SocialNetworking_intercom"];
    
    [[UIApplication sharedApplication] openURL: instaUrl options:@{} completionHandler:^(BOOL success) {
        if (success)
            NSLog(@"Opened Ticket Management Customer Support");
        
    }];
    
}

-(void)swipeHandlerRight:(id)sender
{
    
    LeftRightViewController *LeftrightviewScreenObject = [self.storyboard instantiateViewControllerWithIdentifier:@"leftright"];
    UIViewController *newVC = LeftrightviewScreenObject;
    NSMutableArray *vcs =  [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
    [vcs insertObject:newVC atIndex:vcs.count-1];
    [self.navigationController setViewControllers:vcs animated:YES];
    [self.navigationController popViewControllerAnimated:YES];
    
}

@end
