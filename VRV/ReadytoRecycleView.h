
#import <UIKit/UIKit.h>
//#import "PayPalMobile.h"                    PayPalPaymentDelegate, PayPalFuturePaymentDelegate, PayPalProfileSharingDelegate, UICollectionViewDataSource was originally in @interface
//#import "PayPalConfiguration.h"
// #import "PayPalPaymentViewController.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>



@interface ReadytoRecycleView : UIViewController<UIScrollViewDelegate,UITextFieldDelegate,UICollectionViewDelegate,MFMailComposeViewControllerDelegate>
{
    __weak IBOutlet UIScrollView *MainScrollview;
    __weak IBOutlet UIView *Corkview;
    __weak IBOutlet UIView *clothesview;
    __weak IBOutlet UIView *wholeview;
    __weak IBOutlet UIView *tabHeader;
    
    
    __weak IBOutlet UILabel *Corklabel;
    __weak IBOutlet UILabel *Clotheslabel;
    
    __weak IBOutlet UITextField *NaturalCorkQuantityTxt;
    __weak IBOutlet UITextField *VrvDepositeTextFiled;
    __weak IBOutlet UITextField *ClothesQuantityTxt;
    
    __weak IBOutlet UITextField *ClothesVRVDepositTxt;
    
    __weak IBOutlet UIActivityIndicatorView *loadingIndicator;
    
    __weak IBOutlet UIImageView *MapGifImageview;
    __weak IBOutlet UIImageView *MapGifImageview1;
    
    __weak IBOutlet UILabel *CorkText;
    __weak IBOutlet UILabel *ClothesText;
    
    __weak IBOutlet UIButton *submitradius;
    __weak IBOutlet UIButton *submitradiusc;
    __weak IBOutlet UIButton *hourglass;
    __weak IBOutlet UIButton *puzzle;
    __weak IBOutlet UIButton *payAdd;
    __weak IBOutlet UIButton *payAddCork;
    
    
    __weak IBOutlet UIView *puzzleView;
    __weak IBOutlet UIView *clothPuzzleView;
    
    __weak IBOutlet UIImageView *BNB;
    __weak IBOutlet UIImageView *USDC;
    
    
}

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *AllButtonCollections;

@property (strong, nonatomic) NSString *token;
@property (nonatomic) NSInteger level;

- (IBAction)MenuButtonClick:(id)sender;
- (IBAction)CorkButtonClick:(id)sender;
- (IBAction)ClothesButtsButtonClick:(id)sender;
- (IBAction)SubmitCork:(id)sender;
- (IBAction)ClothesSubmitButtonClick:(id)sender;
- (IBAction)PuzzleButtonClick:(id)sender;
- (IBAction)WebGlButtonClick:(id)sender;
- (IBAction)PaymentAddress:(id)sender;




// new


- (IBAction)corkMapButtonClick:(id)sender;
- (IBAction)ClothesMapButtonClick:(id)sender;

-(IBAction)HourGlassButtonClick:(id)sender;

- (IBAction)GameListClick:(id)sender;

- (IBAction)CorkAdvertiseClick:(id)sender;

- (IBAction)ClothesAdvertiseClick:(id)sender;

- (IBAction)MaterialClick:(id)sender;







@end
